package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = @Index("Currency_ID"))
public class Currency {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Currency_ID")
    public int currencyID;

    @ColumnInfo(name = "Name")
    public String name;

    @ColumnInfo(name = "Short_Name")
    public String shortName;

    @ColumnInfo(name = "Symbol")
    public String symbol;

    @ColumnInfo(name = "Currency_Rate")
    public Double currencyRate;

    public Currency() {

    }

    @Ignore
    public Currency(String name, String shortName, String symbol) {
        this.name = name;
        this.shortName = shortName;
        this.symbol = symbol;
    }
}
