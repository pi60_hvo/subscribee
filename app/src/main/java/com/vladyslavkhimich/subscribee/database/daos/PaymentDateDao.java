package com.vladyslavkhimich.subscribee.database.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.vladyslavkhimich.subscribee.database.entities.PaymentDate;
import com.vladyslavkhimich.subscribee.database.entities.partials.PaymentDatesWithOperations;

import java.util.List;

@Dao
public interface PaymentDateDao {
    @Insert
    long insertOneAndReturnID(PaymentDate paymentDate);

    @Insert
    void insertOne(PaymentDate paymentDate);

    @Update
    void updateMany(List<PaymentDate> paymentDates);

    @Transaction
    @Query("SELECT * FROM PaymentDate WHERE Subscription_ID = :subscriptionID")
    List<PaymentDatesWithOperations> getPaymentDatesWithOperationsBySubscriptionID(int subscriptionID);

    @Transaction
    @Query("SELECT * FROM PaymentDate WHERE Subscription_ID = :subscriptionID")
    LiveData<List<PaymentDatesWithOperations>> getObservablePaymentDatesWithOperationsBySubscriptionID(int subscriptionID);

    @Transaction
    @Query("SELECT Payment_Date_ID FROM PaymentDate WHERE Subscription_ID = :subscriptionID AND Payment_Date = :paymentDate")
    int getIDBySubscriptionIDAndDate(int subscriptionID, long paymentDate);

    @Query("SELECT Payment_Date_ID AS Outer_ID FROM PaymentDate WHERE Subscription_ID = :subscriptionID AND (SELECT COUNT(*) FROM Operation WHERE Payment_Date_ID = Outer_ID) > 0 ORDER BY Payment_Date_ID DESC LIMIT 1")
    int getSubscriptionLastPaymentID(int subscriptionID);

    @Query("SELECT Payment_Date FROM PaymentDate WHERE Subscription_ID = :subscriptionID ORDER BY Payment_Date_ID DESC LIMIT 1")
    long getSubscriptionLastPaymentDate(int subscriptionID);

    @Query("SELECT * FROM PaymentDate WHERE Subscription_ID = :subscriptionID")
    List<PaymentDate> getPaymentDatesBySubscriptionID(int subscriptionID);

    @Query("SELECT COUNT(*) FROM Operation WHERE Payment_Date_ID = :id")
    int getPaymentDateAssociatedOperationsCount(int id);

    /*@Query("SELECT COUNT(*) FROM Operation WHERE Payment_Date_ID = :id")
    int getAssociatedOperationsCount(int id);*/

    @Query("UPDATE PaymentDate SET Payment_Date = :date WHERE Payment_Date_ID = :id")
    void updatePaymentDateByID(int id, long date);

    @Query("DELETE FROM PaymentDate WHERE Subscription_ID = :subscriptionID AND Payment_Date = :date")
    void deleteOne(int subscriptionID, long date);

    @Query("DELETE FROm PaymentDate WHERE Payment_Date_ID = :id")
    void deleteOne(int id);
}
