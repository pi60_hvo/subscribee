package com.vladyslavkhimich.subscribee.database.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.vladyslavkhimich.subscribee.database.entities.Category;

import java.util.List;

@Dao
public interface CategoryDao {

    @Insert
    void insertOne(Category category);
    @Insert
    void insertAll(List<Category> categories);

    @Delete
    void deleteOne(Category category);

    @Update
    void updateOne(Category category);

    @Query("SELECT * FROM Category ORDER BY Category_ID ASC")
    LiveData<List<Category>> getAll();

    @Query("SELECT COUNT(*) FROM Category WHERE Name = :name AND Is_User_Created = 1")
    Integer checkIfCategoryNameExists(String name);

    @Query("SELECT * FROM Category WHERE Category_ID = :id")
    Category getCategoryByID(int id);
}
