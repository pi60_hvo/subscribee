package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@Entity(foreignKeys =
@ForeignKey(
        entity = Subscription.class,
        parentColumns = "Subscription_ID",
        childColumns = "Subscription_ID",
        onDelete = CASCADE
), indices = @Index("Subscription_ID"))
public class Member {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Member_ID")
    public int memberID;

    @ColumnInfo(name = "Name")
    public String name;

    @ColumnInfo(name = "Subscription_Share")
    public Double subscriptionShare;

    @ColumnInfo(name = "Subscription_ID")
    public int subscriptionID;

    public Member() {

    }

    public Member(@Nullable Member member) {
        if (member != null) {
            this.memberID = member.memberID;
            this.name = member.name;
            this.subscriptionShare = member.subscriptionShare;
            this.subscriptionID = member.subscriptionID;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return memberID == member.memberID && subscriptionID == member.subscriptionID && Objects.equals(name, member.name) && Objects.equals(subscriptionShare, member.subscriptionShare);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberID, name, subscriptionShare, subscriptionID);
    }
}
