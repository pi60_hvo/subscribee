package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.vladyslavkhimich.subscribee.database.converters.DateConverter;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.SET_NULL;

@Entity(foreignKeys = {
        @ForeignKey(
                entity = Subscription.class,
                parentColumns = "Subscription_ID",
                childColumns = "Subscription_ID",
                onDelete = CASCADE
        ),
        @ForeignKey(
                entity = Member.class,
                parentColumns = "Member_ID",
                childColumns = "Member_ID",
                onDelete = SET_NULL
        ),
        @ForeignKey(
                entity = Currency.class,
                parentColumns = "Currency_ID",
                childColumns = "Currency_ID"
        ),
        @ForeignKey(
                entity = PaymentDate.class,
                parentColumns = "Payment_Date_ID",
                childColumns = "Payment_Date_ID",
                onDelete = CASCADE
        )
},
indices = {
        @Index("Subscription_ID"),
        @Index("Member_ID"),
        @Index("Currency_ID"),
        @Index("Payment_Date_ID")
})
public class Operation {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Operation_ID")
    public int operationID;

    @ColumnInfo(name = "Price")
    public double price;

    @TypeConverters({DateConverter.class})
    @ColumnInfo(name = "Operation_Date")
    public Date operationDate;

    @ColumnInfo(name = "Is_Owner")
    public boolean isOwner;

    @ColumnInfo(name = "Subscription_ID")
    public int subscriptionID;

    @ColumnInfo(name = "Member_ID")
    public Integer memberID;

    @ColumnInfo(name = "Currency_ID")
    public int currencyID;

    @ColumnInfo(name = "Payment_Date_ID")
    public int paymentDateID;
}
