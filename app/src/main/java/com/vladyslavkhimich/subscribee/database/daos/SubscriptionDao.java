package com.vladyslavkhimich.subscribee.database.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.TypeConverters;
import androidx.room.Update;

import com.vladyslavkhimich.subscribee.database.converters.DateConverter;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.database.entities.partials.ManagingSubscription;

import java.util.Date;
import java.util.List;

@Dao
public interface SubscriptionDao {
    @Insert
    long insertOne(Subscription subscription);
    @Insert
    void insertAll(List<Subscription> subscriptions);

    @Update
    void updateOne(Subscription subscription);

    @Query("UPDATE Subscription " +
            "SET Owner_ID = :ownerID " +
            "WHERE Subscription_ID = :subscriptionID")
    void updateOwnerID(int subscriptionID, int ownerID);

    @Query("UPDATE Subscription " +
            "SET Next_Payment_Date = :nextPaymentDate " +
            "WHERE Subscription_ID = :subscriptionID")
    void updateNextPaymentDate(int subscriptionID, long nextPaymentDate);

    @Query("SELECT * " +
            "FROM Subscription " +
            "WHERE Is_Family = 0 AND Is_Archived = 0 AND Is_Family_Part = 0 " +
            "ORDER BY Subscription_ID ASC")
    LiveData<List<Subscription>> getPersonalSubscriptions();

    @Query("SELECT * " +
            "FROM Subscription " +
            "WHERE (Is_Family = 1 OR Is_Family_Part = 1) " +
            "AND Is_Archived = 0 " +
            "ORDER BY Subscription_ID ASC")
    LiveData<List<Subscription>> getFamilySubscriptions();

    @Query("SELECT * " +
            "FROM Subscription " +
            "WHERE Is_Archived = 1 " +
            "ORDER BY Subscription_ID ASC")
    LiveData<List<Subscription>> getArchivedSubscriptions();

    @Query("UPDATE Subscription " +
            "SET Is_Archived = 1 " +
            "WHERE Subscription_ID = :id")
    void archiveSubscription(int id);

    @Query("UPDATE Subscription " +
            "SET Is_Archived = 0 " +
            "WHERE Subscription_ID = :id")
    void unarchiveSubscription(int id);

    @Query("SELECT COUNT(*) FROM Member WHERE Subscription_ID = :id")
    int getSubscriptionMembersCount(int id);

    @Query("DELETE FROM Subscription " +
            "WHERE Subscription_ID = :id")
    void deleteSubscription(int id);

    @Query("SELECT COUNT(*) " +
            "FROM Subscription " +
            "WHERE Is_Family = 0 AND Is_Archived = 0 AND Is_Family_Part = 0")
    int getPersonalSubscriptionsCount();

    @Query("SELECT COUNT(*) " +
            "FROM Subscription " +
            "WHERE (Is_Family = 1 OR Is_Family_Part = 1) " +
            "AND Is_Archived = 0")
    int getFamilySubscriptionsCount();

    @Query("SELECT COUNT(*) " +
            "FROM Subscription " +
            "WHERE Is_Archived = 1")
    int getArchivedSubscriptionsCount();

    @Query("SELECT Currency_ID " +
            "FROM Subscription " +
            "WHERE Subscription_ID = :id")
    int getAssociatedCurrencyID(int id);

    @Query("SELECT Price " +
            "FROM Subscription " +
            "WHERE Subscription_ID = :id")
    double getSubscriptionPrice(int id);

    @Transaction
    @Query("SELECT * FROM Subscription WHERE Subscription_ID = :id")
    LiveData<ManagingSubscription> getObservableManagingSubscriptionByID(int id);

    @Transaction
    @Query("SELECT * FROM Subscription WHERE Subscription_ID = :id")
    ManagingSubscription getManagingSubscriptionByID(int id);

    @Query("SELECT * FROM Subscription WHERE Subscription_ID = :id")
    Subscription getSubscriptionByID(int id);

    @TypeConverters({DateConverter.class})
    @Query("SELECT Next_Payment_Date FROM Subscription WHERE Subscription_ID = :id")
    Date getSubscriptionNextPaymentDate(int id);
}
