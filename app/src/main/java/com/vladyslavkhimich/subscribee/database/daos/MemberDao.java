package com.vladyslavkhimich.subscribee.database.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.vladyslavkhimich.subscribee.database.entities.Member;
import com.vladyslavkhimich.subscribee.database.entities.partials.MemberWithOperationsAndPaymentDates;

import java.util.List;

@Dao
public interface MemberDao {
    @Insert
    void insertOne(Member member);
    @Insert
    long insertOneAndReturnID(Member member);
    @Insert
    void insertAll(List<Member> members);

    @Update
    void updateOne(Member member);

    @Delete
    void deleteOne(Member member);

    @Transaction
    @Query("SELECT * FROM Member WHERE Subscription_ID = :subscriptionID ORDER BY Member_ID ASC")
    LiveData<List<MemberWithOperationsAndPaymentDates>> getFamilySubscriptionMembersWithData(int subscriptionID);

    @Transaction
    @Query("SELECT * FROM Member WHERE Subscription_ID = :subscriptionID ORDER BY Member_ID ASC")
    List<MemberWithOperationsAndPaymentDates> getFamilySubscriptionMembersWithDataTest(int subscriptionID);

    @Query("UPDATE Member SET Subscription_Share = :subscriptionShare WHERE Member_ID = :id")
    void updateSubscriptionShare(int id, double subscriptionShare);
}
