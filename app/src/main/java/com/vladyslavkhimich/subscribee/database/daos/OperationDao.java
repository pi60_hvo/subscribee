package com.vladyslavkhimich.subscribee.database.daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverters;

import com.vladyslavkhimich.subscribee.database.converters.DateConverter;
import com.vladyslavkhimich.subscribee.database.entities.Operation;
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithAssociatedData;
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats;

import java.util.Date;
import java.util.List;

@Dao
public interface OperationDao {
    @Insert
    void insertOne(Operation operation);

    @Query("DELETE FROM Operation WHERE Payment_Date_ID = :paymentDateID")
    void deleteOne(int paymentDateID);

    @Query("DELETE FROM Operation WHERE Payment_Date_ID = :paymentDateID AND Member_ID = :memberID")
    void deleteOne(int paymentDateID, int memberID);

    @Query("SELECT * FROM Operation WHERE Subscription_ID = :id")
    List<Operation> getOperationsByID(int id);

    @Query("SELECT COUNT(*) FROM Operation WHERE Subscription_ID = :subscriptionID")
    int getSubscriptionOperationsCount(int subscriptionID);

    @TypeConverters({DateConverter.class})
    @Query("SELECT * FROM Operation WHERE Operation_Date BETWEEN :startDate AND :endDate ORDER BY Operation_ID DESC")
    List<OperationWithAssociatedData> getOperationsBetweenDates(Date startDate, Date endDate);

    @TypeConverters({DateConverter.class})
    @Query("SELECT * FROM Operation WHERE Operation_Date BETWEEN :startDate AND :endDate ORDER BY Operation_ID DESC")
    List<OperationWithDataForStats> getOperationsDataForStats(Date startDate, Date endDate);
}
