package com.vladyslavkhimich.subscribee.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.vladyslavkhimich.subscribee.database.daos.CategoryDao;
import com.vladyslavkhimich.subscribee.database.daos.CurrencyDao;
import com.vladyslavkhimich.subscribee.database.daos.CycleDao;
import com.vladyslavkhimich.subscribee.database.daos.FreePeriodDao;
import com.vladyslavkhimich.subscribee.database.daos.MemberDao;
import com.vladyslavkhimich.subscribee.database.daos.OperationDao;
import com.vladyslavkhimich.subscribee.database.daos.PaymentDateDao;
import com.vladyslavkhimich.subscribee.database.daos.ReminderDao;
import com.vladyslavkhimich.subscribee.database.daos.SubscriptionDao;
import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.database.entities.FreePeriod;
import com.vladyslavkhimich.subscribee.database.entities.Member;
import com.vladyslavkhimich.subscribee.database.entities.Operation;
import com.vladyslavkhimich.subscribee.database.entities.PaymentDate;
import com.vladyslavkhimich.subscribee.database.entities.Reminder;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.database.tasks.PrePopulateDBDataTask;

@Database(
        entities = {
                Cycle.class,
                Category.class,
                Currency.class,
                Member.class,
                Subscription.class,
                Reminder.class,
                FreePeriod.class,
                Operation.class,
                PaymentDate.class
        },
        version = 12,
        exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public static AppDatabase appDatabase;
    public static AppDatabase getInstance(Context context) {
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "AppDatabase.db").addCallback(new RoomDatabase.Callback() {
                @Override
                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                    super.onCreate(db);
                    Log.i("CALLED", "OnCreate method called");
                    new PrePopulateDBDataTask(appDatabase, context.getApplicationContext()).execute();
                }
            }).fallbackToDestructiveMigration().build();
        }
        return appDatabase;
    }
    public abstract CycleDao getCycleDao();
    public abstract CategoryDao getCategoryDao();
    public abstract CurrencyDao getCurrencyDao();
    public abstract SubscriptionDao getSubscriptionDao();
    public abstract ReminderDao getReminderDao();
    public abstract FreePeriodDao getFreePeriodDao();
    public abstract MemberDao getMemberDao();
    public abstract OperationDao getOperationDao();
    public abstract PaymentDateDao getPaymentDateDao();
}