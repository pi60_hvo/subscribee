package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = @Index("Category_ID"))
public class Category {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Category_ID")
    public int categoryID;

    @ColumnInfo(name = "Name")
    public String name;

    @ColumnInfo(name = "Color")
    public String color;

    @ColumnInfo(name = "Is_User_Created")
    public boolean isUserCreated;
}
