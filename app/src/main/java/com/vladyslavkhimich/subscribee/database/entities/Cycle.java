package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = @Index("Cycle_ID"))
public class Cycle {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Cycle_ID")
    public int cycleID;

    @ColumnInfo(name = "Days_Count")
    public int daysCount;

    @ColumnInfo(name = "Weeks_Count")
    public int weeksCount;

    @ColumnInfo(name = "Months_Count")
    public int monthsCount;

    @ColumnInfo(name = "Years_Count")
    public int yearsCount;
}
