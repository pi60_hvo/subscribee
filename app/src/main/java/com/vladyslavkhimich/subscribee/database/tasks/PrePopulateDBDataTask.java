package com.vladyslavkhimich.subscribee.database.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.vladyslavkhimich.subscribee.database.AppDatabase;
import com.vladyslavkhimich.subscribee.database.daos.CategoryDao;
import com.vladyslavkhimich.subscribee.database.daos.CurrencyDao;
import com.vladyslavkhimich.subscribee.database.daos.CycleDao;
import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.utils.types.complex.GsonHelper;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class PrePopulateDBDataTask extends AsyncTask<Void, Void, Void> {
    CycleDao cycleDao;
    CategoryDao categoryDao;
    CurrencyDao currencyDao;
    private final WeakReference<Context> weakContext;

    public PrePopulateDBDataTask(AppDatabase appDatabase, Context applicationContext) {
        cycleDao = appDatabase.getCycleDao();
        categoryDao = appDatabase.getCategoryDao();
        currencyDao = appDatabase.getCurrencyDao();
        weakContext = new WeakReference<>(applicationContext);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        prePopulateData();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        new SetCurrencyRatesTask(currencyDao).execute();
    }

    public void prePopulateData() {
        prePopulateCycles();
        prePopulateCategories();
        prePopulateCurrencies();
    }

    void prePopulateCycles() {
        ArrayList<Cycle> cycles = new ArrayList<>();

        Cycle oneWeekCycle = new Cycle();
        oneWeekCycle.weeksCount = 1;
        cycles.add(oneWeekCycle);

        Cycle twoWeeksCycle = new Cycle();
        twoWeeksCycle.weeksCount = 2;
        cycles.add(twoWeeksCycle);

        Cycle fourWeeksCycle = new Cycle();
        fourWeeksCycle.weeksCount = 4;
        cycles.add(fourWeeksCycle);

        Cycle oneMonthCycle = new Cycle();
        oneMonthCycle.monthsCount = 1;
        cycles.add(oneMonthCycle);

        Cycle oneYearCycle = new Cycle();
        oneYearCycle.yearsCount = 1;
        cycles.add(oneYearCycle);
        Log.i("CALLED", "prePopulateCycles method called");
        try {
            cycleDao.insertAll(cycles);
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
        }
    }

    void prePopulateCategories() {

        String categoryPrefix = "category_";
        String colorPickerPrefix = "color_picker_";

        ArrayList<com.vladyslavkhimich.subscribee.models.dto.CategoryDto> categoriesFromJSON = new ArrayList<>(Arrays.asList(GsonHelper.getCategoriesFromJSON(weakContext.get())));

        LinkedList<Category> categoriesForDB = new LinkedList<>();

        for (com.vladyslavkhimich.subscribee.models.dto.CategoryDto categoryDto : categoriesFromJSON) {
            Category newCategory = new Category();
            newCategory.name = categoryPrefix + categoryDto.getName();
            newCategory.color = colorPickerPrefix + categoryDto.getColor();
            categoriesForDB.add(newCategory);
        }

        Log.i("CALLED", "prePopulateCategories method called");
        try {
            categoryDao.insertAll(categoriesForDB);
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
        }
    }

    void prePopulateCurrencies() {
        String currencyPrefix = "currency_";

        ArrayList<com.vladyslavkhimich.subscribee.models.dto.CurrencyDto> currenciesFromJson = new ArrayList<>(Arrays.asList(GsonHelper.getCurrenciesFromJSON(weakContext.get())));

        LinkedList<Currency> currenciesForDB = new LinkedList<>();

        for (com.vladyslavkhimich.subscribee.models.dto.CurrencyDto currencyDto : currenciesFromJson) {
            currenciesForDB.add(new Currency(currencyPrefix + currencyDto.getLowerCaseName(), currencyDto.getUpperCaseName(), currencyDto.getSymbol()));
        }
        Log.i("CALLED", "prePopulateCurrencies method called");
        try {
            currencyDao.insertAll(currenciesForDB);
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
        }
    }
}
