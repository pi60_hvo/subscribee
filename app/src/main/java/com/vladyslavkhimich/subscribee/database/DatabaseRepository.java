package com.vladyslavkhimich.subscribee.database;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.vladyslavkhimich.subscribee.database.converters.DateConverter;
import com.vladyslavkhimich.subscribee.database.daos.CategoryDao;
import com.vladyslavkhimich.subscribee.database.daos.CurrencyDao;
import com.vladyslavkhimich.subscribee.database.daos.CycleDao;
import com.vladyslavkhimich.subscribee.database.daos.FreePeriodDao;
import com.vladyslavkhimich.subscribee.database.daos.MemberDao;
import com.vladyslavkhimich.subscribee.database.daos.OperationDao;
import com.vladyslavkhimich.subscribee.database.daos.PaymentDateDao;
import com.vladyslavkhimich.subscribee.database.daos.ReminderDao;
import com.vladyslavkhimich.subscribee.database.daos.SubscriptionDao;
import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.database.entities.FreePeriod;
import com.vladyslavkhimich.subscribee.database.entities.Member;
import com.vladyslavkhimich.subscribee.database.entities.Operation;
import com.vladyslavkhimich.subscribee.database.entities.PaymentDate;
import com.vladyslavkhimich.subscribee.database.entities.Reminder;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.database.entities.partials.ManagingSubscription;
import com.vladyslavkhimich.subscribee.database.entities.partials.MemberWithOperationsAndPaymentDates;
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithAssociatedData;
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats;
import com.vladyslavkhimich.subscribee.database.entities.partials.PaymentDatesWithOperations;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class DatabaseRepository {
    private final CycleDao cycleDao;
    private final CurrencyDao currencyDao;
    private final CategoryDao categoryDao;
    private final SubscriptionDao subscriptionDao;
    private final ReminderDao reminderDao;
    private final FreePeriodDao freePeriodDao;
    private final MemberDao memberDao;
    private final OperationDao operationDao;
    private final PaymentDateDao paymentDateDao;
    private static DatabaseRepository instance;
    private final ExecutorService executorService;

    public DatabaseRepository(Context context) {
        AppDatabase appDatabase = AppDatabase.getInstance(context);
        executorService = Executors.newCachedThreadPool();
        executorService.execute(() -> appDatabase.runInTransaction(() -> {

        }));
        cycleDao = appDatabase.getCycleDao();
        currencyDao = appDatabase.getCurrencyDao();
        categoryDao = appDatabase.getCategoryDao();
        subscriptionDao = appDatabase.getSubscriptionDao();
        reminderDao = appDatabase.getReminderDao();
        freePeriodDao = appDatabase.getFreePeriodDao();
        memberDao  = appDatabase.getMemberDao();
        operationDao = appDatabase.getOperationDao();
        paymentDateDao = appDatabase.getPaymentDateDao();
    }

    public static DatabaseRepository getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseRepository(context);
        }
        return instance;
    }

    public LiveData<List<Cycle>> getCycles() {
        try {
            return cycleDao.getAll();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public Cycle getCycleByID(int id) {
        Callable<Cycle> getCallable = () -> cycleDao.getCycleByID(id);

        Cycle cycle = null;

        Future<Cycle> future = executorService.submit(getCallable);

        try {
            cycle = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            exception.printStackTrace();
        }

        return cycle;
    }

    public Cycle getCycleBySubscriptionID(int subscriptionID) {
        Callable<Cycle> getCallable = () -> cycleDao.getCycleBySubscriptionID(subscriptionID);

        Cycle cycle = null;

        Future<Cycle> future = executorService.submit(getCallable);

        try {
            cycle = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            exception.printStackTrace();
        }

        return cycle;
    }

    public Cycle returnCycleIfExists(int daysCount, int weeksCount, int monthsCount, int yearsCount) {
        Callable<Cycle> checkCallable = () -> cycleDao.returnCycleIfHasParameters(daysCount, weeksCount, monthsCount, yearsCount);

        Cycle cycle = null;

        Future<Cycle> future = executorService.submit(checkCallable);
        try {
            cycle = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
        return cycle;
    }

    public long insertOneCycleAndReturnID(Cycle cycle) {
        Callable<Long> insertCallable = () -> cycleDao.insertOne(cycle);

        long rowID = 0;

        Future<Long> future = executorService.submit(insertCallable);
        try {
            rowID = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return rowID;
    }

    public LiveData<List<Currency>> getCurrencies() {
        try {
            return currencyDao.getAll();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public String getCurrencyShortNameByID(int id) {
        Callable<String> getCallable = () -> currencyDao.getShortNameByID(id);

        String shortName = "";

        Future<String> future = executorService.submit(getCallable);

        try {
            shortName = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return shortName;
    }

    public Category getCategoryByID(int id) {
        Callable<Category> getCallable = () -> categoryDao.getCategoryByID(id);

        Category receivedCategory = new Category();

        Future<Category> future = executorService.submit(getCallable);

        try {
           receivedCategory = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return receivedCategory;
    }

    public LiveData<List<Category>> getCategories() {
        try {
            return categoryDao.getAll();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public boolean checkIfCategoryNameExists(String name) {
        Callable<Integer> getCallable = () -> categoryDao.checkIfCategoryNameExists(name);

        int categoryCount = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            categoryCount = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            exception.printStackTrace();
        }

        return categoryCount > 0;
    }

    public void insertOneCategory(Category category) {
        try {
            executorService.execute(() -> categoryDao.insertOne(category));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteOneCategory(Category category) {
        try {
            executorService.execute(() -> categoryDao.deleteOne(category));
        }
        catch (Exception exception){
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void updateOneCategory(Category category) {
        try {
            executorService.execute(() -> categoryDao.updateOne(category));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void insertOneReminder(Reminder reminder) {
        try {
            executorService.execute(() -> reminderDao.insertOne(reminder));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void updateOneReminder(Reminder reminder) {
        try {
            executorService.execute(() -> reminderDao.updateOne(reminder));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteOneReminderBySubscriptionID(int subscriptionID) {
        try {
            executorService.execute(() -> reminderDao.deleteOneBySubscriptionID(subscriptionID));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public int getReminderIDBySubscriptionID(int subscriptionID) {
        Callable<Integer> getCallable = () -> reminderDao.getReminderIDBySubscriptionID(subscriptionID);

        int result = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            exception.printStackTrace();
        }

        return result;
    }

    public Reminder getReminderByID(int id) {
        Callable<Reminder> getCallable = () -> reminderDao.getOneByID(id);

        Reminder result = null;

        Future<Reminder> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            exception.printStackTrace();
        }

        return result;
    }

    public void insertOneFreePeriod(FreePeriod freePeriod) {
        try {
            executorService.execute(() -> freePeriodDao.insertOne(freePeriod));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteOneFreePeriod(int id) {
        try {
            executorService.execute(() -> freePeriodDao.deleteOne(id));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void updateOneFreePeriod(FreePeriod freePeriod) {
        try {
            executorService.execute(() -> freePeriodDao.updateOne(freePeriod));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public int getFreePeriodIDBySubscriptionID(int subscriptionID) {
        Callable<Integer> getCallable = () -> freePeriodDao.getIdBySubscriptionId(subscriptionID);

        int freePeriodID = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            freePeriodID = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return freePeriodID;
    }

    public FreePeriod getFreePeriodBySubscriptionID(int subscriptionId) {
        Callable<FreePeriod> getCallable = () -> freePeriodDao.getBySubscriptionId(subscriptionId);

        FreePeriod result = null;

        Future<FreePeriod> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public FreePeriod getFreePeriodByID(int id) {
        Callable<FreePeriod> getCallable = () -> freePeriodDao.getOneByID(id);

        FreePeriod result = null;

        Future<FreePeriod> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public void insertOneMember(Member member) {
        try {
            executorService.execute(() -> memberDao.insertOne(member));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public long insertOneMemberAndReturnID(Member member) {
        Callable<Long> insertCallable = () -> memberDao.insertOneAndReturnID(member);

        long rowID = 0;

        Future<Long> future = executorService.submit(insertCallable);
        try {
            rowID = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return rowID;
    }

    public void updateOneMember(Member member) {
        try {
            executorService.execute(() -> memberDao.updateOne(member));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void updateOneMemberSubscriptionShare(int id, double subscriptionShare) {
        try {
            executorService.execute(() -> memberDao.updateSubscriptionShare(id, subscriptionShare));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteOneMember(Member member) {
        try {
            executorService.execute(() -> memberDao.deleteOne(member));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public LiveData<List<MemberWithOperationsAndPaymentDates>> getFamilySubscriptionMembersWithData(int subscriptionID) {
        try {
            return memberDao.getFamilySubscriptionMembersWithData(subscriptionID);
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public List<MemberWithOperationsAndPaymentDates> getFamilySubscriptionMembersWithDataTest(int subscriptionID) {
        Callable<List<MemberWithOperationsAndPaymentDates>> getCallable = () -> memberDao.getFamilySubscriptionMembersWithDataTest(subscriptionID);

        List<MemberWithOperationsAndPaymentDates> testResult = null;

        Future<List<MemberWithOperationsAndPaymentDates>> future = executorService.submit(getCallable);

        try {
            testResult = future.get();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return testResult;
    }

    public long insertOneSubscriptionAndReturnID(Subscription subscription) {
        Callable<Long> insertCallable = () -> subscriptionDao.insertOne(subscription);

        long rowID = 0;

        Future<Long> future = executorService.submit(insertCallable);

        try {
            rowID = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return rowID;
    }

    public void updateOneSubscription(Subscription subscription) {
        try {
            executorService.execute(() -> subscriptionDao.updateOne(subscription));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public LiveData<ManagingSubscription> getObservableManagingSubscriptionByID(int id) {
        try {
            return subscriptionDao.getObservableManagingSubscriptionByID(id);
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public ManagingSubscription getManagingSubscriptionByID(int id) {
        Callable<ManagingSubscription> getCallable = () -> subscriptionDao.getManagingSubscriptionByID(id);

        ManagingSubscription result = null;

        Future<ManagingSubscription> future = executorService.submit(getCallable);

        try {
            result = future.get();
        }
        catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public Subscription getSubscriptionByID(int id) {
        Callable<Subscription> getCallable = () -> subscriptionDao.getSubscriptionByID(id);

        Subscription result = null;

        Future<Subscription> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public LiveData<List<Subscription>> getPersonalSubscriptions() {
        try {
            return subscriptionDao.getPersonalSubscriptions();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public LiveData<List<Subscription>> getFamilySubscriptions() {
        try {
            return subscriptionDao.getFamilySubscriptions();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public LiveData<List<Subscription>> getArchivedSubscriptions() {
        try {
            return subscriptionDao.getArchivedSubscriptions();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public void updateSubscriptionOwnerID(int subscriptionID, int ownerID) {
        try {
            executorService.execute(() -> subscriptionDao.updateOwnerID(subscriptionID, ownerID));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void updateSubscriptionNextPaymentDate(int subscriptionID, Date paymentDate) {
        try {
            executorService.execute(() -> subscriptionDao.updateNextPaymentDate(subscriptionID, DateConverter.fromDate(paymentDate)));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void archiveSubscription(int id) {
        try {
            executorService.execute(() -> subscriptionDao.archiveSubscription(id));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void unarchiveSubscription(int id) {
        try {
            executorService.execute(() -> subscriptionDao.unarchiveSubscription(id));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteSubscription(int id) {
        try {
            executorService.execute(() -> subscriptionDao.deleteSubscription(id));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public int getSubscriptionMembersCountByID(int id) {
        Callable<Integer> getCallable = () -> subscriptionDao.getSubscriptionMembersCount(id);

        int membersCount = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            membersCount = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return membersCount;
    }

    public int getPersonalSubscriptionsCount() {
        Callable<Integer> getCallable = subscriptionDao::getPersonalSubscriptionsCount;

        int subscriptionsCount = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            subscriptionsCount = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return subscriptionsCount;
    }

    public int getFamilySubscriptionsCount() {
        Callable<Integer> getCallable = subscriptionDao::getFamilySubscriptionsCount;

        int subscriptionsCount = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            subscriptionsCount = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return subscriptionsCount;
    }

    public int getArchivedSubscriptionsCount() {
        Callable<Integer> getCallable = subscriptionDao::getArchivedSubscriptionsCount;

        int subscriptionsCount = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            subscriptionsCount = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return subscriptionsCount;
    }

    public List<PaymentDatesWithOperations> getPaymentsDatesWithOperationsBySubscriptionID(int id) {

        Callable<List<PaymentDatesWithOperations>> getCallable = () -> paymentDateDao.getPaymentDatesWithOperationsBySubscriptionID(id);

        List<PaymentDatesWithOperations> result = null;

        Future<List<PaymentDatesWithOperations>> future = executorService.submit(getCallable);

        try {
            result = future.get();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public LiveData<List<PaymentDatesWithOperations>> getObservablePaymentsDatesWithOperationsBySubscriptionID(int id) {
        try {
            return paymentDateDao.getObservablePaymentDatesWithOperationsBySubscriptionID(id);
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
    }

    public int getCurrencyIDBySubscriptionID(int subscriptionID) {
        Callable<Integer> getCallable = () -> subscriptionDao.getAssociatedCurrencyID(subscriptionID);

        int currencyID = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            currencyID = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return currencyID;
    }

    public double getPriceBySubscriptionID(int subscriptionID) {
        Callable<Double> getCallable = () -> subscriptionDao.getSubscriptionPrice(subscriptionID);

        double price = 0.0;

        Future<Double> future = executorService.submit(getCallable);

        try {
            price = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return price;
    }

    public Date getNextPaymentDateBySubscriptionID(int id) {
        Callable<Date> getCallable = () -> subscriptionDao.getSubscriptionNextPaymentDate(id);

        Date result = null;

        Future<Date> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public long insertOnePaymentDateAndReturnID(PaymentDate paymentDate) {
        Callable<Long> insertCallable = () -> paymentDateDao.insertOneAndReturnID(paymentDate);

        long rowID = 0;
        Future<Long> future = executorService.submit(insertCallable);
        try {
            rowID = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return rowID;
    }

    public void insertOnePaymentDate(PaymentDate paymentDate) {
        try {
            executorService.execute(() -> paymentDateDao.insertOne(paymentDate));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteOnePaymentDate(int subscriptionID, Date date) {
        try {
            executorService.execute(() -> paymentDateDao.deleteOne(subscriptionID, DateConverter.fromDate(date)));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteOnePaymentDate(int id) {
        try {
            executorService.execute(() -> paymentDateDao.deleteOne(id));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public List<PaymentDate> getPaymentDatesBySubscriptionID(int subscriptionID) {
        Callable<List<PaymentDate>> getCallable = () -> paymentDateDao.getPaymentDatesBySubscriptionID(subscriptionID);

        List<PaymentDate> result = null;

        Future<List<PaymentDate>> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public int getPaymentDateIDBySubscriptionIDAndDate(int subscriptionID, Date date) {
        Callable<Integer> getCallable = () -> paymentDateDao.getIDBySubscriptionIDAndDate(subscriptionID, DateConverter.fromDate(date));

        int paymentDateID = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            paymentDateID = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return paymentDateID;
    }

    public int getSubscriptionLastPaymentDateID(int subscriptionID) {
        Callable<Integer> getCallable = () -> paymentDateDao.getSubscriptionLastPaymentID(subscriptionID);

        int rowID = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            rowID = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return rowID;
    }

    public Date getSubscriptionLastPaymentDate(int subscriptionID) {
        Callable<Date> getCallable = () -> DateConverter.toDate(paymentDateDao.getSubscriptionLastPaymentDate(subscriptionID));

        Date resultDate = null;

        Future<Date> future = executorService.submit(getCallable);

        try {
            resultDate = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return resultDate;
    }

    public int getPaymentDateAssociatedOperationsCount(int paymentDateID) {
        Callable<Integer> getCallable = () -> paymentDateDao.getPaymentDateAssociatedOperationsCount(paymentDateID);

        int result = -1;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public void updatePaymentDateDate(int id, Date date) {
        try {
            executorService.execute(() -> paymentDateDao.updatePaymentDateByID(id, DateConverter.fromDate(date)));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void updatePaymentDates(List<PaymentDate> paymentDates) {
        try {
            executorService.execute(() -> paymentDateDao.updateMany(paymentDates));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void insertOneOperation(Operation operation) {
        try {
            executorService.execute(() -> operationDao.insertOne(operation));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteOneOperationByPaymentDateID(int paymentDateID) {
        try {
            executorService.execute(() -> operationDao.deleteOne(paymentDateID));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public void deleteOneOperationByPaymentDateIDAndMemberID(int paymentDateID, int memberID) {
        try {
            executorService.execute(() -> operationDao.deleteOne(paymentDateID, memberID));
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }
    }

    public int getSubscriptionOperationsCountByID(int subscriptionID) {
        Callable<Integer> getCallable = () -> operationDao.getSubscriptionOperationsCount(subscriptionID);

        int result = 0;

        Future<Integer> future = executorService.submit(getCallable);

        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
        }

        return result;
    }

    public List<OperationWithAssociatedData> getOperationsBetweenDates(Date startDate, Date endDate) {
        Callable<List<OperationWithAssociatedData>> getCallable = () -> operationDao.getOperationsBetweenDates(startDate, endDate);

        List<OperationWithAssociatedData> result;

        Future<List<OperationWithAssociatedData>> future = executorService.submit(getCallable);
        try {
            result = future.get();
        }
        catch (Exception exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }

        return result;
    }

    public List<OperationWithDataForStats> getOperationsForStats(Date startDate, Date endDate) {
        Callable<List<OperationWithDataForStats>> getCallable = () -> operationDao.getOperationsDataForStats(startDate, endDate);

        List<OperationWithDataForStats> result;

        Future<List<OperationWithDataForStats>> future = executorService.submit(getCallable);
        try {
            result = future.get();
        }
        catch (InterruptedException | ExecutionException exception) {
            Log.e("DB Error", exception.getMessage());
            exception.printStackTrace();
            return null;
        }
        return result;
    }
}
