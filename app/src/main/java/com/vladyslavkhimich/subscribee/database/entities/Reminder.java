package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys =
@ForeignKey(
        entity = Subscription.class,
        parentColumns = "Subscription_ID",
        childColumns = "Subscription_ID",
        onDelete = CASCADE
),
indices = @Index("Subscription_ID"))
public class Reminder {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Reminder_ID")
    public int reminderID;

    @ColumnInfo(name = "Days_Count")
    public int daysCount;

    @ColumnInfo(name = "Weeks_Count")
    public int weeksCount;

    @ColumnInfo(name = "Months_Count")
    public int monthsCount;

    @ColumnInfo(name = "Years_Count")
    public int yearsCount;

    @ColumnInfo(name = "Subscription_ID")
    public int subscriptionID;
}
