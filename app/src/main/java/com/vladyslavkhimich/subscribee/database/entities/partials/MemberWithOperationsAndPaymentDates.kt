package com.vladyslavkhimich.subscribee.database.entities.partials

import androidx.room.Embedded
import androidx.room.Relation
import com.vladyslavkhimich.subscribee.database.entities.Member
import com.vladyslavkhimich.subscribee.database.entities.Operation

data class MemberWithOperationsAndPaymentDates(
    @Embedded
    var member: Member? = null,

    @Relation(
            parentColumn = "Member_ID",
            entityColumn = "Member_ID",
            entity = Operation::class
    )
    var operationWithPaymentDates: MutableList<OperationWithPaymentDates> = ArrayList()
)