package com.vladyslavkhimich.subscribee.database.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.vladyslavkhimich.subscribee.database.daos.CurrencyDao;
import com.vladyslavkhimich.subscribee.retrofit.RetrofitRepository;

public class SetCurrencyRatesTask extends AsyncTask<Void, Void, Void> {
    CurrencyDao currencyDao;

    public SetCurrencyRatesTask(CurrencyDao currencyDao) {
        this.currencyDao = currencyDao;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        RetrofitRepository.getInstance().getAllCurrenciesRates(result -> {
            Log.i("CALLED", "setRates method called");
            AsyncTask.execute(() -> {
                try {
                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("AED"), result.getAed());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("AFN"), result.getAfn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ALL"), result.getAll());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("AMD"), result.getAmd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ANG"), result.getAng());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("AOA"), result.getAoa());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ARS"), result.getArs());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("AUD"), result.getAud());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("AWG"), result.getAwg());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("AZN"), result.getAzn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BAM"), result.getBam());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BBD"), result.getBbd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BDT"), result.getBdt());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BGN"), result.getBgn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BHD"), result.getBhd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BIF"), result.getBif());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BMD"), result.getBmd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BND"), result.getBnd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BOB"), result.getBob());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BRL"), result.getBrl());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BSD"), result.getBsd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BTC"), result.getBtc());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BTN"), result.getBtn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BWP"), result.getBwp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BYN"), result.getByn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("BZD"), result.getBzd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CAD"), result.getCad());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CDF"), result.getCdf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CHF"), result.getChf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CLP"), result.getClp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CLF"), result.getClf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CNH"), result.getCnh());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CNY"), result.getCny());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("COP"), result.getCop());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CRC"), result.getCrc());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CUP"), result.getCup());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CVE"), result.getCve());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("CZK"), result.getCzk());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("DJF"), result.getDjf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("DKK"), result.getDkk());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("DOP"), result.getDop());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("DZD"), result.getDzd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("EGP"), result.getEgp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ERN"), result.getErn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ETB"), result.getEtb());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("EUR"), result.getEur());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("FJD"), result.getFjd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("FKP"), result.getFkp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GBP"), result.getGbp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GEL"), result.getGel());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GGP"), result.getGgp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GHS"), result.getGhs());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GIP"), result.getGip());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GMD"), result.getGmd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GNF"), result.getGnf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GTQ"), result.getGtq());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("GYD"), result.getGyd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("HKD"), result.getHkd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("HNL"), result.getHnl());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("HRK"), result.getHrk());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("HTG"), result.getHtg());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("HUF"), result.getHuf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("IDR"), result.getIdr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ILS"), result.getIls());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("INR"), result.getInr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("IQD"), result.getIqd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("IRR"), result.getIrr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ISK"), result.getIsk());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("JEP"), result.getJep());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("JMD"), result.getJmd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("JOD"), result.getJod());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("JPY"), result.getJpy());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KES"), result.getKes());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KGS"), result.getKgs());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KHR"), result.getKhr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KMF"), result.getKmf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KPW"), result.getKpw());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KRW"), result.getKrw());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KWD"), result.getKwd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KYD"), result.getKyd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("KZT"), result.getKzt());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("LAK"), result.getLak());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("LBP"), result.getLbp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("LKR"), result.getLkr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("LRD"), result.getLrd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("LSL"), result.getLsl());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("LYD"), result.getLyd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MAD"), result.getMad());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MDL"), result.getMdl());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MGA"), result.getMga());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MKD"), result.getMkd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MMK"), result.getMmk());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MNT"), result.getMnt());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MOP"), result.getMop());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MRU"), result.getMru());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MUR"), result.getMur());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MVR"), result.getMvr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MWK"), result.getMwk());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MXN"), result.getMxn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MYR"), result.getMyr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("MZN"), result.getMzn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("NAD"), result.getNad());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("NGN"), result.getNgn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("NIO"), result.getNio());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("NOK"), result.getNok());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("NPR"), result.getNpr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("NZD"), result.getNzd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("OMR"), result.getOmr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("PAB"), result.getPab());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("PEN"), result.getPen());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("PGK"), result.getPgk());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("PHP"), result.getPhp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("PKR"), result.getPkr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("PLN"), result.getPln());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("PYG"), result.getPyg());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("QAR"), result.getQar());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("RON"), result.getRon());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("RSD"), result.getRsd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("RUB"), result.getRub());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("RWF"), result.getRwf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SAR"), result.getSar());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SBD"), result.getSbd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SCR"), result.getScr());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SDG"), result.getSdg());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SEK"), result.getSek());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SGD"), result.getSgd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SHP"), result.getShp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SLL"), result.getSll());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SOS"), result.getSos());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SRD"), result.getSrd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SSP"), result.getSsp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("STN"), result.getStn());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SVC"), result.getSvc());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SYP"), result.getSyp());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("SZL"), result.getSzl());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("THB"), result.getThb());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("TJS"), result.getTjs());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("TMT"), result.getTmt());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("TND"), result.getTnd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("TOP"), result.getTop());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("TRY"), result.getTry());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("TTD"), result.getTtd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("TZS"), result.getTzs());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("UAH"), result.getUah());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("UGX"), result.getUgx());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("USD"), result.getUsd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("UYU"), result.getUyu());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("UZS"), result.getUzs());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("VES"), result.getVes());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("VND"), result.getVnd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("VUV"), result.getVuv());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("WST"), result.getWst());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("XAF"), result.getXaf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("XCD"), result.getXcd());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("XOF"), result.getXof());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("XPF"), result.getXpf());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("YER"), result.getYer());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ZAR"), result.getZar());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ZMW"), result.getZmw());

                    currencyDao.updateCurrencyRate(currencyDao.getIdByShortName("ZWL"), result.getZwl());
                }
                catch (Exception exception) {
                    Log.e("DB Error", exception.getMessage());
                }
            });
        });
        return null;
    }
}
