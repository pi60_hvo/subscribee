package com.vladyslavkhimich.subscribee.database.entities.partials;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.database.entities.FreePeriod;
import com.vladyslavkhimich.subscribee.database.entities.Reminder;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;

public class ManagingSubscription {
    @Embedded public Subscription subscription;

    @Relation(
            parentColumn = "Subscription_ID",
            entityColumn = "Subscription_ID"
    )
    public Reminder reminder;

    @Relation(
            parentColumn = "Subscription_ID",
            entityColumn = "Subscription_ID"
    )
    public FreePeriod freePeriod;

    @Relation(
            parentColumn = "Category_ID",
            entityColumn = "Category_ID"
    )
    public Category category;

    @Relation(
            parentColumn = "Cycle_ID",
            entityColumn = "Cycle_ID"
    )
    public Cycle cycle;

    @Relation(
            parentColumn = "Currency_ID",
            entityColumn = "Currency_ID"
    )
    public Currency currency;
}
