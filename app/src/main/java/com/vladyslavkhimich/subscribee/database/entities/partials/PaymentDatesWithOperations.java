package com.vladyslavkhimich.subscribee.database.entities.partials;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.vladyslavkhimich.subscribee.database.entities.Operation;
import com.vladyslavkhimich.subscribee.database.entities.PaymentDate;

import java.util.List;

public class PaymentDatesWithOperations {
    @Embedded
    public PaymentDate paymentDate;

    @Relation(
            parentColumn = "Payment_Date_ID",
            entityColumn = "Payment_Date_ID"
    )
    public List<Operation> operations;
}
