package com.vladyslavkhimich.subscribee.database.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.vladyslavkhimich.subscribee.database.entities.Cycle;

import java.util.List;

@Dao
public interface CycleDao {

    @Insert
    long insertOne(Cycle cycle);
    @Insert
    void insertAll(List<Cycle> cycles);

    @Query("SELECT * FROM Cycle ORDER BY Cycle_ID ASC")
    LiveData<List<Cycle>> getAll();

    @Query("SELECT * " +
            "FROM Cycle " +
            "WHERE Days_Count= :daysCount " +
            "AND Weeks_Count= :weeksCount " +
            "AND Months_Count= :monthsCount " +
            "AND Years_Count= :yearsCount " +
            "LIMIT 1")
    Cycle returnCycleIfHasParameters(int daysCount, int weeksCount, int monthsCount, int yearsCount);

    @Query("SELECT * FROM Cycle WHERE Cycle_ID = :id")
    Cycle getCycleByID(int id);

    @Query("SELECT * FROM Cycle WHERE Cycle_ID = (SELECT Cycle_ID FROM Subscription WHERE Subscription_ID = :subscriptionID)")
    Cycle getCycleBySubscriptionID(int subscriptionID);
}
