package com.vladyslavkhimich.subscribee.database.entities.partials

import androidx.room.Embedded
import androidx.room.Relation
import com.vladyslavkhimich.subscribee.database.entities.Currency
import com.vladyslavkhimich.subscribee.database.entities.Member
import com.vladyslavkhimich.subscribee.database.entities.Operation
import com.vladyslavkhimich.subscribee.database.entities.Subscription

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
data class OperationWithDataForStats(
    @Embedded
    var operation: Operation,

    @Relation(
        parentColumn = "Subscription_ID",
        entityColumn = "Subscription_ID",
        entity = Subscription::class
    )
    var subscriptionWithCategory: SubscriptionWithCategory,

    @Relation(
        parentColumn = "Member_ID",
        entityColumn = "Member_ID"
    )
    var member: Member?,

    @Relation(
        parentColumn = "Currency_ID",
        entityColumn = "Currency_ID"
    )
    var currency: Currency
)
