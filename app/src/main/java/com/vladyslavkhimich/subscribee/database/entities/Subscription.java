package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.vladyslavkhimich.subscribee.database.converters.DateConverter;

import java.util.Date;

import static androidx.room.ForeignKey.SET_NULL;

@Entity(foreignKeys = {
        @ForeignKey(
                entity = Cycle.class,
                parentColumns = "Cycle_ID",
                childColumns = "Cycle_ID"
),
        @ForeignKey(
                entity = Category.class,
                parentColumns = "Category_ID",
                childColumns = "Category_ID",
                onDelete = SET_NULL
),
        @ForeignKey(
                entity = Currency.class,
                parentColumns = "Currency_ID",
                childColumns = "Currency_ID"
        ),
        @ForeignKey(
                entity = Member.class,
                parentColumns = "Member_ID",
                childColumns = "Owner_ID"
        )
},
indices = {
        @Index("Cycle_ID"),
        @Index("Category_ID"),
        @Index("Currency_ID"),
        @Index("Owner_ID")
})
public class Subscription {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Subscription_ID")
    public int subscriptionID;

    @ColumnInfo(name = "Name")
    public String name;

    @ColumnInfo(name = "Description")
    public String description;

    @TypeConverters({DateConverter.class})
    @ColumnInfo(name = "Start_Date")
    public Date startDate;

    @TypeConverters({DateConverter.class})
    @ColumnInfo(name = "Next_Payment_Date")
    public Date nextPaymentDate;

    @ColumnInfo(name = "Price")
    public double price;

    @ColumnInfo(name = "Icon_Color")
    public String iconColor;

    @ColumnInfo(name = "Icon")
    public String icon;

    @ColumnInfo(name = "Is_Archived")
    public boolean isArchived;

    @ColumnInfo(name = "Is_Family")
    public boolean isFamily;

    @ColumnInfo(name = "Is_Family_Part")
    public boolean isFamilyPart;

    @ColumnInfo(name = "Cycle_ID")
    public int cycleID;

    @ColumnInfo(name = "Category_ID")
    public int categoryID;

    @ColumnInfo(name = "Currency_ID")
    public int currencyID;

    @ColumnInfo(name = "Owner_ID")
    public Integer ownerID;
}
