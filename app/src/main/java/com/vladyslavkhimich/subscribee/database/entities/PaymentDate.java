package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.vladyslavkhimich.subscribee.database.converters.DateConverter;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys =
@ForeignKey(
        entity = Subscription.class,
        parentColumns = "Subscription_ID",
        childColumns = "Subscription_ID",
        onDelete = CASCADE
),
        indices = {
        @Index("Subscription_ID")
        }
)
public class PaymentDate {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Payment_Date_ID")
    public int paymentDateID;

    @TypeConverters({DateConverter.class})
    @ColumnInfo(name = "Payment_Date")
    public Date paymentDate;

    @ColumnInfo(name = "Subscription_ID")
    public int subscriptionID;
}
