package com.vladyslavkhimich.subscribee.database.entities.partials

import androidx.room.Embedded
import androidx.room.Relation
import com.vladyslavkhimich.subscribee.database.entities.Operation
import com.vladyslavkhimich.subscribee.database.entities.PaymentDate

data class OperationWithPaymentDates(
    @Embedded
    var operation: Operation? = null,

    @Relation(
            parentColumn = "Payment_Date_ID",
            entityColumn = "Payment_Date_ID"
    )
    var paymentDates: List<PaymentDate> = ArrayList()
)