package com.vladyslavkhimich.subscribee.database.entities.partials

import androidx.room.Embedded
import androidx.room.Relation
import com.vladyslavkhimich.subscribee.database.entities.Currency
import com.vladyslavkhimich.subscribee.database.entities.Member
import com.vladyslavkhimich.subscribee.database.entities.Operation
import com.vladyslavkhimich.subscribee.database.entities.Subscription

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 24.11.2021
 * @email vladislav.himich@4k.com.ua
 */
data class OperationWithAssociatedData(
    @Embedded
    var operation: Operation? = null,

    @Relation(
        parentColumn = "Subscription_ID",
        entityColumn = "Subscription_ID"
    )
    var subscription: Subscription,

    @Relation(
        parentColumn = "Member_ID",
        entityColumn = "Member_ID"
    )
    var member: Member?,

    @Relation(
        parentColumn = "Currency_ID",
        entityColumn = "Currency_ID"
    )
    var currency: Currency
)
