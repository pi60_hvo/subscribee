package com.vladyslavkhimich.subscribee.database.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.vladyslavkhimich.subscribee.database.entities.Currency;

import java.util.List;

@Dao
public interface CurrencyDao {
    @Insert
    void insertAll(List<Currency> currencies);
    @Query("UPDATE Currency SET Currency_Rate= :currencyRate WHERE Currency_ID= :id")
    void updateCurrencyRate(int id, double currencyRate);
    @Query("SELECT Currency_ID FROM Currency WHERE Short_Name= :shortName")
    int getIdByShortName(String shortName);
    @Query("SELECT * FROM Currency ORDER BY Currency_ID ASC")
    LiveData<List<Currency>> getAll();
    @Query("SELECT * FROM Currency ORDER BY Currency_ID ASC")
    List<Currency> getAllList();
    @Query("SELECT Short_Name FROM Currency WHERE Currency_ID = :id")
    String getShortNameByID(int id);
}
