package com.vladyslavkhimich.subscribee.database.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.vladyslavkhimich.subscribee.database.entities.Reminder;

import java.util.List;

@Dao
public interface ReminderDao {
    @Insert
    void insertOne(Reminder reminder);
    @Insert
    void insertAll(List<Reminder> reminders);

    @Update
    void updateOne(Reminder reminder);

    @Delete
    void deleteOne(Reminder reminder);

    @Query("DELETE FROM Reminder WHERE Subscription_ID = :subscriptionID")
    void deleteOneBySubscriptionID(int subscriptionID);

    @Query("SELECT Reminder_ID FROM Reminder WHERE Subscription_ID = :subscriptionID")
    int getReminderIDBySubscriptionID(int subscriptionID);

    @Query("SELECT * FROM Reminder WHERE Reminder_ID = :id")
    Reminder getOneByID(int id);
}
