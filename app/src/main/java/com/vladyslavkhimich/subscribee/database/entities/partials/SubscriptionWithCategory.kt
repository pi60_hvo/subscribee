package com.vladyslavkhimich.subscribee.database.entities.partials

import androidx.room.Embedded
import androidx.room.Relation
import com.vladyslavkhimich.subscribee.database.entities.Category
import com.vladyslavkhimich.subscribee.database.entities.Subscription

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
data class SubscriptionWithCategory(
    @Embedded
    var subscription: Subscription,
    @Relation(
        parentColumn = "Category_ID",
        entityColumn = "Category_ID"
    )
    var category: Category? = null
)
