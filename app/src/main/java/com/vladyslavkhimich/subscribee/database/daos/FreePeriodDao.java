package com.vladyslavkhimich.subscribee.database.daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.vladyslavkhimich.subscribee.database.entities.FreePeriod;

import java.util.List;

@Dao
public interface FreePeriodDao {
    @Insert
    void insertOne(FreePeriod freePeriod);
    @Insert
    void insertAll(List<FreePeriod> freePeriods);

    @Update
    void updateOne(FreePeriod freePeriod);

    @Query("DELETE FROM FreePeriod WHERE Free_Period_ID = :id")
    void deleteOne(int id);

    @Query("SELECT Free_Period_ID FROM FreePeriod WHERE Subscription_ID = :subscriptionID")
    int getIdBySubscriptionId(int subscriptionID);

    @Query("SELECT * FROM FreePeriod WHERE Free_Period_ID = :id")
    FreePeriod getOneByID(int id);

    @Query("SELECT * FROM FreePeriod WHERE Subscription_ID = :subscriptionId")
    FreePeriod getBySubscriptionId(int subscriptionId);
}
