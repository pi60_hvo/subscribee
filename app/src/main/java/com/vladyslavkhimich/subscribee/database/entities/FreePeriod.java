package com.vladyslavkhimich.subscribee.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import static androidx.room.ForeignKey.CASCADE;

import com.vladyslavkhimich.subscribee.database.converters.DateConverter;

import java.util.Date;

@Entity(foreignKeys =
@ForeignKey(
        entity = Subscription.class,
        parentColumns = "Subscription_ID",
        childColumns = "Subscription_ID",
        onDelete = CASCADE
),
        indices = {
        @Index("Subscription_ID")
})
public class FreePeriod {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Free_Period_ID")
    public int freePeriodID;

    @ColumnInfo(name = "Days_Count")
    public int daysCount;

    @ColumnInfo(name = "Weeks_Count")
    public int weeksCount;

    @ColumnInfo(name = "Months_Count")
    public int monthsCount;

    @ColumnInfo(name = "Years_Count")
    public int yearsCount;

    @ColumnInfo(name = "Subscription_ID")
    public int subscriptionID;

    @TypeConverters({DateConverter.class})
    @ColumnInfo(name = "Start_Date")
    public Date startDate;
}
