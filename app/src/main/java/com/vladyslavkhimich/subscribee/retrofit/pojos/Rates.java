package com.vladyslavkhimich.subscribee.retrofit.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rates {

    @SerializedName("AED")
    @Expose
    private double aed;
    @SerializedName("AFN")
    @Expose
    private double afn;
    @SerializedName("ALL")
    @Expose
    private double all;
    @SerializedName("AMD")
    @Expose
    private double amd;
    @SerializedName("ANG")
    @Expose
    private double ang;
    @SerializedName("AOA")
    @Expose
    private double aoa;
    @SerializedName("ARS")
    @Expose
    private double ars;
    @SerializedName("AUD")
    @Expose
    private double aud;
    @SerializedName("AWG")
    @Expose
    private double awg;
    @SerializedName("AZN")
    @Expose
    private double azn;
    @SerializedName("BAM")
    @Expose
    private double bam;
    @SerializedName("BBD")
    @Expose
    private double bbd;
    @SerializedName("BDT")
    @Expose
    private double bdt;
    @SerializedName("BGN")
    @Expose
    private double bgn;
    @SerializedName("BHD")
    @Expose
    private double bhd;
    @SerializedName("BIF")
    @Expose
    private double bif;
    @SerializedName("BMD")
    @Expose
    private double bmd;
    @SerializedName("BND")
    @Expose
    private double bnd;
    @SerializedName("BOB")
    @Expose
    private double bob;
    @SerializedName("BRL")
    @Expose
    private double brl;
    @SerializedName("BSD")
    @Expose
    private double bsd;
    @SerializedName("BTC")
    @Expose
    private double btc;
    @SerializedName("BTN")
    @Expose
    private double btn;
    @SerializedName("BWP")
    @Expose
    private double bwp;
    @SerializedName("BYN")
    @Expose
    private double byn;
    @SerializedName("BZD")
    @Expose
    private double bzd;
    @SerializedName("CAD")
    @Expose
    private double cad;
    @SerializedName("CDF")
    @Expose
    private double cdf;
    @SerializedName("CHF")
    @Expose
    private double chf;
    @SerializedName("CLF")
    @Expose
    private double clf;
    @SerializedName("CLP")
    @Expose
    private double clp;
    @SerializedName("CNH")
    @Expose
    private double cnh;
    @SerializedName("CNY")
    @Expose
    private double cny;
    @SerializedName("COP")
    @Expose
    private double cop;
    @SerializedName("CRC")
    @Expose
    private double crc;
    @SerializedName("CUC")
    @Expose
    private double cuc;
    @SerializedName("CUP")
    @Expose
    private double cup;
    @SerializedName("CVE")
    @Expose
    private double cve;
    @SerializedName("CZK")
    @Expose
    private double czk;
    @SerializedName("DJF")
    @Expose
    private double djf;
    @SerializedName("DKK")
    @Expose
    private double dkk;
    @SerializedName("DOP")
    @Expose
    private double dop;
    @SerializedName("DZD")
    @Expose
    private double dzd;
    @SerializedName("EGP")
    @Expose
    private double egp;
    @SerializedName("ERN")
    @Expose
    private double ern;
    @SerializedName("ETB")
    @Expose
    private double etb;
    @SerializedName("EUR")
    @Expose
    private double eur;
    @SerializedName("FJD")
    @Expose
    private double fjd;
    @SerializedName("FKP")
    @Expose
    private double fkp;
    @SerializedName("GBP")
    @Expose
    private double gbp;
    @SerializedName("GEL")
    @Expose
    private double gel;
    @SerializedName("GGP")
    @Expose
    private double ggp;
    @SerializedName("GHS")
    @Expose
    private double ghs;
    @SerializedName("GIP")
    @Expose
    private double gip;
    @SerializedName("GMD")
    @Expose
    private double gmd;
    @SerializedName("GNF")
    @Expose
    private double gnf;
    @SerializedName("GTQ")
    @Expose
    private double gtq;
    @SerializedName("GYD")
    @Expose
    private double gyd;
    @SerializedName("HKD")
    @Expose
    private double hkd;
    @SerializedName("HNL")
    @Expose
    private double hnl;
    @SerializedName("HRK")
    @Expose
    private double hrk;
    @SerializedName("HTG")
    @Expose
    private double htg;
    @SerializedName("HUF")
    @Expose
    private double huf;
    @SerializedName("IDR")
    @Expose
    private double idr;
    @SerializedName("ILS")
    @Expose
    private double ils;
    @SerializedName("IMP")
    @Expose
    private double imp;
    @SerializedName("INR")
    @Expose
    private double inr;
    @SerializedName("IQD")
    @Expose
    private double iqd;
    @SerializedName("IRR")
    @Expose
    private double irr;
    @SerializedName("ISK")
    @Expose
    private double isk;
    @SerializedName("JEP")
    @Expose
    private double jep;
    @SerializedName("JMD")
    @Expose
    private double jmd;
    @SerializedName("JOD")
    @Expose
    private double jod;
    @SerializedName("JPY")
    @Expose
    private double jpy;
    @SerializedName("KES")
    @Expose
    private double kes;
    @SerializedName("KGS")
    @Expose
    private double kgs;
    @SerializedName("KHR")
    @Expose
    private double khr;
    @SerializedName("KMF")
    @Expose
    private double kmf;
    @SerializedName("KPW")
    @Expose
    private double kpw;
    @SerializedName("KRW")
    @Expose
    private double krw;
    @SerializedName("KWD")
    @Expose
    private double kwd;
    @SerializedName("KYD")
    @Expose
    private double kyd;
    @SerializedName("KZT")
    @Expose
    private double kzt;
    @SerializedName("LAK")
    @Expose
    private double lak;
    @SerializedName("LBP")
    @Expose
    private double lbp;
    @SerializedName("LKR")
    @Expose
    private double lkr;
    @SerializedName("LRD")
    @Expose
    private double lrd;
    @SerializedName("LSL")
    @Expose
    private double lsl;
    @SerializedName("LYD")
    @Expose
    private double lyd;
    @SerializedName("MAD")
    @Expose
    private double mad;
    @SerializedName("MDL")
    @Expose
    private double mdl;
    @SerializedName("MGA")
    @Expose
    private double mga;
    @SerializedName("MKD")
    @Expose
    private double mkd;
    @SerializedName("MMK")
    @Expose
    private double mmk;
    @SerializedName("MNT")
    @Expose
    private double mnt;
    @SerializedName("MOP")
    @Expose
    private double mop;
    @SerializedName("MRO")
    @Expose
    private double mro;
    @SerializedName("MRU")
    @Expose
    private double mru;
    @SerializedName("MUR")
    @Expose
    private double mur;
    @SerializedName("MVR")
    @Expose
    private double mvr;
    @SerializedName("MWK")
    @Expose
    private double mwk;
    @SerializedName("MXN")
    @Expose
    private double mxn;
    @SerializedName("MYR")
    @Expose
    private double myr;
    @SerializedName("MZN")
    @Expose
    private double mzn;
    @SerializedName("NAD")
    @Expose
    private double nad;
    @SerializedName("NGN")
    @Expose
    private double ngn;
    @SerializedName("NIO")
    @Expose
    private double nio;
    @SerializedName("NOK")
    @Expose
    private double nok;
    @SerializedName("NPR")
    @Expose
    private double npr;
    @SerializedName("NZD")
    @Expose
    private double nzd;
    @SerializedName("OMR")
    @Expose
    private double omr;
    @SerializedName("PAB")
    @Expose
    private double pab;
    @SerializedName("PEN")
    @Expose
    private double pen;
    @SerializedName("PGK")
    @Expose
    private double pgk;
    @SerializedName("PHP")
    @Expose
    private double php;
    @SerializedName("PKR")
    @Expose
    private double pkr;
    @SerializedName("PLN")
    @Expose
    private double pln;
    @SerializedName("PYG")
    @Expose
    private double pyg;
    @SerializedName("QAR")
    @Expose
    private double qar;
    @SerializedName("RON")
    @Expose
    private double ron;
    @SerializedName("RSD")
    @Expose
    private double rsd;
    @SerializedName("RUB")
    @Expose
    private double rub;
    @SerializedName("RWF")
    @Expose
    private double rwf;
    @SerializedName("SAR")
    @Expose
    private double sar;
    @SerializedName("SBD")
    @Expose
    private double sbd;
    @SerializedName("SCR")
    @Expose
    private double scr;
    @SerializedName("SDG")
    @Expose
    private double sdg;
    @SerializedName("SEK")
    @Expose
    private double sek;
    @SerializedName("SGD")
    @Expose
    private double sgd;
    @SerializedName("SHP")
    @Expose
    private double shp;
    @SerializedName("SLL")
    @Expose
    private double sll;
    @SerializedName("SOS")
    @Expose
    private double sos;
    @SerializedName("SRD")
    @Expose
    private double srd;
    @SerializedName("SSP")
    @Expose
    private double ssp;
    @SerializedName("STD")
    @Expose
    private double std;
    @SerializedName("STN")
    @Expose
    private double stn;
    @SerializedName("SVC")
    @Expose
    private double svc;
    @SerializedName("SYP")
    @Expose
    private double syp;
    @SerializedName("SZL")
    @Expose
    private double szl;
    @SerializedName("THB")
    @Expose
    private double thb;
    @SerializedName("TJS")
    @Expose
    private double tjs;
    @SerializedName("TMT")
    @Expose
    private double tmt;
    @SerializedName("TND")
    @Expose
    private double tnd;
    @SerializedName("TOP")
    @Expose
    private double top;
    @SerializedName("TRY")
    @Expose
    private double _try;
    @SerializedName("TTD")
    @Expose
    private double ttd;
    @SerializedName("TWD")
    @Expose
    private double twd;
    @SerializedName("TZS")
    @Expose
    private double tzs;
    @SerializedName("UAH")
    @Expose
    private double uah;
    @SerializedName("UGX")
    @Expose
    private double ugx;
    @SerializedName("USD")
    @Expose
    private double usd;
    @SerializedName("UYU")
    @Expose
    private double uyu;
    @SerializedName("UZS")
    @Expose
    private double uzs;
    @SerializedName("VES")
    @Expose
    private double ves;
    @SerializedName("VND")
    @Expose
    private double vnd;
    @SerializedName("VUV")
    @Expose
    private double vuv;
    @SerializedName("WST")
    @Expose
    private double wst;
    @SerializedName("XAF")
    @Expose
    private double xaf;
    @SerializedName("XAG")
    @Expose
    private double xag;
    @SerializedName("XAU")
    @Expose
    private double xau;
    @SerializedName("XCD")
    @Expose
    private double xcd;
    @SerializedName("XDR")
    @Expose
    private double xdr;
    @SerializedName("XOF")
    @Expose
    private double xof;
    @SerializedName("XPD")
    @Expose
    private double xpd;
    @SerializedName("XPF")
    @Expose
    private double xpf;
    @SerializedName("XPT")
    @Expose
    private double xpt;
    @SerializedName("YER")
    @Expose
    private double yer;
    @SerializedName("ZAR")
    @Expose
    private double zar;
    @SerializedName("ZMW")
    @Expose
    private double zmw;
    @SerializedName("ZWL")
    @Expose
    private double zwl;

    public double getAed() {
        return aed;
    }

    public void setAed(double aed) {
        this.aed = aed;
    }

    public double getAfn() {
        return afn;
    }

    public void setAfn(double afn) {
        this.afn = afn;
    }

    public double getAll() {
        return all;
    }

    public void setAll(double all) {
        this.all = all;
    }

    public double getAmd() {
        return amd;
    }

    public void setAmd(double amd) {
        this.amd = amd;
    }

    public double getAng() {
        return ang;
    }

    public void setAng(double ang) {
        this.ang = ang;
    }

    public double getAoa() {
        return aoa;
    }

    public void setAoa(double aoa) {
        this.aoa = aoa;
    }

    public double getArs() {
        return ars;
    }

    public void setArs(double ars) {
        this.ars = ars;
    }

    public double getAud() {
        return aud;
    }

    public void setAud(double aud) {
        this.aud = aud;
    }

    public double getAwg() {
        return awg;
    }

    public void setAwg(double awg) {
        this.awg = awg;
    }

    public double getAzn() {
        return azn;
    }

    public void setAzn(double azn) {
        this.azn = azn;
    }

    public double getBam() {
        return bam;
    }

    public void setBam(double bam) {
        this.bam = bam;
    }

    public double getBbd() {
        return bbd;
    }

    public void setBbd(double bbd) {
        this.bbd = bbd;
    }

    public double getBdt() {
        return bdt;
    }

    public void setBdt(double bdt) {
        this.bdt = bdt;
    }

    public double getBgn() {
        return bgn;
    }

    public void setBgn(double bgn) {
        this.bgn = bgn;
    }

    public double getBhd() {
        return bhd;
    }

    public void setBhd(double bhd) {
        this.bhd = bhd;
    }

    public double getBif() {
        return bif;
    }

    public void setBif(double bif) {
        this.bif = bif;
    }

    public double getBmd() {
        return bmd;
    }

    public void setBmd(double bmd) {
        this.bmd = bmd;
    }

    public double getBnd() {
        return bnd;
    }

    public void setBnd(double bnd) {
        this.bnd = bnd;
    }

    public double getBob() {
        return bob;
    }

    public void setBob(double bob) {
        this.bob = bob;
    }

    public double getBrl() {
        return brl;
    }

    public void setBrl(double brl) {
        this.brl = brl;
    }

    public double getBsd() {
        return bsd;
    }

    public void setBsd(double bsd) {
        this.bsd = bsd;
    }

    public double getBtc() {
        return btc;
    }

    public void setBtc(double btc) {
        this.btc = btc;
    }

    public double getBtn() {
        return btn;
    }

    public void setBtn(double btn) {
        this.btn = btn;
    }

    public double getBwp() {
        return bwp;
    }

    public void setBwp(double bwp) {
        this.bwp = bwp;
    }

    public double getByn() {
        return byn;
    }

    public void setByn(double byn) {
        this.byn = byn;
    }

    public double getBzd() {
        return bzd;
    }

    public void setBzd(double bzd) {
        this.bzd = bzd;
    }

    public double getCad() {
        return cad;
    }

    public void setCad(double cad) {
        this.cad = cad;
    }

    public double getCdf() {
        return cdf;
    }

    public void setCdf(double cdf) {
        this.cdf = cdf;
    }

    public double getChf() {
        return chf;
    }

    public void setChf(double chf) {
        this.chf = chf;
    }

    public double getClf() {
        return clf;
    }

    public void setClf(double clf) {
        this.clf = clf;
    }

    public double getClp() {
        return clp;
    }

    public void setClp(double clp) {
        this.clp = clp;
    }

    public double getCnh() {
        return cnh;
    }

    public void setCnh(double cnh) {
        this.cnh = cnh;
    }

    public double getCny() {
        return cny;
    }

    public void setCny(double cny) {
        this.cny = cny;
    }

    public double getCop() {
        return cop;
    }

    public void setCop(double cop) {
        this.cop = cop;
    }

    public double getCrc() {
        return crc;
    }

    public void setCrc(double crc) {
        this.crc = crc;
    }

    public double getCuc() {
        return cuc;
    }

    public void setCuc(double cuc) {
        this.cuc = cuc;
    }

    public double getCup() {
        return cup;
    }

    public void setCup(double cup) {
        this.cup = cup;
    }

    public double getCve() {
        return cve;
    }

    public void setCve(double cve) {
        this.cve = cve;
    }

    public double getCzk() {
        return czk;
    }

    public void setCzk(double czk) {
        this.czk = czk;
    }

    public double getDjf() {
        return djf;
    }

    public void setDjf(double djf) {
        this.djf = djf;
    }

    public double getDkk() {
        return dkk;
    }

    public void setDkk(double dkk) {
        this.dkk = dkk;
    }

    public double getDop() {
        return dop;
    }

    public void setDop(double dop) {
        this.dop = dop;
    }

    public double getDzd() {
        return dzd;
    }

    public void setDzd(double dzd) {
        this.dzd = dzd;
    }

    public double getEgp() {
        return egp;
    }

    public void setEgp(double egp) {
        this.egp = egp;
    }

    public double getErn() {
        return ern;
    }

    public void setErn(double ern) {
        this.ern = ern;
    }

    public double getEtb() {
        return etb;
    }

    public void setEtb(double etb) {
        this.etb = etb;
    }

    public double getEur() {
        return eur;
    }

    public void setEur(double eur) {
        this.eur = eur;
    }

    public double getFjd() {
        return fjd;
    }

    public void setFjd(double fjd) {
        this.fjd = fjd;
    }

    public double getFkp() {
        return fkp;
    }

    public void setFkp(double fkp) {
        this.fkp = fkp;
    }

    public double getGbp() {
        return gbp;
    }

    public void setGbp(double gbp) {
        this.gbp = gbp;
    }

    public double getGel() {
        return gel;
    }

    public void setGel(double gel) {
        this.gel = gel;
    }

    public double getGgp() {
        return ggp;
    }

    public void setGgp(double ggp) {
        this.ggp = ggp;
    }

    public double getGhs() {
        return ghs;
    }

    public void setGhs(double ghs) {
        this.ghs = ghs;
    }

    public double getGip() {
        return gip;
    }

    public void setGip(double gip) {
        this.gip = gip;
    }

    public double getGmd() {
        return gmd;
    }

    public void setGmd(double gmd) {
        this.gmd = gmd;
    }

    public double getGnf() {
        return gnf;
    }

    public void setGnf(double gnf) {
        this.gnf = gnf;
    }

    public double getGtq() {
        return gtq;
    }

    public void setGtq(double gtq) {
        this.gtq = gtq;
    }

    public double getGyd() {
        return gyd;
    }

    public void setGyd(double gyd) {
        this.gyd = gyd;
    }

    public double getHkd() {
        return hkd;
    }

    public void setHkd(double hkd) {
        this.hkd = hkd;
    }

    public double getHnl() {
        return hnl;
    }

    public void setHnl(double hnl) {
        this.hnl = hnl;
    }

    public double getHrk() {
        return hrk;
    }

    public void setHrk(double hrk) {
        this.hrk = hrk;
    }

    public double getHtg() {
        return htg;
    }

    public void setHtg(double htg) {
        this.htg = htg;
    }

    public double getHuf() {
        return huf;
    }

    public void setHuf(double huf) {
        this.huf = huf;
    }

    public double getIdr() {
        return idr;
    }

    public void setIdr(double idr) {
        this.idr = idr;
    }

    public double getIls() {
        return ils;
    }

    public void setIls(double ils) {
        this.ils = ils;
    }

    public double getImp() {
        return imp;
    }

    public void setImp(double imp) {
        this.imp = imp;
    }

    public double getInr() {
        return inr;
    }

    public void setInr(double inr) {
        this.inr = inr;
    }

    public double getIqd() {
        return iqd;
    }

    public void setIqd(double iqd) {
        this.iqd = iqd;
    }

    public double getIrr() {
        return irr;
    }

    public void setIrr(double irr) {
        this.irr = irr;
    }

    public double getIsk() {
        return isk;
    }

    public void setIsk(double isk) {
        this.isk = isk;
    }

    public double getJep() {
        return jep;
    }

    public void setJep(double jep) {
        this.jep = jep;
    }

    public double getJmd() {
        return jmd;
    }

    public void setJmd(double jmd) {
        this.jmd = jmd;
    }

    public double getJod() {
        return jod;
    }

    public void setJod(double jod) {
        this.jod = jod;
    }

    public double getJpy() {
        return jpy;
    }

    public void setJpy(double jpy) {
        this.jpy = jpy;
    }

    public double getKes() {
        return kes;
    }

    public void setKes(double kes) {
        this.kes = kes;
    }

    public double getKgs() {
        return kgs;
    }

    public void setKgs(double kgs) {
        this.kgs = kgs;
    }

    public double getKhr() {
        return khr;
    }

    public void setKhr(double khr) {
        this.khr = khr;
    }

    public double getKmf() {
        return kmf;
    }

    public void setKmf(double kmf) {
        this.kmf = kmf;
    }

    public double getKpw() {
        return kpw;
    }

    public void setKpw(double kpw) {
        this.kpw = kpw;
    }

    public double getKrw() {
        return krw;
    }

    public void setKrw(double krw) {
        this.krw = krw;
    }

    public double getKwd() {
        return kwd;
    }

    public void setKwd(double kwd) {
        this.kwd = kwd;
    }

    public double getKyd() {
        return kyd;
    }

    public void setKyd(double kyd) {
        this.kyd = kyd;
    }

    public double getKzt() {
        return kzt;
    }

    public void setKzt(double kzt) {
        this.kzt = kzt;
    }

    public double getLak() {
        return lak;
    }

    public void setLak(double lak) {
        this.lak = lak;
    }

    public double getLbp() {
        return lbp;
    }

    public void setLbp(double lbp) {
        this.lbp = lbp;
    }

    public double getLkr() {
        return lkr;
    }

    public void setLkr(double lkr) {
        this.lkr = lkr;
    }

    public double getLrd() {
        return lrd;
    }

    public void setLrd(double lrd) {
        this.lrd = lrd;
    }

    public double getLsl() {
        return lsl;
    }

    public void setLsl(double lsl) {
        this.lsl = lsl;
    }

    public double getLyd() {
        return lyd;
    }

    public void setLyd(double lyd) {
        this.lyd = lyd;
    }

    public double getMad() {
        return mad;
    }

    public void setMad(double mad) {
        this.mad = mad;
    }

    public double getMdl() {
        return mdl;
    }

    public void setMdl(double mdl) {
        this.mdl = mdl;
    }

    public double getMga() {
        return mga;
    }

    public void setMga(double mga) {
        this.mga = mga;
    }

    public double getMkd() {
        return mkd;
    }

    public void setMkd(double mkd) {
        this.mkd = mkd;
    }

    public double getMmk() {
        return mmk;
    }

    public void setMmk(double mmk) {
        this.mmk = mmk;
    }

    public double getMnt() {
        return mnt;
    }

    public void setMnt(double mnt) {
        this.mnt = mnt;
    }

    public double getMop() {
        return mop;
    }

    public void setMop(double mop) {
        this.mop = mop;
    }

    public double getMro() {
        return mro;
    }

    public void setMro(double mro) {
        this.mro = mro;
    }

    public double getMru() {
        return mru;
    }

    public void setMru(double mru) {
        this.mru = mru;
    }

    public double getMur() {
        return mur;
    }

    public void setMur(double mur) {
        this.mur = mur;
    }

    public double getMvr() {
        return mvr;
    }

    public void setMvr(double mvr) {
        this.mvr = mvr;
    }

    public double getMwk() {
        return mwk;
    }

    public void setMwk(double mwk) {
        this.mwk = mwk;
    }

    public double getMxn() {
        return mxn;
    }

    public void setMxn(double mxn) {
        this.mxn = mxn;
    }

    public double getMyr() {
        return myr;
    }

    public void setMyr(double myr) {
        this.myr = myr;
    }

    public double getMzn() {
        return mzn;
    }

    public void setMzn(double mzn) {
        this.mzn = mzn;
    }

    public double getNad() {
        return nad;
    }

    public void setNad(double nad) {
        this.nad = nad;
    }

    public double getNgn() {
        return ngn;
    }

    public void setNgn(double ngn) {
        this.ngn = ngn;
    }

    public double getNio() {
        return nio;
    }

    public void setNio(double nio) {
        this.nio = nio;
    }

    public double getNok() {
        return nok;
    }

    public void setNok(double nok) {
        this.nok = nok;
    }

    public double getNpr() {
        return npr;
    }

    public void setNpr(double npr) {
        this.npr = npr;
    }

    public double getNzd() {
        return nzd;
    }

    public void setNzd(double nzd) {
        this.nzd = nzd;
    }

    public double getOmr() {
        return omr;
    }

    public void setOmr(double omr) {
        this.omr = omr;
    }

    public double getPab() {
        return pab;
    }

    public void setPab(double pab) {
        this.pab = pab;
    }

    public double getPen() {
        return pen;
    }

    public void setPen(double pen) {
        this.pen = pen;
    }

    public double getPgk() {
        return pgk;
    }

    public void setPgk(double pgk) {
        this.pgk = pgk;
    }

    public double getPhp() {
        return php;
    }

    public void setPhp(double php) {
        this.php = php;
    }

    public double getPkr() {
        return pkr;
    }

    public void setPkr(double pkr) {
        this.pkr = pkr;
    }

    public double getPln() {
        return pln;
    }

    public void setPln(double pln) {
        this.pln = pln;
    }

    public double getPyg() {
        return pyg;
    }

    public void setPyg(double pyg) {
        this.pyg = pyg;
    }

    public double getQar() {
        return qar;
    }

    public void setQar(double qar) {
        this.qar = qar;
    }

    public double getRon() {
        return ron;
    }

    public void setRon(double ron) {
        this.ron = ron;
    }

    public double getRsd() {
        return rsd;
    }

    public void setRsd(double rsd) {
        this.rsd = rsd;
    }

    public double getRub() {
        return rub;
    }

    public void setRub(double rub) {
        this.rub = rub;
    }

    public double getRwf() {
        return rwf;
    }

    public void setRwf(double rwf) {
        this.rwf = rwf;
    }

    public double getSar() {
        return sar;
    }

    public void setSar(double sar) {
        this.sar = sar;
    }

    public double getSbd() {
        return sbd;
    }

    public void setSbd(double sbd) {
        this.sbd = sbd;
    }

    public double getScr() {
        return scr;
    }

    public void setScr(double scr) {
        this.scr = scr;
    }

    public double getSdg() {
        return sdg;
    }

    public void setSdg(double sdg) {
        this.sdg = sdg;
    }

    public double getSek() {
        return sek;
    }

    public void setSek(double sek) {
        this.sek = sek;
    }

    public double getSgd() {
        return sgd;
    }

    public void setSgd(double sgd) {
        this.sgd = sgd;
    }

    public double getShp() {
        return shp;
    }

    public void setShp(double shp) {
        this.shp = shp;
    }

    public double getSll() {
        return sll;
    }

    public void setSll(double sll) {
        this.sll = sll;
    }

    public double getSos() {
        return sos;
    }

    public void setSos(double sos) {
        this.sos = sos;
    }

    public double getSrd() {
        return srd;
    }

    public void setSrd(double srd) {
        this.srd = srd;
    }

    public double getSsp() {
        return ssp;
    }

    public void setSsp(double ssp) {
        this.ssp = ssp;
    }

    public double getStd() {
        return std;
    }

    public void setStd(double std) {
        this.std = std;
    }

    public double getStn() {
        return stn;
    }

    public void setStn(double stn) {
        this.stn = stn;
    }

    public double getSvc() {
        return svc;
    }

    public void setSvc(double svc) {
        this.svc = svc;
    }

    public double getSyp() {
        return syp;
    }

    public void setSyp(double syp) {
        this.syp = syp;
    }

    public double getSzl() {
        return szl;
    }

    public void setSzl(double szl) {
        this.szl = szl;
    }

    public double getThb() {
        return thb;
    }

    public void setThb(double thb) {
        this.thb = thb;
    }

    public double getTjs() {
        return tjs;
    }

    public void setTjs(double tjs) {
        this.tjs = tjs;
    }

    public double getTmt() {
        return tmt;
    }

    public void setTmt(double tmt) {
        this.tmt = tmt;
    }

    public double getTnd() {
        return tnd;
    }

    public void setTnd(double tnd) {
        this.tnd = tnd;
    }

    public double getTop() {
        return top;
    }

    public void setTop(double top) {
        this.top = top;
    }

    public double getTry() {
        return _try;
    }

    public void setTry(double _try) {
        this._try = _try;
    }

    public double getTtd() {
        return ttd;
    }

    public void setTtd(double ttd) {
        this.ttd = ttd;
    }

    public double getTwd() {
        return twd;
    }

    public void setTwd(double twd) {
        this.twd = twd;
    }

    public double getTzs() {
        return tzs;
    }

    public void setTzs(double tzs) {
        this.tzs = tzs;
    }

    public double getUah() {
        return uah;
    }

    public void setUah(double uah) {
        this.uah = uah;
    }

    public double getUgx() {
        return ugx;
    }

    public void setUgx(double ugx) {
        this.ugx = ugx;
    }

    public double getUsd() {
        return usd;
    }

    public void setUsd(double usd) {
        this.usd = usd;
    }

    public double getUyu() {
        return uyu;
    }

    public void setUyu(double uyu) {
        this.uyu = uyu;
    }

    public double getUzs() {
        return uzs;
    }

    public void setUzs(double uzs) {
        this.uzs = uzs;
    }

    public double getVes() {
        return ves;
    }

    public void setVes(double ves) {
        this.ves = ves;
    }

    public double getVnd() {
        return vnd;
    }

    public void setVnd(double vnd) {
        this.vnd = vnd;
    }

    public double getVuv() {
        return vuv;
    }

    public void setVuv(double vuv) {
        this.vuv = vuv;
    }

    public double getWst() {
        return wst;
    }

    public void setWst(double wst) {
        this.wst = wst;
    }

    public double getXaf() {
        return xaf;
    }

    public void setXaf(double xaf) {
        this.xaf = xaf;
    }

    public double getXag() {
        return xag;
    }

    public void setXag(double xag) {
        this.xag = xag;
    }

    public double getXau() {
        return xau;
    }

    public void setXau(double xau) {
        this.xau = xau;
    }

    public double getXcd() {
        return xcd;
    }

    public void setXcd(double xcd) {
        this.xcd = xcd;
    }

    public double getXdr() {
        return xdr;
    }

    public void setXdr(double xdr) {
        this.xdr = xdr;
    }

    public double getXof() {
        return xof;
    }

    public void setXof(double xof) {
        this.xof = xof;
    }

    public double getXpd() {
        return xpd;
    }

    public void setXpd(double xpd) {
        this.xpd = xpd;
    }

    public double getXpf() {
        return xpf;
    }

    public void setXpf(double xpf) {
        this.xpf = xpf;
    }

    public double getXpt() {
        return xpt;
    }

    public void setXpt(double xpt) {
        this.xpt = xpt;
    }

    public double getYer() {
        return yer;
    }

    public void setYer(double yer) {
        this.yer = yer;
    }

    public double getZar() {
        return zar;
    }

    public void setZar(double zar) {
        this.zar = zar;
    }

    public double getZmw() {
        return zmw;
    }

    public void setZmw(double zmw) {
        this.zmw = zmw;
    }

    public double getZwl() {
        return zwl;
    }

    public void setZwl(double zwl) {
        this.zwl = zwl;
    }

}
