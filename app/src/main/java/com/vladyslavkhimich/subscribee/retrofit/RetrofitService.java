package com.vladyslavkhimich.subscribee.retrofit;

import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    private static final String CURRENCY_API_URL = "https://api.exchangerate.host/";
    public static WebService webService;

    public static WebService getWebService() {
        if (webService == null) {
            Retrofit retrofit = new Retrofit
                    .Builder()
                    .baseUrl(CURRENCY_API_URL)
                    .addConverterFactory(GsonConverterFactory.create(
                            new GsonBuilder()
                                    .excludeFieldsWithoutExposeAnnotation()
                                    .create()))
                    .client(
                            new OkHttpClient.Builder()
                                    .addInterceptor(
                                            new HttpLoggingInterceptor()
                                            .setLevel(HttpLoggingInterceptor.Level.BODY))
                                    .build())
                    .build();
            webService = retrofit.create(WebService.class);
        }
        return webService;
    }
}
