package com.vladyslavkhimich.subscribee.retrofit;

import android.util.Log;

import com.vladyslavkhimich.subscribee.utils.interfaces.OnResponseCallback;
import com.vladyslavkhimich.subscribee.retrofit.pojos.AllCurrenciesResponse;
import com.vladyslavkhimich.subscribee.retrofit.pojos.Rates;

import org.jetbrains.annotations.NotNull;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitRepository {
    private static RetrofitRepository instance;
    private final WebService webService;

    public RetrofitRepository() {
        webService = RetrofitService.getWebService();
    }

    public static RetrofitRepository getInstance() {
        if (instance == null) {
            instance = new RetrofitRepository();
        }
        return instance;
    }

    public void getAllCurrenciesRates(OnResponseCallback<Rates> callback) {
        webService.getAllCurrenciesRates().enqueue(new Callback<AllCurrenciesResponse>() {
            @Override
            public void onResponse(@NotNull Call<AllCurrenciesResponse> call, @NotNull Response<AllCurrenciesResponse> response) {
                if (response.isSuccessful()) {
                    AllCurrenciesResponse allCurrenciesResponse = response.body();
                    callback.next(allCurrenciesResponse != null ? allCurrenciesResponse.getRates(): null);
                }
                else {
                    Log.e("Failed", "Didn't retrieve rates");
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            retry(call);
                        }
                    }, 30000);
                }
            }

            @Override
            public void onFailure(@NotNull Call<AllCurrenciesResponse> call, @NotNull Throwable t) {
                Log.e("Failed", t.getMessage());
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        retry(call);
                    }
                }, 30000);
            }

            private void retry(Call<AllCurrenciesResponse> call) {
                Log.i("Retry", "Rates call will be retried");
                call.clone().enqueue(this);
            }
        });
    }
}
