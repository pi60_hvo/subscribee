package com.vladyslavkhimich.subscribee.retrofit;

import com.vladyslavkhimich.subscribee.retrofit.pojos.AllCurrenciesResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebService {
    @GET("/latest?base=USD")
    Call<AllCurrenciesResponse> getAllCurrenciesRates();
}
