package com.vladyslavkhimich.subscribee.utils.ui.adapters.payments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.models.Payment;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;

public class PersonalPaymentsAdapter extends RecyclerView.Adapter<PersonalPaymentsAdapter.PersonalPaymentViewHolder> {

    ArrayList<Payment> payments = new ArrayList<>();
    OnItemClickListenerCallback onItemClickListenerCallback;

    public PersonalPaymentsAdapter(OnItemClickListenerCallback onItemClickListenerCallback) {
        this.onItemClickListenerCallback = onItemClickListenerCallback;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setPayments(ArrayList<Payment> payments) {
        this.payments = payments;
        notifyDataSetChanged();
    }

    public Payment getPayment(int position) {
        return payments.get(position);
    }

    public Payment getNextPaymentDateForSubscription() {
        int lastCheckedIndex = -1;

        for (int i = 0; i < payments.size(); i++) {
            if (payments.get(i).isChecked())
                lastCheckedIndex = i;
        }

        if (lastCheckedIndex == -1)
            return payments.get(0);

        if (lastCheckedIndex < payments.size() - 1)
            return payments.get(lastCheckedIndex + 1);

        return payments.get(payments.size() - 1);
    }

    public Date getMinimumNextPaymentDateForDialog() {
        int lastCheckedIndex = 0;

        for (int i = 0; i < payments.size(); i++) {
            if (payments.get(i).isChecked())
                lastCheckedIndex = i;
        }

        Date minNextPaymentDate = payments.get(lastCheckedIndex).getPaymentDate();

        return DateHelper.addOneDayToDate(minNextPaymentDate);
    }

    public boolean checkIfPositionBeforeLastCheckedPosition(int position) {
        int lastCheckedPosition = 0;
        for (int i = 0; i < payments.size(); i++) {
            if (payments.get(i).isChecked())
                lastCheckedPosition = i;
        }
        return position < lastCheckedPosition;
    }

    @NonNull
    @NotNull
    @Override
    public PersonalPaymentViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View personalPaymentView = LayoutInflater.from(context).inflate(R.layout.item_payment, parent, false);
        return new PersonalPaymentViewHolder(personalPaymentView);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull PersonalPaymentViewHolder holder, int position) {
        Payment payment = payments.get(position);

        holder.paymentDateTextView.setText(DateHelper.getNextPaymentDate(payment.getPaymentDate()));
        holder.paymentCheckBox.setChecked(payment.isChecked());
    }

    @Override
    public int getItemCount() {
        return payments.size();
    }

    public class PersonalPaymentViewHolder extends RecyclerView.ViewHolder {
        TextView paymentDateTextView;
        CheckBox paymentCheckBox;

        public PersonalPaymentViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            paymentDateTextView = itemView.findViewById(R.id.paymentItemDateTextView);
            paymentCheckBox = itemView.findViewById(R.id.paymentItemCheckBox);
            paymentCheckBox.setOnClickListener(v -> onItemClickListenerCallback.onItemClicked(v, getAdapterPosition()));
        }
    }
}
