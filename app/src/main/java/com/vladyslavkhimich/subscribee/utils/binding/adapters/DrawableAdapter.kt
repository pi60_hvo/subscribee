package com.vladyslavkhimich.subscribee.utils.binding.adapters

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 13.12.2021
 * @email vladislav.himich@4k.com.ua
 */
object DrawableAdapter {
    @JvmStatic
    @BindingAdapter("setBackgroundDrawable")
    fun bindingSetBackgroundDrawable(view: View, colorHex: String) {
        view.setBackgroundColor(ContextCompat.getColor(view.context, R.color.transparent))
        val backgroundDrawable = view.background
        if (backgroundDrawable is GradientDrawable) {
            backgroundDrawable.mutate()
            backgroundDrawable.setColor(Color.parseColor(colorHex))
            view.background = backgroundDrawable
        }
    }

    @JvmStatic
    @BindingAdapter("setIcon")
    fun bindingSetIcon(imageView: ImageView, iconId: String) {
        imageView.setImageResource(ResourcesHelper.getImageIDFromName(imageView.context, iconId))
    }
}