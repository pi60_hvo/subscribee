package com.vladyslavkhimich.subscribee.utils.ui.adapters.items;

public abstract class OperationListItem {
    public static final int TYPE_DATE = 0;
    public static final int TYPE_GENERAL = 1;

    abstract public int getType();
}
