package com.vladyslavkhimich.subscribee.utils.others.stats.realization

import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats
import com.vladyslavkhimich.subscribee.models.stats.all.AllStatsOverview
import com.vladyslavkhimich.subscribee.utils.interfaces.StatsFactory
import com.vladyslavkhimich.subscribee.utils.mappers.mapFamilySubscriptionsData
import com.vladyslavkhimich.subscribee.utils.others.stats.helpers.StatsHelper
import java.util.*

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
class AllStatsFactory : StatsFactory {
    override fun getOverviewStats(operationsWithData: List<OperationWithDataForStats>, previousOperationsWithData: List<OperationWithDataForStats>) : AllStatsOverview {
        val mappedFamilyOperationsWithData = LinkedList<OperationWithDataForStats>()
        mappedFamilyOperationsWithData.mapFamilySubscriptionsData(operationsWithData)
        val resultStats = AllStatsOverview()
        resultStats.totalSpent = StatsHelper.getTotalSpent(operationsWithData)
        resultStats.dayAverageSpent = StatsHelper.getSpentPerDay(operationsWithData)
        resultStats.percentage = StatsHelper.getPercentage(operationsWithData, previousOperationsWithData)
        resultStats.mostSpentSubscription = StatsHelper.getMostSpentOrPaidSubscription(operationsWithData, isSpent = true, isFamily = false)
        resultStats.mostSpentCategory = StatsHelper.getMostSpentCategory(operationsWithData)
        resultStats.mostPaidSubscription = StatsHelper.getMostSpentOrPaidSubscription(mappedFamilyOperationsWithData, isSpent = false, isFamily = true)
        resultStats.mostPaidMember = StatsHelper.getMostPaidMember(mappedFamilyOperationsWithData)
        return resultStats
    }

    override fun getTimelineStats() {
        TODO("Not yet implemented")
    }

    override fun getRadialStats() {
        TODO("Not yet implemented")
    }
}