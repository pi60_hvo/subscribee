package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.familypart

import android.content.Context
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart.FamilyPartManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.IconPickerDialog

class FamilyPartManagingIconPickerDialog(context: Context) : IconPickerDialog(context) {
    private lateinit var familyPartManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyPartManagingViewModel = (context as FamilyPartManagingActivity).subscriptionViewModel
    }

    override fun setActivitySelectedIcon(iconId: Int) {
        familyPartManagingViewModel.setSelectedIconID(iconId)
    }

    override fun setActivitySelectedColor(color: Int) {
        familyPartManagingViewModel.setSelectedIconColor(color)
    }

    override fun getActivitySelectedIcon(): Int {
        return familyPartManagingViewModel.selectedIconIDValue
    }

    override fun getActivitySelectedColor(): Int {
        return familyPartManagingViewModel.selectedColorIDValue
    }
}