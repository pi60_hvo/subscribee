package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.personal.PersonalManagingActivity;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.NextPaymentManagingDialog;

import java.util.Calendar;


public class PersonalManagingNextPaymentDialog extends NextPaymentManagingDialog {
    public PersonalManagingNextPaymentDialog(Context context) {
        super(context);
    }

    @Override
    public void setSelectedDate(int year, int month, int dayOfMonth) {
        ((PersonalManagingActivity)context).setNextPaymentDate(year, month, dayOfMonth);
        ((PersonalManagingActivity)context).setNextPaymentDateString();
        ((PersonalManagingActivity)context).setNextPaymentTextInputText();
    }

    @Override
    public void setSelectedDate(Calendar calendar) {
        ((PersonalManagingActivity)context).setNextPaymentDate(calendar);
        ((PersonalManagingActivity)context).setNextPaymentDateString();
        ((PersonalManagingActivity)context).setNextPaymentTextInputText();
    }

    @Override
    public void addToNextPaymentFreePeriod() {
        ((PersonalManagingActivity)context).freePeriodDialog.getFreePeriodsAdapter().addToNextPaymentFreePeriod();
    }

    @Override
    public int getSubscriptionOperationsCount() {
        return DatabaseRepository.getInstance(context).getSubscriptionOperationsCountByID(((PersonalManagingActivity)context).subscriptionID);
    }
}
