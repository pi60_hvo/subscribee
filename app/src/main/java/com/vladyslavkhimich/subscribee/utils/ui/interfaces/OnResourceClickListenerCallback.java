package com.vladyslavkhimich.subscribee.utils.ui.interfaces;

public interface OnResourceClickListenerCallback {
    void onResourceClicked(Integer resourceId);
}
