package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.personal.PersonalManagingActivity;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CategoryDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PersonalManagingCategoryDialog extends CategoryDialog {
    SubscriptionViewModel personalManagingViewModel;
    int categoriesListItemsCount;
    int categoriesSetCount;
    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalManagingViewModel = ((PersonalManagingActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCategories(List<Category> categories) {
        categoriesSetCount++;
        getCategoryAdapter().setCategories((ArrayList<Category>) categories);
        if (categoriesSetCount > 1 && categories.size() > categoriesListItemsCount)
            scrollRecyclerViewToBottom();
        categoriesListItemsCount = categories.size();
    }

    @Override
    public void setSelectedCategory(Category category) {
        personalManagingViewModel.setSelectedCategory(category);
    }

    @Override
    public Category getSelectedCategory() {
        return personalManagingViewModel.getSelectedCategoryObject();
    }

    @Override
    public void resetSelectedCategoryIfDeleted(Category category) {
        if (Objects.equals(personalManagingViewModel.getSelectedCategory().getValue(), category))
            personalManagingViewModel.setSelectedCategory(null);
    }
}
