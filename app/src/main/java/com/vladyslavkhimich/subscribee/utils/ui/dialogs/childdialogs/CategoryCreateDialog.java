package com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CategoryDialog;
import com.vladyslavkhimich.subscribee.utils.ui.validators.StringValidator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class CategoryCreateDialog {
    CategoryDialog parentDialog;
    AlertDialog categoryCreateAlertDialog;
    public TextInputLayout newCategoryInputLayout;
    public TextInputEditText newCategoryEditText;
    public boolean isTextHasError = true;

    public void initialize(Context context) {
        AlertDialog.Builder categoryCreateAlertDialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_category_create_custom, null);
        categoryCreateAlertDialogBuilder.setView(dialogView);
        newCategoryInputLayout = dialogView.findViewById(R.id.newCategoryDialogInputLayout);
        newCategoryEditText = dialogView.findViewById(R.id.newCategoryDialogEditText);
        categoryCreateAlertDialogBuilder.setPositiveButton(R.string.create_button, ((dialog, which) -> {

        }));
        categoryCreateAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, ((dialog, which) -> {
            categoryCreateAlertDialog.dismiss();
            parentDialog.getCategoryAlertDialog().show();
        }));
        newCategoryEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().replaceAll(" +", " ");
                isTextHasError = StringValidator.isErrorText(s.toString());
                if (!isTextHasError) {
                    newCategoryInputLayout.setError(null);
                    newCategoryInputLayout.setErrorEnabled(isTextHasError);
                }
                else {
                    newCategoryInputLayout.setError(context.getResources().getText(R.string.error_text));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        categoryCreateAlertDialog = categoryCreateAlertDialogBuilder.create();
        categoryCreateAlertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public AlertDialog getCategoryCreateAlertDialog() {
        return categoryCreateAlertDialog;
    }

    public void setParentDialog(CategoryDialog parentDialog) {
        this.parentDialog = parentDialog;
    }
}
