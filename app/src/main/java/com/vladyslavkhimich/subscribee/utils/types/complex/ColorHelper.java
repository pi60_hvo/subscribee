package com.vladyslavkhimich.subscribee.utils.types.complex;

import android.content.Context;

import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.ColorUtils;

import com.vladyslavkhimich.subscribee.app.MyApplication;
import com.vladyslavkhimich.subscribee.utils.types.primitives.IntegerHelper;

import java.util.ArrayList;

public class ColorHelper {
    public static ArrayList<Integer> getColorShadesFromColor(int color) {
        ArrayList<Integer> resultArray = new ArrayList<>();
        float colorLightness = getColorLightness(color) - .2f;
        for (int i = 0; i < 10; i++) {
            resultArray.add(getShadedColor(color, colorLightness));
            colorLightness += .04f;
        }
        return resultArray;
    }

    public static int getShadedColor(int color, float lightness) {
        float[] outHsl = new float[3];
        ColorUtils.colorToHSL(color, outHsl);
        outHsl[2] = lightness;
        return ColorUtils.HSLToColor(outHsl);
    }

    public static float getColorLightness(int color) {
        float[] outHsl = new float[3];
        ColorUtils.colorToHSL(color, outHsl);
        return outHsl[2];
    }

    public static String intColorToHexString(int color) {
        return String.format("#%06X", 0xFFFFFF & color);
    }

    public static ArrayList<Integer> convertColorsIdToIntColors(Context context, ArrayList<String> colorIds) {
        ArrayList<Integer> resultArray = new ArrayList<>();
        for (String colorId : colorIds) {
            resultArray.add(ResourcesCompat.getColor(context.getResources(), ResourcesHelper.getColorIDFromName(context, colorId), null));
        }
        return resultArray;
    }

    public static String getRandomColorFromSharedPrefs(Context context) {
        return MyApplication.getPickerColorsFromSharedPrefs(context)[IntegerHelper.getRandomIntegerInRange(0, MyApplication.COLORS_COUNT - 1)];
    }
}
