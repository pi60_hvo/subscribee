package com.vladyslavkhimich.subscribee.utils.ui.validators;

import android.util.Log;

public class NumberValidator {
    public static boolean isValidDoubleNumber(String s) {
        try {
            Double.parseDouble(s.replace(',', '.'));
            return true;
        }
        catch (NumberFormatException e) {
            Log.e("Conversion Error", e.getMessage());
            return false;
        }
    }
}
