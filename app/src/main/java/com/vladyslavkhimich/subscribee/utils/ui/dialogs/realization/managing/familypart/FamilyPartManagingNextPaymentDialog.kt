package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.familypart

import android.content.Context
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart.FamilyPartManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.NextPaymentManagingDialog
import java.util.*

class FamilyPartManagingNextPaymentDialog(context: Context) : NextPaymentManagingDialog(context) {
    override fun setSelectedDate(year: Int, month: Int, dayOfMonth: Int) {
        (context as FamilyPartManagingActivity).setNextPaymentDate(year, month, dayOfMonth)
        (context as FamilyPartManagingActivity).setNextPaymentDateString()
        (context as FamilyPartManagingActivity).setNextPaymentTextInputText()
    }

    override fun setSelectedDate(calendar: Calendar?) {
        (context as FamilyPartManagingActivity).setNextPaymentDate(calendar)
        (context as FamilyPartManagingActivity).setNextPaymentDateString()
        (context as FamilyPartManagingActivity).setNextPaymentTextInputText()
    }

    override fun addToNextPaymentFreePeriod() {
        (context as FamilyPartManagingActivity).freePeriodDialog.freePeriodsAdapter.addToNextPaymentFreePeriod()
    }

    override fun getSubscriptionOperationsCount(): Int {
        return DatabaseRepository.getInstance(context).getSubscriptionOperationsCountByID((context as FamilyPartManagingActivity).subscriptionID)
    }
}