package com.vladyslavkhimich.subscribee.utils.others.sorting.factory

import com.vladyslavkhimich.subscribee.utils.enums.SubscriptionSortings
import com.vladyslavkhimich.subscribee.utils.interfaces.SubscriptionSorting
import com.vladyslavkhimich.subscribee.utils.others.sorting.realization.SortingDate
import com.vladyslavkhimich.subscribee.utils.others.sorting.realization.SortingName
import com.vladyslavkhimich.subscribee.utils.others.sorting.realization.SortingPrice

class SortingFactory {
    companion object {
        fun getSorting(sorting: SubscriptionSortings) : SubscriptionSorting {
            return when (sorting) {
                SubscriptionSortings.PRICE -> SortingPrice()
                SubscriptionSortings.DATE -> SortingDate()
                else -> SortingName()
            }
        }
    }
}