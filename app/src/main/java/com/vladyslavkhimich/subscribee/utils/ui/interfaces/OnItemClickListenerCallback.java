package com.vladyslavkhimich.subscribee.utils.ui.interfaces;

import android.view.View;

public interface OnItemClickListenerCallback {
    void onItemClicked(View view, int position);
}
