package com.vladyslavkhimich.subscribee.utils.ui.dialogs.general;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.cycle.CycleAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs.PeriodPickerCycleDialog;

import java.util.List;

public abstract class CycleDialog {
    PeriodPickerCycleDialog childDialog;
    AlertDialog cycleAlertDialog;
    CycleAdapter cycleAdapter;

    public void initialize(Context context) {
        setAssociatedActivityViewModel(context);
        AlertDialog.Builder cycleAlertDialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_cycle_custom, null);
        cycleAlertDialogBuilder.setView(dialogView);
        cycleAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {

        });
        cycleAlertDialogBuilder.setNeutralButton(R.string.custom_reminder, (dialog, which) -> {
            //childDialog.getPeriodPickerAlertDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            childDialog.getPeriodPickerAlertDialog().show();
            childDialog.getNumberEditText().requestFocus();
        });
        RecyclerView cycleRecyclerView = dialogView.findViewById(R.id.dialogCycleRecyclerView);
        cycleAdapter = new CycleAdapter((view, position) -> {
            setSelectedCycle(cycleAdapter.getCycles().get(position));
            cycleAlertDialog.dismiss();
        });
        cycleRecyclerView.setAdapter(cycleAdapter);
        cycleRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        cycleRecyclerView.setHasFixedSize(true);
        cycleAlertDialog = cycleAlertDialogBuilder.create();
        initializeChildAlertDialog(context);
    }

    public void initializeChildAlertDialog(Context context) {
        childDialog = new PeriodPickerCycleDialog();
        childDialog.setParentDialog(this);
        childDialog.initialize(context);
    }

    public AlertDialog getCycleAlertDialog() {
        return cycleAlertDialog;
    }

    public CycleAdapter getCycleAdapter() {
        return cycleAdapter;
    }

    public abstract void setAssociatedActivityViewModel(Context context);

    public abstract void setAdapterCycles(List<Cycle> cycles);

    public abstract void setSelectedCycle(Cycle cycle);
}
