package com.vladyslavkhimich.subscribee.utils.enums

enum class SubscriptionSortings(var code: String) {
    NAME("N"),
    PRICE("P"),
    DATE("D");

    companion object {
        val valuesByCode: HashMap<String, SubscriptionSortings> = HashMap(values().size)

        init {
            values().forEach {
                valuesByCode[it.code] = it
            }
        }

        fun lookupByCode(code: String) : SubscriptionSortings? {
            return valuesByCode[code]
        }
    }


}