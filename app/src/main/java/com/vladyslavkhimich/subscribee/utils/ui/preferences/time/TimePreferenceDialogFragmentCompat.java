package com.vladyslavkhimich.subscribee.utils.ui.preferences.time;

import android.os.Bundle;
import android.view.View;
import android.widget.TimePicker;

import androidx.preference.DialogPreference;
import androidx.preference.PreferenceDialogFragmentCompat;

import com.vladyslavkhimich.subscribee.R;

import android.text.format.DateFormat;

public class TimePreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat {

    TimePicker timePicker;

    public static TimePreferenceDialogFragmentCompat newInstance(String key) {
        final TimePreferenceDialogFragmentCompat fragmentCompat = new TimePreferenceDialogFragmentCompat();
        final Bundle bundle = new Bundle(1);
        bundle.putString(ARG_KEY, key);
        fragmentCompat.setArguments(bundle);

        return fragmentCompat;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        timePicker = view.findViewById(R.id.dialogTimePicker);

        Integer minutesAfterMidnight = null;
        DialogPreference preference = getPreference();
        if (preference instanceof TimePreference) {
            minutesAfterMidnight = ((TimePreference) preference).getTime();
        }

        if (minutesAfterMidnight != null) {
            int hours = minutesAfterMidnight / 60;
            int minutes = minutesAfterMidnight % 60;
            boolean is24HourFormat = DateFormat.is24HourFormat(getContext());

            timePicker.setIs24HourView(is24HourFormat);
            timePicker.setCurrentHour(hours);
            timePicker.setCurrentMinute(minutes);
        }
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            int hours = timePicker.getCurrentHour();
            int minutes = timePicker.getCurrentMinute();
            int minutesAfterMidnight = (hours * 60) + minutes;

            DialogPreference dialogPreference = getPreference();
            if (dialogPreference instanceof TimePreference) {
                TimePreference timePreference = ((TimePreference) dialogPreference);

                if (timePreference.callChangeListener(minutesAfterMidnight)) {
                    timePreference.setTime(minutesAfterMidnight);
                }
            }
        }
    }


}
