package com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.familymember

import android.annotation.SuppressLint
import android.content.Context
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.Cycle
import com.vladyslavkhimich.subscribee.database.entities.Member
import com.vladyslavkhimich.subscribee.models.MemberWithDataForAdapter
import com.vladyslavkhimich.subscribee.models.Payment
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnFamilyPaymentClickListenerCallback
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback
import java.util.*
import kotlin.collections.ArrayList

class FamilyMembersAdapter(val context: Context,
                           val subscriptionID: Int,
                           val onEditMenuItemClickListenerCallback: OnItemClickListenerCallback,
                           val onDeleteMenuItemClickListenerCallback: OnItemClickListenerCallback,
                           private val onMemberPaymentClickListenerCallback: OnFamilyPaymentClickListenerCallback,
                           ) : RecyclerView.Adapter<FamilyMembersAdapter.FamilyMemberViewHolder>() {

    companion object {
        const val MANAGER_MEMBER_NAME = "Manager"
    }

    var membersWithData: List<MemberWithDataForAdapter> = ArrayList()
    var cycle: Cycle? = null
    var firstPaymentDate: Date? = null
    lateinit var innerAdapters: Array<FamilyMemberPaymentsAdapter?>

    @SuppressLint("NotifyDataSetChanged")
    fun setMembersWithDataAndNotify(membersWithDataFromDB: ArrayList<MemberWithDataForAdapter>, isNotify: Boolean) {
        cycle = DatabaseRepository.getInstance(context).getCycleBySubscriptionID(subscriptionID)
        if (membersWithDataFromDB[0].operationsAndPaymentDates.size == 0)
            firstPaymentDate = DatabaseRepository.getInstance(context).getNextPaymentDateBySubscriptionID(subscriptionID)
        else {
            val lastNotPaidPaymentDate = membersWithDataFromDB[0].operationsAndPaymentDates.lastOrNull {it.operation == null}?.paymentDate?.paymentDate
            val calendar = Calendar.getInstance()
            if (lastNotPaidPaymentDate != null) {
                calendar.time = lastNotPaidPaymentDate
                DateHelper.addCyclePeriodToCalendar(calendar, cycle)
                firstPaymentDate = calendar.time
            }
            else {
                calendar.time = membersWithDataFromDB[0].operationsAndPaymentDates.last {it.operation != null}.paymentDate.paymentDate
                DateHelper.addCyclePeriodToCalendar(calendar, cycle)
                firstPaymentDate = calendar.time
            }
        }

        val adapterMembers = ArrayList<Member?>()
        membersWithData.forEach {
            item ->
            adapterMembers.add(item.member)
        }
        val membersFromDB = ArrayList<Member?>()
        membersWithDataFromDB.forEach {
            item ->
            membersFromDB.add(item.member)
        }

        when {
            isNotify || (adapterMembers != membersFromDB) -> {
                innerAdapters = arrayOfNulls<FamilyMemberPaymentsAdapter?>(membersWithDataFromDB.size)
                membersWithData = ArrayList()
                membersWithData = membersWithDataFromDB.map { it.copy() }
                notifyDataSetChanged()
            }
            else -> {
                for (i in innerAdapters.indices) {
                    innerAdapters[i]!!.mapAndSetMemberPayments(membersWithDataFromDB[i].operationsAndPaymentDates, firstPaymentDate)
                }
            }
        }
    }

    fun getNextPaymentDateForSubscription(): Payment {
        var lastCheckedIndex = -1

        val managerPayments = innerAdapters[0]!!.memberPayments

        for (i in managerPayments.indices) {
            if (managerPayments[i].isChecked)
                lastCheckedIndex = i
        }

        return when {
            lastCheckedIndex == -1 -> managerPayments[0]
            lastCheckedIndex < managerPayments.size - 1 -> managerPayments[lastCheckedIndex + 1]
            else -> managerPayments[managerPayments.size - 1]
        }
    }

    fun getMinimumNextPaymentDateForDialog(): Date {
        var lastCheckedIndex = -1

        val managerPayments = innerAdapters[0]!!.memberPayments

        for (i in managerPayments.indices) {
            if (managerPayments[i].isChecked)
                lastCheckedIndex = i
        }

        return when (lastCheckedIndex) {
            -1 -> DateHelper.addOneDayToDate(Date())
            0 -> DateHelper.addOneDayToDate(Date())
            else -> DateHelper.addOneDayToDate(managerPayments[lastCheckedIndex].paymentDate)
        }
    }

    fun checkIfPositionBeforeLastCheckedPosition(position: Int) : Boolean {
        var lastCheckedIndex = 0

        val managerPayments = innerAdapters[0]!!.memberPayments

        for (i in managerPayments.indices) {
            if (managerPayments[i].isChecked)
                lastCheckedIndex = i
        }

        return position < lastCheckedIndex
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FamilyMemberViewHolder {
        val familyMemberView = LayoutInflater.from(parent.context).inflate(R.layout.item_family_member, parent, false)
        return FamilyMemberViewHolder(familyMemberView)
    }

    override fun onBindViewHolder(holder: FamilyMemberViewHolder, position: Int) {
        val memberWithData = membersWithData[position]
        if (memberWithData.member?.name == MANAGER_MEMBER_NAME) {
            holder.nameTextView.setText(R.string.you)
            holder.roleTextView.setText(R.string.manager)
            holder.actionsImageView.visibility = View.GONE
        } else {
            holder.nameTextView.text = memberWithData.member?.name
            holder.roleTextView.setText(R.string.role_member)
        }
        holder.nameTextView.isSelected = true
        val innerAdapter = FamilyMemberPaymentsAdapter(onMemberPaymentClickListenerCallback, position, cycle)
        innerAdapters[position] = innerAdapter
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        holder.paymentsRecyclerView.layoutManager = layoutManager
        holder.paymentsRecyclerView.adapter = innerAdapter
        holder.paymentsRecyclerView.setHasFixedSize(true)
        holder.noPaymentsTextView.visibility = View.GONE
        innerAdapter.mapAndSetMemberPayments(memberWithData.operationsAndPaymentDates, firstPaymentDate)
    }

    /*override fun onBindViewHolder(holder: FamilyMemberViewHolder, position: Int, payloads: MutableList<Any>) {
        Log.i("CALLED", "Family member onBind payloads called")
        val memberWithData = membersWithData[position]
        if (memberWithData.member?.name == "Manager") {
            holder.nameTextView.setText(R.string.you)
            holder.roleTextView.setText(R.string.manager)
            holder.actionsImageView.visibility = View.GONE
        } else {
            holder.nameTextView.text = memberWithData.member?.name
            holder.roleTextView.setText(R.string.role_member)
        }
        val innerAdapter = FamilyMemberPaymentsAdapter(onMemberPaymentClickListenerCallback, position, cycle)
        innerAdapters[position] = innerAdapter
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        holder.paymentsRecyclerView.layoutManager = layoutManager
        holder.paymentsRecyclerView.adapter = innerAdapter
        holder.paymentsRecyclerView.setHasFixedSize(true)
        innerAdapter.mapAndSetMemberPayments(memberWithData.operationsAndPaymentDates, firstPaymentDate)
    }*/

    override fun getItemCount(): Int {
        return membersWithData.size
    }

    @SuppressLint("RestrictedApi")
    inner class FamilyMemberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val actionsImageView: ImageView = itemView.findViewById(R.id.familyMemberActionsImageView)
        val nameTextView: TextView = itemView.findViewById(R.id.familyMemberNameTextView)
        val roleTextView: TextView = itemView.findViewById(R.id.familyMemberRoleTextView)
        val noPaymentsTextView: TextView = itemView.findViewById(R.id.familyMemberNoPaymentsTextView)
        val paymentsRecyclerView: RecyclerViewEmptySupport = itemView.findViewById(R.id.familyMemberPaymentsRecyclerView)

        init {

            actionsImageView.setOnClickListener { view ->
                val menuBuilder = MenuBuilder(context)
                val menuInflater = MenuInflater(context)
                menuInflater.inflate(R.menu.menu_family_member, menuBuilder)
                val popupMenu = MenuPopupHelper(context, menuBuilder, view)
                popupMenu.setForceShowIcon(true)
                menuBuilder.setCallback(object : MenuBuilder.Callback {
                    override fun onMenuItemSelected(menu: MenuBuilder, item: MenuItem): Boolean {
                        return when(item.itemId) {
                            R.id.family_member_edit -> {
                                onEditMenuItemClickListenerCallback.onItemClicked(view, adapterPosition)
                                true
                            }
                            R.id.family_member_delete -> {
                                onDeleteMenuItemClickListenerCallback.onItemClicked(view, adapterPosition)
                                true
                            }
                            else -> false
                        }
                    }

                    override fun onMenuModeChange(menu: MenuBuilder) {

                    }
                })
                popupMenu.show()
                /*val popupMenu = PopupMenu(context, actionsImageView)
                popupMenu.inflate(R.menu.menu_family_member)
                popupMenu.setOnMenuItemClickListener {
                    when(it.itemId) {
                        R.id.family_member_edit -> {
                            onEditMenuItemClickListenerCallback.onItemClicked(view, adapterPosition)
                            true
                        }
                        R.id.family_member_delete -> {
                            onDeleteMenuItemClickListenerCallback.onItemClicked(view, adapterPosition)
                            true
                        }
                        else -> false
                    }
                }*/

                /*popupMenu.show()*/
            }
        }

    }

}