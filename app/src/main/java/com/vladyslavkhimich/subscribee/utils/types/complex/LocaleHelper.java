package com.vladyslavkhimich.subscribee.utils.types.complex;

public class LocaleHelper {
    public static String getLanguageISO(String userSavedLocale) {
        switch(userSavedLocale) {
            case "russian":
                return "ru";
            case "ukrainian":
                return "uk";
            default:
                return "en";
        }
    }

}
