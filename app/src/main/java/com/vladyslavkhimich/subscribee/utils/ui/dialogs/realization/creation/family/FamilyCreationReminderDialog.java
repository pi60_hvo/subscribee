package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family;

import android.content.Context;

import com.vladyslavkhimich.subscribee.models.Reminder;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.family.FamilyCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.general.CreationViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.ReminderDialog;

public class FamilyCreationReminderDialog extends ReminderDialog {
    SubscriptionViewModel familyCreationViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        familyCreationViewModel = ((FamilyCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setSelectedReminder(Reminder selectedReminder) {
        ((CreationViewModel)familyCreationViewModel).setSelectedReminder(selectedReminder);
    }
}
