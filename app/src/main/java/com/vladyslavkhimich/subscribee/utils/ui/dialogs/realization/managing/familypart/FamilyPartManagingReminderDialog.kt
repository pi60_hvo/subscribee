package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.familypart

import android.content.Context
import com.vladyslavkhimich.subscribee.models.Reminder
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart.FamilyPartManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.ReminderDialog

class FamilyPartManagingReminderDialog : ReminderDialog() {
    private lateinit var familyPartManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyPartManagingViewModel = (context as FamilyPartManagingActivity).subscriptionViewModel
    }

    override fun setSelectedReminder(selectedReminder: Reminder?) {
        familyPartManagingViewModel.setSelectedReminder(selectedReminder)
    }
}