package com.vladyslavkhimich.subscribee.utils.mappers

import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithAssociatedData
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats
import com.vladyslavkhimich.subscribee.models.OperationModel
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper
import com.vladyslavkhimich.subscribee.utils.ui.adapters.items.OperationDateItem
import com.vladyslavkhimich.subscribee.utils.ui.adapters.items.OperationItem
import com.vladyslavkhimich.subscribee.utils.ui.adapters.items.OperationListItem
import com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.familymember.FamilyMembersAdapter.Companion.MANAGER_MEMBER_NAME
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 24.11.2021
 * @email vladislav.himich@4k.com.ua
 */

fun TreeMap<Date, ArrayList<OperationModel>>.mapFromDB(operationsFromDB: List<OperationWithAssociatedData>) {
    operationsFromDB.forEach {
        operationFromDB ->
        if (operationFromDB.operation != null) {
            val dateCalendar = Calendar.getInstance()
            dateCalendar.time = operationFromDB.operation!!.operationDate
            DateHelper.getOnlyDateCalendar(dateCalendar)
            val hashMapKey = dateCalendar.time
            val operationModel = OperationModel(
                operationFromDB.subscription.name,
                operationFromDB.currency.symbol,
                operationFromDB.operation!!.price,
                operationFromDB.subscription.iconColor,
                operationFromDB.subscription.icon,
                if (operationFromDB.member != null) operationFromDB.member!!.name else null
            )
            if (this.containsKey(hashMapKey)) {
                this[hashMapKey]?.add(operationModel)
            }
            else {
                val operationsList = ArrayList<OperationModel>()
                operationsList.add(operationModel)
                this[hashMapKey] = operationsList
            }
        }
    }
}

fun ArrayList<OperationListItem>.mapFromTreeMap(operationsTreeMap: TreeMap<Date, ArrayList<OperationModel>>) {
    for (date in operationsTreeMap.keys) {
        val dateItem = OperationDateItem()
        dateItem.date = date
        this.add(dateItem)
        if (operationsTreeMap[date] != null) {
            for (operation in operationsTreeMap[date]!!) {
                val operationModel = OperationItem()
                operationModel.operation = operation
                this.add(operationModel)
            }
        }
    }
}

fun LinkedList<OperationWithDataForStats>.mapPersonalSubscriptionsData(operationsWithDataForStats: List<OperationWithDataForStats>) {
    val filteredSubscriptions = operationsWithDataForStats.filter { !it.subscriptionWithCategory.subscription.isFamily && !it.subscriptionWithCategory.subscription.isFamilyPart }
    filteredSubscriptions.forEach {
        this.add(it)
    }
}

fun LinkedList<OperationWithDataForStats>.mapFamilySubscriptionsData(operationsWithDataForStats: List<OperationWithDataForStats>) {
    val filteredSubscriptions = operationsWithDataForStats.filter { it.subscriptionWithCategory.subscription.isFamily || it.subscriptionWithCategory.subscription.isFamilyPart }
    filteredSubscriptions.forEach {
        this.add(it)
    }
}

fun HashMap<Int, List<OperationWithDataForStats>>.mapSubscriptionsWithOperationsDistinct(operationsWithDataForStats: List<OperationWithDataForStats>) {
    val distinctIdsSet = HashSet<Int>()
    operationsWithDataForStats.forEach {
        distinctIdsSet.add(it.subscriptionWithCategory.subscription.subscriptionID)
    }
    distinctIdsSet.forEach { subscriptionId ->
        val filteredOperationsWithData = operationsWithDataForStats.filter { it.subscriptionWithCategory.subscription.subscriptionID == subscriptionId }
        this[subscriptionId] = filteredOperationsWithData
    }
}

fun HashMap<Int, List<OperationWithDataForStats>>.mapFamilySubscriptionsWithOperationsDistinct(operationsWithDataForStats: List<OperationWithDataForStats>, isSpent: Boolean) {
    val distinctIdsSet = HashSet<Int>()
    operationsWithDataForStats.forEach {
        distinctIdsSet.add(it.subscriptionWithCategory.subscription.subscriptionID)
    }
    distinctIdsSet.forEach { subscriptionId ->
        var filteredOperationsWithData = operationsWithDataForStats.filter { it.subscriptionWithCategory.subscription.subscriptionID == subscriptionId }
        filteredOperationsWithData = filteredOperationsWithData.filter { it.operation.isOwner == isSpent }
        this[subscriptionId] = filteredOperationsWithData
    }
}

fun HashMap<Int, List<OperationWithDataForStats>>.mapCategoriesWithOperationsDistinct(operationsWithDataForStats: List<OperationWithDataForStats>) {
    val distinctIdsSet = HashSet<Int>()
    operationsWithDataForStats.forEach {
        distinctIdsSet.add(it.subscriptionWithCategory.subscription.categoryID)
    }
    distinctIdsSet.forEach { categoryID ->
        var filteredOperationsWithData = operationsWithDataForStats.filter { it.subscriptionWithCategory.subscription.categoryID == categoryID }
        filteredOperationsWithData = filteredOperationsWithData.filter { it.operation.isOwner }
        this[categoryID] = filteredOperationsWithData
    }
}

fun HashMap<Int, List<OperationWithDataForStats>>.mapMembersWithOperationsDistinct(operationsWithDataForStats: List<OperationWithDataForStats>) {
    val distinctIdsSet = HashSet<Int>()
    operationsWithDataForStats.forEach {
        if (!it.operation.isOwner)
            distinctIdsSet.add(it.operation.memberID)
    }
    distinctIdsSet.forEach { memberId ->
        val filteredOperationsWithData = operationsWithDataForStats.filter { it.operation.memberID == memberId }
        this[memberId] = filteredOperationsWithData
    }
}