package com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.reminder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback;
import com.vladyslavkhimich.subscribee.models.Reminder;

import java.util.ArrayList;

public class RemindersAdapter extends RecyclerView.Adapter<RemindersAdapter.ReminderViewHolder> {
    ArrayList<Reminder> reminders = new ArrayList<>();
    OnItemClickListenerCallback onItemClickListenerCallback;

    public RemindersAdapter(OnItemClickListenerCallback onItemClickListenerCallback) {
        setOnItemClickListenerCallback(onItemClickListenerCallback);
    }

    public void setReminders(ArrayList<Reminder> reminders) {
        this.reminders = reminders;
        notifyDataSetChanged();
    }

    public ArrayList<Reminder> getReminders() {
        return reminders;
    }

    public void setOnItemClickListenerCallback(OnItemClickListenerCallback onItemClickListenerCallback) {
        this.onItemClickListenerCallback = onItemClickListenerCallback;
    }

    @NonNull
    @Override
    public ReminderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View reminderView = inflater.inflate(R.layout.item_dialog_reminder, parent, false);
        return new ReminderViewHolder(reminderView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReminderViewHolder holder, int position) {
        Reminder reminder = reminders.get(position);
        holder.reminderTextView.setText(ResourcesHelper.getStringIDFromName(holder.reminderTextView.getContext(), reminder.getStringIDName()));
    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    public class ReminderViewHolder extends RecyclerView.ViewHolder {
        TextView reminderTextView;

        public ReminderViewHolder(@NonNull View itemView) {
            super(itemView);
            reminderTextView = (TextView) itemView.findViewById(R.id.reminderTextView);
            itemView.setOnClickListener(v -> onItemClickListenerCallback.onItemClicked(v, getAdapterPosition()));
        }
    }
}
