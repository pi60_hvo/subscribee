package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.family

import android.content.Context
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.Cycle
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.family.FamilyManagingActivity
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingViewModel
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.CycleManagingDialog
import java.util.ArrayList

class FamilyManagingCycleDialog : CycleManagingDialog() {
    private lateinit var context: Context
    private lateinit var familyManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        if (context != null) {
            this.context = context
        }
        familyManagingViewModel = (context as FamilyManagingActivity).subscriptionViewModel
    }

    override fun setAdapterCycles(cycles: MutableList<Cycle>?) {
        cycleAdapter.cycles = cycles as ArrayList<Cycle>
    }

    override fun setSelectedCycle(cycle: Cycle?) {
        familyManagingViewModel.setSelectedCycle(cycle)
    }

    override fun getSubscriptionOperationsCount(): Int {
        return DatabaseRepository.getInstance(context).getSubscriptionOperationsCountByID((context as FamilyManagingActivity).subscriptionID)
    }

    override fun setChangeAllViewModelValue(isChangeAll: Boolean?) {
        (familyManagingViewModel as ManagingViewModel).setIsCycleChangeAll(isChangeAll)
    }
}