package com.vladyslavkhimich.subscribee.utils.types.complex;

import android.app.Activity;
import android.graphics.Point;

public class UIHelper {
    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    public static int getScreenHeight(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.y;
    }
}
