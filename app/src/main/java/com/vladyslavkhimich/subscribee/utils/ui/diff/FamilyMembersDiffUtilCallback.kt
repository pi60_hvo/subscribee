package com.vladyslavkhimich.subscribee.utils.ui.diff

import androidx.recyclerview.widget.DiffUtil
import com.vladyslavkhimich.subscribee.models.MemberWithDataForAdapter

class FamilyMembersDiffUtilCallback(private val oldList: List<MemberWithDataForAdapter>, private val newList: List<MemberWithDataForAdapter>) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldMember = oldList[oldItemPosition]
        val newMember = newList[newItemPosition]
        return oldMember.member!!.memberID == newMember.member!!.memberID
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldMember = oldList[oldItemPosition]
        val newMember = newList[newItemPosition]
        return oldMember.member!!.name.equals(newMember.member!!.name) && oldMember.member.subscriptionShare.equals(newMember.member.subscriptionShare)
    }
}