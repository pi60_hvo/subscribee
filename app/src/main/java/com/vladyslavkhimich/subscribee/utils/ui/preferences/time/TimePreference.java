package com.vladyslavkhimich.subscribee.utils.ui.preferences.time;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.preference.DialogPreference;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.types.primitives.StringHelper;

public class TimePreference extends DialogPreference {
    int time;
    int dialogLayoutResID = R.layout.dialog_pref_time;

    public TimePreference(Context context) {
        this(context, null);
    }

    public TimePreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimePreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, defStyleAttr);
    }

    public TimePreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
        persistInt(time);
        setSummary(StringHelper.getStringFromMinutes(getContext(), time));
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, 0);
    }

    @Override
    protected void onSetInitialValue(@Nullable @org.jetbrains.annotations.Nullable Object defaultValue) {
        setTime(defaultValue == null ? getPersistedInt(time) : (int) defaultValue);
    }

    @Override
    public int getDialogLayoutResource() {
        return dialogLayoutResID;
    }


}
