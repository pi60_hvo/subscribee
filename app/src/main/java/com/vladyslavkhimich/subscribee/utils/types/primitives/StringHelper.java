package com.vladyslavkhimich.subscribee.utils.types.primitives;

import android.content.Context;
import android.text.format.DateFormat;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.models.Reminder;

public class StringHelper {
    public static String getStringFromCycle(Context context, Cycle cycle) {
        String result = "";
        if (cycle.daysCount != 0) {
            result = context.getResources().getString(R.string.cycle_days, cycle.daysCount);
        }
        else if (cycle.weeksCount != 0) {
            result = context.getResources().getString(R.string.cycle_weeks, cycle.weeksCount);
        }
        else if (cycle.monthsCount != 0) {
            result = context.getResources().getString(R.string.cycle_months, cycle.monthsCount);
        }
        else if (cycle.yearsCount != 0) {
            result = context.getResources().getString(R.string.cycle_years, cycle.yearsCount);
        }
        return result;
    }

    public static String getStringFromReminder(Context context, Reminder reminder) {
        String resultString = "";
        if (reminder.getIsNever()) {
            resultString = context.getString(R.string.never_reminder);
        }
        else if (reminder.getIsSameDay()) {
            resultString = context.getString(R.string.same_day_reminder);
        }
        else if (reminder.getDaysCount() != 0) {
            resultString = context.getResources().getQuantityString(R.plurals.plurals_days_before, reminder.getDaysCount(), reminder.getDaysCount());
        }
        else if (reminder.getWeeksCount() != 0) {
            resultString = context.getResources().getQuantityString(R.plurals.plurals_weeks_before, reminder.getWeeksCount(), reminder.getWeeksCount());
        }
        else if (reminder.getMonthsCount() != 0) {
            resultString = context.getResources().getQuantityString(R.plurals.plurals_months_before, reminder.getMonthsCount(), reminder.getMonthsCount());
        }
        else if (reminder.getYearsCount() != 0) {
            resultString =  context.getResources().getQuantityString(R.plurals.plurals_years_before, reminder.getYearsCount(), reminder.getYearsCount());
        }
        return resultString;
    }

    public static String getStringFromFreePeriod(Context context, FreePeriod freePeriod) {
        String resultString = "";
        if (freePeriod.getIsToNextPayment()) {
            resultString = context.getString(R.string.to_next_payment_free_period);
        }
        else if (freePeriod.getStartDate() != null) {
            resultString = context.getString(R.string.text_until_date, java.text.DateFormat.getDateInstance().format(DateHelper.getFreePeriodEndingDate(freePeriod)));
        }
        /*else if (freePeriod.getDaysCount() != 0) {
            resultString = context.getResources().getQuantityString(R.plurals.plurals_days, freePeriod.getDaysCount(), freePeriod.getDaysCount());
        }
        else if (freePeriod.getWeeksCount() != 0) {
            resultString = context.getResources().getQuantityString(R.plurals.plurals_weeks, freePeriod.getWeeksCount(), freePeriod.getWeeksCount());
        }
        else if (freePeriod.getMonthsCount() != 0) {
            resultString = context.getResources().getQuantityString(R.plurals.plurals_months, freePeriod.getMonthsCount(), freePeriod.getMonthsCount());
        }
        else if (freePeriod.getYearsCount() != 0) {
            resultString = context.getResources().getQuantityString(R.plurals.plurals_years, freePeriod.getYearsCount(), freePeriod.getYearsCount());
        }*/
        return resultString;
    }

    public static String getStringFromMinutes(Context context, int minutes) {
        if (DateFormat.is24HourFormat(context)) {
            return getStringHoursFromMinutes(minutes) + ":" + getStringMinutesReminderFromMinutes(minutes);
        }
        int hourOfDay = minutes / 60;
        String minutesRemainder = getStringMinutesReminderFromMinutes(minutes);
        if (hourOfDay > 12) {
            return getStringHoursFromMinutes(minutes - 12 * 60) + ":" + minutesRemainder + " PM";
        }
        else if (hourOfDay == 12) {
            return "12:" + minutesRemainder + " PM";
        }
        if (hourOfDay == 0) {
            return "12:" + minutesRemainder + " AM";
        }
        return  getStringHoursFromMinutes(minutes) + ":" + minutesRemainder + " AM";
    }

    public static String getStringMinutesReminderFromMinutes(int minutes) {
        String minutesString;
        int minutesRemainder = minutes % 60;
        if (minutesRemainder < 10) {
            minutesString = "0" + ((Integer)minutesRemainder).toString();
        }
        else {
            minutesString = ((Integer)minutesRemainder).toString();
        }
        return minutesString;
    }

    public static String getStringHoursFromMinutes(int minutes) {
        String hoursString;
        int hours = minutes / 60;
        if (hours < 10) {
            hoursString = "0" + ((Integer)hours).toString();
        }
        else {
            hoursString = ((Integer)hours).toString();
        }
        return hoursString;
    }
}
