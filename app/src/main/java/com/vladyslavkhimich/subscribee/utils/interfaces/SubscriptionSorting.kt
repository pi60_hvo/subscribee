package com.vladyslavkhimich.subscribee.utils.interfaces

import com.vladyslavkhimich.subscribee.database.entities.Subscription

interface SubscriptionSorting {
    fun sort(subscriptions: List<Subscription>) : List<Subscription>
}