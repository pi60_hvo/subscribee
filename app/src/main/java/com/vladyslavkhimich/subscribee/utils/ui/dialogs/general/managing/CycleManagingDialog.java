package com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.cycle.CycleAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs.managing.ManagingPeriodPickerCycleDialog;

import java.util.List;

public abstract class CycleManagingDialog {
    ManagingPeriodPickerCycleDialog childDialog;
    AlertDialog cycleAlertDialog;
    AlertDialog cycleChangeAlertDialog;
    CycleAdapter cycleAdapter;
    RadioButton changeAllRadioButton;
    Boolean isChangeAll;
    boolean isAdapterCycleClicked = false;
    int cycleSelectedPosition;

    public void initialize(Context context) {
        setAssociatedActivityViewModel(context);
        AlertDialog.Builder cycleAlertDialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_cycle_custom, null);
        cycleAlertDialogBuilder.setView(dialogView);
        cycleAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {

        });
        cycleAlertDialogBuilder.setNeutralButton(R.string.custom_reminder, (dialog, which) -> {
            //childDialog.getPeriodPickerAlertDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            childDialog.getPeriodPickerAlertDialog().show();
            childDialog.getNumberEditText().requestFocus();
        });
        RecyclerView cycleRecyclerView = dialogView.findViewById(R.id.dialogCycleRecyclerView);
        cycleAdapter = new CycleAdapter((view, position) -> {
            cycleSelectedPosition = position;
            isAdapterCycleClicked = true;
            if (getSubscriptionOperationsCount() > 0) {
                setChangeAll(changeAllRadioButton.isChecked());
                cycleChangeAlertDialog.show();
            }
            else {
                setChangeAll(null);
                handleSelectedCycle();
                cycleAlertDialog.dismiss();
            }
        });
        cycleRecyclerView.setAdapter(cycleAdapter);
        cycleRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        cycleRecyclerView.setHasFixedSize(true);
        cycleAlertDialog = cycleAlertDialogBuilder.create();
        initializeChildAlertDialog(context);
        initializeCycleChangeAlertDialog(context);
    }

    public void initializeChildAlertDialog(Context context) {
        childDialog = new ManagingPeriodPickerCycleDialog();
        childDialog.setParentDialog(this);
        childDialog.initialize(context);
    }

    public void initializeCycleChangeAlertDialog(Context context) {
        AlertDialog.Builder cycleChangeAlertDialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_cycle_change, null);
        cycleChangeAlertDialogBuilder.setView(dialogView);

        RadioGroup cycleChangeRadioGroup = dialogView.findViewById(R.id.dialogCycleChangeRadioGroup);

        cycleChangeRadioGroup.setOnCheckedChangeListener((group, checkedId) -> setChangeAll(checkedId == R.id.dialogCycleChangeAllPaymentsRadioButton));

        changeAllRadioButton = dialogView.findViewById(R.id.dialogCycleChangeAllPaymentsRadioButton);

        cycleChangeAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {
            cycleChangeAlertDialog.dismiss();
            setChangeAll(null);
        });
        cycleChangeAlertDialogBuilder.setPositiveButton("OK", (dialog, which) -> {
            handleSelectedCycle();
            cycleAlertDialog.dismiss();
            childDialog.getPeriodPickerAlertDialog().dismiss();
        });
        cycleChangeAlertDialogBuilder.setTitle(R.string.dialog_title_cycle_change);

        cycleChangeAlertDialog = cycleChangeAlertDialogBuilder.create();
    }

    public void handleSelectedCycle() {
        if (isAdapterCycleClicked)
            setSelectedCycle(cycleAdapter.getCycles().get(cycleSelectedPosition));
        else {
            Cycle resultCycle = new Cycle();
            switch(childDialog.periodSpinner.getSelectedItemPosition()) {
                case 0:
                    resultCycle.daysCount = childDialog.periodNumber;
                    break;
                case 1:
                    resultCycle.weeksCount = childDialog.periodNumber;
                    break;
                case 2:
                    resultCycle.monthsCount = childDialog.periodNumber;
                    break;
                case 3:
                    resultCycle.yearsCount = childDialog.periodNumber;
                    break;
            }
            this.setSelectedCycle(resultCycle);
        }
    }

    public AlertDialog getCycleAlertDialog() {
        return cycleAlertDialog;
    }

    public AlertDialog getCycleChangeAlertDialog() {
        return cycleChangeAlertDialog;
    }

    public CycleAdapter getCycleAdapter() {
        return cycleAdapter;
    }

    public void setChangeAll(Boolean changeAll) {
        isChangeAll = changeAll;
        setChangeAllViewModelValue(changeAll);
    }

    public Boolean getChangeAll() {
        return isChangeAll;
    }

    public void setAdapterCycleClicked(boolean adapterCycleClicked) {
        isAdapterCycleClicked = adapterCycleClicked;
    }

    public abstract void setAssociatedActivityViewModel(Context context);

    public abstract void setAdapterCycles(List<Cycle> cycles);

    public abstract void setSelectedCycle(Cycle cycle);

    public abstract int getSubscriptionOperationsCount();

    public abstract void setChangeAllViewModelValue(Boolean isChangeAll);
}
