package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.family.FamilyCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CategoryDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FamilyCreationCategoryDialog extends CategoryDialog {
    SubscriptionViewModel familyCreationViewModel;
    int categoriesListItemsCount;
    int categoriesSetCount;
    @Override
    public void setAssociatedActivityViewModel(Context context) {
        familyCreationViewModel = ((FamilyCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCategories(List<Category> categories) {
        categoriesSetCount++;
        getCategoryAdapter().setCategories((ArrayList<Category>) categories);
        if (categoriesSetCount > 1 && categories.size() > categoriesListItemsCount)
            scrollRecyclerViewToBottom();
        categoriesListItemsCount = categories.size();
    }

    @Override
    public void setSelectedCategory(Category category) {
        familyCreationViewModel.setSelectedCategory(category);
    }

    @Override
    public Category getSelectedCategory() {
        return familyCreationViewModel.getSelectedCategoryObject();
    }

    @Override
    public void resetSelectedCategoryIfDeleted(Category category) {
        if (Objects.equals(familyCreationViewModel.getSelectedCategory().getValue(), category))
            familyCreationViewModel.setSelectedCategory(null);
    }
}
