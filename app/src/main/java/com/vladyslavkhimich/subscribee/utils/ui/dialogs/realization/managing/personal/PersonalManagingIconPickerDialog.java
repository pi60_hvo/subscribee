package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal;

import android.content.Context;

import androidx.annotation.NonNull;

import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.personal.PersonalManagingActivity;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.IconPickerDialog;

public class PersonalManagingIconPickerDialog extends IconPickerDialog {
    SubscriptionViewModel personalManagingViewModel;

    public PersonalManagingIconPickerDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalManagingViewModel = ((PersonalManagingActivity)context).subscriptionViewModel;
    }

    @Override
    public void setActivitySelectedIcon(int iconId) {
        personalManagingViewModel.setSelectedIconID(iconId);
    }

    @Override
    public void setActivitySelectedColor(int color) {
        personalManagingViewModel.setSelectedIconColor(color);
    }

    @Override
    public int getActivitySelectedIcon() {
        return personalManagingViewModel.getSelectedIconIDValue();
    }

    @Override
    public int getActivitySelectedColor() {
        return personalManagingViewModel.getSelectedColorIDValue();
    }
}
