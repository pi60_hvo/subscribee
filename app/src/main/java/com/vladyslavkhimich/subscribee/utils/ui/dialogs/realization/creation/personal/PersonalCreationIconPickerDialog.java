package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal;

import android.content.Context;

import androidx.annotation.NonNull;

import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.IconPickerDialog;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.personal.PersonalCreationActivity;

public class PersonalCreationIconPickerDialog extends IconPickerDialog {
    SubscriptionViewModel personalCreationViewModel;

    public PersonalCreationIconPickerDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalCreationViewModel = ((PersonalCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setActivitySelectedIcon(int iconId) {
        personalCreationViewModel.setSelectedIconID(iconId);
    }

    @Override
    public void setActivitySelectedColor(int color) {
        personalCreationViewModel.setSelectedIconColor(color);
    }

    @Override
    public int getActivitySelectedIcon() {
        return personalCreationViewModel.getSelectedIconIDValue();
    }

    @Override
    public int getActivitySelectedColor() {
        return personalCreationViewModel.getSelectedColorIDValue();
    }
}
