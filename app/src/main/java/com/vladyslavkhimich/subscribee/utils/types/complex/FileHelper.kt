package com.vladyslavkhimich.subscribee.utils.types.complex

import android.content.Context
import android.util.Log
import java.lang.Exception
import java.nio.charset.StandardCharsets

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 24.11.2021
 * @email vladislav.himich@4k.com.ua
 */
object FileHelper {
    fun readJsonAndConvertToString(context: Context, fileName: String) : String? {
        var jsonString: String? = null
        try {
                val inputStream = context.assets.open(fileName)
                val size = inputStream.available()
                val byteArray = ByteArray(size)
                inputStream.read(byteArray)
                inputStream.close()
                jsonString = String(byteArray, StandardCharsets.UTF_8)
        } catch (exception: Exception) {
            Log.e("Exception", "Exception caught: " + exception.message)
        }
        return jsonString
    }
}