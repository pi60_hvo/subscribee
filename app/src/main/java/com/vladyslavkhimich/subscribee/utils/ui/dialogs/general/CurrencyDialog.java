package com.vladyslavkhimich.subscribee.utils.ui.dialogs.general;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.currency.CurrencyAdapter;

import java.util.List;
import java.util.Objects;

public abstract class CurrencyDialog {
    AlertDialog currencyAlertDialog;
    CurrencyAdapter currencyAdapter;
    RecyclerViewEmptySupport currencyRecyclerView;
    RecyclerView.SmoothScroller smoothScroller;

    public void initialize(Context context) {
        setAssociatedActivityViewModel(context);
        AlertDialog.Builder currencyAlertDialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_currency_custom, null);
        currencyAlertDialogBuilder.setView(dialogView);
        currencyAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {

        });
        currencyAlertDialogBuilder.setPositiveButton("OK", (dialog, which) -> setSelectedCurrency(currencyAdapter.getSelectedCurrency()));
        EditText searchEditText = dialogView.findViewById(R.id.currencyDialogEditText);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2)
                    currencyAdapter.filterCurrencies(s.toString());
                else
                {
                    currencyAdapter.fallbackToOriginalList();
                    scrollToSelectedPosition();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        currencyRecyclerView = dialogView.findViewById(R.id.currencyDialogRecyclerView);
        currencyAdapter = new CurrencyAdapter(context);
        currencyRecyclerView.setAdapter(currencyAdapter);
        currencyRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        currencyRecyclerView.setEmptyView(dialogView.findViewById(R.id.notFoundContainerCurrencyDialog));
        smoothScroller = new LinearSmoothScroller(context) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
        currencyAlertDialog = currencyAlertDialogBuilder.create();
    }

    public void scrollToSelectedPosition() {
        if (currencyAdapter.getLastCheckedPosition() != -1) {
            if (currencyAdapter.getLastCheckedPosition() < 50) {
                smoothScroller.setTargetPosition(currencyAdapter.getLastCheckedPosition());
                Objects.requireNonNull(currencyRecyclerView.getLayoutManager()).startSmoothScroll(smoothScroller);
            } else
                ((LinearLayoutManager) Objects.requireNonNull(currencyRecyclerView.getLayoutManager())).scrollToPositionWithOffset(currencyAdapter.getLastCheckedPosition(), 0);
        }
    }

    public AlertDialog getCurrencyAlertDialog() {
        return currencyAlertDialog;
    }

    public CurrencyAdapter getCurrencyAdapter() {
        return currencyAdapter;
    }

    public abstract void setAssociatedActivityViewModel(Context context);
    public abstract void setAdapterCurrencies(List<Currency> currencies);
    public abstract void setSelectedCurrency(Currency currency);
}
