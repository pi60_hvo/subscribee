package com.vladyslavkhimich.subscribee.utils.types.complex;

import android.content.Context;

import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.models.Reminder;
import com.vladyslavkhimich.subscribee.models.dto.CategoryDto;
import com.vladyslavkhimich.subscribee.models.dto.CurrencyDto;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

public class GsonHelper {
    public static final String CATEGORIES_FILENAME = "categories.json";
    public static final String CURRENCIES_FILENAME = "currencies.json";

    public static String convertRemindersArrayListToJSON(ArrayList<Reminder> arrayList) {
        return new Gson().toJson(arrayList);
    }

    public static ArrayList<Reminder> convertRemindersJSONToArrayList(String remindersJSON) {
        return new ArrayList<>(Arrays.asList(new Gson().fromJson(remindersJSON, Reminder[].class)));
    }

    public static String convertFreePeriodsArrayListToJSON(ArrayList<FreePeriod> arrayList) {
        return new Gson().toJson(arrayList);
    }

    public static ArrayList<FreePeriod> convertFreePeriodsJSONToArrayList(String freePeriodsJSON) {
        return new ArrayList<>(Arrays.asList(new Gson().fromJson(freePeriodsJSON, FreePeriod[].class)));
    }

    public static String convertStringArrayToJSON(String[] stringArray) {
        return new Gson().toJson(stringArray);
    }

    public static String[] convertJSONToStringArray(String stringJSON) {
        return new Gson().fromJson(stringJSON, String[].class);
    }

    public static CategoryDto[] getCategoriesFromJSON(Context context) {
        String jsonString = FileHelper.INSTANCE.readJsonAndConvertToString(context, CATEGORIES_FILENAME);
        return new Gson().fromJson(jsonString, CategoryDto[].class);
    }

    public static CurrencyDto[] getCurrenciesFromJSON(Context context) {
        String jsonString = FileHelper.INSTANCE.readJsonAndConvertToString(context, CURRENCIES_FILENAME);
        return new Gson().fromJson(jsonString, CurrencyDto[].class);
    }
}
