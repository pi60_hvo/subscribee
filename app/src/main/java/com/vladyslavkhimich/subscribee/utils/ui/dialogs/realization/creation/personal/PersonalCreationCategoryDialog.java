package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.personal.PersonalCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CategoryDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PersonalCreationCategoryDialog extends CategoryDialog {
    SubscriptionViewModel personalCreationViewModel;
    int categoriesListItemsCount;
    int categoriesSetCount;
    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalCreationViewModel = ((PersonalCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCategories(List<Category> categories) {
        categoriesSetCount++;
        getCategoryAdapter().setCategories((ArrayList<Category>) categories);
        if (categoriesSetCount > 1 && categories.size() > categoriesListItemsCount)
            scrollRecyclerViewToBottom();
        categoriesListItemsCount = categories.size();
    }

    @Override
    public void setSelectedCategory(Category category) {
        personalCreationViewModel.setSelectedCategory(category);
    }

    @Override
    public Category getSelectedCategory() {
        return personalCreationViewModel.getSelectedCategoryObject();
    }

    @Override
    public void resetSelectedCategoryIfDeleted(Category category) {
        if (Objects.equals(personalCreationViewModel.getSelectedCategory().getValue(), category))
            personalCreationViewModel.setSelectedCategory(null);
    }
}
