package com.vladyslavkhimich.subscribee.utils.ui.adapters.items;

import com.vladyslavkhimich.subscribee.models.OperationModel;

public class OperationItem extends OperationListItem {
    private OperationModel operation;

    public OperationModel getOperation() {
        return operation;
    }

    public void setOperation(OperationModel operation) {
        this.operation = operation;
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }
}
