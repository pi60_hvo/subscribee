package com.vladyslavkhimich.subscribee.utils.ui.adapters.subscriptions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.family.FamilyManagingActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart.FamilyPartManagingActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingActivity;
import com.vladyslavkhimich.subscribee.utils.enums.SubscriptionSortings;
import com.vladyslavkhimich.subscribee.utils.interfaces.SubscriptionSorting;
import com.vladyslavkhimich.subscribee.utils.others.sorting.factory.SortingFactory;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ViewAnimator;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FamilySubscriptionsAdapter extends RecyclerView.Adapter<FamilySubscriptionsAdapter.FamilySubscriptionViewHolder> {

    ArrayList<Subscription> familySubscriptions = new ArrayList<>();
    ArrayList<Integer> selectedSubscriptions = new ArrayList<>();
    OnItemClickListenerCallback onItemLongClickListenerCallback;

    public FamilySubscriptionsAdapter(OnItemClickListenerCallback onItemLongClickListenerCallback) {
        this.onItemLongClickListenerCallback = onItemLongClickListenerCallback;
    }

    public void setFamilySubscriptions(ArrayList<Subscription> familySubscriptions) {
        this.familySubscriptions = familySubscriptions;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void sortSubscriptions(SubscriptionSortings subscriptionSortings) {
        SubscriptionSorting sorter = SortingFactory.Companion.getSorting(subscriptionSortings);
        familySubscriptions = new ArrayList<>(sorter.sort(familySubscriptions));
        notifyDataSetChanged();
    }

    public void addItemSelected(int position) {
        selectedSubscriptions.add(position);
        notifyItemChanged(position);
    }

    public void removeItemSelected(int position) {
        selectedSubscriptions.remove((Integer) position);
        notifyItemChanged(position);
    }

    public boolean checkIfItemSelected(int position) {
        return selectedSubscriptions.contains(position);
    }

    public int getSelectedCount() {
        return selectedSubscriptions.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearSelectedArrayList() {
        selectedSubscriptions.clear();
        notifyDataSetChanged();
    }

    public ArrayList<Integer> getSelectedSubscriptionsIDs() {
        ArrayList<Integer> selectedSubscriptionsIDs = new ArrayList<>();
        for (Integer index : selectedSubscriptions) {
            selectedSubscriptionsIDs.add(familySubscriptions.get(index).subscriptionID);
        }
        return selectedSubscriptionsIDs;
    }

    @NonNull
    @Override
    public FamilySubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View familySubscriptionView = inflater.inflate(R.layout.item_subscription_family, parent, false);
        return new FamilySubscriptionViewHolder(familySubscriptionView);
    }

    @Override
    public void onBindViewHolder(@NonNull FamilySubscriptionViewHolder holder, int position) {
        Subscription familySubscription = familySubscriptions.get(position);
        holder.familyNameTextView.setText(familySubscription.name);
        int membersCount = DatabaseRepository.getInstance(holder.familyIconContainer.getContext()).getSubscriptionMembersCountByID(familySubscription.subscriptionID);
        if (familySubscription.isFamilyPart) {
            holder.familyMembersTextView.setText(
                    holder.familyMembersTextView.getContext().getString(R.string.part_of_family)
            );
        }
        else {
            holder.familyMembersTextView.setText(
                    holder.itemView.getContext().getResources().
                            getQuantityString(
                                    R.plurals.family_members,
                                    membersCount,
                                    membersCount));
        }
        holder.familyPriceTextView.setText(
                holder.itemView.getContext().getString(
                                                R.string.price,
                                        new DecimalFormat("#0.00").format(familySubscription.price),
                        DatabaseRepository.getInstance(holder.familyIconContainer.getContext()).getCurrencyShortNameByID(familySubscription.currencyID)));
        holder.familyPaymentTextView.setText(holder.itemView.getContext().getString(R.string.next_payment, DateHelper.getNextPaymentDate(familySubscription.nextPaymentDate)));
        boolean isSelected = checkIfItemSelected(position);

        if (isSelected) {
            ViewAnimator.flipImageView(holder.familyIconContainer.getContext(), holder.familyIconContainer, holder.familyImageView);
        }
        else {
            holder.familyIconContainer.setBackgroundResource(R.drawable.shape_oval_transparent);
            GradientDrawable backgroundDrawable = (GradientDrawable) holder.familyIconContainer.getBackground();
            backgroundDrawable.mutate();
            backgroundDrawable.setColor(Color.parseColor(familySubscription.iconColor));
            holder.familyIconContainer.setBackground(backgroundDrawable);

            holder.familyImageView.setVisibility(View.VISIBLE);
            holder.familyImageView.setImageResource(ResourcesHelper.getImageIDFromName(holder.familyIconContainer.getContext(), familySubscription.icon));
        }

        holder.familyItemContainer.setSelected(isSelected);
    }

    @Override
    public int getItemCount() {
        return familySubscriptions.size();
    }

    public class FamilySubscriptionViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout familyIconContainer;
        ImageView familyImageView;
        TextView familyNameTextView;
        TextView familyMembersTextView;
        TextView familyPriceTextView;
        TextView familyPaymentTextView;
        ConstraintLayout familyItemContainer;

        public FamilySubscriptionViewHolder(View itemView) {
            super(itemView);
            familyIconContainer = itemView.findViewById(R.id.familySubscriptionItemIconContainer);
            familyImageView = itemView.findViewById(R.id.familySubscriptionItemIconImageView);
            familyNameTextView = itemView.findViewById(R.id.familyNameTextView);
            familyMembersTextView = itemView.findViewById(R.id.familyMembersTextView);
            familyPriceTextView = itemView.findViewById(R.id.familyPriceTextView);
            familyPaymentTextView = itemView.findViewById(R.id.familyPaymentTextView);
            familyItemContainer = itemView.findViewById(R.id.familySubscriptionItemContainer);
            itemView.setOnLongClickListener(v -> {
                onItemLongClickListenerCallback.onItemClicked(v, getAdapterPosition());
                return true;
            });
            itemView.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putInt(ManagingActivity.ID_KEY, familySubscriptions.get(getAdapterPosition()).subscriptionID);
                Intent familySubscriptionIntent = new Intent(v.getContext(), familySubscriptions.get(getAdapterPosition()).isFamilyPart ? FamilyPartManagingActivity.class : FamilyManagingActivity.class).putExtras(bundle);
                v.getContext().startActivity(familySubscriptionIntent);
            });
        }
    }

}
