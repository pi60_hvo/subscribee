package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.family

import android.content.Context
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.ui.subscription.managing.family.FamilyManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.NextPaymentManagingDialog
import java.util.*

class FamilyManagingNextPaymentDialog(context: Context) : NextPaymentManagingDialog(context) {
    override fun setSelectedDate(year: Int, month: Int, dayOfMonth: Int) {
        (context as FamilyManagingActivity).setNextPaymentDate(year, month, dayOfMonth)
        (context as FamilyManagingActivity).setNextPaymentDateString()
        (context as FamilyManagingActivity).setNextPaymentTextInputText()
    }

    override fun setSelectedDate(calendar: Calendar?) {
        (context as FamilyManagingActivity).setNextPaymentDate(calendar)
        (context as FamilyManagingActivity).setNextPaymentDateString()
        (context as FamilyManagingActivity).setNextPaymentTextInputText()
    }

    override fun addToNextPaymentFreePeriod() {
        (context as FamilyManagingActivity).freePeriodDialog.freePeriodsAdapter.addToNextPaymentFreePeriod()
    }

    override fun getSubscriptionOperationsCount(): Int {
        return DatabaseRepository.getInstance(context).getSubscriptionOperationsCountByID((context as FamilyManagingActivity).subscriptionID)
    }
}