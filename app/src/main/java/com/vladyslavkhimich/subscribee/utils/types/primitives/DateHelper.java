package com.vladyslavkhimich.subscribee.utils.types.primitives;

import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.database.entities.FreePeriod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateHelper {
    public static String getNextPaymentDate(Date date) {
        Calendar currentDateCalendar = Calendar.getInstance();
        currentDateCalendar.setTime(new Date());
        Calendar nextPaymentCalendar = Calendar.getInstance();
        nextPaymentCalendar.setTime(date);
        SimpleDateFormat dateFormat;
        if (currentDateCalendar.get(Calendar.YEAR) < nextPaymentCalendar.get(Calendar.YEAR)) {
                dateFormat = new SimpleDateFormat("dd MMM y", Locale.getDefault());
                return dateFormat.format(date);
        }
        dateFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());
        return  dateFormat.format(date);
    }

    public static String getMonthYearString(Date date) {
        String formattedDateString = new SimpleDateFormat("LLLL yyyy", Locale.getDefault()).format(date);
        return formattedDateString.substring(0, 1).toUpperCase() + formattedDateString.substring(1);
    }

    public static String getDayMonthWeekDayString(Date date) {
        return new SimpleDateFormat("dd MMM, EEEE", Locale.getDefault()).format(date);
    }

    public static int getDaysCountBetweenTwoDates(Date firstDate, Date secondDate) {
        long difference = secondDate.getTime() - firstDate.getTime();
        return (int) Math.ceil((double) difference / (1000 * 60 * 60 * 24));
    }

    public static ArrayList<Date> getCycleDates(Date startDate, Cycle cycle, int count, boolean isSkipStartDate) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(startDate);

        getOnlyDateCalendar(startCalendar);

        ArrayList<Date> cycleDates = new ArrayList<>();

        if (!isSkipStartDate) {
            cycleDates.add(startCalendar.getTime());
        }

        for (int i = 0; i < count; i++) {
            addCyclePeriodToCalendar(startCalendar, cycle);

            cycleDates.add(startCalendar.getTime());
        }
        return cycleDates;
    }

    public static void addCyclePeriodToCalendar(Calendar calendar, Cycle cycle) {
        calendar.add(Calendar.DATE, cycle.daysCount);
        calendar.add(Calendar.WEEK_OF_YEAR, cycle.weeksCount);
        calendar.add(Calendar.MONTH, cycle.monthsCount);
        calendar.add(Calendar.YEAR, cycle.yearsCount);
    }

    public static void getOnlyDateCalendar(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static Date addOneDayToDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);

        return calendar.getTime();
    }

    public static int convertDayOfMonthToNotAfter28(int dayOfMonth) {
        if (dayOfMonth == 29 || dayOfMonth == 30 || dayOfMonth == 31)
            dayOfMonth = 28;
        return dayOfMonth;
    }

    public static Date getFirstDayOfMonth(Calendar calendar) {
        Calendar calendarTemp = Calendar.getInstance();
        calendarTemp.setTime(calendar.getTime());
        calendarTemp.set(Calendar.DAY_OF_MONTH, 1);
        getOnlyDateCalendar(calendarTemp);
        return calendarTemp.getTime();
    }

    public static Date getLastDayOfMonth(Calendar calendar) {
        Calendar calendarTemp = Calendar.getInstance();
        calendarTemp.setTime(calendar.getTime());
        int lastDayOfCalendarMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendarTemp.set(Calendar.DAY_OF_MONTH, lastDayOfCalendarMonth);
        getOnlyDateCalendar(calendarTemp);
        return calendarTemp.getTime();
    }

    public static Date getFreePeriodDBEndingDate(FreePeriod freePeriod) {
        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.setTime(freePeriod.startDate);
        startDateCalendar.add(Calendar.DATE, freePeriod.daysCount);
        startDateCalendar.add(Calendar.WEEK_OF_YEAR, freePeriod.weeksCount);
        startDateCalendar.add(Calendar.MONTH, freePeriod.monthsCount);
        startDateCalendar.add(Calendar.YEAR, freePeriod.yearsCount);
        return startDateCalendar.getTime();
    }

    public static Date getFreePeriodEndingDate(com.vladyslavkhimich.subscribee.models.FreePeriod freePeriod) {
        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.setTime(freePeriod.getStartDate());
        startDateCalendar.add(Calendar.DATE, freePeriod.getDaysCount());
        startDateCalendar.add(Calendar.WEEK_OF_YEAR, freePeriod.getWeeksCount());
        startDateCalendar.add(Calendar.MONTH, freePeriod.getMonthsCount());
        startDateCalendar.add(Calendar.YEAR, freePeriod.getYearsCount());
        return startDateCalendar.getTime();
    }

    public static void subtractDays(Calendar calendar, int daysCount) {
        calendar.add(Calendar.DAY_OF_YEAR, -daysCount);
    }
}
