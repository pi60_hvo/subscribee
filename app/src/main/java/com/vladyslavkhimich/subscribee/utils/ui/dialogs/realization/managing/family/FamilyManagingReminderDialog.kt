package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.family

import android.content.Context
import com.vladyslavkhimich.subscribee.models.Reminder
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.family.FamilyManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.ReminderDialog

class FamilyManagingReminderDialog : ReminderDialog() {
    private lateinit var familyManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyManagingViewModel = (context as FamilyManagingActivity).subscriptionViewModel
    }

    override fun setSelectedReminder(selectedReminder: Reminder?) {
        familyManagingViewModel.setSelectedReminder(selectedReminder)
    }
}