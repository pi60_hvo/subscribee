package com.vladyslavkhimich.subscribee.utils.types.primitives;

import android.util.Log;

import java.text.DecimalFormat;
import java.util.Locale;

public class DoubleHelper {
    public static String getFormattedDoubleOrInteger(Double number) {
        if (number % 1 == 0) {
            return String.valueOf(number.intValue());
        }
        return new DecimalFormat("###.##").format(number);
    }

    public static String getFormattedDoubleOrIntegerUnsigned(Double number) {
        number = Math.abs(number);
        if (number % 1 == 0) {
            return String.valueOf(number.intValue());
        }
        return new DecimalFormat("###.##").format(number);
    }

    public static Double parseDoubleFromString(String string) {
        try {
            return Double.parseDouble(string.replace(',', '.'));
        }
        catch (NumberFormatException e) {
            Log.e("Conversion Error", e.getMessage());
            return null;
        }
    }

    public static String getStringFromDouble(Double number) {
        return String.format(Locale.getDefault(), "%1$.2f", number);
    }

    public static String getIntegerFromDouble(Double number) {
        return String.valueOf(number.intValue());
    }
}
