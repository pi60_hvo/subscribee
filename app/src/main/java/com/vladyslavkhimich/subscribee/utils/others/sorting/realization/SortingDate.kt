package com.vladyslavkhimich.subscribee.utils.others.sorting.realization

import com.vladyslavkhimich.subscribee.database.entities.Subscription
import com.vladyslavkhimich.subscribee.utils.interfaces.SubscriptionSorting

class SortingDate : SubscriptionSorting {
    override fun sort(subscriptions: List<Subscription>) : List<Subscription> {
        return subscriptions.sortedBy { it.nextPaymentDate }
    }
}