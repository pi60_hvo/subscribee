package com.vladyslavkhimich.subscribee.utils.others.stats.realization

import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats
import com.vladyslavkhimich.subscribee.models.stats.family.FamilyStatsOverview
import com.vladyslavkhimich.subscribee.utils.interfaces.StatsFactory
import com.vladyslavkhimich.subscribee.utils.mappers.mapFamilySubscriptionsData
import com.vladyslavkhimich.subscribee.utils.mappers.mapPersonalSubscriptionsData
import com.vladyslavkhimich.subscribee.utils.others.stats.helpers.StatsHelper
import java.util.*

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
class FamilyStatsFactory : StatsFactory {
    override fun getOverviewStats(operationsWithData: List<OperationWithDataForStats>, previousOperationsWithData: List<OperationWithDataForStats>) : FamilyStatsOverview {
        val mappedOperationsWithData = mapFamilySubscriptionsData(operationsWithData)
        val mappedPreviousOperationsWithData = mapFamilySubscriptionsData(previousOperationsWithData)
        val resultStats = FamilyStatsOverview()
        resultStats.totalSpent = StatsHelper.getTotalSpent(operationsWithData)
        resultStats.dayAverageSpent = StatsHelper.getSpentPerDay(operationsWithData)
        resultStats.percentage = StatsHelper.getPercentage(mappedOperationsWithData, mappedPreviousOperationsWithData)
        resultStats.mostSpentSubscription = StatsHelper.getMostSpentOrPaidSubscription(operationsWithData, isSpent = true, isFamily = true)
        resultStats.mostSpentCategory = StatsHelper.getMostSpentCategory(operationsWithData)
        resultStats.mostPaidSubscription = StatsHelper.getMostSpentOrPaidSubscription(operationsWithData, isSpent = false, isFamily = true)
        resultStats.mostPaidMember = StatsHelper.getMostPaidMember(operationsWithData)
        return resultStats
    }

    override fun getTimelineStats() {
        TODO("Not yet implemented")
    }

    override fun getRadialStats() {
        TODO("Not yet implemented")
    }

    fun mapFamilySubscriptionsData(operationsWithData: List<OperationWithDataForStats>) : LinkedList<OperationWithDataForStats> {
        val mappedOperationsWithData = LinkedList<OperationWithDataForStats>()
        mappedOperationsWithData.mapFamilySubscriptionsData(operationsWithData)
        return mappedOperationsWithData
    }
}