package com.vladyslavkhimich.subscribee.utils.ui.adapters.items;

import java.util.Date;

public class OperationDateItem extends OperationListItem {

    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int getType() {
        return TYPE_DATE;
    }
}
