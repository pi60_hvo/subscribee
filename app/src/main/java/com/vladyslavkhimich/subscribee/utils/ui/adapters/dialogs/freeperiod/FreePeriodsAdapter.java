package com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.freeperiod;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback;
import com.vladyslavkhimich.subscribee.models.FreePeriod;

import java.util.ArrayList;
import java.util.Iterator;

public class FreePeriodsAdapter extends RecyclerView.Adapter<FreePeriodsAdapter.FreePeriodViewHolder> {
    ArrayList<FreePeriod> freePeriods = new ArrayList<>();
    OnItemClickListenerCallback onItemClickListenerCallback;

    public FreePeriodsAdapter(OnItemClickListenerCallback onItemClickListenerCallback) {
        setOnItemClickListenerCallback(onItemClickListenerCallback);
    }

    @NonNull
    @Override
    public FreePeriodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View freePeriodView = inflater.inflate(R.layout.item_dialog_free_period, parent, false);
        return new FreePeriodViewHolder(freePeriodView);
    }

    @Override
    public void onBindViewHolder(@NonNull FreePeriodViewHolder holder, int position) {
        FreePeriod freePeriod = freePeriods.get(position);
        holder.freePeriodTextView.setText(ResourcesHelper.getStringIDFromName(holder.freePeriodTextView.getContext(), freePeriod.getStringIDName()));
    }

    @Override
    public int getItemCount() {
        return freePeriods.size();
    }

    public void setOnItemClickListenerCallback(OnItemClickListenerCallback onItemClickListenerCallback) {
        this.onItemClickListenerCallback = onItemClickListenerCallback;
    }

    public void removeToNextPaymentFreePeriod() {
        Iterator<FreePeriod> iterator = freePeriods.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getIsToNextPayment()) {
                iterator.remove();
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void addToNextPaymentFreePeriod() {
        FreePeriod toNextPaymentFreePeriod = new FreePeriod();
        toNextPaymentFreePeriod.setStringIDName("to_next_payment_free_period");
        toNextPaymentFreePeriod.setToNextPayment(true);
        boolean hasToNextPaymentFreePeriod = false;
        for (FreePeriod freePeriod : freePeriods) {
            if (freePeriod.getIsToNextPayment()) {
                hasToNextPaymentFreePeriod = true;
                break;
            }
        }
        if (!hasToNextPaymentFreePeriod)
            freePeriods.add(0, toNextPaymentFreePeriod);
        notifyDataSetChanged();
    }

    public ArrayList<FreePeriod> getFreePeriods() {
        return freePeriods;
    }

    public void setFreePeriods(ArrayList<FreePeriod> freePeriods) {
        this.freePeriods = freePeriods;
    }

    public class FreePeriodViewHolder extends RecyclerView.ViewHolder {
        TextView freePeriodTextView;

        public FreePeriodViewHolder(@NonNull View itemView) {
            super(itemView);
            freePeriodTextView = (TextView) itemView.findViewById(R.id.freePeriodTextView);
            itemView.setOnClickListener(v -> onItemClickListenerCallback.onItemClicked(v, getAdapterPosition()));
        }
    }
}
