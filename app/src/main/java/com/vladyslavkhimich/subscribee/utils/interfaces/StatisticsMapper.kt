package com.vladyslavkhimich.subscribee.utils.interfaces

import com.vladyslavkhimich.subscribee.models.stats.base.BaseStatsOverview

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 16.12.2021
 * @email vladislav.himich@4k.com.ua
 */
interface StatisticsMapper {
    fun mapStatistics(tabIndex: Int, overviewStatsData: BaseStatsOverview)
}