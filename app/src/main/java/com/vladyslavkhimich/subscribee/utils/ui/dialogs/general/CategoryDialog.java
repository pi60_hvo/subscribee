package com.vladyslavkhimich.subscribee.utils.ui.dialogs.general;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;

import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.utils.types.complex.ColorHelper;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.category.CategoryAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs.CategoryCreateDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs.CategoryEditDialog;

import java.util.List;
import java.util.Objects;

public abstract class CategoryDialog {
    AlertDialog categoryAlertDialog;
    CategoryCreateDialog categoryCreateChildDialog;
    CategoryEditDialog categoryEditChildDialog;
    CategoryAdapter categoryAdapter;
    RecyclerView categoryRecyclerView;

    public void initialize(Context context) {
        setAssociatedActivityViewModel(context);
        AlertDialog.Builder categoryAlertDialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_category_custom, null);
        categoryAlertDialogBuilder.setView(dialogView);
        categoryAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, ((dialog, which) -> {}));
        Button addCategoryButton = dialogView.findViewById(R.id.categoryDialogAddButton);
        addCategoryButton.setOnClickListener(v -> {
            categoryCreateChildDialog.getCategoryCreateAlertDialog().show();
            categoryCreateChildDialog.newCategoryEditText.requestFocus();
            categoryCreateChildDialog.getCategoryCreateAlertDialog().getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v1 -> {
                if (!categoryCreateChildDialog.isTextHasError) {
                    boolean isCategoryExists = DatabaseRepository.getInstance(context).checkIfCategoryNameExists(Objects.requireNonNull(categoryCreateChildDialog.newCategoryEditText.getText()).toString());
                        if (isCategoryExists)
                        {
                            categoryCreateChildDialog.newCategoryInputLayout.setError(context.getText(R.string.error_new_category_exists));
                        }
                        else {
                            Category categoryToInsert = new Category();
                            categoryToInsert.isUserCreated = true;
                            categoryToInsert.name = categoryCreateChildDialog.newCategoryEditText.getText().toString();
                            categoryToInsert.color = ColorHelper.getRandomColorFromSharedPrefs(context);
                            DatabaseRepository.getInstance(context).insertOneCategory(categoryToInsert);
                            categoryCreateChildDialog.getCategoryCreateAlertDialog().dismiss();
                            categoryCreateChildDialog.newCategoryEditText.setText("");
                            categoryCreateChildDialog.newCategoryInputLayout.setError(null);
                            categoryCreateChildDialog.newCategoryInputLayout.setErrorEnabled(false);
                            categoryAlertDialog.show();
                        }
                }
            });
        });
        categoryRecyclerView = dialogView.findViewById(R.id.categoryDialogRecyclerView);
        categoryAdapter = new CategoryAdapter((
                (view, position) -> {
                    setSelectedCategory(categoryAdapter.getCategoriesItem(position));
                    categoryAlertDialog.dismiss();
                }),
                ((view, position) -> {
                    categoryEditChildDialog.setCategoryToEdit(getCategoryAdapter().getCategoriesItem(position));
                    categoryEditChildDialog.getCategoryEditAlertDialog().show();
                    categoryEditChildDialog.getCategoryEditAlertDialog().getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v -> {
                                if (!categoryEditChildDialog.isTextHasError) {
                                    boolean isCategoryExists = DatabaseRepository.getInstance(context).checkIfCategoryNameExists(Objects.requireNonNull(categoryEditChildDialog.editCategoryEditText.getText()).toString());
                                        if (isCategoryExists) {
                                            categoryCreateChildDialog.newCategoryInputLayout.setError(context.getText(R.string.error_new_category_exists));
                                        }
                                        else {
                                            Category categoryToUpdate = categoryAdapter.getCategoriesItem(position);
                                            boolean isSetCategory = categoryToUpdate.equals(getSelectedCategory());
                                            categoryToUpdate.name = categoryEditChildDialog.editCategoryEditText.getText().toString();
                                            DatabaseRepository.getInstance(context).updateOneCategory(categoryToUpdate);
                                            categoryEditChildDialog.getCategoryEditAlertDialog().dismiss();
                                            categoryAlertDialog.show();
                                            if (isSetCategory)
                                                setSelectedCategory(categoryToUpdate);
                                        }
                                }
                    });
                    categoryEditChildDialog.editCategoryEditText.requestFocus();
        }), ((view, position) -> new AlertDialog.Builder(context).
                setTitle(R.string.dialog_delete_category).
                setMessage(HtmlCompat.fromHtml(context.getString(R.string.dialog_text_delete_category, categoryAdapter.getCategoriesItem(position).name), HtmlCompat.FROM_HTML_MODE_LEGACY))
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    resetSelectedCategoryIfDeleted(categoryAdapter.getCategoriesItem(position));
                    DatabaseRepository.getInstance(context).deleteOneCategory(categoryAdapter.getCategoriesItem(position));
                })
                .setNegativeButton(R.string.no, (dialog, which) -> {

                }).show()));
        categoryRecyclerView.setAdapter(categoryAdapter);
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        categoryRecyclerView.setHasFixedSize(true);
        categoryAlertDialog = categoryAlertDialogBuilder.create();
        initializeChildDialogs(context);
    }

    public void initializeChildDialogs(Context context) {
        initializeCategoryCreateChildDialog(context);
        initializeCategoryEditChildDialog(context);
    }

    public void initializeCategoryCreateChildDialog(Context context) {
        categoryCreateChildDialog = new CategoryCreateDialog();
        categoryCreateChildDialog.setParentDialog(this);
        categoryCreateChildDialog.initialize(context);
    }

    public void initializeCategoryEditChildDialog(Context context) {
        categoryEditChildDialog = new CategoryEditDialog();
        categoryEditChildDialog.setParentDialog(this);
        categoryEditChildDialog.initialize(context);
    }

    public void scrollRecyclerViewToBottom() {
        categoryRecyclerView.scrollToPosition(categoryAdapter.getItemCount() - 1);
    }


    public CategoryAdapter getCategoryAdapter() {
        return categoryAdapter;
    }

    public AlertDialog getCategoryAlertDialog() {
        return categoryAlertDialog;
    }

    public abstract void setAssociatedActivityViewModel(Context context);

    public abstract void setAdapterCategories(List<Category> categories);

    public abstract void setSelectedCategory(Category category);

    public abstract Category getSelectedCategory();

    public abstract void resetSelectedCategoryIfDeleted(Category category);
}
