package com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.utils.types.primitives.IntegerHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CycleDialog;

public class PeriodPickerCycleDialog {
    CycleDialog parentDialog;
    AlertDialog periodPickerAlertDialog;
    EditText numberEditText;
    Spinner periodSpinner;
    Integer periodNumber;

    public void initialize(Context context) {
        AlertDialog.Builder periodPickerAlertDialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_period_cycle_custom, null);
        periodPickerAlertDialogBuilder.setView(dialogView);
        numberEditText = dialogView.findViewById(R.id.numberCycleDialogEditText);
        periodSpinner = dialogView.findViewById(R.id.cycleDialogSpinner);
        periodPickerAlertDialogBuilder.setPositiveButton("OK", (dialog, which) -> {
            if (numberEditText.getText().toString().trim().length() > 0) {
                periodNumber = IntegerHelper.tryParse(numberEditText.getText().toString().trim());
                if (periodNumber != null && periodNumber != 0) {
                    Cycle resultCycle = new Cycle();
                    switch(periodSpinner.getSelectedItemPosition()) {
                        case 0:
                            resultCycle.daysCount = periodNumber;
                            break;
                        case 1:
                            resultCycle.weeksCount = periodNumber;
                            break;
                        case 2:
                            resultCycle.monthsCount = periodNumber;
                            break;
                        case 3:
                            resultCycle.yearsCount = periodNumber;
                            break;
                    }
                    parentDialog.setSelectedCycle(resultCycle);
                    periodPickerAlertDialog.dismiss();
                    parentDialog.getCycleAlertDialog().dismiss();
                }
            }
            else {
                periodPickerAlertDialog.dismiss();
                parentDialog.getCycleAlertDialog().show();
            }
        });
        periodPickerAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {
            periodPickerAlertDialog.dismiss();
            parentDialog.getCycleAlertDialog().show();
        });
        periodPickerAlertDialog = periodPickerAlertDialogBuilder.create();
        periodPickerAlertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public AlertDialog getPeriodPickerAlertDialog() {
        return periodPickerAlertDialog;
    }

    public EditText getNumberEditText() {
        return numberEditText;
    }

    public void setParentDialog(CycleDialog parentDialog) {
        this.parentDialog = parentDialog;
    }
}
