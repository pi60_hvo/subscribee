package com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.utils.types.primitives.IntegerHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.FreePeriodDialog;

public class PeriodPickerFreePeriodDialog {
    FreePeriodDialog parentDialog;
    AlertDialog freePeriodPickerAlertDialog;
    EditText numberEditText;
    Spinner freePeriodSpinner;
    Integer periodNumber;

    public void initialize(Context context) {
        AlertDialog.Builder freePeriodPickerAlertDialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_period_free_period_custom, null);
        freePeriodPickerAlertDialogBuilder.setView(dialogView);
        numberEditText = dialogView.findViewById(R.id.numberFreePeriodDialogEditText);
        freePeriodSpinner = dialogView.findViewById(R.id.freePeriodDialogSpinner);
        freePeriodPickerAlertDialogBuilder.setPositiveButton("OK", (dialog, which) -> {
            if (numberEditText.getText().toString().trim().length() > 0) {
                periodNumber = IntegerHelper.tryParse(numberEditText.getText().toString().trim());
                if (periodNumber != null && periodNumber != 0) {
                    FreePeriod resultFreePeriod = new FreePeriod();
                    switch (freePeriodSpinner.getSelectedItemPosition()) {
                        case 0:
                            resultFreePeriod.setDaysCount(periodNumber);
                            break;
                        case 1:
                            resultFreePeriod.setWeeksCount(periodNumber);
                            break;
                        case 2:
                            resultFreePeriod.setMonthsCount(periodNumber);
                            break;
                        case 3:
                            resultFreePeriod.setYearsCount(periodNumber);
                            break;
                    }
                    parentDialog.setSelectedFreePeriod(resultFreePeriod);
                    freePeriodPickerAlertDialog.dismiss();
                    parentDialog.getFreePeriodAlertDialog().dismiss();
                }
            }
            else {
                freePeriodPickerAlertDialog.dismiss();
                parentDialog.getFreePeriodAlertDialog().show();
            }
        });
        freePeriodPickerAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {
            freePeriodPickerAlertDialog.dismiss();
            parentDialog.getFreePeriodAlertDialog().show();
        });
        freePeriodPickerAlertDialog = freePeriodPickerAlertDialogBuilder.create();
        freePeriodPickerAlertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public AlertDialog getFreePeriodPickerAlertDialog() {
        return freePeriodPickerAlertDialog;
    }

    public EditText getNumberEditText() {
        return numberEditText;
    }

    public void setParentDialog(FreePeriodDialog parentDialog) {
        this.parentDialog = parentDialog;
    }
}
