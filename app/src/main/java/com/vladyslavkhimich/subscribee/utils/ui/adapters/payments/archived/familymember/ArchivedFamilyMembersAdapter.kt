package com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.archived.familymember

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.models.MemberWithDataForAdapter
import com.vladyslavkhimich.subscribee.models.Payment
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport
import com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.archived.general.ArchivedPaymentsAdapter

class ArchivedFamilyMembersAdapter : RecyclerView.Adapter<ArchivedFamilyMembersAdapter.ArchivedFamilyMemberViewHolder>() {

    var membersWithData: List<MemberWithDataForAdapter> = ArrayList()

    @SuppressLint("NotifyDataSetChanged")
    fun setMembersWithDataAndNotify(membersWithDataFromDB: ArrayList<MemberWithDataForAdapter>) {
        membersWithData = membersWithDataFromDB
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArchivedFamilyMemberViewHolder {
        val familyMemberView = LayoutInflater.from(parent.context).inflate(R.layout.item_family_member, parent, false)
        return ArchivedFamilyMemberViewHolder(familyMemberView)
    }

    override fun onBindViewHolder(holder: ArchivedFamilyMemberViewHolder, position: Int) {
        val memberWithData = membersWithData[position]
        if (memberWithData.member?.name == "Manager") {
            holder.nameTextView.setText(R.string.you)
            holder.roleTextView.setText(R.string.manager)
        }
        else {
            holder.nameTextView.text = memberWithData.member?.name
            holder.roleTextView.setText(R.string.role_member)
        }
        holder.actionsImageView.visibility = View.GONE
        val innerAdapter = ArchivedPaymentsAdapter()
        holder.paymentsRecyclerView.layoutManager = LinearLayoutManager(holder.nameTextView.context, LinearLayoutManager.HORIZONTAL, false)
        holder.paymentsRecyclerView.setHasFixedSize(true)
        holder.paymentsRecyclerView.setEmptyView(holder.noPaymentsTextView)
        holder.paymentsRecyclerView.adapter = innerAdapter
        val mappedList = ArrayList<Payment>()
        memberWithData.operationsAndPaymentDates.forEach {
            item -> mappedList.add(Payment(item.paymentDate.paymentDate, item.operation != null))
        }
        innerAdapter.setPaymentsAndNotify(mappedList)
    }

    override fun getItemCount(): Int {
        return membersWithData.size
    }

    inner class ArchivedFamilyMemberViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {
        val actionsImageView: ImageView = itemView.findViewById(R.id.familyMemberActionsImageView)
        val nameTextView: TextView = itemView.findViewById(R.id.familyMemberNameTextView)
        val roleTextView: TextView = itemView.findViewById(R.id.familyMemberRoleTextView)
        val noPaymentsTextView: TextView = itemView.findViewById(R.id.familyMemberNoPaymentsTextView)
        val paymentsRecyclerView: RecyclerViewEmptySupport  = itemView.findViewById(R.id.familyMemberPaymentsRecyclerView)
    }
}