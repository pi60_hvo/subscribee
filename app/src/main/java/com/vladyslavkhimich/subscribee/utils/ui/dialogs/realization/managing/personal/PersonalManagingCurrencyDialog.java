package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.personal.PersonalManagingActivity;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CurrencyDialog;

import java.util.ArrayList;
import java.util.List;

public class PersonalManagingCurrencyDialog extends CurrencyDialog {
    SubscriptionViewModel personalManagingViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalManagingViewModel = ((PersonalManagingActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCurrencies(List<Currency> currencies) {
        getCurrencyAdapter().setAllCurrencies((ArrayList<Currency>) currencies);
    }

    @Override
    public void setSelectedCurrency(Currency currency) {
        if (currency != null)
            personalManagingViewModel.setSelectedCurrency(currency);
    }
}
