package com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.icon;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.app.MyApplication;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.google.android.material.color.MaterialColors;

import java.util.ArrayList;
import java.util.Arrays;

public class IconPickerIconAdapter extends BaseAdapter {
    private final ArrayList<Integer> iconsIdsArrayList;
    private final Context context;
    private int selectedPosition = -1;

    public IconPickerIconAdapter(Context context) {
        this.context = context;
        iconsIdsArrayList = ResourcesHelper.convertIconNamesToIntArrayList(context, new ArrayList<>(Arrays.asList(MyApplication.getPickerIconsFromSharedPrefs(context))));
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public ArrayList<Integer> getIconsIdsArrayList() {
        return iconsIdsArrayList;
    }

    public int getItemPosition(int itemId) {
        return getIconsIdsArrayList().indexOf(itemId);
    }

    @Override
    public int getCount() {
        return iconsIdsArrayList.size();
    }

    @Override
    public Integer getItem(int position) {
        return iconsIdsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Integer iconIdInteger = iconsIdsArrayList.get(position);
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_dialog_icon, null);
        }
        ImageView dialogIconImageView = convertView.findViewById(R.id.dialogIconImageView);
        dialogIconImageView.setImageResource(iconIdInteger);
        ConstraintLayout dialogIconIconConstraintLayout = convertView.findViewById(R.id.dialogIconIconConstraintLayout);
        if (position == selectedPosition) {
            dialogIconIconConstraintLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_dialog_icon_icon_selected));
            dialogIconImageView.setColorFilter(MaterialColors.getColor(context, R.attr.iconDialogIcon, Color.BLACK));
        }
        else {
            dialogIconIconConstraintLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.shape_dialog_icon_icon));
            dialogIconImageView.setColorFilter(MaterialColors.getColor(context, R.attr.iconDialogBackground, Color.BLACK));
        }
        return convertView;
    }
}
