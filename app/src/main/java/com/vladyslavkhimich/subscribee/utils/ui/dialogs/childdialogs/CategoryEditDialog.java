package com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CategoryDialog;
import com.vladyslavkhimich.subscribee.utils.ui.validators.StringValidator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class CategoryEditDialog {
    CategoryDialog parentDialog;
    AlertDialog categoryEditAlertDialog;
    Category categoryToEdit;
    TextView dialogHeader;
    Context context;
    public TextInputLayout editCategoryInputLayout;
    public TextInputEditText editCategoryEditText;
    public boolean isTextHasError;

    public void initialize(Context context) {
        this.context = context;
        AlertDialog.Builder categoryEditAlertDialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_category_edit_custom, null);
        categoryEditAlertDialogBuilder.setView(dialogView);
        dialogHeader = dialogView.findViewById(R.id.editCategoryDialogHeader);
        editCategoryInputLayout = dialogView.findViewById(R.id.editCategoryDialogInputLayout);
        editCategoryEditText = dialogView.findViewById(R.id.editCategoryDialogEditText);
        categoryEditAlertDialogBuilder.setPositiveButton(R.string.dialog_done, (dialog, which) -> {

        });
        categoryEditAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {
           categoryEditAlertDialog.dismiss();
           parentDialog.getCategoryAlertDialog().show();
        });
        editCategoryEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isTextHasError = StringValidator.isErrorText(s.toString());
                if (!isTextHasError) {
                    editCategoryInputLayout.setError(null);
                    editCategoryInputLayout.setErrorEnabled(isTextHasError);
                }
                else {
                    editCategoryInputLayout.setError(context.getResources().getText(R.string.error_text));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        categoryEditAlertDialog = categoryEditAlertDialogBuilder.create();
        categoryEditAlertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public void setCategoryToEdit(Category categoryToEdit) {
        this.categoryToEdit = categoryToEdit;
        dialogHeader.setText(context.getString(R.string.dialog_category_edit_header, categoryToEdit.name));
        editCategoryEditText.setText(categoryToEdit.name);
        editCategoryEditText.setSelection(categoryToEdit.name.length());
    }

    public AlertDialog getCategoryEditAlertDialog() {
        return categoryEditAlertDialog;
    }

    public void setParentDialog(CategoryDialog parentDialog) {
        this.parentDialog = parentDialog;
    }
}
