package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.familypart

import android.content.Context
import com.vladyslavkhimich.subscribee.database.entities.Category
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart.FamilyPartManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CategoryDialog
import java.util.ArrayList

class FamilyPartManagingCategoryDialog : CategoryDialog() {
    private lateinit var familyPartManagingViewModel: SubscriptionViewModel
    private var categoriesListItemsCount: Int = 0
    private var categoriesSetCount: Int = 0

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyPartManagingViewModel = (context as FamilyPartManagingActivity).subscriptionViewModel
    }

    override fun setAdapterCategories(categories: MutableList<Category>?) {
        categoriesSetCount++
        categoryAdapter.categories = categories as ArrayList<Category>?
        if (categoriesSetCount > 1 && categories!!.size.compareTo(categoriesListItemsCount) == 1)
            scrollRecyclerViewToBottom()
        categoriesListItemsCount = categories!!.size
    }

    override fun setSelectedCategory(category: Category?) {
        familyPartManagingViewModel.setSelectedCategory(category)
    }

    override fun getSelectedCategory(): Category {
        return familyPartManagingViewModel.selectedCategoryObject
    }

    override fun resetSelectedCategoryIfDeleted(category: Category?) {
        if (familyPartManagingViewModel.selectedCategory.value == category)
            familyPartManagingViewModel.setSelectedCategory(null)
    }
}