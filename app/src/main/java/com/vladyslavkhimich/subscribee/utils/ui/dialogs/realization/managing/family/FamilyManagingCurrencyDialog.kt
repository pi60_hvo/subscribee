package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.family

import android.content.Context
import com.vladyslavkhimich.subscribee.database.entities.Currency
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.family.FamilyManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CurrencyDialog
import java.util.ArrayList

class FamilyManagingCurrencyDialog : CurrencyDialog() {
    private lateinit var familyManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyManagingViewModel = (context as FamilyManagingActivity).subscriptionViewModel
    }

    override fun setAdapterCurrencies(currencies: MutableList<Currency>?) {
        currencyAdapter.allCurrencies = currencies as ArrayList<Currency>?
    }

    override fun setSelectedCurrency(currency: Currency?) {
        if (currency != null)
            familyManagingViewModel.setSelectedCurrency(currency)
    }
}