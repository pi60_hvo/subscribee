package com.vladyslavkhimich.subscribee.utils.ui.validators;

public class StringValidator {
    public static boolean isErrorText(String s) {
        if (s.trim().length() == 0)
            return true;
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isLetterOrDigit(s.charAt(i)) && !Character.toString(s.charAt(i)).equals(" ") && !Character.toString(s.charAt(i)).equals("-")) {
                return true;
            }
        }
        return false;
    }
}