package com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs.managing;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.types.primitives.IntegerHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.CycleManagingDialog;

public class ManagingPeriodPickerCycleDialog {
    CycleManagingDialog parentDialog;
    AlertDialog periodPickerAlertDialog;
    public EditText numberEditText;
    public Spinner periodSpinner;
    public Integer periodNumber;

    public void initialize(Context context) {
        AlertDialog.Builder periodPickerAlertDialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_period_cycle_custom, null);
        periodPickerAlertDialogBuilder.setView(dialogView);
        numberEditText = dialogView.findViewById(R.id.numberCycleDialogEditText);
        periodSpinner = dialogView.findViewById(R.id.cycleDialogSpinner);
        periodPickerAlertDialogBuilder.setPositiveButton("OK", (dialog, which) -> {
            if (numberEditText.getText().toString().trim().length() > 0) {
                periodNumber = IntegerHelper.tryParse(numberEditText.getText().toString().trim());
                if (periodNumber != null && periodNumber != 0) {
                    parentDialog.setAdapterCycleClicked(false);
                    parentDialog.setChangeAll(true);
                    if (parentDialog.getSubscriptionOperationsCount() > 1)
                        parentDialog.getCycleChangeAlertDialog().show();
                    else
                        parentDialog.handleSelectedCycle();
                }
            }
            else {
                periodPickerAlertDialog.dismiss();
                parentDialog.getCycleAlertDialog().show();
            }
        });
        periodPickerAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {
            periodPickerAlertDialog.dismiss();
            parentDialog.getCycleAlertDialog().show();
        });
        periodPickerAlertDialog = periodPickerAlertDialogBuilder.create();
        periodPickerAlertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public AlertDialog getPeriodPickerAlertDialog() {
        return periodPickerAlertDialog;
    }

    public EditText getNumberEditText() {
        return numberEditText;
    }

    public void setParentDialog(CycleManagingDialog parentDialog) {
        this.parentDialog = parentDialog;
    }
}
