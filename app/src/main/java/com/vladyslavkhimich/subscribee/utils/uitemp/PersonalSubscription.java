package com.vladyslavkhimich.subscribee.utils.uitemp;

import java.util.Date;

public class PersonalSubscription extends Subscription {
    public String category;

    public PersonalSubscription() {

    }

    public PersonalSubscription(int imageID, String name, Double price, Date nextPaymentDate, String category) {
        super(imageID, name, price, nextPaymentDate);
        this.category = category;
    }
}
