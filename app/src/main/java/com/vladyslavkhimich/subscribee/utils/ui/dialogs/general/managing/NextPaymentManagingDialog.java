package com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;

import java.util.Calendar;
import java.util.Date;

public abstract class NextPaymentManagingDialog {
    public Context context;
    DatePickerDialog nextPaymentDatePickerDialog;
    AlertDialog nextPaymentChangeAlertDialog;
    RadioButton changeLastPaidRadioButton;
    public Boolean isChangeLastPaid;
    int year;
    int month;
    int dayOfMonth;

    public NextPaymentManagingDialog(Context context) {
        this.context = context;
        initializeNextPaymentChangeAlertDialog(context);
    }

    public void setNextPaymentDialogWithMinDate(Date selectedDate, Date minDate) {
        Calendar selectedDateCalendar = Calendar.getInstance();
        selectedDateCalendar.setTime(selectedDate);

        Calendar minDateCalendar = Calendar.getInstance();
        minDateCalendar.setTime(minDate);
        nextPaymentDatePickerDialog = new DatePickerDialog(context, (view, yearDialog, monthDialog, dayOfMonthDialog) -> {
             year = yearDialog;
             month = monthDialog;
             setDayOfMonth(dayOfMonthDialog);
             int operationsCount = getSubscriptionOperationsCount();
             if (operationsCount > 0) {
                 isChangeLastPaid = changeLastPaidRadioButton.isChecked();
                 nextPaymentChangeAlertDialog.show();
             }
             else {
                 isChangeLastPaid = null;
                 setSelectedDate(year, month, dayOfMonth);
                 addToNextPaymentFreePeriod();
             }
        }, selectedDateCalendar.get(Calendar.YEAR), selectedDateCalendar.get(Calendar.MONTH), selectedDateCalendar.get(Calendar.DAY_OF_MONTH));

        nextPaymentDatePickerDialog.getDatePicker().setMinDate(minDate.getTime());
    }

    public void initializeNextPaymentChangeAlertDialog(Context context) {
        AlertDialog.Builder nextPaymentChangeAlertDialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_next_payment_change, null);
        changeLastPaidRadioButton = dialogView.findViewById(R.id.dialogNextPaymentChangeLastPaidRadioButton);

        nextPaymentChangeAlertDialogBuilder.setView(dialogView);

        RadioGroup nextPaymentChangeRadioGroup = dialogView.findViewById(R.id.dialogNextPaymentChangeRadioGroup);

        nextPaymentChangeRadioGroup.setOnCheckedChangeListener((group, checkedId) -> isChangeLastPaid = checkedId == R.id.dialogNextPaymentChangeLastPaidRadioButton);

        nextPaymentChangeAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {
            nextPaymentChangeAlertDialog.dismiss();
            showDatePickerDialog();
            isChangeLastPaid = null;
        });

        nextPaymentChangeAlertDialogBuilder.setPositiveButton("OK", (dialog, which) -> {
            setSelectedDate(year, month, dayOfMonth);
            addToNextPaymentFreePeriod();
            nextPaymentDatePickerDialog.dismiss();
        });

        nextPaymentChangeAlertDialogBuilder.setTitle(R.string.dialog_title_next_payment_change);

        nextPaymentChangeAlertDialog = nextPaymentChangeAlertDialogBuilder.create();
    }

    public void showDatePickerDialog() {
        nextPaymentDatePickerDialog.show();
    }

    void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = DateHelper.convertDayOfMonthToNotAfter28(dayOfMonth);
    }

    /*public void setNextPaymentDate(int year, int month, int dayOfMonth) {
        nextPaymentDate.set(Calendar.YEAR, year);
        nextPaymentDate.set(Calendar.MONTH, month);
        nextPaymentDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
    }

    public void setNextPaymentDateString() {
        nextPaymentDateString = DateFormat.getDateInstance().format(nextPaymentDate.getTime());
    }

    public void setNextPaymentTextInputText() {
        nextPaymentTextInputEditText.setText(nextPaymentDateString);
    }*/

    //public abstract void setAssociatedActivityViewModel(Context context);

    public abstract void setSelectedDate(int year, int month, int dayOfMonth);

    public abstract void setSelectedDate(Calendar calendar);

    public abstract void addToNextPaymentFreePeriod();

    public abstract int getSubscriptionOperationsCount();
}
