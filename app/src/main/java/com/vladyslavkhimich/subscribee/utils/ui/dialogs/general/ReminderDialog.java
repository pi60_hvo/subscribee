package com.vladyslavkhimich.subscribee.utils.ui.dialogs.general;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.models.Reminder;
import com.vladyslavkhimich.subscribee.app.MyApplication;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.reminder.RemindersAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs.PeriodPickerReminderDialog;

public abstract class ReminderDialog {
    PeriodPickerReminderDialog childDialog;
    AlertDialog reminderAlertDialog;
    RemindersAdapter remindersAdapter;

    public void initialize(Context context) {
        setAssociatedActivityViewModel(context);
        AlertDialog.Builder reminderAlertDialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_reminder_custom, null);
        reminderAlertDialogBuilder.setView(dialogView);
        reminderAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {

        });
        reminderAlertDialogBuilder.setNeutralButton(R.string.custom_reminder, (dialog, which) -> {
            childDialog.getPeriodPickerAlertDialog().show();
            childDialog.getNumberEditText().requestFocus();
        });
        RecyclerView remindersRecyclerView = dialogView.findViewById(R.id.dialogReminderRecyclerView);
        remindersAdapter = new RemindersAdapter((view, position) -> {
            Reminder selectedReminder = remindersAdapter.getReminders().get(position);
            selectedReminder.setSameDay(selectedReminder.isAllFieldsZero());
            setSelectedReminder(selectedReminder.getIsNever() ? null : selectedReminder);
            reminderAlertDialog.dismiss();
        });
        remindersRecyclerView.setAdapter(remindersAdapter);
        remindersRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        remindersRecyclerView.setHasFixedSize(true);
        remindersAdapter.setReminders(MyApplication.getRemindersFromSharedPrefs(context));
        reminderAlertDialog = reminderAlertDialogBuilder.create();
        initializeChildAlertDialog(context);
    }

    public AlertDialog getReminderAlertDialog() {
        return reminderAlertDialog;
    }

    void initializeChildAlertDialog(Context context) {
        childDialog = new PeriodPickerReminderDialog();
        childDialog.setParentDialog(this);
        childDialog.initialize(context);
    }

    public abstract void setAssociatedActivityViewModel(Context context);

    public abstract void setSelectedReminder(Reminder selectedReminder);
}
