package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.personal.PersonalManagingActivity;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.CycleManagingDialog;

import java.util.ArrayList;
import java.util.List;

public class PersonalManagingCycleDialog extends CycleManagingDialog {
    Context context;
    SubscriptionViewModel personalManagingViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        this.context = context;
        personalManagingViewModel = ((PersonalManagingActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCycles(List<Cycle> cycles) {
        getCycleAdapter().setCycles((ArrayList<Cycle>) cycles);
    }

    @Override
    public void setSelectedCycle(Cycle cycle) {
        personalManagingViewModel.setSelectedCycle(cycle);
    }

    @Override
    public int getSubscriptionOperationsCount() {
        return DatabaseRepository.getInstance(context).getSubscriptionOperationsCountByID(((PersonalManagingActivity)context).subscriptionID);
    }

    @Override
    public void setChangeAllViewModelValue(Boolean isChangeAll) {
        ((ManagingViewModel)personalManagingViewModel).setIsCycleChangeAll(isChangeAll);
    }
}
