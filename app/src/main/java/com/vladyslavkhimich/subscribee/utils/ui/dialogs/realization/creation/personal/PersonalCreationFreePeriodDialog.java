package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.ui.subscription.creation.general.CreationViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.FreePeriodDialog;
import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.personal.PersonalCreationActivity;

public class PersonalCreationFreePeriodDialog extends FreePeriodDialog {
    SubscriptionViewModel personalCreationViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalCreationViewModel = ((PersonalCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setSelectedFreePeriod(FreePeriod selectedFreePeriod) {
        ((CreationViewModel)personalCreationViewModel).setSelectedFreePeriod(selectedFreePeriod);
    }
}
