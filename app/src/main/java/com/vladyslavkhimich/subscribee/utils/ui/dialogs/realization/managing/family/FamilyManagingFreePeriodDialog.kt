package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.family

import android.content.Context
import com.vladyslavkhimich.subscribee.models.FreePeriod
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.family.FamilyManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.FreePeriodDialog

class FamilyManagingFreePeriodDialog : FreePeriodDialog() {
    private lateinit var familyManagingViewModel: SubscriptionViewModel
    override fun setAssociatedActivityViewModel(context: Context?) {
        familyManagingViewModel = (context as FamilyManagingActivity).subscriptionViewModel
    }

    override fun setSelectedFreePeriod(freePeriod: FreePeriod?) {
        familyManagingViewModel.setSelectedFreePeriod(freePeriod)
    }
}