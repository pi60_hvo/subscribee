package com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.icon;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnColorClickListenerCallback;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnResourceClickListenerCallback;
import com.vladyslavkhimich.subscribee.ui.dialogs.icon.ColorPickerFragment;
import com.vladyslavkhimich.subscribee.ui.dialogs.icon.IconPickerFragment;

public class IconPickerPagerAdapter extends FragmentStateAdapter {
    private final IconPickerFragment iconPickerFragment;
    private final ColorPickerFragment colorPickerFragment;
    final int NUM_PAGES = 2;

    public IconPickerPagerAdapter(@NonNull FragmentManager fm, Lifecycle lifecycle, OnResourceClickListenerCallback onIconClickListenerCallback, OnColorClickListenerCallback onColorClickListenerCallback, Context context) {
        super(fm, lifecycle);
        iconPickerFragment = new IconPickerFragment(onIconClickListenerCallback, context);
        colorPickerFragment = new ColorPickerFragment(onColorClickListenerCallback, context);
    }

    public ColorPickerFragment getColorPickerFragment() {
        return colorPickerFragment;
    }

    public IconPickerFragment getIconPickerFragment() {
        return iconPickerFragment;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = iconPickerFragment;
                break;
            case 1:
                fragment = colorPickerFragment;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + position);
        }
        return fragment;
    }

    @Override
    public int getItemCount() {
        return NUM_PAGES;
    }
}
