package com.vladyslavkhimich.subscribee.utils.ui.dialogs.general;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.app.MyApplication;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.freeperiod.FreePeriodsAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs.PeriodPickerFreePeriodDialog;

public abstract class FreePeriodDialog {
    PeriodPickerFreePeriodDialog childDialog;
    AlertDialog freePeriodAlertDialog;
    FreePeriodsAdapter freePeriodsAdapter;

    public void initializeFreePeriodAlertDialog(Context context) {
        setAssociatedActivityViewModel(context);
        AlertDialog.Builder freePeriodAlertDialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_free_period_custom, null);
        freePeriodAlertDialogBuilder.setView(dialogView);
        freePeriodAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {

        });
        freePeriodAlertDialogBuilder.setNeutralButton(R.string.custom_free_period, (dialog, which) -> {
            childDialog.getFreePeriodPickerAlertDialog().show();
            childDialog.getNumberEditText().requestFocus();
        });
        RecyclerView freePeriodsRecyclerView = dialogView.findViewById(R.id.dialogFreePeriodRecyclerView);
        freePeriodsAdapter = new FreePeriodsAdapter((view, position) -> {
            setSelectedFreePeriod(freePeriodsAdapter.getFreePeriods().get(position));
            freePeriodAlertDialog.dismiss();
        });
        freePeriodsRecyclerView.setAdapter(freePeriodsAdapter);
        freePeriodsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        freePeriodsRecyclerView.setHasFixedSize(true);
        freePeriodsAdapter.setFreePeriods(MyApplication.getFreePeriodsFromSharedPrefs(context));
        freePeriodsAdapter.removeToNextPaymentFreePeriod();
        freePeriodAlertDialog = freePeriodAlertDialogBuilder.create();
        initializeChildAlertDialog(context);
    }

    void initializeChildAlertDialog(Context context) {
        childDialog = new PeriodPickerFreePeriodDialog();
        childDialog.setParentDialog(this);
        childDialog.initialize(context);
    }

    public AlertDialog getFreePeriodAlertDialog() {
        return freePeriodAlertDialog;
    }

    public FreePeriodsAdapter getFreePeriodsAdapter() {
        return freePeriodsAdapter;
    }

    public abstract void setAssociatedActivityViewModel(Context context);

    public abstract void setSelectedFreePeriod(FreePeriod freePeriod);
}
