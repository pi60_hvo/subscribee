package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.family

import android.content.Context
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.family.FamilyManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.IconPickerDialog

class FamilyManagingIconPickerDialog(context: Context) : IconPickerDialog(context) {
    private lateinit var familyManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyManagingViewModel = (context as FamilyManagingActivity).subscriptionViewModel
    }

    override fun setActivitySelectedIcon(iconId: Int) {
        familyManagingViewModel.setSelectedIconID(iconId)
    }

    override fun setActivitySelectedColor(color: Int) {
        familyManagingViewModel.setSelectedIconColor(color)
    }

    override fun getActivitySelectedIcon(): Int {
        return familyManagingViewModel.selectedIconIDValue
    }

    override fun getActivitySelectedColor(): Int {
        return familyManagingViewModel.selectedColorIDValue
    }
}