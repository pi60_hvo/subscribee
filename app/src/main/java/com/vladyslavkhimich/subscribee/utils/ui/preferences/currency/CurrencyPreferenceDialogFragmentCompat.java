package com.vladyslavkhimich.subscribee.utils.ui.preferences.currency;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.preference.DialogPreference;
import androidx.preference.PreferenceDialogFragmentCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.currency.CurrencyAdapter;

import java.util.ArrayList;
import java.util.Objects;

public class CurrencyPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat {

    CurrencyAdapter currencyAdapter;
    RecyclerViewEmptySupport currencyRecyclerView;
    RecyclerView.SmoothScroller smoothScroller;
    ArrayList<Currency> allCurrencies;

    public static CurrencyPreferenceDialogFragmentCompat newInstance(String key) {
        CurrencyPreferenceDialogFragmentCompat fragmentCompat = new CurrencyPreferenceDialogFragmentCompat();
        final Bundle bundle = new Bundle(1);
        bundle.putString(ARG_KEY, key);
        fragmentCompat.setArguments(bundle);

        return fragmentCompat;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        view.findViewById(R.id.currencyDialogHeader).setVisibility(View.GONE);

        EditText searchEditText = view.findViewById(R.id.currencyDialogEditText);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 2)
                    currencyAdapter.filterCurrencies(s.toString());
                else
                {
                    currencyAdapter.fallbackToOriginalList();
                    if (currencyAdapter.getLastCheckedPosition() != -1) {
                        if (currencyAdapter.getLastCheckedPosition() < 50)
                        {
                            smoothScroller.setTargetPosition(currencyAdapter.getLastCheckedPosition());
                            Objects.requireNonNull(currencyRecyclerView.getLayoutManager()).startSmoothScroll(smoothScroller);
                        }
                        else
                            ((LinearLayoutManager) Objects.requireNonNull(currencyRecyclerView.getLayoutManager())).scrollToPositionWithOffset(currencyAdapter.getLastCheckedPosition(), 0);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        currencyRecyclerView = view.findViewById(R.id.currencyDialogRecyclerView);
        currencyAdapter = new CurrencyAdapter(getContext());
        currencyRecyclerView.setAdapter(currencyAdapter);
        currencyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        currencyRecyclerView.setEmptyView(view.findViewById(R.id.notFoundContainerCurrencyDialog));
        smoothScroller = new LinearSmoothScroller(Objects.requireNonNull(requireContext())) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
        currencyAdapter.setAllCurrencies(allCurrencies);
        String savedCurrencyShortName = ((CurrencyPreference)getPreference()).getCurrencyShortName();
        Currency savedCurrency = null;
        for (Currency currency : allCurrencies) {
            if (currency.shortName.equals(savedCurrencyShortName)) {
                savedCurrency = currency;
                break;
            }
        }
        if (savedCurrency != null)
            currencyAdapter.setLastCheckedPosition(allCurrencies.indexOf(savedCurrency));
        if (currencyAdapter.getLastCheckedPosition() != -1) {
            if (currencyAdapter.getLastCheckedPosition() < 50)
            {
                smoothScroller.setTargetPosition(currencyAdapter.getLastCheckedPosition());
                Objects.requireNonNull(currencyRecyclerView.getLayoutManager()).startSmoothScroll(smoothScroller);
            }
            else
                ((LinearLayoutManager) Objects.requireNonNull(currencyRecyclerView.getLayoutManager())).scrollToPositionWithOffset(currencyAdapter.getLastCheckedPosition(), 0);
        }
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            Currency selectedCurrency = currencyAdapter.getSelectedCurrency();
            if (selectedCurrency != null) {
                String selectedCurrencyShortName = selectedCurrency.shortName;

                DialogPreference dialogPreference = getPreference();
                if (dialogPreference instanceof CurrencyPreference) {
                    CurrencyPreference currencyPreference = ((CurrencyPreference) dialogPreference);
                    if (currencyPreference.callChangeListener(selectedCurrencyShortName)) {
                        currencyPreference.setCurrencyShortName(selectedCurrencyShortName);
                    }
                }
            }
        }
    }

    public CurrencyAdapter getCurrencyAdapter() {
        return currencyAdapter;
    }

    public void setAllCurrencies(ArrayList<Currency> allCurrencies) {
        this.allCurrencies = allCurrencies;
    }
}
