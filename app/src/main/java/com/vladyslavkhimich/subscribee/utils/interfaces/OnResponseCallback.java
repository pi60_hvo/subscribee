package com.vladyslavkhimich.subscribee.utils.interfaces;

public interface OnResponseCallback<T> {
    void next(T result);
}
