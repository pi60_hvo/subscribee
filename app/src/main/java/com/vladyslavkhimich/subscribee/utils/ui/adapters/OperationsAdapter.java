package com.vladyslavkhimich.subscribee.utils.ui.adapters;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.color.MaterialColors;
import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.items.OperationDateItem;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.items.OperationItem;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.items.OperationListItem;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DoubleHelper;

import java.util.ArrayList;

public class OperationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String MANAGER_OPERATION_NAME = "Manager";

    ArrayList<OperationListItem> operationListItems = new ArrayList<>();

    public OperationsAdapter () {

    }

    @SuppressLint("NotifyDataSetChanged")
    public void setOperationListItems(ArrayList<OperationListItem> operationListItems) {
        this.operationListItems = operationListItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case OperationListItem.TYPE_DATE:
                View operationDateView = inflater.inflate(R.layout.item_operation_date, parent, false);
                viewHolder = new DateViewHolder(operationDateView);
                break;
            case OperationListItem.TYPE_GENERAL:
                View operationView = inflater.inflate(R.layout.item_operation, parent, false);
                viewHolder = new OperationViewHolder(operationView);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case OperationListItem.TYPE_DATE:
                OperationDateItem operationDateItem = (OperationDateItem) operationListItems.get(position);
                DateViewHolder dateViewHolder = (DateViewHolder) holder;
                dateViewHolder.operationDateTextView.setText(DateHelper.getDayMonthWeekDayString(operationDateItem.getDate()));
                break;
            case OperationListItem.TYPE_GENERAL:
                OperationItem operationItem = (OperationItem) operationListItems.get(position);
                OperationViewHolder operationViewHolder = (OperationViewHolder) holder;

                operationViewHolder.operationSubscriptionTextView.setText(operationItem.getOperation().getSubscriptionName());

                if (operationItem.getOperation().getMemberName() == null || operationItem.getOperation().getMemberName().equals(MANAGER_OPERATION_NAME)) {
                operationViewHolder.operationTypeTextView.setText(R.string.withdrawal);
                }
                else {
                operationViewHolder.operationTypeTextView.setText(HtmlCompat.fromHtml(
                        operationViewHolder.operationTypeTextView.getContext()
                                .getString(
                                        R.string.payment_from,
                                        operationItem.getOperation().getMemberName()
                                ), HtmlCompat.FROM_HTML_MODE_LEGACY));
                }

                operationViewHolder.operationImageViewContainer.setBackgroundResource(R.drawable.shape_oval_transparent);
                GradientDrawable backgroundDrawable = (GradientDrawable) operationViewHolder.operationImageViewContainer.getBackground();
                backgroundDrawable.mutate();
                backgroundDrawable.setColor(Color.parseColor(operationItem.getOperation().getIconColor()));
                operationViewHolder.operationImageViewContainer.setBackground(backgroundDrawable);
                operationViewHolder.operationImageView.setImageResource(ResourcesHelper.getImageIDFromName(operationViewHolder.operationImageView.getContext(), operationItem.getOperation().getIconName()));

                if (operationItem.getOperation().getPrice() < 0) {
                    operationViewHolder.operationPriceTextView.setText(
                            operationViewHolder.operationPriceTextView.getContext()
                                    .getString(
                                            R.string.operation_price_minus,
                                            operationItem.getOperation().getCurrencySymbol(),
                                            DoubleHelper.getFormattedDoubleOrIntegerUnsigned(operationItem.getOperation().getPrice())));
                    operationViewHolder.operationPriceTextView.setTextColor(holder.itemView.getResources().getColor(R.color.refusal_red));
                }
                else if (operationItem.getOperation().getPrice() == 0.0) {
                    operationViewHolder.operationPriceTextView.setText(
                            operationViewHolder.operationPriceTextView.getContext()
                                .getString(R.string.operation_price_zero,
                                        operationItem.getOperation().getCurrencySymbol())
                    );
                    operationViewHolder.operationPriceTextView.setTextColor(MaterialColors.getColor(holder.itemView.getContext(), R.attr.colorOnSecondary, holder.itemView.getResources().getColor(R.color.sub_gray_font)));
                }
                else {
                    operationViewHolder.operationPriceTextView.setText(
                            operationViewHolder.operationPriceTextView.getContext()
                                    .getString(
                                            R.string.operation_price_plus,
                                            operationItem.getOperation().getCurrencySymbol(),
                                            DoubleHelper.getFormattedDoubleOrIntegerUnsigned(operationItem.getOperation().getPrice())));
                    operationViewHolder.operationPriceTextView.setTextColor(holder.itemView.getResources().getColor(R.color.agree_green));
                }
                /*OperationItem operationItem = (OperationItem) operationListItems.get(position);
                OperationViewHolder operationViewHolder = (OperationViewHolder) holder;
                operationViewHolder.operationImageView.setImageResource(operationItem.getOperation().ImageID);
                operationViewHolder.operationSubscriptionTextView.setText(operationItem.getOperation().Name);
                if (operationItem.getOperation().IsPersonal) {
                    operationViewHolder.operationTypeTextView.setText(HtmlCompat.fromHtml(holder.itemView.getResources().getString(R.string.payment_from) + "<b>" + operationItem.getOperation().PayerName + "</b>", HtmlCompat.FROM_HTML_MODE_COMPACT));
                }
                else {
                    operationViewHolder.operationTypeTextView.setText(R.string.withdrawal);
                }
                String operationPriceString = "";
                if (operationItem.getOperation().Price > 0) {
                    operationPriceString += "+₴ ";
                    operationViewHolder.operationPriceTextView.setTextColor(holder.itemView.getResources().getColor(R.color.agree_green));
                }
                else {
                    operationPriceString += "-₴ ";
                    operationViewHolder.operationPriceTextView.setTextColor(holder.itemView.getResources().getColor(R.color.refusal_red));
                }
                operationPriceString += DoubleHelper.getFormattedDoubleOrIntegerUnsigned(operationItem.getOperation().Price);
                operationViewHolder.operationPriceTextView.setText(operationPriceString);
*/                break;
        }
    }

    @Override
    public int getItemCount() {
        return operationListItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return operationListItems.get(position).getType();
    }

    class DateViewHolder extends RecyclerView.ViewHolder {
        TextView operationDateTextView;

        public DateViewHolder(@NonNull View itemView) {
            super(itemView);
            operationDateTextView = itemView.findViewById(R.id.operationDateTextView);
        }
    }

    class OperationViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout operationImageViewContainer;
        ImageView operationImageView;
        TextView operationSubscriptionTextView;
        TextView operationTypeTextView;
        TextView operationPriceTextView;

        public OperationViewHolder(@NonNull View itemView) {
            super(itemView);
            operationImageViewContainer = itemView.findViewById(R.id.operation_image_view_container);
            operationImageView = itemView.findViewById(R.id.operation_image_view);
            operationSubscriptionTextView = itemView.findViewById(R.id.operationSubscriptionTextView);
            operationTypeTextView = itemView.findViewById(R.id.operationTypeTextView);
            operationPriceTextView = itemView.findViewById(R.id.operationPriceTextView);
        }
    }
}
