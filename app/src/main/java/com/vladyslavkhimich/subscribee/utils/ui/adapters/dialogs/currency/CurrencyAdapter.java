package com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.currency;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;

import java.util.ArrayList;
import java.util.Collections;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder> {
    ArrayList<Currency> allCurrencies = new ArrayList<>();
    ArrayList<Currency> currencies = new ArrayList<>();
    String[] allCurrenciesNames;
    Currency selectedCurrency;
    Context context;
    int lastCheckedPosition = -1;

    public CurrencyAdapter(Context context) {
        this.context = context;
    }

    public void setCurrencies(ArrayList<Currency> currencies) {
        this.currencies = currencies;
        notifyDataSetChanged();
    }

    public ArrayList<Currency> getAllCurrencies() {
        return allCurrencies;
    }

    public void setAllCurrencies(ArrayList<Currency> allCurrencies) {
        Collections.sort(allCurrencies, (o1, o2) -> context.getString(ResourcesHelper.getStringIDFromName(context, o1.name)).compareTo(context.getString(ResourcesHelper.getStringIDFromName(context, o2.name))));
        setAllCurrenciesNames(allCurrencies);
        setCurrencies(allCurrencies);
        this.allCurrencies = allCurrencies;
    }

    public void setAllCurrenciesNames(ArrayList<Currency> allCurrencies) {
        allCurrenciesNames = new String[allCurrencies.size()];
        for (int i = 0; i < allCurrenciesNames.length; i++)
            allCurrenciesNames[i] = context.getString(ResourcesHelper.getStringIDFromName(context, allCurrencies.get(i).name));
    }

    public Currency getSelectedCurrency() {
        return selectedCurrency;
    }

    public void setLastCheckedPosition(int lastCheckedPosition) {
        int copyOfLastCheckedPosition = this.lastCheckedPosition;
        this.lastCheckedPosition = lastCheckedPosition;
        notifyItemChanged(copyOfLastCheckedPosition);
        notifyItemChanged(lastCheckedPosition);
    }

    public int getLastCheckedPosition() {
        return lastCheckedPosition;
    }

    public void filterCurrencies(String searchText) {
        ArrayList<Currency> filteredList = new ArrayList<>();
        for (int i = 0; i < allCurrenciesNames.length; i++) {
            if (allCurrenciesNames[i].trim().toLowerCase().contains(searchText.trim().toLowerCase()))
                filteredList.add(allCurrencies.get(i));
        }
        setLastCheckedPosition(filteredList.indexOf(selectedCurrency));
        setCurrencies(filteredList);
    }

    public void fallbackToOriginalList() {
        if (currencies.size() != allCurrencies.size()) {
            currencies = allCurrencies;
            setLastCheckedPosition(allCurrencies.indexOf(selectedCurrency));
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View currencyView = inflater.inflate(R.layout.item_dialog_currency, parent, false);
        return new CurrencyViewHolder(currencyView);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder holder, int position) {
        Currency currency = currencies.get(position);
        holder.currencyNameTextView.setText(ResourcesHelper.getStringIDFromName(holder.currencyNameTextView.getContext(), currency.name));
        holder.currencySymbolTextView.setText(currency.symbol);
        holder.currencyRadioButton.setChecked(position == lastCheckedPosition);
    }

    @Override
    public int getItemCount() {
        return currencies.size();
    }

    public class CurrencyViewHolder extends RecyclerView.ViewHolder {
        RadioButton currencyRadioButton;
        TextView currencyNameTextView;
        TextView currencySymbolTextView;

        public CurrencyViewHolder(@NonNull View itemView) {
            super(itemView);
            currencyRadioButton = (RadioButton) itemView.findViewById(R.id.currencyDialogRadioButton);
            currencyNameTextView = (TextView) itemView.findViewById(R.id.currencyNameDialogTextView);
            currencySymbolTextView = (TextView) itemView.findViewById(R.id.currencySymbolTextView);
            itemView.setOnClickListener(v -> {
                setLastCheckedPosition(getAdapterPosition());
                selectedCurrency = currencies.get(getAdapterPosition());
            });
        }
    }
}
