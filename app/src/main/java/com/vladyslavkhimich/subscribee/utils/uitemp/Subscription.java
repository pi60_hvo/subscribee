package com.vladyslavkhimich.subscribee.utils.uitemp;

import java.util.Date;

public class Subscription {
    public int imageID;
    public String name;
    public Double price;
    public Date nextPaymentDate;

    public Subscription() {

    }

    public Subscription(int imageID, String name, Double price, Date nextPaymentDate) {
        this.imageID = imageID;
        this.name = name;
        this.price = price;
        this.nextPaymentDate = nextPaymentDate;
    }
}
