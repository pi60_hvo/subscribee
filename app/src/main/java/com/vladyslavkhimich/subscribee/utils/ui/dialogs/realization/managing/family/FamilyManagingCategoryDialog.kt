package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.family

import android.content.Context
import com.vladyslavkhimich.subscribee.database.entities.Category
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.family.FamilyManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CategoryDialog
import java.util.ArrayList

class FamilyManagingCategoryDialog : CategoryDialog() {
    private lateinit var familyManagingViewModel: SubscriptionViewModel
    private var categoriesListItemsCount: Int = 0
    private var categoriesSetCount: Int = 0

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyManagingViewModel = (context as FamilyManagingActivity).subscriptionViewModel
    }

    override fun setAdapterCategories(categories: MutableList<Category>?) {
        categoriesSetCount++
        categoryAdapter.categories = categories as ArrayList<Category>?
        if (categoriesSetCount > 1 && categories!!.size.compareTo(categoriesListItemsCount) == 1)
            scrollRecyclerViewToBottom()
        categoriesListItemsCount = categories!!.size
    }

    override fun setSelectedCategory(category: Category?) {
        familyManagingViewModel.setSelectedCategory(category)
    }

    override fun getSelectedCategory(): Category {
        return familyManagingViewModel.selectedCategoryObject
    }

    override fun resetSelectedCategoryIfDeleted(category: Category?) {
        if (familyManagingViewModel.selectedCategory.value == category)
            familyManagingViewModel.setSelectedCategory(null)
    }
}