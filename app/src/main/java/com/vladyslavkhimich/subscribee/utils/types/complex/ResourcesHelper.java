package com.vladyslavkhimich.subscribee.utils.types.complex;

import android.content.Context;

import com.vladyslavkhimich.subscribee.app.MyApplication;
import com.vladyslavkhimich.subscribee.utils.types.primitives.IntegerHelper;

import java.util.ArrayList;

public class ResourcesHelper {
    public static int getStringIDFromName(Context context, String name) {
        return context.getResources().getIdentifier(name, "string", context.getPackageName());
    }

    public static int getImageIDFromName(Context context, String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName()) != 0 ? context.getResources().getIdentifier(name, "drawable", context.getPackageName()) : context.getResources().getIdentifier("ic_at", "drawable", context.getPackageName());
    }

    public static int getColorIDFromName(Context context, String name) {
        return context.getResources().getIdentifier(name, "color", context.getPackageName());
    }

    public static int getRandomIconID(Context context) {
        return getImageIDFromName(context, MyApplication.getPickerIconsFromSharedPrefs(context)[IntegerHelper.getRandomIntegerInRange(0, MyApplication.ICONS_COUNT - 1)]);
    }

    public static int getRandomColor(Context context) {
        return getColorIDFromName(context, MyApplication.getPickerColorsFromSharedPrefs(context)[IntegerHelper.getRandomIntegerInRange(0, MyApplication.COLORS_COUNT - 1)]);
    }

    public static ArrayList<Integer> convertIconNamesToIntArrayList(Context context, ArrayList<String> iconNames) {
        ArrayList<Integer> resultArray = new ArrayList<Integer>();
        for (String iconName : iconNames) {
            resultArray.add(getImageIDFromName(context, iconName));
        }
        return resultArray;
    }

    public static String getResourceNameFromID(Context context, int id) {
        return context.getResources().getResourceEntryName(id);
    }
}
