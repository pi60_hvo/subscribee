package com.vladyslavkhimich.subscribee.utils.ui.dialogs;

import android.annotation.SuppressLint;

import com.google.android.material.datepicker.RangeDateSelector;

import java.util.Calendar;
import java.util.TimeZone;

@SuppressLint("RestrictedApi")
public class MonthRangeDateSelector extends RangeDateSelector {
    @Override
    public void select(long selection) {
        Calendar selectedDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        selectedDate.setTimeInMillis(selection);
        int selectedDayOfMonth = selectedDate.get(Calendar.DAY_OF_MONTH);
        Calendar firstDayOfMonth = (Calendar) selectedDate.clone();
        firstDayOfMonth.add(Calendar.DAY_OF_MONTH, -selectedDayOfMonth + 1);
        long lowerBound = firstDayOfMonth.getTimeInMillis();
        Calendar lastDayOfMonth = (Calendar)selectedDate.clone();
        lastDayOfMonth.add(Calendar.MONTH, 1);
        lastDayOfMonth.add(Calendar.DAY_OF_MONTH, -selectedDayOfMonth);
        long upperBound = lastDayOfMonth.getTimeInMillis();
        super.select(lowerBound);
        super.select(upperBound);
    }
}
