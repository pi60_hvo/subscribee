package com.vladyslavkhimich.subscribee.utils.ui.preferences.currency;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.preference.DialogPreference;

import com.vladyslavkhimich.subscribee.R;

public class CurrencyPreference extends DialogPreference {
    String currencyShortName;
    int dialogLayoutResID = R.layout.dialog_currency_custom;

    public CurrencyPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public CurrencyPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, defStyleAttr);
    }

    public CurrencyPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CurrencyPreference(Context context) {
        this(context, null);
    }

    public String getCurrencyShortName() {
        return currencyShortName;
    }

    public void setCurrencyShortName(String currencyShortName) {
        this.currencyShortName = currencyShortName;
        persistString(currencyShortName);
        setSummary(currencyShortName);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    @Override
    protected void onSetInitialValue(@Nullable @org.jetbrains.annotations.Nullable Object defaultValue) {
        setCurrencyShortName(defaultValue == null ? getPersistedString(currencyShortName) : (String) defaultValue);
    }

    @Override
    public int getDialogLayoutResource() {
        return dialogLayoutResID;
    }
}
