package com.vladyslavkhimich.subscribee.utils.uitemp;

import java.util.Date;

public class ArchivedSubscription extends Subscription {
    public Boolean isPersonal;

    public ArchivedSubscription() {

    }

    public ArchivedSubscription(int imageID, String name, Double price, Date nextPayment, Boolean isPersonal) {
        super(imageID, name, price, nextPayment);
        this.isPersonal = isPersonal;
    }
}
