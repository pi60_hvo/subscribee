package com.vladyslavkhimich.subscribee.utils.ui.dialogs.general;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager2.widget.ViewPager2;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionActivity;
import com.vladyslavkhimich.subscribee.utils.types.complex.UIHelper;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.icon.IconPickerPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public abstract class IconPickerDialog extends Dialog {
    IconPickerPagerAdapter iconPickerPagerAdapter;
    TabLayout iconPickerTabLayout;
    ViewPager2 iconPickerViewPager2;
    Button iconPickerPositiveButton;
    Button iconPickerNegativeButton;
    Button iconPickerBackButton;
    ImageView iconImageView;
    ConstraintLayout iconBackgroundConstraintLayout;
    Context context;
    int selectedIcon;
    int selectedColor;

    public IconPickerDialog(@NonNull Context context) {
        super(context, R.style.CustomDialogStyle);
        this.context = context;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_icon_picker);
        setAssociatedActivityViewModel(context);
        initializeUI();
        setTabLayout();
        setButtons();
        getWindow().setLayout((int) (UIHelper.getScreenWidth((SubscriptionActivity)context) * .9), (int) (UIHelper.getScreenHeight((SubscriptionActivity)context) * .9));
        setSelectedIcon(getActivitySelectedIcon());
        setSelectedColor(getActivitySelectedColor());
    }

    void initializeUI() {
        iconBackgroundConstraintLayout = findViewById(R.id.iconBackgroundConstraintLayout);
        iconImageView = findViewById(R.id.personalSubscriptionIconImageView);
    }

    void setTabLayout() {
        iconPickerPagerAdapter = new IconPickerPagerAdapter(((SubscriptionActivity) context).getSupportFragmentManager(), ((SubscriptionActivity) context).getLifecycle(), resourceId -> {
            iconImageView.setImageResource(resourceId);
            selectedIcon = resourceId;
        }, (color, isSubColor) -> {
            if (isSubColor) {
                iconPickerBackButton.setVisibility(View.VISIBLE);
                iconPickerNegativeButton.setVisibility(View.INVISIBLE);
                GradientDrawable backgroundDrawable = (GradientDrawable) iconBackgroundConstraintLayout.getBackground();
                backgroundDrawable.setColor(color);
                iconBackgroundConstraintLayout.setBackground(backgroundDrawable);
                selectedColor = color;
            }
        }, context);
        iconPickerTabLayout = findViewById(R.id.dialogIconTabLayout);
        iconPickerViewPager2 = findViewById(R.id.dialogIconViewPager);
        iconPickerViewPager2.setAdapter(iconPickerPagerAdapter);
        iconPickerViewPager2.setOffscreenPageLimit(2);
        new TabLayoutMediator(iconPickerTabLayout, iconPickerViewPager2, (tab, position) -> {
            switch(position) {
                case 0:
                    tab.setText(R.string.icon);
                    break;
                case 1:
                    tab.setText(R.string.color);
                    break;
            }
        }).attach();
    }

    void setButtons() {
        iconPickerPositiveButton = findViewById(R.id.dialogIconPositiveButton);
        iconPickerNegativeButton = findViewById(R.id.dialogIconNegativeButton);
        iconPickerBackButton = findViewById(R.id.dialogIconBackButton);
        setButtonsOnClickListeners();
    }

    void setButtonsOnClickListeners() {
        iconPickerPositiveButton.setOnClickListener(v -> {
            setActivitySelectedColor(selectedColor);
            setActivitySelectedIcon(selectedIcon);
            dismiss();
        });
        iconPickerNegativeButton.setOnClickListener(v -> dismiss());
        iconPickerBackButton.setOnClickListener(v -> {
            iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().setColorsArrayList(iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().mainColorsArrayList);
            iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().setIsSubColorCollection(false);
            iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().setCurrentSelectedPosition(iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().getPreviousSelectedPosition());
            iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().notifyDataSetChanged();
            v.setVisibility(View.INVISIBLE);
            iconPickerNegativeButton.setVisibility(View.VISIBLE);
        });
    }

    public void setSelectedIcon(int selectedIcon) {
        this.selectedIcon = selectedIcon;
        iconPickerPagerAdapter.getIconPickerFragment().getIconPickerIconAdapter().setSelectedPosition(iconPickerPagerAdapter.getIconPickerFragment().getIconPickerIconAdapter().getItemPosition(selectedIcon));
        iconPickerPagerAdapter.getIconPickerFragment().getIconPickerIconAdapter().notifyDataSetChanged();
        iconImageView.setImageResource(selectedIcon);
    }

    public void setSelectedColor(int selectedColor) {
        this.selectedColor = selectedColor;
        iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().setCurrentSelectedPosition(iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().getItemPosition(selectedColor));
        iconPickerPagerAdapter.getColorPickerFragment().getIconPickerColorAdapter().notifyDataSetChanged();
    }

    public abstract void setAssociatedActivityViewModel(Context context);

    public abstract void setActivitySelectedIcon(int iconId);

    public abstract void setActivitySelectedColor(int color);

    public abstract int getActivitySelectedIcon();

    public abstract int getActivitySelectedColor();
}
