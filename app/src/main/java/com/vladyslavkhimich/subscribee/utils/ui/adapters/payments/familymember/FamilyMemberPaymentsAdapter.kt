package com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.familymember

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.database.entities.Cycle
import com.vladyslavkhimich.subscribee.models.OperationAndPaymentDate
import com.vladyslavkhimich.subscribee.models.Payment
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper
import com.vladyslavkhimich.subscribee.utils.ui.diff.PaymentsDiffUtilCallback
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnFamilyPaymentClickListenerCallback
import java.util.*
import kotlin.collections.ArrayList

class FamilyMemberPaymentsAdapter (val onItemClickListenerCallback: OnFamilyPaymentClickListenerCallback,
                                   val memberPosition: Int,
                                   val cycle: Cycle?) : RecyclerView.Adapter<FamilyMemberPaymentsAdapter.FamilyMemberPaymentViewHolder>() {
    var memberPayments: ArrayList<Payment> = ArrayList()

    fun mapAndSetMemberPayments(operationAndPaymentDate: SortedSet<OperationAndPaymentDate>, cycleDate: Date?) {
        val mappedList = ArrayList<Payment>()
        operationAndPaymentDate.forEach { item -> mappedList.add(Payment(item.paymentDate.paymentDate, item.operation != null)) }
        val nextCycleDates = DateHelper.getCycleDates(cycleDate, cycle, 12, false)
        nextCycleDates.forEach {
            item -> mappedList.add(Payment(item, false))
        }
        val paymentsDiffCallback = PaymentsDiffUtilCallback(memberPayments, mappedList)
        val diffResult = DiffUtil.calculateDiff(paymentsDiffCallback)
        diffResult.dispatchUpdatesTo(this)
        memberPayments = mappedList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FamilyMemberPaymentViewHolder {
        val familyMemberPaymentsView = LayoutInflater.from(parent.context).inflate(R.layout.item_payment, parent, false)
        return FamilyMemberPaymentViewHolder(familyMemberPaymentsView)
    }

    override fun onBindViewHolder(holder: FamilyMemberPaymentViewHolder, position: Int) {
        val payment = memberPayments[position]

        holder.paymentCheckBox.isChecked = payment.isChecked
        holder.paymentDateTextView.text = DateHelper.getNextPaymentDate(payment.paymentDate)
    }

    override fun getItemCount(): Int {
        return memberPayments.size
    }

    inner class FamilyMemberPaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val paymentDateTextView: TextView = itemView.findViewById(R.id.paymentItemDateTextView)
        val paymentCheckBox: CheckBox = itemView.findViewById(R.id.paymentItemCheckBox)

        init {
            paymentCheckBox.setOnClickListener {
                onItemClickListenerCallback.onItemClicked(it, adapterPosition, memberPosition)
            }
        }
    }
}