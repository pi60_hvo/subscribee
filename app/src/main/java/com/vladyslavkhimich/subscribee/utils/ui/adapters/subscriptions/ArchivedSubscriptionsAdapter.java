package com.vladyslavkhimich.subscribee.utils.ui.adapters.subscriptions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.utils.enums.SubscriptionSortings;
import com.vladyslavkhimich.subscribee.utils.interfaces.SubscriptionSorting;
import com.vladyslavkhimich.subscribee.utils.others.sorting.factory.SortingFactory;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ViewAnimator;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ArchivedSubscriptionsAdapter extends RecyclerView.Adapter<ArchivedSubscriptionsAdapter.ArchivedSubscriptionViewHolder> {
    ArrayList<Subscription> archivedSubscriptions = new ArrayList<>();
    ArrayList<Integer> selectedSubscriptions = new ArrayList<>();
    OnItemClickListenerCallback onLongItemClickListenerCallback;
    OnItemClickListenerCallback onItemClickListenerCallback;

    public ArchivedSubscriptionsAdapter(OnItemClickListenerCallback onLongItemClickListenerCallback, OnItemClickListenerCallback onItemClickListenerCallback) {
        this.onLongItemClickListenerCallback = onLongItemClickListenerCallback;
        this.onItemClickListenerCallback = onItemClickListenerCallback;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setArchivedSubscriptions(ArrayList<Subscription> archivedSubscriptions) {
        this.archivedSubscriptions = archivedSubscriptions;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void sortSubscriptions(SubscriptionSortings subscriptionSortings) {
        SubscriptionSorting sorter = SortingFactory.Companion.getSorting(subscriptionSortings);
        archivedSubscriptions = new ArrayList<>(sorter.sort(archivedSubscriptions));
        notifyDataSetChanged();
    }

    public ArrayList<Subscription> getArchivedSubscriptions() {
        return archivedSubscriptions;
    }

    public void addItemSelected(int position) {
        selectedSubscriptions.add(position);
        notifyItemChanged(position);
    }

    public void removeItemSelected(int position) {
        selectedSubscriptions.remove((Integer) position);
        notifyItemChanged(position);
    }

    public boolean checkIfItemSelected(int position) {
        return selectedSubscriptions.contains(position);
    }

    public int getSelectedCount() {
        return selectedSubscriptions.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearSelectedArrayList() {
        selectedSubscriptions.clear();
        notifyDataSetChanged();
    }

    public ArrayList<Integer> getSelectedSubscriptionsIDs() {
        ArrayList<Integer> selectedSubscriptionsIDs = new ArrayList<>();
        for (Integer index : selectedSubscriptions) {
            selectedSubscriptionsIDs.add(archivedSubscriptions.get(index).subscriptionID);
        }
        return selectedSubscriptionsIDs;
    }

    @NonNull
    @Override
    public ArchivedSubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View archivedSubscriptionView = inflater.inflate(R.layout.item_subscription_archived, parent, false);
        return new ArchivedSubscriptionViewHolder(archivedSubscriptionView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArchivedSubscriptionViewHolder holder, int position) {
        Subscription archivedSubscription = archivedSubscriptions.get(position);

        holder.archivedNameTextView.setText(archivedSubscription.name);
        if (archivedSubscription.isFamily) {
            holder.archivedTypeTextView.setText(holder.itemView.getContext().getString(R.string.family_item));
        }
        else if (archivedSubscription.isFamilyPart) {
            holder.archivedTypeTextView.setText(holder.itemView.getContext().getString(R.string.part_of_family));
        }
        else {
            holder.archivedTypeTextView.setText(holder.itemView.getContext().getString(R.string.personal_item));
        }
        holder.archivedPriceTextView.setText(
                holder.itemView.getContext().
                        getString(
                                R.string.price,
                                new DecimalFormat("#0.00").format(archivedSubscription.price),
                                DatabaseRepository.getInstance(holder.archivedIconContainer.getContext()).getCurrencyShortNameByID(archivedSubscription.currencyID)));
        holder.archivedPaymentTextView.setText(holder.itemView.getContext().getString(R.string.next_payment, DateHelper.getNextPaymentDate(archivedSubscription.nextPaymentDate)));

        boolean isSelected = checkIfItemSelected(position);

        if (isSelected) {
            ViewAnimator.flipImageView(holder.archivedIconContainer.getContext(), holder.archivedIconContainer, holder.archivedImageView);
        }
        else {
            holder.archivedIconContainer.setBackgroundResource(R.drawable.shape_oval_transparent);
            GradientDrawable backgroundDrawable = (GradientDrawable) holder.archivedIconContainer.getBackground();
            backgroundDrawable.mutate();
            backgroundDrawable.setColor(Color.parseColor(archivedSubscription.iconColor));
            holder.archivedIconContainer.setBackground(backgroundDrawable);

            holder.archivedImageView.setVisibility(View.VISIBLE);
            holder.archivedImageView.setImageResource(ResourcesHelper.getImageIDFromName(holder.archivedIconContainer.getContext(), archivedSubscription.icon));
        }

        holder.archivedContainer.setSelected(isSelected);
    }

    @Override
    public int getItemCount() {
        return archivedSubscriptions.size();
    }

    public class ArchivedSubscriptionViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout archivedIconContainer;
        ImageView archivedImageView;
        TextView archivedNameTextView;
        TextView archivedTypeTextView;
        TextView archivedPriceTextView;
        TextView archivedPaymentTextView;
        ConstraintLayout archivedContainer;

        public ArchivedSubscriptionViewHolder(View itemView) {
            super(itemView);
            archivedIconContainer = itemView.findViewById(R.id.archivedSubscriptionItemIconContainer);
            archivedImageView = itemView.findViewById(R.id.archivedSubscriptionItemIconImageView);
            archivedNameTextView = itemView.findViewById(R.id.archivedNameTextView);
            archivedTypeTextView = itemView.findViewById(R.id.archivedTypeTextView);
            archivedPriceTextView = itemView.findViewById(R.id.archivedPriceTextView);
            archivedPaymentTextView = itemView.findViewById(R.id.archivedPaymentTextView);
            archivedContainer = itemView.findViewById(R.id.archivedSubscriptionContainer);
            itemView.setOnLongClickListener(v -> {
                onLongItemClickListenerCallback.onItemClicked(v, getAdapterPosition());
                return true;
            });
            itemView.setOnClickListener(v -> onItemClickListenerCallback.onItemClicked(v, getAdapterPosition()));
        }
    }
}
