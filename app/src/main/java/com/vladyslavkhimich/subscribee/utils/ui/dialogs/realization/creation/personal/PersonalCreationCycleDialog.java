package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.personal.PersonalCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CycleDialog;

import java.util.ArrayList;
import java.util.List;

public class PersonalCreationCycleDialog extends CycleDialog {
    SubscriptionViewModel personalCreationViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalCreationViewModel = ((PersonalCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCycles(List<Cycle> cycles) {
        getCycleAdapter().setCycles((ArrayList<Cycle>) cycles);
    }

    @Override
    public void setSelectedCycle(Cycle cycle) {
        personalCreationViewModel.setSelectedCycle(cycle);
    }
}
