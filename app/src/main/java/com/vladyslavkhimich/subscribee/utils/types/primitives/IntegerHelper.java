package com.vladyslavkhimich.subscribee.utils.types.primitives;

public class IntegerHelper {
    public static Integer tryParse(String stringToParse) {
        try {
            return Integer.parseInt(stringToParse);
        }
        catch (Exception exception) {
            return null;
        }
    }

    public static int getRandomIntegerInRange(int min, int max) {
        return min + (int)(Math.random() * (max - min) + 1);
    }
}
