package com.vladyslavkhimich.subscribee.utils.ui.interfaces

import android.view.View

interface OnFamilyPaymentClickListenerCallback {
    fun onItemClicked(view: View, adapterPosition: Int, familyMemberPosition: Int)
}