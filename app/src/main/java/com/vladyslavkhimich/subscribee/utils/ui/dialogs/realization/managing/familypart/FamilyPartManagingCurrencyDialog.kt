package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.familypart

import android.content.Context
import com.vladyslavkhimich.subscribee.database.entities.Currency
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart.FamilyPartManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CurrencyDialog
import java.util.ArrayList

class FamilyPartManagingCurrencyDialog : CurrencyDialog() {
    private lateinit var familyPartManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyPartManagingViewModel = (context as FamilyPartManagingActivity).subscriptionViewModel
    }

    override fun setAdapterCurrencies(currencies: MutableList<Currency>?) {
        currencyAdapter.allCurrencies = currencies as ArrayList<Currency>?
    }

    override fun setSelectedCurrency(currency: Currency?) {
        if (currency != null)
            familyPartManagingViewModel.setSelectedCurrency(currency)
    }
}