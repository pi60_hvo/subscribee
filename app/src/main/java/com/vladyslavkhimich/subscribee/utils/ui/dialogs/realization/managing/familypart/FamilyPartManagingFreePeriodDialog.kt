package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.familypart

import android.content.Context
import com.vladyslavkhimich.subscribee.models.FreePeriod
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart.FamilyPartManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.FreePeriodDialog

class FamilyPartManagingFreePeriodDialog : FreePeriodDialog() {
    private lateinit var familyPartManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        familyPartManagingViewModel = (context as FamilyPartManagingActivity).subscriptionViewModel
    }

    override fun setSelectedFreePeriod(freePeriod: FreePeriod?) {
        familyPartManagingViewModel.setSelectedFreePeriod(freePeriod)
    }
}