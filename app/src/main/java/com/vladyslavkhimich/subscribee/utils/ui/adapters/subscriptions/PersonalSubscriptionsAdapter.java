package com.vladyslavkhimich.subscribee.utils.ui.adapters.subscriptions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.personal.PersonalManagingActivity;
import com.vladyslavkhimich.subscribee.utils.enums.SubscriptionSortings;
import com.vladyslavkhimich.subscribee.utils.interfaces.SubscriptionSorting;
import com.vladyslavkhimich.subscribee.utils.others.sorting.factory.SortingFactory;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ViewAnimator;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PersonalSubscriptionsAdapter extends RecyclerView.Adapter<PersonalSubscriptionsAdapter.PersonalSubscriptionViewHolder> {

    ArrayList<Subscription> personalSubscriptions = new ArrayList<>();
    ArrayList<Integer> selectedSubscriptions = new ArrayList<>();
    OnItemClickListenerCallback onItemLongClickListenerCallback;

    public PersonalSubscriptionsAdapter(OnItemClickListenerCallback onItemLongClickListenerCallback) {
        this.onItemLongClickListenerCallback = onItemLongClickListenerCallback;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setPersonalSubscriptions(ArrayList<Subscription> personalSubscriptions) {
        this.personalSubscriptions = personalSubscriptions;
        //notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void sortSubscriptions(SubscriptionSortings sortings) {
        SubscriptionSorting sorter = SortingFactory.Companion.getSorting(sortings);
        personalSubscriptions = new ArrayList<>(sorter.sort(personalSubscriptions));
        notifyDataSetChanged();
    }

    public void addItemSelected(int position) {
        selectedSubscriptions.add(position);
        notifyItemChanged(position);
    }

    public void removeItemSelected(int position) {
        selectedSubscriptions.remove((Integer) position);
        notifyItemChanged(position);
    }

    public boolean checkIfItemSelected(int position) {
        return selectedSubscriptions.contains(position);
    }

    public int getSelectedCount() {
        return selectedSubscriptions.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearSelectedArrayList() {
        selectedSubscriptions.clear();
        notifyDataSetChanged();
    }

    public ArrayList<Integer> getSelectedSubscriptionsIDs() {
        ArrayList<Integer> selectedSubscriptionsIDs = new ArrayList<>();
        for (Integer index : selectedSubscriptions) {
            selectedSubscriptionsIDs.add(personalSubscriptions.get(index).subscriptionID);
        }
        return selectedSubscriptionsIDs;
    }

    @NonNull
    @Override
    public PersonalSubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View personalSubscriptionView = inflater.inflate(R.layout.item_subscription_personal, parent, false);
        return new PersonalSubscriptionViewHolder(personalSubscriptionView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonalSubscriptionViewHolder holder, int position) {
        Subscription personalSubscription = personalSubscriptions.get(position);

        holder.personalNameTextView.setText(personalSubscription.name);
        Category associatedCategory = DatabaseRepository.getInstance(holder.personalIconContainer.getContext()).getCategoryByID(personalSubscription.categoryID);
        if (associatedCategory.isUserCreated)
            holder.personalCategoryTextView.setText(associatedCategory.name);
        else
            holder.personalCategoryTextView.setText(ResourcesHelper.getStringIDFromName(holder.personalIconContainer.getContext(), associatedCategory.name));
        holder.personalPriceTextView.setText(
                holder.itemView.getContext().
                        getString(
                                R.string.price,
                                new DecimalFormat("#0.00").format(personalSubscription.price),
                                DatabaseRepository.getInstance(holder.personalIconContainer.getContext()).getCurrencyShortNameByID(personalSubscription.currencyID)));
        holder.personalPaymentTextView.setText(holder.itemView.getContext().getString(R.string.next_payment, DateHelper.getNextPaymentDate(personalSubscription.nextPaymentDate)));
        boolean isSelected = checkIfItemSelected(position);

        if (isSelected) {
            ViewAnimator.flipImageView(holder.personalIconContainer.getContext(), holder.personalIconContainer, holder.personalImageView);
        }
        else {
            holder.personalIconContainer.setBackgroundResource(R.drawable.shape_oval_transparent);
            GradientDrawable backgroundDrawable = (GradientDrawable) holder.personalIconContainer.getBackground();
            backgroundDrawable.mutate();
            backgroundDrawable.setColor(Color.parseColor(personalSubscription.iconColor));
            holder.personalIconContainer.setBackground(backgroundDrawable);

            holder.personalImageView.setVisibility(View.VISIBLE);
            holder.personalImageView.setImageResource(ResourcesHelper.getImageIDFromName(holder.personalIconContainer.getContext(), personalSubscription.icon));
        }

        holder.personalContainer.setSelected(isSelected);
    }

    @Override
    public int getItemCount() {
        return personalSubscriptions.size();
    }

    public class PersonalSubscriptionViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout personalIconContainer;
        ImageView personalImageView;
        TextView personalNameTextView;
        TextView personalCategoryTextView;
        TextView personalPriceTextView;
        TextView personalPaymentTextView;
        ConstraintLayout personalContainer;

        public PersonalSubscriptionViewHolder(View itemView) {
            super(itemView);
            personalIconContainer = itemView.findViewById(R.id.personalSubscriptionIconContainer);
            personalImageView = itemView.findViewById(R.id.personalSubscriptionIconImageView);
            personalNameTextView = itemView.findViewById(R.id.personalNameTextView);
            personalCategoryTextView = itemView.findViewById(R.id.personalCategoryTextView);
            personalPriceTextView = itemView.findViewById(R.id.personalPriceTextView);
            personalPaymentTextView = itemView.findViewById(R.id.personalPaymentTextView);
            personalContainer = itemView.findViewById(R.id.personalSubscriptionItemContainer);
            itemView.setOnLongClickListener(v -> {
                onItemLongClickListenerCallback.onItemClicked(v, getAdapterPosition());
                return true;
            });
            itemView.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putInt(ManagingActivity.ID_KEY, personalSubscriptions.get(getAdapterPosition()).subscriptionID);
                Intent personalSubscriptionIntent = new Intent(v.getContext(), PersonalManagingActivity.class).putExtras(bundle);
                v.getContext().startActivity(personalSubscriptionIntent);
            });
        }
    }

}
