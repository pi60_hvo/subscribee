package com.vladyslavkhimich.subscribee.utils.types.complex;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.vladyslavkhimich.subscribee.R;

import java.util.Objects;

public class ViewAnimator {
    public static boolean rotateFab(final View view, boolean rotate) {
        view.animate().setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                }).rotation(rotate ? 135f : 0f);
        return rotate;
    }

    public static void showIn(final View view) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0f);
        view.setTranslationY(view.getHeight());
        view.animate()
                .setDuration(200)
                .translationY(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .alpha(1f)
                .start();
    }

    public static void showOut(final View view) {
        view.setVisibility(View.VISIBLE);
        view.setAlpha(1f);
        view.setTranslationY(0);
        view.animate()
                .setDuration(200)
                .translationY(view.getHeight())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                        super.onAnimationEnd(animation);
                    }
                })
                .alpha(0f)
                .start();
    }

    public static void init(final View view) {
        view.setVisibility(View.GONE);
        view.setTranslationY(view.getHeight());
        view.setAlpha(0f);
    }

    public static void animateInRecyclerView(RecyclerView recyclerView) {
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    public static void animateViewFade(ViewGroup viewParent, View view, boolean isVisible) {
        Transition transition = new Fade();
        transition.setDuration(500);
        transition.addTarget(view);
        TransitionManager.beginDelayedTransition(viewParent, transition);
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public static void animateViewFade(ViewGroup viewParent, ViewGroup viewGroup, boolean isVisible) {
        Transition transition = new Fade();
        transition.setDuration(500);
        transition.addTarget(viewGroup);
        TransitionManager.beginDelayedTransition(viewParent, transition);
        viewGroup.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public static void animateViewFade(View view, boolean isVisible) {
        view.animate()
                .translationX(isVisible ? 0 : -view.getWidth())
                .alpha(isVisible ? 1.0f : 0.0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
                    }
                });
    }

    public static void animateViewFadeInAndGoneOut(View view, boolean isVisible) {
        if (isVisible) {
            view.animate()
                    .alpha(1.0f)
                    .setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            view.setVisibility(View.VISIBLE);
                        }
                    });
        }
        else {
            view.setVisibility(View.GONE);
        }
    }

    public static void flipImageView(Context context, View view, ImageView iconImageView) {
        Drawable flipDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.img_item_subscription_selection, null);
        view.setRotationY(0f);
        view.animate()
                .rotationY(90f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        iconImageView.setVisibility(View.GONE);
                        ((ConstraintLayout)view).setBackground(flipDrawable);
                        view.setRotationY(270f);
                        view.animate().rotationY(360f).setListener(null);
                    }
                });
    }
}
