package com.vladyslavkhimich.subscribee.utils.binding.adapters

import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.utils.others.stats.helpers.StatsHelper
import com.vladyslavkhimich.subscribee.utils.types.primitives.DoubleHelper

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 26.11.2021
 * @email vladislav.himich@4k.com.ua
 */
object TextAdapters {
    @JvmStatic
    @BindingAdapter("setHtmlText")
    fun bindingSetHtmlText(textView: TextView, string: String) {
        textView.text = HtmlCompat.fromHtml(string, HtmlCompat.FROM_HTML_MODE_LEGACY)
    }

    @JvmStatic
    @BindingAdapter("setMoneyValue", "setCurrencySymbol")
    fun bindingSetMoneyValue(textView: TextView, moneyValue: Double, currencySymbol: String) {
        when {
            moneyValue > 0 -> {
                textView.text = textView.context.getString(R.string.operation_price_plus, currencySymbol, DoubleHelper.getFormattedDoubleOrIntegerUnsigned(moneyValue))
            }
            moneyValue == 0.0 -> {
                textView.text = textView.context.getString(R.string.operation_price_zero, currencySymbol)
            }
            else -> {
                textView.text = textView.context.getString(R.string.operation_price_minus, currencySymbol, DoubleHelper.getFormattedDoubleOrIntegerUnsigned(moneyValue))
            }
        }
    }

    @JvmStatic
    @BindingAdapter("setPercentage")
    fun bindingSetPercentage(textView: TextView, percentage: Double) {
        if (percentage != StatsHelper.PERCENTAGE_NO_DATA)
            textView.text = textView.context.getString(R.string.text_percentage, DoubleHelper.getIntegerFromDouble(percentage))
        else
            textView.text = textView.context.getString(R.string.text_no_data)
    }
}