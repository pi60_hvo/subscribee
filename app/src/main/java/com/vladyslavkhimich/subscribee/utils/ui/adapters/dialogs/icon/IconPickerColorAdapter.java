package com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.icon;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.types.complex.ColorHelper;
import com.vladyslavkhimich.subscribee.app.MyApplication;

import java.util.ArrayList;
import java.util.Arrays;

public class IconPickerColorAdapter extends BaseAdapter {
    public ArrayList<Integer> mainColorsArrayList;
    private ArrayList<Integer> colorsArrayList;
    private final Context context;
    private int currentSelectedPosition = -1;
    private int previousSelectedPosition = -1;
    private boolean isSubColorCollection = false;


    public IconPickerColorAdapter(Context context) {
        this.context = context;
        initColorsArrayList(context);
    }

    public void setCurrentSelectedPosition(int currentSelectedPosition) {
        this.currentSelectedPosition = currentSelectedPosition;
    }

    public void setPreviousSelectedPosition(int previousSelectedPosition) {
        this.previousSelectedPosition = previousSelectedPosition;
    }

    public void setIsSubColorCollection(boolean isSubColorCollection) {
        this.isSubColorCollection = isSubColorCollection;
    }

    public int getCurrentSelectedPosition() {
        return currentSelectedPosition;
    }

    public int getPreviousSelectedPosition() {
        return previousSelectedPosition;
    }

    public boolean getIsSubColorCollection() {
        return isSubColorCollection;
    }

    public int getItemPosition(int itemId) {
        return getColorsArrayList().indexOf(itemId);
    }

    public void initColorsArrayList(Context context) {
        ArrayList<Integer> mainColorsArrayList = new ArrayList<>(ColorHelper.convertColorsIdToIntColors(context, new ArrayList<>(Arrays.asList(MyApplication.getPickerColorsFromSharedPrefs(context)))));
        colorsArrayList = new ArrayList<>(mainColorsArrayList);
        this.mainColorsArrayList = new ArrayList<>(mainColorsArrayList);
    }

    public void setColorsArrayList(ArrayList<Integer> colorsArrayList) {
        this.colorsArrayList = new ArrayList<>(colorsArrayList);
        notifyDataSetChanged();
    }

    public ArrayList<Integer> getColorsArrayList() {
        return colorsArrayList;
    }

    @Override
    public int getCount() {
        return colorsArrayList.size();
    }

    @Override
    public Integer getItem(int position) {
        return colorsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Integer colorInt = colorsArrayList.get(position);
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_dialog_color, null);
        }
        ConstraintLayout dialogIconColorConstraintLayout = convertView.findViewById(R.id.dialogIconColorConstraintLayout);
        GradientDrawable backgroundDrawable = (GradientDrawable) dialogIconColorConstraintLayout.getBackground();
        backgroundDrawable.mutate();
        backgroundDrawable.setColor(colorInt);
        dialogIconColorConstraintLayout.setBackground(backgroundDrawable);
        ImageView dialogIconColorSelectedImageView = convertView.findViewById(R.id.dialogIconColorSelectedImageView);
        if (position == currentSelectedPosition) {
            dialogIconColorSelectedImageView.setVisibility(View.VISIBLE);
        }
        else {
            dialogIconColorSelectedImageView.setVisibility(View.GONE);
        }
        return convertView;
    }
}
