package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.personal.PersonalCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CurrencyDialog;

import java.util.ArrayList;
import java.util.List;

public class PersonalCreationCurrencyDialog extends CurrencyDialog {
    SubscriptionViewModel personalCreationViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalCreationViewModel = ((PersonalCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCurrencies(List<Currency> currencies) {
        getCurrencyAdapter().setAllCurrencies((ArrayList<Currency>) currencies);
    }

    @Override
    public void setSelectedCurrency(Currency currency) {
        if (currency != null)
            personalCreationViewModel.setSelectedCurrency(currency);
    }
}
