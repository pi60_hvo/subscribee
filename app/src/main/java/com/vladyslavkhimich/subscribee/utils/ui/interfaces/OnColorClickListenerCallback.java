package com.vladyslavkhimich.subscribee.utils.ui.interfaces;

public interface OnColorClickListenerCallback {
    void onColorClicked(int color, boolean isSubColor);
}
