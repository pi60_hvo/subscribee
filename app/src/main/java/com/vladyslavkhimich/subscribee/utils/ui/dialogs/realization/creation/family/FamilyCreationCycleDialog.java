package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.family.FamilyCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CycleDialog;

import java.util.ArrayList;
import java.util.List;

public class FamilyCreationCycleDialog extends CycleDialog {
    SubscriptionViewModel familyCreationViewModel;
    @Override
    public void setAssociatedActivityViewModel(Context context) {
        familyCreationViewModel = ((FamilyCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCycles(List<Cycle> cycles) {
        getCycleAdapter().setCycles((ArrayList<Cycle>) cycles);
    }

    @Override
    public void setSelectedCycle(Cycle cycle) {
        familyCreationViewModel.setSelectedCycle(cycle);
    }
}
