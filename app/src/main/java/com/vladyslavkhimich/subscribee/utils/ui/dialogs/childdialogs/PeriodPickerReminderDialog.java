package com.vladyslavkhimich.subscribee.utils.ui.dialogs.childdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.models.Reminder;
import com.vladyslavkhimich.subscribee.utils.types.primitives.IntegerHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.ReminderDialog;

public class PeriodPickerReminderDialog {
    ReminderDialog parentDialog;
    AlertDialog periodPickerAlertDialog;
    EditText numberEditText;
    Spinner periodSpinner;
    Integer periodNumber;

    public void initialize(Context context) {
        AlertDialog.Builder periodPickerAlertDialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_period_custom, null);
        periodPickerAlertDialogBuilder.setView(dialogView);
        numberEditText = dialogView.findViewById(R.id.numberPeriodDialogEditText);
        periodSpinner = dialogView.findViewById(R.id.periodDialogSpinner);
        periodPickerAlertDialogBuilder.setPositiveButton("OK", (dialog, which) -> {
            if (numberEditText.getText().toString().trim().length() > 0) {
                periodNumber = IntegerHelper.tryParse(numberEditText.getText().toString().trim());
                if (periodNumber != null && periodNumber != 0) {
                    Reminder resultReminder = new Reminder();
                    switch(periodSpinner.getSelectedItemPosition()) {
                        case 0:
                            resultReminder.setDaysCount(periodNumber);
                            break;
                        case 1:
                            resultReminder.setWeeksCount(periodNumber);
                            break;
                        case 2:
                            resultReminder.setMonthsCount(periodNumber);
                            break;
                        case 3:
                            resultReminder.setYearsCount(periodNumber);
                            break;
                    }
                    parentDialog.setSelectedReminder(resultReminder);
                    periodPickerAlertDialog.dismiss();
                    parentDialog.getReminderAlertDialog().dismiss();
                }
            }
            else {
                periodPickerAlertDialog.dismiss();
                parentDialog.getReminderAlertDialog().show();
            }
        });
        periodPickerAlertDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {
            periodPickerAlertDialog.dismiss();
            parentDialog.getReminderAlertDialog().show();
        });
        periodPickerAlertDialog = periodPickerAlertDialogBuilder.create();
        periodPickerAlertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public AlertDialog getPeriodPickerAlertDialog() {
        return periodPickerAlertDialog;
    }

    public EditText getNumberEditText() {
        return numberEditText;
    }

    public void setParentDialog(ReminderDialog parentDialog) {
        this.parentDialog = parentDialog;
    }


}
