package com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.cycle;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.StringHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class CycleAdapter extends RecyclerView.Adapter<CycleAdapter.CycleViewHolder> {
    ArrayList<Cycle> cycles = new ArrayList<>();
    OnItemClickListenerCallback onItemClickListenerCallback;

    public CycleAdapter(OnItemClickListenerCallback onItemClickListenerCallback) {
        this.onItemClickListenerCallback = onItemClickListenerCallback;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setCycles(ArrayList<Cycle> cycles) {
        this.cycles = cycles;
        sortCycles();
        notifyDataSetChanged();
    }

    private void sortCycles() {
        Collections.sort(cycles, (a, b) -> {
            Calendar first = Calendar.getInstance();
            DateHelper.addCyclePeriodToCalendar(first, a);
            Calendar second = Calendar.getInstance();
            DateHelper.addCyclePeriodToCalendar(second, b);
            return first.compareTo(second);
        });
    }

    public ArrayList<Cycle> getCycles() {
        return cycles;
    }

    @NonNull
    @Override
    public CycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new CycleViewHolder(inflater.inflate(R.layout.item_dialog_cycle, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CycleViewHolder holder, int position) {
        Cycle cycle = cycles.get(position);
        holder.cycleTextView.setText(StringHelper.getStringFromCycle(holder.cycleTextView.getContext(), cycle));
    }

    @Override
    public int getItemCount() {
        return cycles.size();
    }

    public class CycleViewHolder extends RecyclerView.ViewHolder {
        TextView cycleTextView;

        public CycleViewHolder(@NonNull View itemView) {
            super(itemView);
            cycleTextView = itemView.findViewById(R.id.cycleTextView);
            itemView.setOnClickListener(v -> onItemClickListenerCallback.onItemClicked(v, getAdapterPosition()));
        }
    }
}
