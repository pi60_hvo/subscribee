package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.familypart

import android.content.Context
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.Cycle
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel
import com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart.FamilyPartManagingActivity
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingViewModel
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.CycleManagingDialog
import java.util.ArrayList

class FamilyPartManagingCycleDialog : CycleManagingDialog() {
    private lateinit var context: Context
    private lateinit var familyPartManagingViewModel: SubscriptionViewModel

    override fun setAssociatedActivityViewModel(context: Context?) {
        if (context != null) {
            this.context = context
        }
        familyPartManagingViewModel = (context as FamilyPartManagingActivity).subscriptionViewModel
    }

    override fun setAdapterCycles(cycles: MutableList<Cycle>?) {
        cycleAdapter.cycles = cycles as ArrayList<Cycle>
    }

    override fun setSelectedCycle(cycle: Cycle?) {
        familyPartManagingViewModel.setSelectedCycle(cycle)
    }

    override fun getSubscriptionOperationsCount(): Int {
        return DatabaseRepository.getInstance(context).getSubscriptionOperationsCountByID((context as FamilyPartManagingActivity).subscriptionID)
    }

    override fun setChangeAllViewModelValue(isChangeAll: Boolean?) {
        (familyPartManagingViewModel as ManagingViewModel).setIsCycleChangeAll(isChangeAll)
    }
}