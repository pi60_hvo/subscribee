package com.vladyslavkhimich.subscribee.utils.binding.adapters

import android.view.View
import androidx.databinding.BindingAdapter

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 13.12.2021
 * @email vladislav.himich@4k.com.ua
 */
object HelperAdapters {
    @JvmStatic
    @BindingAdapter("setVisible")
    fun bindingSetVisible(view: View, isVisible: Boolean) {
        view.visibility = if (isVisible) View.VISIBLE else View.GONE
    }
}