package com.vladyslavkhimich.subscribee.utils.uitemp;

import java.util.Date;

public class FamilySubscription extends Subscription {
    public Integer membersCount;

    public FamilySubscription() {

    }

    public FamilySubscription(int imageID, String name, Double price, Date nextPayment, Integer membersCount) {
        super(imageID, name, price, nextPayment);
        this.membersCount = membersCount;
    }
}
