package com.vladyslavkhimich.subscribee.utils.types.primitives;

import java.util.Arrays;

public class EnumHelper {
    public static String[] getStringArrayFromEnum(Class<? extends Enum<?>> e) {
        return Arrays.toString(e.getEnumConstants()).replaceAll("^.|.$", "").split(", ");
    }
}
