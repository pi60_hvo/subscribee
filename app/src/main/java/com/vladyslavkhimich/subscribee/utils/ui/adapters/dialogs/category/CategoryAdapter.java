package com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.category;

import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.utils.types.complex.ColorHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnItemClickListenerCallback;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    ArrayList<Category> categories = new ArrayList<>();
    OnItemClickListenerCallback onItemClickListenerCallback;
    OnItemClickListenerCallback onEditClickListenerCallback;
    OnItemClickListenerCallback onDeleteClickListenerCallback;

    public CategoryAdapter() {

    }

    public CategoryAdapter(OnItemClickListenerCallback onItemClickListenerCallback, OnItemClickListenerCallback onEditClickListenerCallback, OnItemClickListenerCallback onDeleteClickListenerCallback) {
        this.onItemClickListenerCallback = onItemClickListenerCallback;
        this.onEditClickListenerCallback = onEditClickListenerCallback;
        this.onDeleteClickListenerCallback = onDeleteClickListenerCallback;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public Category getCategoriesItem (int position) {
        return categories.get(position);
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new CategoryViewHolder(inflater.inflate(R.layout.item_dialog_category, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = categories.get(position);
        int color = ResourcesCompat.getColor(holder.categoryDialogLinearLayout.getContext().getResources(), ResourcesHelper.getColorIDFromName(holder.categoryDialogLinearLayout.getContext(), category.color), null);
        GradientDrawable backgroundDrawable = (GradientDrawable) holder.categoryDialogLinearLayout.getBackground();
        backgroundDrawable.mutate();
        backgroundDrawable.setColor(ColorHelper.getShadedColor(color, ColorHelper.getColorLightness(color) + .25f));
        holder.categoryDialogLinearLayout.setBackground(backgroundDrawable);
        if (category.isUserCreated) {
            holder.categoryItemTextView.setText(category.name);
            holder.categoryItemEditButton.setVisibility(View.VISIBLE);
            holder.categoryItemDeleteButton.setVisibility(View.VISIBLE);
        }
        else {
            holder.categoryItemEditButton.setVisibility(View.GONE);
            holder.categoryItemDeleteButton.setVisibility(View.GONE);
            holder.categoryItemTextView.setText(ResourcesHelper.getStringIDFromName(holder.categoryItemTextView.getContext(), category.name));
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        LinearLayout categoryDialogLinearLayout;
        TextView categoryItemTextView;
        ImageButton categoryItemDeleteButton;
        ImageButton categoryItemEditButton;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryDialogLinearLayout = itemView.findViewById(R.id.categoryDialogItemContainer);
            categoryItemTextView = itemView.findViewById(R.id.categoryDialogItemTextView);
            categoryItemDeleteButton = itemView.findViewById(R.id.categoryDialogItemDeleteButton);
            categoryItemEditButton = itemView.findViewById(R.id.categoryDialogItemEditButton);
            itemView.setOnClickListener(v -> onItemClickListenerCallback.onItemClicked(v, getAdapterPosition()));
            categoryItemEditButton.setOnClickListener(v -> onEditClickListenerCallback.onItemClicked(v, getAdapterPosition()));
            categoryItemDeleteButton.setOnClickListener(v -> onDeleteClickListenerCallback.onItemClicked(v, getAdapterPosition()));
        }
    }
}
