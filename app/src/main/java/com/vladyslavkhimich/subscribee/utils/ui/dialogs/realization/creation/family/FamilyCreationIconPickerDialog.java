package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family;

import android.content.Context;

import androidx.annotation.NonNull;

import com.vladyslavkhimich.subscribee.ui.subscription.creation.family.FamilyCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.IconPickerDialog;

public class FamilyCreationIconPickerDialog extends IconPickerDialog {
    SubscriptionViewModel familyCreationViewModel;

    public FamilyCreationIconPickerDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        familyCreationViewModel = ((FamilyCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setActivitySelectedIcon(int iconId) {
        familyCreationViewModel.setSelectedIconID(iconId);
    }

    @Override
    public void setActivitySelectedColor(int color) {
        familyCreationViewModel.setSelectedIconColor(color);
    }

    @Override
    public int getActivitySelectedIcon() {
        return familyCreationViewModel.getSelectedIconIDValue();
    }

    @Override
    public int getActivitySelectedColor() {
        return familyCreationViewModel.getSelectedColorIDValue();
    }
}
