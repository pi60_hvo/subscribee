package com.vladyslavkhimich.subscribee.utils.interfaces

import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats
import com.vladyslavkhimich.subscribee.models.stats.base.BaseStatsOverview

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
interface StatsFactory {
    fun getOverviewStats(operationsWithData: List<OperationWithDataForStats>, previousOperationsWithData: List<OperationWithDataForStats>) : BaseStatsOverview
    fun getTimelineStats()
    fun getRadialStats()
}