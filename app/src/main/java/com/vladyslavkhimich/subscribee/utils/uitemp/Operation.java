package com.vladyslavkhimich.subscribee.utils.uitemp;

import java.util.Date;

public class Operation {
    public int imageID;
    public String name;
    public boolean isPersonal;
    public String payerName;
    public Double price;
    public Date operationDate;

    public Operation() {

    }

    public Operation(int imageID, String name, boolean isPersonal, String payerName, Double price, Date operationDate) {
        this.imageID = imageID;
        this.name = name;
        this.isPersonal = isPersonal;
        this.payerName = payerName;
        this.price = price;
        this.operationDate = operationDate;
    }

    public Operation(int imageID, String name, boolean isPersonal, Double price, Date operationDate) {
        this.imageID = imageID;
        this.name = name;
        this.isPersonal = isPersonal;
        this.price = price;
        this.operationDate = operationDate;
    }
}
