package com.vladyslavkhimich.subscribee.utils.ui.diff

import androidx.recyclerview.widget.DiffUtil
import com.vladyslavkhimich.subscribee.models.Payment

class PaymentsDiffUtilCallback(private val oldList: List<Payment>, private val newList: List<Payment>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldPayment = oldList[oldItemPosition]
        val newPayment = newList[newItemPosition]
        return oldPayment.paymentDate.equals(newPayment.paymentDate)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldPayment = oldList[oldItemPosition]
        val newPayment = newList[newItemPosition]
        return oldPayment.isChecked == newPayment.isChecked
    }
}