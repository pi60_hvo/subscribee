package com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.archived.general

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.models.Payment
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper

class ArchivedPaymentsAdapter : RecyclerView.Adapter<ArchivedPaymentsAdapter.ArchivedPaymentViewHolder>() {

    var payments: ArrayList<Payment> = ArrayList()

    @SuppressLint("NotifyDataSetChanged")
    fun setPaymentsAndNotify(payments: ArrayList<Payment>) {
        this.payments = payments
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArchivedPaymentViewHolder {
        val archivedPaymentView = LayoutInflater.from(parent.context).inflate(R.layout.item_payment, parent, false)
        return ArchivedPaymentViewHolder(archivedPaymentView)
    }

    override fun onBindViewHolder(holder: ArchivedPaymentViewHolder, position: Int) {
        val payment = payments[position]

        holder.paymentCheckBox.isChecked = payment.isChecked
        holder.paymentDateTextView.text = DateHelper.getNextPaymentDate(payment.paymentDate)
    }

    override fun getItemCount(): Int {
        return payments.size
    }

    inner class ArchivedPaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val paymentDateTextView: TextView = itemView.findViewById(R.id.paymentItemDateTextView)
        val paymentCheckBox: CheckBox = itemView.findViewById(R.id.paymentItemCheckBox)

        init {
            paymentCheckBox.isEnabled = false
        }
    }
}