package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.personal.PersonalManagingActivity;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.FreePeriodDialog;

public class PersonalManagingFreePeriodDialog extends FreePeriodDialog {
    SubscriptionViewModel personalManagingViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalManagingViewModel = ((PersonalManagingActivity)context).subscriptionViewModel;
    }

    @Override
    public void setSelectedFreePeriod(FreePeriod selectedFreePeriod) {
        personalManagingViewModel.setSelectedFreePeriod(selectedFreePeriod);
    }
}
