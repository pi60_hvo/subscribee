package com.vladyslavkhimich.subscribee.utils.others.stats.realization

import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats
import com.vladyslavkhimich.subscribee.models.stats.personal.PersonalStatsOverview
import com.vladyslavkhimich.subscribee.utils.interfaces.StatsFactory
import com.vladyslavkhimich.subscribee.utils.mappers.mapPersonalSubscriptionsData
import com.vladyslavkhimich.subscribee.utils.others.stats.helpers.StatsHelper
import java.util.*

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
class PersonalStatsFactory : StatsFactory {
    override fun getOverviewStats(operationsWithData: List<OperationWithDataForStats>, previousOperationsWithData: List<OperationWithDataForStats>) : PersonalStatsOverview {
        val mappedOperationsWithData = mapPersonalSubscriptionsData(operationsWithData)
        val mappedPreviousOperationsWithData = mapPersonalSubscriptionsData(previousOperationsWithData)
        val resultStats = PersonalStatsOverview()
        resultStats.totalSpent = StatsHelper.getTotalSpent(mappedOperationsWithData)
        resultStats.dayAverageSpent = StatsHelper.getSpentPerDay(mappedPreviousOperationsWithData)
        resultStats.percentage = StatsHelper.getPercentage(mappedOperationsWithData, mappedPreviousOperationsWithData)
        resultStats.mostSpentSubscription = StatsHelper.getMostSpentOrPaidSubscription(mappedOperationsWithData, isSpent = true, isFamily = false)
        resultStats.mostSpentCategory = StatsHelper.getMostSpentCategory(mappedOperationsWithData)
        return resultStats
    }

    override fun getTimelineStats() {
        TODO("Not yet implemented")
    }

    override fun getRadialStats() {
        TODO("Not yet implemented")
    }

    fun mapPersonalSubscriptionsData(operationsWithData: List<OperationWithDataForStats>) : LinkedList<OperationWithDataForStats> {
        val mappedOperationsWithData = LinkedList<OperationWithDataForStats>()
        mappedOperationsWithData.mapPersonalSubscriptionsData(operationsWithData)
        return mappedOperationsWithData
    }
}