package com.vladyslavkhimich.subscribee.utils.others.stats.helpers

import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats
import com.vladyslavkhimich.subscribee.models.dto.StatsCategoryDto
import com.vladyslavkhimich.subscribee.models.dto.StatsMemberDto
import com.vladyslavkhimich.subscribee.models.dto.StatsSubscriptionDto
import com.vladyslavkhimich.subscribee.utils.mappers.mapCategoriesWithOperationsDistinct
import com.vladyslavkhimich.subscribee.utils.mappers.mapFamilySubscriptionsWithOperationsDistinct
import com.vladyslavkhimich.subscribee.utils.mappers.mapMembersWithOperationsDistinct
import com.vladyslavkhimich.subscribee.utils.mappers.mapSubscriptionsWithOperationsDistinct
import java.util.*
import kotlin.collections.HashMap

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 15.12.2021
 * @email vladislav.himich@4k.com.ua
 */
object StatsHelper {
    const val PERCENTAGE_NO_DATA = Double.MIN_VALUE

    fun getTotalSpent(operationsWithData: List<OperationWithDataForStats>): Double {
        var totalSpent = 0.0
        operationsWithData.forEach { operationWithData ->
            totalSpent += operationWithData.operation.price
        }
        return totalSpent
    }

    fun getSpentPerDay(operationsWithData: List<OperationWithDataForStats>): Double {
        val result = getTotalSpent(operationsWithData)
        val calendar = Calendar.getInstance()
        if (operationsWithData.isNotEmpty()) {
            calendar.time = operationsWithData[0].operation.operationDate
        }
        return result / calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    }

    fun getPercentage(
        operationsWithDataCurrent: List<OperationWithDataForStats>,
        operationsWithDataPrevious: List<OperationWithDataForStats>
    ): Double {
        val currentTotal = getTotalSpent(operationsWithDataCurrent)
        val previousTotal = if (operationsWithDataPrevious.isEmpty()) 0.0 else getTotalSpent(
            operationsWithDataPrevious
        )
        if (currentTotal == 0.0 || previousTotal == 0.0) {
            return PERCENTAGE_NO_DATA
        }
        return -(100 - (100 * currentTotal) / previousTotal)
    }

    fun getMostSpentOrPaidSubscription(operationsWithData: List<OperationWithDataForStats>, isFamily: Boolean, isSpent: Boolean): StatsSubscriptionDto? {
        val subscriptionsWithOperationsHashMap = HashMap<Int, List<OperationWithDataForStats>>()
        if (isFamily) {
            subscriptionsWithOperationsHashMap.mapFamilySubscriptionsWithOperationsDistinct(operationsWithData, isSpent)
        }
        else {
            subscriptionsWithOperationsHashMap.mapSubscriptionsWithOperationsDistinct(
                operationsWithData
            )
        }
        if (subscriptionsWithOperationsHashMap.isNotEmpty()) {
            var totalSubscriptionSpentMax = 0.0
            var subscriptionMaxId = subscriptionsWithOperationsHashMap.keys.toIntArray()[0]
            for (key in subscriptionsWithOperationsHashMap.keys) {
                if (subscriptionsWithOperationsHashMap[key] != null) {
                    val currentSubscriptionTotalValue =
                        getTotalSpent(subscriptionsWithOperationsHashMap[key]!!)
                    if (isSpent) {
                        if (currentSubscriptionTotalValue < totalSubscriptionSpentMax) {
                            totalSubscriptionSpentMax = currentSubscriptionTotalValue
                            subscriptionMaxId = key
                        }
                    }
                    else {
                        if (currentSubscriptionTotalValue > totalSubscriptionSpentMax) {
                            totalSubscriptionSpentMax = currentSubscriptionTotalValue
                            subscriptionMaxId = key
                        }
                    }
                }
            }
            with(subscriptionsWithOperationsHashMap[subscriptionMaxId]) {
                if (this?.get(0) != null) {
                    return StatsSubscriptionDto(
                        this[0].subscriptionWithCategory.subscription.iconColor,
                        this[0].subscriptionWithCategory.subscription.icon,
                        this[0].subscriptionWithCategory.subscription.name,
                        totalSubscriptionSpentMax
                    )
                }
            }
        }
        return null
    }

    fun getMostSpentCategory(operationsWithData: List<OperationWithDataForStats>) : StatsCategoryDto? {
        val categoriesWithOperationsHashMap = HashMap<Int, List<OperationWithDataForStats>>()
        categoriesWithOperationsHashMap.mapCategoriesWithOperationsDistinct(operationsWithData)
        if (categoriesWithOperationsHashMap.isNotEmpty()) {
            var totalCategorySpentMax = 0.0
            var categoryMaxId = categoriesWithOperationsHashMap.keys.toIntArray()[0]
            for (key in categoriesWithOperationsHashMap.keys) {
                if (categoriesWithOperationsHashMap[key] != null) {
                    val currentCategoryTotalValue = getTotalSpent(categoriesWithOperationsHashMap[key]!!)
                    if (currentCategoryTotalValue < totalCategorySpentMax) {
                        totalCategorySpentMax = currentCategoryTotalValue
                        categoryMaxId = key
                    }
                }
            }
            with(categoriesWithOperationsHashMap[categoryMaxId]) {
                if (this?.get(0) != null) {
                    return StatsCategoryDto(
                        this[0].subscriptionWithCategory.category!!.color,
                        this[0].subscriptionWithCategory.category!!.name,
                        totalCategorySpentMax
                    )
                }
            }
        }
        return null
    }

    fun getMostPaidMember(operationsWithData: List<OperationWithDataForStats>) : StatsMemberDto? {
        val membersWithOperationsHashMap = HashMap<Int, List<OperationWithDataForStats>>()
        membersWithOperationsHashMap.mapMembersWithOperationsDistinct(operationsWithData)
        if (membersWithOperationsHashMap.isNotEmpty()) {
            var memberPaidMax = 0.0
            var memberMaxId = membersWithOperationsHashMap.keys.toIntArray()[0]
            for (key in membersWithOperationsHashMap.keys) {
                if (membersWithOperationsHashMap[key] != null) {
                    val currentMemberValue = getTotalSpent(membersWithOperationsHashMap[key]!!)
                    if (currentMemberValue > memberPaidMax) {
                        memberPaidMax = currentMemberValue
                        memberMaxId = key
                    }
                }
            }
            with (membersWithOperationsHashMap[memberMaxId]) {
                if (this?.get(0) != null) {
                    if (this[0].member != null) {
                        return StatsMemberDto(
                            this[0].member!!.name,
                            this[0].subscriptionWithCategory.subscription.name,
                            memberPaidMax
                        )
                    }
                }
            }
        }
        return null
    }
}