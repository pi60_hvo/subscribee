package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal;

import android.content.Context;

import com.vladyslavkhimich.subscribee.ui.subscription.creation.general.CreationViewModel;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.ReminderDialog;
import com.vladyslavkhimich.subscribee.models.Reminder;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.personal.PersonalCreationActivity;

public class PersonalCreationReminderDialog extends ReminderDialog {
    SubscriptionViewModel personalCreationViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        personalCreationViewModel = ((PersonalCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setSelectedReminder(Reminder selectedReminder) {
        ((CreationViewModel)personalCreationViewModel).setSelectedReminder(selectedReminder);
    }
}
