package com.vladyslavkhimich.subscribee.utils.enums;

public enum AppLocales {
    en,
    ru,
    uk
}
