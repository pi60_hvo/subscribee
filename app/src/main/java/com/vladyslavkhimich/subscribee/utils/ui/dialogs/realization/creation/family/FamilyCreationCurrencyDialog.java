package com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family;

import android.content.Context;

import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.family.FamilyCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CurrencyDialog;

import java.util.ArrayList;
import java.util.List;

public class FamilyCreationCurrencyDialog extends CurrencyDialog {
    SubscriptionViewModel familyCreationViewModel;

    @Override
    public void setAssociatedActivityViewModel(Context context) {
        familyCreationViewModel = ((FamilyCreationActivity)context).subscriptionViewModel;
    }

    @Override
    public void setAdapterCurrencies(List<Currency> currencies) {
        getCurrencyAdapter().setAllCurrencies((ArrayList<Currency>) currencies);
    }

    @Override
    public void setSelectedCurrency(Currency currency) {
        if (currency != null)
            familyCreationViewModel.setSelectedCurrency(currency);
    }
}
