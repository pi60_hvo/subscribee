package com.vladyslavkhimich.subscribee;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.ui.donation.DonationActivity;
import com.vladyslavkhimich.subscribee.ui.operations.OperationsFragment;
import com.vladyslavkhimich.subscribee.ui.settings.SettingsActivity;
import com.vladyslavkhimich.subscribee.ui.statistics.StatisticsFragment;
import com.vladyslavkhimich.subscribee.ui.subscriptions.SubscriptionsFragment;
import com.vladyslavkhimich.subscribee.app.BaseActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends BaseActivity {
    Menu optionsMenu;
    ImageView toolbarImageView;
    TextView toolbarTextView;
    int selectedSubscriptionsCount;
    private final SubscriptionsFragment subscriptionsFragment = new SubscriptionsFragment();
    private final OperationsFragment operationsFragment = new OperationsFragment();
    private final StatisticsFragment statisticsFragment = new StatisticsFragment();
    Fragment activeFragment = subscriptionsFragment;
    private final androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setTheme(R.style.Theme_Subscribee_NoActionBar);
            setContentView(R.layout.activity_main);

            BottomNavigationView navView = findViewById(R.id.nav_view);
            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
            /*AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.mobile_navigation_subscription, R.id.mobile_navigation_operation, R.id.mobile_navigation_statistic)
                    .build();*/
            //NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            Toolbar customToolbar = findViewById(R.id.main_toolbar);
            setSupportActionBar(customToolbar);
            //NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
            //NavigationUI.setupWithNavController(navView, navController);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
            setBottomMenuOnClickListener(navView);
            //switchToSubscriptionsFragment();
            toolbarImageView = customToolbar.findViewById(R.id.toolbarAppIconImageView);
            toolbarTextView = customToolbar.findViewById(R.id.toolbarAppNameTextView);
            addFragmentsOnCreate();
            hideOperationsAndStatisticsFragments();
    }

    private void addFragmentsOnCreate() {
        fragmentManager.beginTransaction().add(R.id.mainActivityFragmentContainerView, statisticsFragment).commit();
        fragmentManager.beginTransaction().add(R.id.mainActivityFragmentContainerView, operationsFragment).commit();
        fragmentManager.beginTransaction().add(R.id.mainActivityFragmentContainerView, subscriptionsFragment).commit();
    }

    private void hideOperationsAndStatisticsFragments() {
        fragmentManager.beginTransaction().hide(operationsFragment).commit();
        fragmentManager.beginTransaction().hide(statisticsFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_toolbar_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem supportMenuItem = menu.findItem(R.id.support_option);
        LinearLayout rootView = (LinearLayout) supportMenuItem.getActionView();

        rootView.setOnClickListener(v -> onOptionsItemSelected(supportMenuItem));
        hideNotNecessaryOptionsMenuOptionsOnStart();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.support_option) {
            Intent donationIntent = new Intent(MainActivity.this, DonationActivity.class);
            startActivity(donationIntent);
            //Toast.makeText(this, "Support Pressed", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.settings_option) {
            Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        else if (id == R.id.archive_option) {
            new AlertDialog.Builder(MainActivity.this)
                    .setPositiveButton(R.string.yes, (dialog, which) -> {
                        //SubscriptionsFragment subscriptionsFragment = (SubscriptionsFragment) fragmentManager.findFragmentById(R.id.mainActivityFragmentContainerView);
                        if (subscriptionsFragment != null) {
                            ArrayList<Integer> selectedSubscriptionIDs = subscriptionsFragment.getAllSelectedSubscriptionsIDs();
                            for (Integer subscriptionID : selectedSubscriptionIDs) {
                                DatabaseRepository.getInstance(getApplicationContext()).archiveSubscription(subscriptionID);
                            }
                            subscriptionsFragment.deselectAllSelectedSubscriptions();
                            toggleNotArchivedSelectionOptionsMenu(false);
                            Toast.makeText(
                                    this,
                                    getResources().
                                            getQuantityString(
                                                    R.plurals.toast_plurals_archived_n_subscriptions,
                                                    selectedSubscriptionIDs.size(),
                                                    selectedSubscriptionIDs.size()),
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    })
                    .setNegativeButton(R.string.no, ((dialog, which) ->  {

                    }))
                    .setTitle(getResources().getQuantityString(R.plurals.plurals_archive_n_subscriptions, selectedSubscriptionsCount, selectedSubscriptionsCount))
                    .setMessage(R.string.dialog_archive_subscriptions_message)
                    .show();
            return true;
        }
        else if (id == R.id.delete_option) {
            new AlertDialog.Builder(MainActivity.this)
                    .setPositiveButton(R.string.yes, (dialog, which) -> {
                        //SubscriptionsFragment subscriptionsFragment = (SubscriptionsFragment) fragmentManager.findFragmentById(R.id.mainActivityFragmentContainerView);
                        if (subscriptionsFragment != null) {
                            ArrayList<Integer> selectedSubscriptionIDs = subscriptionsFragment.getAllSelectedSubscriptionsIDs();
                            for (Integer subscriptionID : selectedSubscriptionIDs) {
                                DatabaseRepository.getInstance(getApplicationContext()).deleteSubscription(subscriptionID);
                            }
                            subscriptionsFragment.deselectAllSelectedSubscriptions();
                            Toast.makeText(
                                    this,
                                    getResources().
                                            getQuantityString(
                                                    R.plurals.toast_plurals_deleted_n_subscriptions,
                                                    selectedSubscriptionIDs.size(),
                                                    selectedSubscriptionIDs.size()),
                                    Toast.LENGTH_SHORT)
                                    .show();
                            toggleNotArchivedSelectionOptionsMenu(false);
                            toggleArchivedSelectionOptionsMenu(false);
                        }
                    })
                    .setNegativeButton(R.string.no, ((dialog, which) ->  {

                    }))
                    .setTitle(getResources().getQuantityString(R.plurals.plurals_delete_n_subscriptions, selectedSubscriptionsCount, selectedSubscriptionsCount))
                    .setMessage(R.string.dialog_delete_subscriptions_message)
                    .show();
            return true;
        }
        else if (id == R.id.unarchive_option) {
            //SubscriptionsFragment subscriptionsFragment = (SubscriptionsFragment) fragmentManager.findFragmentById(R.id.mainActivityFragmentContainerView);
            if (subscriptionsFragment != null) {
                ArrayList<Integer> selectedSubscriptionIDs = subscriptionsFragment.getAllSelectedSubscriptionsIDs();
                for (Integer subscriptionID : selectedSubscriptionIDs) {
                    DatabaseRepository.getInstance(getApplicationContext()).unarchiveSubscription(subscriptionID);
                }
                subscriptionsFragment.deselectAllSelectedSubscriptions();
                Toast.makeText(
                        this,
                        getResources().
                                getQuantityString(
                                        R.plurals.toast_plurals_unarchived_n_subscriptions,
                                        selectedSubscriptionIDs.size(),
                                        selectedSubscriptionIDs.size()),
                        Toast.LENGTH_SHORT)
                        .show();
                toggleArchivedSelectionOptionsMenu(false);
            }
            return true;
        }
        else if (id == R.id.close_option) {
            //SubscriptionsFragment subscriptionsFragment = (SubscriptionsFragment) getSupportFragmentManager().findFragmentById(R.id.mainActivityFragmentContainerView);
            if (subscriptionsFragment != null)
                subscriptionsFragment.deselectAllSelectedSubscriptions();
            toggleNotArchivedSelectionOptionsMenu(false);
            toggleArchivedSelectionOptionsMenu(false);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBNVFirstItemSelected(findViewById(R.id.nav_view));
    }

    void setBottomMenuOnClickListener(BottomNavigationView bnView) {
        bnView.setOnItemSelectedListener(item -> {
            int fragmentId = item.getItemId();
            if (fragmentId == R.id.navigation_subscriptions)
                switchToSubscriptionsFragment();
            else if (fragmentId == R.id.navigation_operations)
                switchToOperationsFragment();
            else if (fragmentId == R.id.navigation_statistics)
                switchToStatisticsFragment();
            return true;
        });
    }

    void switchToSubscriptionsFragment() {
        if (activeFragment != subscriptionsFragment) {
            showSelectionOptionsMenu();
            fragmentManager.beginTransaction().show(subscriptionsFragment).commit();
        }
        else {
            showSelectionOptionsMenu();
            fragmentManager.beginTransaction().hide(activeFragment).show(subscriptionsFragment).commit();
        }
        activeFragment = subscriptionsFragment;
    }

    void showSelectionOptionsMenu() {
        int notArchivedSelectedCount = subscriptionsFragment.getNotArchivedSelectedItemsCount();
        if (notArchivedSelectedCount != 0) {
            toggleNotArchivedSelectionOptionsMenu(true);
            setSelectedSubscriptionsToolbarTitle(notArchivedSelectedCount);
            return;
        }
        int archivedSelectedCount = subscriptionsFragment.getArchivedSelectedItemsCount();
        if (archivedSelectedCount != 0) {
            toggleArchivedSelectionOptionsMenu(true);
            setSelectedSubscriptionsToolbarTitle(archivedSelectedCount);
        }
    }

    void switchToOperationsFragment() {
        toggleArchivedSelectionOptionsMenu(false);
        toggleNotArchivedSelectionOptionsMenu(false);
        fragmentManager.beginTransaction().hide(activeFragment).show(operationsFragment).commit();
        activeFragment = operationsFragment;
    }

    void switchToStatisticsFragment() {
        toggleArchivedSelectionOptionsMenu(false);
        toggleNotArchivedSelectionOptionsMenu(false);
        fragmentManager.beginTransaction().hide(activeFragment).show(statisticsFragment).commit();
        activeFragment = statisticsFragment;
    }

    void setBNVFirstItemSelected(BottomNavigationView bnView) {
        bnView.setSelectedItemId(R.id.navigation_subscriptions);
    }

    void hideNotNecessaryOptionsMenuOptionsOnStart() {
        hideOptionInOptionsMenu(R.id.archive_option);
        hideOptionInOptionsMenu(R.id.unarchive_option);
        hideOptionInOptionsMenu(R.id.delete_option);
        hideOptionInOptionsMenu(R.id.close_option);
    }

    public void toggleNotArchivedSelectionOptionsMenu(boolean isVisible) {
        if (isVisible) {
            hideOptionInOptionsMenu(R.id.support_option);
            hideOptionInOptionsMenu(R.id.settings_option);
            hideOptionInOptionsMenu(R.id.unarchive_option);
            showOptionInOptionsMenu(R.id.archive_option);
            showOptionInOptionsMenu(R.id.delete_option);
            showOptionInOptionsMenu(R.id.close_option);
            toolbarImageView.setVisibility(View.GONE);
            toolbarTextView.setVisibility(View.GONE);
        }
        else {
            showOptionInOptionsMenu(R.id.support_option);
            showOptionInOptionsMenu(R.id.settings_option);
            hideOptionInOptionsMenu(R.id.archive_option);
            hideOptionInOptionsMenu(R.id.delete_option);
            hideOptionInOptionsMenu(R.id.close_option);
            toolbarImageView.setVisibility(View.VISIBLE);
            toolbarTextView.setVisibility(View.VISIBLE);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
            Objects.requireNonNull(getSupportActionBar()).setTitle("");
        }
    }

    public void toggleArchivedSelectionOptionsMenu(boolean isVisible) {
        if (isVisible) {
            hideOptionInOptionsMenu(R.id.support_option);
            hideOptionInOptionsMenu(R.id.settings_option);
            hideOptionInOptionsMenu(R.id.archive_option);
            showOptionInOptionsMenu(R.id.unarchive_option);
            showOptionInOptionsMenu(R.id.delete_option);
            showOptionInOptionsMenu(R.id.close_option);
            toolbarImageView.setVisibility(View.GONE);
            toolbarTextView.setVisibility(View.GONE);
        }
        else {
            showOptionInOptionsMenu(R.id.support_option);
            showOptionInOptionsMenu(R.id.settings_option);
            hideOptionInOptionsMenu(R.id.unarchive_option);
            hideOptionInOptionsMenu(R.id.delete_option);
            hideOptionInOptionsMenu(R.id.close_option);
            toolbarImageView.setVisibility(View.VISIBLE);
            toolbarTextView.setVisibility(View.VISIBLE);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
            Objects.requireNonNull(getSupportActionBar()).setTitle("");
        }
    }

    public void setSelectedSubscriptionsToolbarTitle(Integer selectedCount) {
        selectedSubscriptionsCount = selectedCount;
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.selected_subscriptions_count, selectedCount));
    }

    private void hideOptionInOptionsMenu(int id) {
        if (optionsMenu != null) {
            MenuItem item = optionsMenu.findItem(id);
            item.setVisible(false);
        }
    }

    private void showOptionInOptionsMenu(int id) {
        if (optionsMenu != null) {
            MenuItem item = optionsMenu.findItem(id);
            item.setVisible(true);
        }
    }
}