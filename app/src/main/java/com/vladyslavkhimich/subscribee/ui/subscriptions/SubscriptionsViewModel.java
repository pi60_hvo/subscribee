package com.vladyslavkhimich.subscribee.ui.subscriptions;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.utils.enums.SubscriptionSortings;

import java.util.List;

public class SubscriptionsViewModel extends AndroidViewModel {
    LiveData<List<Subscription>> personalSubscriptions = new MutableLiveData<>();
    LiveData<List<Subscription>> familySubscriptions = new MutableLiveData<>();
    LiveData<List<Subscription>> archivedSubscriptions = new MutableLiveData<>();
    MutableLiveData<SubscriptionSortings> personalSubscriptionSorting = new MutableLiveData<>();
    MutableLiveData<SubscriptionSortings> familySubscriptionSorting = new MutableLiveData<>();
    MutableLiveData<SubscriptionSortings> archivedSubscriptionSorting = new MutableLiveData<>();

    public SubscriptionsViewModel(@NonNull Application application) {
        super(application);
        setPersonalSubscriptionsFromDatabase();
        setFamilySubscriptionsFromDatabase();
        setArchivedSubscriptionsFromDatabase();
    }

    public void setPersonalSubscriptions(LiveData<List<Subscription>> personalSubscriptions) {
        this.personalSubscriptions = personalSubscriptions;
    }

    public void setPersonalSubscriptionSorting(SubscriptionSortings personalSubscriptionSorting) {
        if (this.personalSubscriptionSorting.getValue() != personalSubscriptionSorting)
            this.personalSubscriptionSorting.setValue(personalSubscriptionSorting);
    }

    public void setFamilySubscriptionSorting(SubscriptionSortings familySubscriptionSorting) {
        if (this.familySubscriptionSorting.getValue() != familySubscriptionSorting)
            this.familySubscriptionSorting.setValue(familySubscriptionSorting);
    }

    public void setArchivedSubscriptionSorting(SubscriptionSortings archivedSubscriptionSorting) {
        if (this.archivedSubscriptionSorting.getValue() != archivedSubscriptionSorting)
            this.archivedSubscriptionSorting.setValue(archivedSubscriptionSorting);
    }

    public LiveData<List<Subscription>> getPersonalSubscriptions() {
        return personalSubscriptions;
    }

    public void setFamilySubscriptions(LiveData<List<Subscription>> familySubscriptions) {
        this.familySubscriptions = familySubscriptions;
    }

    public LiveData<List<Subscription>> getFamilySubscriptions() {
        return familySubscriptions;
    }

    public void setArchivedSubscriptions(LiveData<List<Subscription>> archivedSubscriptions) {
        this.archivedSubscriptions = archivedSubscriptions;
    }

    public LiveData<List<Subscription>> getArchivedSubscriptions() {
        return archivedSubscriptions;
    }

    public MutableLiveData<SubscriptionSortings> getPersonalSubscriptionSorting() {
        return personalSubscriptionSorting;
    }

    public SubscriptionSortings getPersonalSubscriptionsSortingValue() {
        return personalSubscriptionSorting.getValue();
    }

    public MutableLiveData<SubscriptionSortings> getFamilySubscriptionSorting() {
        return familySubscriptionSorting;
    }

    public SubscriptionSortings getFamilySubscriptionSortingValue() {
        return familySubscriptionSorting.getValue();
    }

    public MutableLiveData<SubscriptionSortings> getArchivedSubscriptionSorting() {
        return archivedSubscriptionSorting;
    }

    public SubscriptionSortings getArchivedSubscriptionSortingValue() {
        return archivedSubscriptionSorting.getValue();
    }

    public void setPersonalSubscriptionsFromDatabase() {
        setPersonalSubscriptions(DatabaseRepository.getInstance(getApplication().getApplicationContext()).getPersonalSubscriptions());
    }

    public void setFamilySubscriptionsFromDatabase() {
        setFamilySubscriptions(DatabaseRepository.getInstance(getApplication().getApplicationContext()).getFamilySubscriptions());
    }

    public void setArchivedSubscriptionsFromDatabase() {
        setArchivedSubscriptions(DatabaseRepository.getInstance(getApplication().getApplicationContext()).getArchivedSubscriptions());
    }
}