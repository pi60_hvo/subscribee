package com.vladyslavkhimich.subscribee.ui.statistics.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.vladyslavkhimich.subscribee.databinding.FragmentStatisticsOverviewBinding
import com.vladyslavkhimich.subscribee.models.stats.all.AllStatsOverview
import com.vladyslavkhimich.subscribee.models.stats.base.BaseStatsOverview
import com.vladyslavkhimich.subscribee.models.stats.family.FamilyStatsOverview
import com.vladyslavkhimich.subscribee.models.stats.personal.PersonalStatsOverview
import com.vladyslavkhimich.subscribee.utils.interfaces.StatisticsMapper

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 13.12.2021
 * @email vladislav.himich@4k.com.ua
 */
class StatisticsOverviewFragment : Fragment(), StatisticsMapper {
    private lateinit var binding: FragmentStatisticsOverviewBinding

    val statisticsOverviewViewModel by viewModels<StatisticsOverviewViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStatisticsOverviewBinding.inflate(inflater)
        initModels()
        return binding.root
    }

    private fun initModels() {
        binding.statisticsOverviewModel = statisticsOverviewViewModel.statisticsOverviewModel
    }

    override fun mapStatistics(tabIndex: Int, overviewStatsData: BaseStatsOverview) {
        when (tabIndex) {
            0 -> mapPersonalStats(overviewStatsData as PersonalStatsOverview)
            1 -> mapFamilyStats(overviewStatsData as FamilyStatsOverview)
            2 -> mapAllStats(overviewStatsData as AllStatsOverview)
        }
    }

    private fun mapPersonalStats(overviewStatsData: PersonalStatsOverview) {

    }

    private fun mapFamilyStats(overviewStatsData: FamilyStatsOverview) {

    }

    private fun mapAllStats(overviewStatsData: AllStatsOverview) {

    }
}