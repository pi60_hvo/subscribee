package com.vladyslavkhimich.subscribee.ui.subscription.general;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Category;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.models.Reminder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class SubscriptionViewModel extends AndroidViewModel {
    MutableLiveData<Reminder> selectedReminder = new MutableLiveData<>();
    MutableLiveData<FreePeriod> selectedFreePeriod = new MutableLiveData<>();
    MutableLiveData<Integer> selectedIconColor = new MutableLiveData<>();
    MutableLiveData<Integer> selectedIconID = new MutableLiveData<>();
    LiveData<List<Cycle>> cycles;
    MutableLiveData<Cycle> selectedCycle = new MutableLiveData<>();
    LiveData<List<Currency>> currencies;
    MutableLiveData<Currency> selectedCurrency = new MutableLiveData<>();
    LiveData<List<Category>> categories;
    MutableLiveData<Category> selectedCategory = new MutableLiveData<>();

    public SubscriptionViewModel(@NonNull @NotNull Application application) {
        super(application);
        setCyclesFromDatabase();
        setCurrenciesFromDatabase();
        setCategoriesFromDatabase();
    }

    public void setSelectedReminder(Reminder reminder) {
        selectedReminder.setValue(reminder);
    }

    public void setSelectedFreePeriod(FreePeriod selectedFreePeriod) {
        this.selectedFreePeriod.setValue(selectedFreePeriod);
    }

    public void setSelectedIconColor(Integer selectedIconColor) {
        this.selectedIconColor.setValue(selectedIconColor);
    }

    public void setSelectedIconID(Integer selectedIconID) {
        this.selectedIconID.setValue(selectedIconID);
    }

    public void setCycles(LiveData<List<Cycle>> cycles) {
        this.cycles = cycles;
    }

    public void setSelectedCycle(Cycle selectedCycle) {
        this.selectedCycle.setValue(selectedCycle);
    }

    public void setCurrencies(LiveData<List<Currency>> currencies) {
        this.currencies = currencies;
    }

    public void setSelectedCurrency(Currency selectedCurrency) {
        this.selectedCurrency.setValue(selectedCurrency);
    }

    public void setCategories(LiveData<List<Category>> categories) {
        this.categories = categories;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory.setValue(selectedCategory);
    }

    public MutableLiveData<Reminder> getSelectedReminder() {
        return selectedReminder;
    }

    public Reminder getSelectedReminderObject() {
        return selectedReminder.getValue();
    }

    public MutableLiveData<FreePeriod> getSelectedFreePeriod() {
        return selectedFreePeriod;
    }
    public FreePeriod getSelectedFreePeriodObject() {
        return selectedFreePeriod.getValue();
    }

    public MutableLiveData<Cycle> getSelectedCycle() {
        return selectedCycle;
    }
    public Cycle getSelectedCycleObject() {
        return selectedCycle.getValue();
    }

    public MutableLiveData<Integer> getSelectedIconColor() {
        return selectedIconColor;
    }

    public MutableLiveData<Integer> getSelectedIconID() {
        return selectedIconID;
    }

    public Integer getSelectedIconIDValue() {
        return selectedIconID.getValue();
    }

    public Integer getSelectedColorIDValue() {
        return selectedIconColor.getValue();
    }

    public LiveData<List<Cycle>> getCycles() {
        return cycles;
    }

    public MutableLiveData<Currency> getSelectedCurrency() {
        return selectedCurrency;
    }

    public Currency getSelectedCurrencyObject() {
        return selectedCurrency.getValue();
    }

    public LiveData<List<Currency>> getCurrencies() {
        return currencies;
    }

    public LiveData<List<Category>> getCategories() {
        return categories;
    }

    public LiveData<Category> getSelectedCategory() {
        return selectedCategory;
    }

    public Category getSelectedCategoryObject() {
        return selectedCategory.getValue();
    }

    public void setCyclesFromDatabase() {
        setCycles(DatabaseRepository.getInstance(getApplication().getApplicationContext()).getCycles());
    }

    public void setCurrenciesFromDatabase() {
        setCurrencies(DatabaseRepository.getInstance(getApplication().getApplicationContext()).getCurrencies());
    }

    public void setCategoriesFromDatabase() {
        setCategories(DatabaseRepository.getInstance(getApplication().getApplicationContext()).getCategories());
    }
}
