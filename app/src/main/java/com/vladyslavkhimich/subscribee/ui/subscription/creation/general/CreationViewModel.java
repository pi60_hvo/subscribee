package com.vladyslavkhimich.subscribee.ui.subscription.creation.general;

import android.app.Application;

import androidx.annotation.NonNull;

import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;

public abstract class CreationViewModel extends SubscriptionViewModel {
    /*Application Application;
    MutableLiveData<Reminder> SelectedReminder = new MutableLiveData<>();
    MutableLiveData<FreePeriod> SelectedFreePeriod = new MutableLiveData<>();
    MutableLiveData<Integer> SelectedIconColor = new MutableLiveData<>();
    MutableLiveData<Integer> SelectedIconID = new MutableLiveData<>();
    LiveData<List<Cycle>> Cycles;
    MutableLiveData<Cycle> SelectedCycle = new MutableLiveData<>();
    LiveData<List<Currency>> Currencies;
    MutableLiveData<Currency> SelectedCurrency = new MutableLiveData<>();
    LiveData<List<Category>> Categories;
    MutableLiveData<Category> SelectedCategory = new MutableLiveData<>();*/

    public CreationViewModel(@NonNull Application application) {
        super(application);
        setCyclesFromDatabase();
        setCurrenciesFromDatabase();
        setCategoriesFromDatabase();
    }

    /*public void setSelectedReminder(Reminder reminder) {
        SelectedReminder.setValue(reminder);
    }

    public void setSelectedFreePeriod(FreePeriod selectedFreePeriod) {
        SelectedFreePeriod.setValue(selectedFreePeriod);
    }

    public void setSelectedIconColor(Integer selectedIconColor) {
        SelectedIconColor.setValue(selectedIconColor);
    }

    public void setSelectedIconID(Integer selectedIconID) {
        SelectedIconID.setValue(selectedIconID);
    }

    public void setCycles(LiveData<List<Cycle>> cycles) {
        Cycles = cycles;
    }

    public void setSelectedCycle(Cycle selectedCycle) {
        SelectedCycle.setValue(selectedCycle);
    }

    public void setCurrencies(LiveData<List<Currency>> currencies) {
        Currencies = currencies;
    }

    public void setSelectedCurrency(Currency selectedCurrency) {
        SelectedCurrency.setValue(selectedCurrency);
    }

    public void setCategories(LiveData<List<Category>> categories) {
        Categories = categories;
    }

    public void setSelectedCategory(Category selectedCategory) {
        SelectedCategory.setValue(selectedCategory);
    }

    public MutableLiveData<Reminder> getSelectedReminder() {
        return SelectedReminder;
    }

    public Reminder getSelectedReminderObject() {
        return SelectedReminder.getValue();
    }

    public MutableLiveData<FreePeriod> getSelectedFreePeriod() {
        return SelectedFreePeriod;
    }
    public FreePeriod getSelectedFreePeriodObject() {
        return SelectedFreePeriod.getValue();
    }

    public MutableLiveData<Cycle> getSelectedCycle() {
        return SelectedCycle;
    }
    public Cycle getSelectedCycleObject() {
        return SelectedCycle.getValue();
    }

    public MutableLiveData<Integer> getSelectedIconColor() {
        return SelectedIconColor;
    }

    public MutableLiveData<Integer> getSelectedIconID() {
        return SelectedIconID;
    }

    public Integer getSelectedIconIDValue() {
        return SelectedIconID.getValue();
    }

    public Integer getSelectedColorIDValue() {
        return SelectedIconColor.getValue();
    }

    public LiveData<List<Cycle>> getCycles() {
        return Cycles;
    }

    public MutableLiveData<Currency> getSelectedCurrency() {
        return SelectedCurrency;
    }

    public Currency getSelectedCurrencyObject() {
        return SelectedCurrency.getValue();
    }

    public LiveData<List<Currency>> getCurrencies() {
        return Currencies;
    }

    public LiveData<List<Category>> getCategories() {
        return Categories;
    }

    public LiveData<Category> getSelectedCategory() {
        return SelectedCategory;
    }

    public Category getSelectedCategoryObject() {
        return SelectedCategory.getValue();
    }

    public void setCyclesFromDatabase() {
        setCycles(DatabaseRepository.getInstance(Application.getApplicationContext()).getCycles());
    }

    public void setCurrenciesFromDatabase() {
        setCurrencies(DatabaseRepository.getInstance(Application.getApplicationContext()).getCurrencies());
    }

    public void setCategoriesFromDatabase() {
        setCategories(DatabaseRepository.getInstance(Application.getApplicationContext()).getCategories());
    }*/
}
