package com.vladyslavkhimich.subscribee.ui.operations;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.models.OperationModel;
import com.vladyslavkhimich.subscribee.utils.mappers.CollectionsMappersKt;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.OperationsAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.items.OperationListItem;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.MonthRangeDateSelector;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeMap;

public class OperationsFragment extends Fragment {

    private OperationsViewModel operationsViewModel;
    ConstraintLayout operationPlaceholderContainer;
    View datePickerView;
    TextView datePickerTextView;
    RecyclerViewEmptySupport operationsRecyclerView;
    /*ShimmerFrameLayout operationsShimmerContainer;*/
    OperationsAdapter operationsAdapter;
    DatePickerDialog datePickerDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        operationsViewModel =
                new ViewModelProvider(this).get(OperationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_operations, container, false);
        initListeners();
        initializeDatePicker(root);
        initializeRecyclerView(root);
        initDatePickerDialog();
        setDatePickerDate();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("CALLED", "Operations Fragment onResume called");
        //operationsShimmerContainer.startShimmerAnimation();
    }

    @Override
    public void onStart() {
        super.onStart();
        operationsViewModel.loadOperationsWithData();
        Log.i("CALLED", "Operations Fragment onStart called");
    }

    @Override
    public void onPause() {
        super.onPause();
        //operationsShimmerContainer.stopShimmerAnimation();
    }

    private void setDatePickerDate() {
        operationsViewModel.setSelectedDate(Calendar.getInstance());
    }

    void initializeDatePicker(View root) {
        datePickerView = root.findViewById(R.id.date_picker);
        datePickerTextView = root.findViewById(R.id.datePickerTextView);
        //datePickerTextView.setText(DateHelper.getMonthYearString(new Date()));
        setDatePickerViewOnClickListener();
    }

    void setDatePickerViewOnClickListener() {
        datePickerView.setOnClickListener(v -> {
            //datePickerDialog.show();
            MonthRangeDateSelector monthRangeDateSelector = new MonthRangeDateSelector();
            @SuppressLint("RestrictedApi") MaterialDatePicker.Builder<Pair<Long, Long>> materialDatePickerBuilder = MaterialDatePicker.Builder.customDatePicker(monthRangeDateSelector);
            materialDatePickerBuilder.setTitleText(R.string.select_month);
            materialDatePickerBuilder.setTheme(R.style.ThemeOverlay_MaterialComponents_MaterialCalendar);
            MaterialDatePicker<Pair<Long, Long>> materialDatePicker = materialDatePickerBuilder.build();
            materialDatePicker.addOnPositiveButtonClickListener(selection -> {
                Pair<Long, Long> selectedDates = (Pair<Long, Long>) materialDatePicker.getSelection();
                if (selectedDates != null) {
                    Date startDate = new Date((Long) selectedDates.first);
                    //datePickerTextView.setText(DateHelper.getMonthYearString(startDate));
                    Calendar selectedStartDateCalendar = Calendar.getInstance();
                    selectedStartDateCalendar.setTime(startDate);
                    operationsViewModel.setSelectedDate(selectedStartDateCalendar);
                }
            });
            materialDatePicker.show(getChildFragmentManager(), "TAG");
        });
    }

    void initializeRecyclerView(View root) {
        operationsRecyclerView = root.findViewById(R.id.operationsRecyclerView);
        operationPlaceholderContainer = root.findViewById(R.id.operations_placeholder_container);
        //operationsShimmerContainer = root.findViewById(R.id.operationsShimmerContainer);
        operationsAdapter = new OperationsAdapter();
        operationsRecyclerView.setAdapter(operationsAdapter);
        operationsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        operationsRecyclerView.setEmptyView(operationPlaceholderContainer);
    }

    void initDatePickerDialog() {
        datePickerDialog = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            operationsViewModel.setSelectedDate(calendar);
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setMessage(getResources().getString(R.string.dialog_date_select_any_day));
    }

    private void initListeners() {
        operationsViewModel.getSelectedDate().observe(getViewLifecycleOwner(), calendar -> {
            datePickerTextView.setText(DateHelper.getMonthYearString(calendar.getTime()));
            operationsViewModel.loadOperationsWithData();
        });

        operationsViewModel.getOperationsWithData().observe(getViewLifecycleOwner(), operationsWithAssociatedData -> {
            if (operationsWithAssociatedData != null) {
                TreeMap<Date, ArrayList<OperationModel>> treeMap = new TreeMap<>((o1, o2) -> o2.compareTo(o1));
                CollectionsMappersKt.mapFromDB(treeMap, operationsWithAssociatedData);
                ArrayList<OperationListItem> operationListItems = new ArrayList<>();
                CollectionsMappersKt.mapFromTreeMap(operationListItems, treeMap);
                operationsAdapter.setOperationListItems(operationListItems);
            }
        });
    }

}

