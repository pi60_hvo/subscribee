package com.vladyslavkhimich.subscribee.ui.subscription.managing.family

import android.app.Application
import androidx.lifecycle.LiveData
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.partials.MemberWithOperationsAndPaymentDates
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingViewModel

class FamilyManagingViewModel(application: Application) : ManagingViewModel(application) {
    var membersWithData: LiveData<List<MemberWithOperationsAndPaymentDates>>? = null

    override fun loadSubscriptionFromDatabase(id: Int) {
        managingSubscription = DatabaseRepository.getInstance(getApplication()).getObservableManagingSubscriptionByID(id)
    }

    fun loadMembersWithDataFromDatabase(subscriptionID: Int) {
        membersWithData = DatabaseRepository.getInstance(getApplication()).getFamilySubscriptionMembersWithData(subscriptionID)
    }
}