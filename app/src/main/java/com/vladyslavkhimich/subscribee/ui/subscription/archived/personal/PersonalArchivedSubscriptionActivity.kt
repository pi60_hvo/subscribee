package com.vladyslavkhimich.subscribee.ui.subscription.archived.personal

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.models.FreePeriod
import com.vladyslavkhimich.subscribee.models.Payment
import com.vladyslavkhimich.subscribee.models.Reminder
import com.vladyslavkhimich.subscribee.ui.subscription.archived.general.ArchivedSubscriptionActivity
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper
import com.vladyslavkhimich.subscribee.utils.types.primitives.DoubleHelper
import com.vladyslavkhimich.subscribee.utils.types.primitives.StringHelper
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport
import com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.archived.general.ArchivedPaymentsAdapter
import com.facebook.shimmer.ShimmerFrameLayout

open class PersonalArchivedSubscriptionActivity : ArchivedSubscriptionActivity() {
    lateinit var isFamilyPartCheckBox: CheckBox
    lateinit var paymentsRecyclerView: RecyclerViewEmptySupport
    lateinit var paymentsAdapter: ArchivedPaymentsAdapter
    lateinit var noPaymentsTextView: TextView
    lateinit var paymentsShimmerFrameLayout: ShimmerFrameLayout

    override fun initializeViewModel() {
        archivedSubscriptionViewModel = ViewModelProvider(this)[PersonalArchivedSubscriptionViewModel::class.java]
        archivedSubscriptionViewModel.loadSubscriptionFromDB(subscriptionID)
        archivedSubscriptionViewModel.loadPaymentsFromDB(subscriptionID)
    }

    override fun setContentView() {
        setContentView(R.layout.activity_managing_personal)
    }

    override fun initializeUI() {
        setTextViews()
        setTextInputs()
        setCheckBoxes()
        setLayouts()
        setImageViews()
        setRecyclerViews()
    }

    private fun setTextViews() {
        noPaymentsTextView = findViewById(R.id.personalManagingNoPaymentsTextView)

        nextPaymentNoteTextView = findViewById(R.id.next_payment_note_text)
        durationNoteTextView = findViewById(R.id.duration_note_text)
    }

    private fun setTextInputs() {
        durationTextInputLayout = findViewById(R.id.personalManagingDurationTextLayout)
        nameTextInputEditText = findViewById(R.id.personalManagingNameEditText)
        categoryTextInputEditText = findViewById(R.id.personalManagingCategoryEditText)
        descriptionTextInputEditText = findViewById(R.id.personalManagingDescriptionEditText)
        cycleTextInputEditText = findViewById(R.id.personalManagingCycleEditText)
        nextPaymentTextInputEditText = findViewById(R.id.personalManagingNextPaymentEditText)
        freePeriodTextInputEditText = findViewById(R.id.personalManagingDurationEditText)
        priceTextInputEditText = findViewById(R.id.personalManagingPriceEditText)
        currencyTextInputEditText = findViewById(R.id.personalManagingCurrencyEditText)
        reminderTextInputEditText = findViewById(R.id.personalManagingReminderEditText)
    }

    private fun setCheckBoxes() {
        freePeriodCheckBox = findViewById(R.id.personalManagingFreePeriodCheckBox)
        isFamilyPartCheckBox = findViewById(R.id.personalManagingFamilyPartCheckBox)
    }

    private fun setLayouts() {
        subscriptionConstraintLayout = findViewById(R.id.personalManagingConstraintLayout)
        iconContainerConstraintLayout = findViewById(R.id.personalManagingIconContainer)
        iconChangeLinearLayout = findViewById(R.id.personalManagingChangeIconLayout)
        paymentsShimmerFrameLayout = findViewById(R.id.personalManagingPaymentsShimmerLayout)
    }

    private fun setImageViews() {
        iconImageView = findViewById(R.id.personalManagingIconImageView)
    }

    private fun setRecyclerViews() {
        paymentsRecyclerView = findViewById(R.id.personalManagingPaymentsRecyclerView)
        setPaymentsRecyclerView()
    }

    private fun setPaymentsRecyclerView() {
        paymentsRecyclerView.setHasFixedSize(true)
        paymentsAdapter = ArchivedPaymentsAdapter()
        paymentsRecyclerView.adapter = paymentsAdapter
        paymentsRecyclerView.setEmptyView(noPaymentsTextView)
        paymentsRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    override fun disableUI() {
        nameTextInputEditText.isEnabled = false
        categoryTextInputEditText.isEnabled = false
        descriptionTextInputEditText.isEnabled = false
        cycleTextInputEditText.isEnabled = false
        nextPaymentTextInputEditText.isEnabled = false
        freePeriodTextInputEditText.isEnabled = false
        priceTextInputEditText.isEnabled = false
        currencyTextInputEditText.isEnabled = false
        reminderTextInputEditText.isEnabled = false

        freePeriodCheckBox.isEnabled = false
        isFamilyPartCheckBox.isEnabled = false

        iconChangeLinearLayout.visibility = View.GONE
        paymentsShimmerFrameLayout.visibility = View.GONE
    }

    override fun setObservers() {
        (archivedSubscriptionViewModel as PersonalArchivedSubscriptionViewModel).personalSubscription.observe(this, {
            if (it != null) {
                supportActionBar?.title = it.subscription.name
                nameTextInputEditText.setText(it.subscription.name)
                descriptionTextInputEditText.setText(it.subscription.description)
                priceTextInputEditText.setText(DoubleHelper.getStringFromDouble(it.subscription.price))
                nextPaymentTextInputEditText.setText(DateHelper.getNextPaymentDate(it.subscription.nextPaymentDate))
                currencyTextInputEditText.setText(it.currency.shortName)
                cycleTextInputEditText.setText(StringHelper.getStringFromCycle(this, it.cycle))

                if (it.reminder != null)
                    reminderTextInputEditText.setText(StringHelper.getStringFromReminder(this, Reminder(it.reminder)))

                if (it.category.isUserCreated)
                    categoryTextInputEditText.setText(it.category.name)
                else
                    categoryTextInputEditText.setText(ResourcesHelper.getStringIDFromName(this, it.category.name))

                toggleFreePeriodViews(it.freePeriod != null)
                if (it.freePeriod != null)
                    freePeriodTextInputEditText.setText(StringHelper.getStringFromFreePeriod(this, FreePeriod(it.freePeriod)))

                iconImageView.setImageResource(ResourcesHelper.getImageIDFromName(this, it.subscription.icon))

                val iconBackgroundDrawable: GradientDrawable = iconContainerConstraintLayout.background as GradientDrawable
                iconBackgroundDrawable.setColor(Color.parseColor(it.subscription.iconColor))
                iconContainerConstraintLayout.background = iconBackgroundDrawable

                isFamilyPartCheckBox.isChecked = it.subscription.isFamilyPart
            }
        })

        (archivedSubscriptionViewModel as PersonalArchivedSubscriptionViewModel).paymentsDatesWithOperations.observe(this, {
            if (it != null) {
                val paymentsArrayList = ArrayList<Payment>()
                it.forEach {
                    paymentDateWithOperation ->
                    paymentsArrayList.add(Payment(paymentDateWithOperation.paymentDate.paymentDate, paymentDateWithOperation.operations.isNotEmpty()))
                }
                paymentsAdapter.setPaymentsAndNotify(paymentsArrayList)
            }
        })
    }
}