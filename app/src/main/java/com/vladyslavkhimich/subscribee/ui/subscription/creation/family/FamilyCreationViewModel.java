package com.vladyslavkhimich.subscribee.ui.subscription.creation.family;

import android.app.Application;

import androidx.annotation.NonNull;

import com.vladyslavkhimich.subscribee.ui.subscription.creation.general.CreationViewModel;

public class FamilyCreationViewModel extends CreationViewModel {
    public FamilyCreationViewModel(@NonNull Application application) {
        super(application);
    }
}
