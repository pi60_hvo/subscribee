package com.vladyslavkhimich.subscribee.ui.subscription.archived.familypart

import androidx.recyclerview.widget.LinearLayoutManager
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.ui.subscription.archived.personal.PersonalArchivedSubscriptionActivity
import com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.archived.general.ArchivedPaymentsAdapter

class FamilyPartArchivedSubscriptionActivity : PersonalArchivedSubscriptionActivity() {
    override fun setContentView() {
        setContentView(R.layout.activity_managing_family_part)
    }

    override fun initializeUI() {
        setTextViews()
        setTextInputs()
        setCheckBoxes()
        setLayouts()
        setImageViews()
        setRecyclerViews()
    }

    private fun setTextViews() {
        noPaymentsTextView = findViewById(R.id.familyPartManagingNoPaymentsTextView)

        nextPaymentNoteTextView = findViewById(R.id.next_payment_note_text)
        durationNoteTextView = findViewById(R.id.duration_note_text)
    }

    private fun setTextInputs() {
        durationTextInputLayout = findViewById(R.id.familyPartManagingDurationTextLayout)
        nameTextInputEditText = findViewById(R.id.familyPartManagingNameEditText)
        categoryTextInputEditText = findViewById(R.id.familyPartManagingCategoryEditText)
        descriptionTextInputEditText = findViewById(R.id.familyPartManagingDescriptionEditText)
        cycleTextInputEditText = findViewById(R.id.familyPartManagingCycleEditText)
        nextPaymentTextInputEditText = findViewById(R.id.familyPartManagingNextPaymentEditText)
        freePeriodTextInputEditText = findViewById(R.id.familyPartManagingDurationEditText)
        priceTextInputEditText = findViewById(R.id.familyPartManagingPriceEditText)
        currencyTextInputEditText = findViewById(R.id.familyPartManagingCurrencyEditText)
        reminderTextInputEditText = findViewById(R.id.familyPartManagingReminderEditText)
    }

    private fun setCheckBoxes() {
        freePeriodCheckBox = findViewById(R.id.familyPartManagingFreePeriodCheckBox)
        isFamilyPartCheckBox = findViewById(R.id.familyPartManagingFamilyPartCheckBox)
    }

    private fun setLayouts() {
        subscriptionConstraintLayout = findViewById(R.id.familyPartManagingConstraintLayout)
        iconContainerConstraintLayout = findViewById(R.id.familyPartManagingIconContainer)
        iconChangeLinearLayout = findViewById(R.id.familyPartManagingChangeIconLayout)
        paymentsShimmerFrameLayout = findViewById(R.id.familyPartManagingPaymentsShimmerLayout)
    }

    private fun setImageViews() {
        iconImageView = findViewById(R.id.familyPartManagingIconImageView)
    }

    private fun setRecyclerViews() {
        paymentsRecyclerView = findViewById(R.id.familyPartManagingPaymentsRecyclerView)
        setPaymentsRecyclerView()
    }

    private fun setPaymentsRecyclerView() {
        paymentsRecyclerView.setHasFixedSize(true)
        paymentsAdapter = ArchivedPaymentsAdapter()
        paymentsRecyclerView.adapter = paymentsAdapter
        paymentsRecyclerView.setEmptyView(noPaymentsTextView)
        paymentsRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }
}