package com.vladyslavkhimich.subscribee.ui.subscription.creation.personal;

import android.app.Application;

import androidx.annotation.NonNull;

import com.vladyslavkhimich.subscribee.ui.subscription.creation.general.CreationViewModel;

public class PersonalCreationViewModel extends CreationViewModel {
    public PersonalCreationViewModel(@NonNull Application application) {
        super(application);
    }
}
