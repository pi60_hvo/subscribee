package com.vladyslavkhimich.subscribee.ui.subscriptions;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vladyslavkhimich.subscribee.MainActivity;
import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.ui.subscription.archived.family.FamilyArchivedSubscriptionActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.archived.familypart.FamilyPartArchivedSubscriptionActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.archived.personal.PersonalArchivedSubscriptionActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.family.FamilyCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.personal.PersonalCreationActivity;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingActivity;
import com.vladyslavkhimich.subscribee.utils.enums.SubscriptionSortings;
import com.vladyslavkhimich.subscribee.utils.types.complex.ViewAnimator;
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.subscriptions.ArchivedSubscriptionsAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.subscriptions.FamilySubscriptionsAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.subscriptions.PersonalSubscriptionsAdapter;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SubscriptionsFragment extends Fragment {

    public static final String PERSONAL_SORTING_KEY = "personal_sorting";
    public static final String FAMILY_SORTING_KEY = "family_sorting";
    public static final String ARCHIVED_SORTING_KEY = "archived_sorting";

    ConstraintLayout mainContainer;
    ConstraintLayout personalSubscriptionsContainer;
    ConstraintLayout familySubscriptionsContainer;
    ConstraintLayout archivedSubscriptionsContainer;
    SubscriptionsViewModel subscriptionsViewModel;
    ShimmerFrameLayout personalShimmerFrameLayout;
    ShimmerFrameLayout familyShimmerFrameLayout;
    ShimmerFrameLayout archivedShimmerFrameLayout;
    FloatingActionButton mainFAB;
    FloatingActionButton personalFAB;
    FloatingActionButton familyFAB;
    RecyclerViewEmptySupport personalRecyclerView;
    PersonalSubscriptionsAdapter personalSubscriptionsAdapter;
    RecyclerViewEmptySupport familyRecyclerView;
    FamilySubscriptionsAdapter familySubscriptionsAdapter;
    RecyclerViewEmptySupport archivedRecyclerView;
    ArchivedSubscriptionsAdapter archivedSubscriptionsAdapter;
    View personalSortByView;
    View familySortByView;
    View archivedSortByView;
    ImageView allSubscriptionsPlaceholderImageView;

    boolean isRotateMainFAB = false;

    boolean isPersonalAbsent;
    boolean isFamilyAbsent;
    boolean isArchivedAbsent;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        subscriptionsViewModel =
                new ViewModelProvider(this).get(SubscriptionsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_subscriptions, container, false);
        initializeOtherUI(root);
        initializeShimmerFrameLayouts(root);
        initializeFABs(root);
        setRecyclerViews(root);
        setSortByViews(root);
        hideSubscriptionsIfAbsent(true);
        setObservers();
        loadSortingsPreferences();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        startShimmerAnimations();
    }

    @Override
    public void onPause() {
        Log.i("CALLED", "SubscriptionsFragment onPause called");
        super.onPause();
        stopShimmerAnimations();
    }

    @Override
    public void onStop() {
        Log.i("CALLED", "SubscriptionsFragment onStop called");
        super.onStop();
        if (isRotateMainFAB) {
            mainFAB.performClick();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity)requireActivity()).toggleNotArchivedSelectionOptionsMenu(false);
        ((MainActivity)requireActivity()).toggleArchivedSelectionOptionsMenu(false);
    }

    void startShimmerAnimations() {
        personalShimmerFrameLayout.startShimmerAnimation();
        familyShimmerFrameLayout.startShimmerAnimation();
        archivedShimmerFrameLayout.startShimmerAnimation();
    }

    void stopShimmerAnimations() {
        personalShimmerFrameLayout.stopShimmerAnimation();
        familyShimmerFrameLayout.stopShimmerAnimation();
        archivedShimmerFrameLayout.stopShimmerAnimation();
    }

    void initializeOtherUI(View root) {
        mainContainer = root.findViewById(R.id.subscriptionsFragmentMainContainer);
        personalSubscriptionsContainer = root.findViewById(R.id.personalSubscriptionsFragmentContainer);
        familySubscriptionsContainer = root.findViewById(R.id.familySubscriptionsFragmentContainer);
        archivedSubscriptionsContainer = root.findViewById(R.id.archivedSubscriptionsFragmentContainer);
        allSubscriptionsPlaceholderImageView = root.findViewById(R.id.allSubscriptionsPlaceholderImageView);
    }

    void initializeShimmerFrameLayouts(View root) {
        personalShimmerFrameLayout = root.findViewById(R.id.personalShimmerContainer);
        familyShimmerFrameLayout = root.findViewById(R.id.familyShimmerContainer);
        archivedShimmerFrameLayout = root.findViewById(R.id.archivedShimmerContainer);
    }

    void initializeFABs(View root) {
        personalFAB = root.findViewById(R.id.fabPersonal);
        familyFAB = root.findViewById(R.id.fabFamily);
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            ViewAnimator.init(personalFAB);
            ViewAnimator.init(familyFAB);
        }, 50);
        mainFAB = root.findViewById(R.id.fabSubscriptions);
        setFABsOnClickListeners();
    }

    void setFABsOnClickListeners() {
        mainFAB.setOnClickListener(v -> {
            isRotateMainFAB = ViewAnimator.rotateFab(v, !isRotateMainFAB);
            if (isRotateMainFAB) {
                ViewAnimator.showIn(familyFAB);
                ViewAnimator.showIn(personalFAB);
            }
            else {
                hideMiniFABs();
            }
        });

        personalFAB.setOnClickListener(v -> {
            Intent personalCreationActivityIntent = new Intent(getContext(), PersonalCreationActivity.class);
            startActivity(personalCreationActivityIntent);
        });

        familyFAB.setOnClickListener(v -> {
            Intent familyCreationActivityIntent = new Intent(getContext(), FamilyCreationActivity.class);
            startActivity(familyCreationActivityIntent);
        });
    }

    void hideMiniFABs() {
        ViewAnimator.showOut(familyFAB);
        ViewAnimator.showOut(personalFAB);
    }

    void setRecyclerViews(View root) {
        initializeRecyclerViews(root);
        setPersonalRecyclerView();
        setFamilyRecyclerView();
        setArchivedRecyclerView();
    }

    void initializeRecyclerViews(View root) {
        personalRecyclerView = root.findViewById(R.id.personalRecyclerView);
        familyRecyclerView = root.findViewById(R.id.familyRecyclerView);
        archivedRecyclerView = root.findViewById(R.id.archivedRecyclerView);
    }

    void setPersonalRecyclerView() {
        personalSubscriptionsAdapter = new PersonalSubscriptionsAdapter((view, position) -> {
            if (archivedSubscriptionsAdapter.getSelectedCount() != 0) {
                Toast.makeText(getContext(), R.string.toast_clear_archived_selected, Toast.LENGTH_SHORT).show();
            }
            else {
                if (!personalSubscriptionsAdapter.checkIfItemSelected(position))
                    personalSubscriptionsAdapter.addItemSelected(position);
                else
                    personalSubscriptionsAdapter.removeItemSelected(position);
                ((MainActivity) requireActivity()).toggleNotArchivedSelectionOptionsMenu(checkIfAnyNotArchivedItemSelected());
                if (checkIfAnyNotArchivedItemSelected())
                    ((MainActivity) requireActivity()).setSelectedSubscriptionsToolbarTitle(getNotArchivedSelectedItemsCount());
            }
        });
        personalRecyclerView.setAdapter(personalSubscriptionsAdapter);
        personalRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        personalRecyclerView.setHasFixedSize(true);
        personalRecyclerView.setEmptyView(personalShimmerFrameLayout);
    }

    void setFamilyRecyclerView() {
        familySubscriptionsAdapter = new FamilySubscriptionsAdapter(((view, position) -> {
            if (archivedSubscriptionsAdapter.getSelectedCount() != 0) {
                Toast.makeText(getContext(), R.string.toast_clear_archived_selected, Toast.LENGTH_SHORT).show();
            }
            else {
                if (!familySubscriptionsAdapter.checkIfItemSelected(position))
                    familySubscriptionsAdapter.addItemSelected(position);
                else
                    familySubscriptionsAdapter.removeItemSelected(position);
                ((MainActivity)requireActivity()).toggleNotArchivedSelectionOptionsMenu(checkIfAnyNotArchivedItemSelected());
                if (checkIfAnyNotArchivedItemSelected())
                    ((MainActivity)requireActivity()).setSelectedSubscriptionsToolbarTitle(getNotArchivedSelectedItemsCount());
            }
        }));

        familyRecyclerView.setAdapter(familySubscriptionsAdapter);
        familyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        familyRecyclerView.setHasFixedSize(true);
        familyRecyclerView.setEmptyView(familyShimmerFrameLayout);
    }

    void setArchivedRecyclerView() {
        archivedSubscriptionsAdapter = new ArchivedSubscriptionsAdapter((view, position) -> {
            if (getNotArchivedSelectedItemsCount() != 0)
                Toast.makeText(getContext(), R.string.toast_clear_selected, Toast.LENGTH_SHORT).show();
            else {
                if (!archivedSubscriptionsAdapter.checkIfItemSelected(position))
                    archivedSubscriptionsAdapter.addItemSelected(position);
                else
                    archivedSubscriptionsAdapter.removeItemSelected(position);
                ((MainActivity)requireActivity()).toggleArchivedSelectionOptionsMenu(archivedSubscriptionsAdapter.getSelectedCount() != 0);
                if (archivedSubscriptionsAdapter.getSelectedCount() != 0)
                    ((MainActivity)requireActivity()).setSelectedSubscriptionsToolbarTitle(archivedSubscriptionsAdapter.getSelectedCount());
            }
        },
                (view, position) -> {
                    if (archivedSubscriptionsAdapter.getSelectedCount() == 0) {
                        Subscription clickedSubscription = archivedSubscriptionsAdapter.getArchivedSubscriptions().get(position);
                        Bundle bundle = new Bundle();
                        bundle.putInt(ManagingActivity.ID_KEY, clickedSubscription.subscriptionID);
                        if (clickedSubscription.isFamilyPart) {
                            Intent familyPartSubscriptionIntent = new Intent(getContext(), FamilyPartArchivedSubscriptionActivity.class).putExtras(bundle);
                            startActivity(familyPartSubscriptionIntent);
                        }
                        else if (!clickedSubscription.isFamily) {
                            Intent personalSubscriptionIntent = new Intent(getContext(), PersonalArchivedSubscriptionActivity.class).putExtras(bundle);
                            startActivity(personalSubscriptionIntent);
                        }
                        else {
                            Intent familySubscriptionIntent = new Intent(getContext(), FamilyArchivedSubscriptionActivity.class).putExtras(bundle);
                            startActivity(familySubscriptionIntent);
                        }
                    }
                });

        archivedRecyclerView.setAdapter(archivedSubscriptionsAdapter);
        archivedRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        archivedRecyclerView.setHasFixedSize(true);
        archivedRecyclerView.setEmptyView(archivedShimmerFrameLayout);
    }

    void setObservers() {
        subscriptionsViewModel.getPersonalSubscriptions().observe(getViewLifecycleOwner(), personalSubscriptions -> {
            hideOrShowPersonalIfAbsent(false);
            hideOrShowAllSubscriptionsPlaceholder();
            ViewAnimator.animateViewFade(personalSubscriptionsContainer, personalSortByView, personalSubscriptions.size() > 1);
            if (isPersonalAbsent) {
                new Timer().schedule(
                        new TimerTask() {
                            @SuppressLint("NotifyDataSetChanged")
                            @Override
                            public void run() {
                                if (isAdded())
                                    requireActivity().runOnUiThread(() -> {
                                        personalSubscriptionsAdapter.setPersonalSubscriptions(new ArrayList<>(personalSubscriptions));
                                        personalSubscriptionsAdapter.notifyDataSetChanged();
                                    });

                            }
                        }
                        , 600);
            }
            else {
                personalSubscriptionsAdapter.setPersonalSubscriptions(new ArrayList<>(personalSubscriptions));
                personalSubscriptionsAdapter.sortSubscriptions(subscriptionsViewModel.getPersonalSubscriptionsSortingValue());
                ViewAnimator.animateInRecyclerView(personalRecyclerView);
            }
        });

        subscriptionsViewModel.getFamilySubscriptions().observe(getViewLifecycleOwner(), familySubscriptions -> {
            hideOrShowFamilyIfAbsent(false);
            hideOrShowAllSubscriptionsPlaceholder();
            ViewAnimator.animateViewFade(familySubscriptionsContainer, familySortByView, familySubscriptions.size() > 1);
            if (isFamilyAbsent) {
                new Timer().schedule(
                        new TimerTask() {
                            @SuppressLint("NotifyDataSetChanged")
                            @Override
                            public void run() {
                                if (isAdded())
                                    requireActivity().runOnUiThread(() -> {
                                        familySubscriptionsAdapter.setFamilySubscriptions(new ArrayList<>(familySubscriptions));
                                        familySubscriptionsAdapter.notifyDataSetChanged();
                                    });

                            }
                        }
                        , 600);
            }
            else {
                familySubscriptionsAdapter.setFamilySubscriptions(new ArrayList<>(familySubscriptions));
                familySubscriptionsAdapter.sortSubscriptions(subscriptionsViewModel.getFamilySubscriptionSortingValue());
                ViewAnimator.animateInRecyclerView(familyRecyclerView);
            }
        });

        subscriptionsViewModel.getArchivedSubscriptions().observe(getViewLifecycleOwner(), archivedSubscriptions -> {
            hideOrShowArchivedIfAbsent(false);
            hideOrShowAllSubscriptionsPlaceholder();
            ViewAnimator.animateViewFade(archivedSubscriptionsContainer, archivedSortByView, archivedSubscriptions.size() > 1);
            if (isArchivedAbsent) {
                new Timer().schedule(
                        new TimerTask() {
                            @SuppressLint("NotifyDataSetChanged")
                            @Override
                            public void run() {
                                if (isAdded())
                                    requireActivity().runOnUiThread(() -> {
                                        archivedSubscriptionsAdapter.setArchivedSubscriptions(new ArrayList<>(archivedSubscriptions));
                                        archivedSubscriptionsAdapter.notifyDataSetChanged();
                                    });

                            }
                        }
                        , 600);
            }
            else {
                archivedSubscriptionsAdapter.setArchivedSubscriptions(new ArrayList<>(archivedSubscriptions));
                archivedSubscriptionsAdapter.sortSubscriptions(subscriptionsViewModel.getArchivedSubscriptionSortingValue());
                ViewAnimator.animateInRecyclerView(archivedRecyclerView);
            }
        });

        subscriptionsViewModel.getPersonalSubscriptionSorting().observe(getViewLifecycleOwner(), subscriptionSortings -> {
            personalSubscriptionsAdapter.sortSubscriptions(subscriptionSortings);
            if (getActivity() != null) {
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString(PERSONAL_SORTING_KEY, subscriptionSortings.getCode()).apply();
            }
        });

        subscriptionsViewModel.getFamilySubscriptionSorting().observe(getViewLifecycleOwner(), subscriptionSortings -> {
            familySubscriptionsAdapter.sortSubscriptions(subscriptionSortings);
            if (getActivity() != null) {
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString(FAMILY_SORTING_KEY, subscriptionSortings.getCode()).apply();
            }
        });

        subscriptionsViewModel.getArchivedSubscriptionSorting().observe(getViewLifecycleOwner(), subscriptionSortings -> {
            archivedSubscriptionsAdapter.sortSubscriptions(subscriptionSortings);
            if (getActivity() != null) {
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString(ARCHIVED_SORTING_KEY, subscriptionSortings.getCode()).apply();
            }
        });
    }

    void loadSortingsPreferences() {
        if (getActivity() != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            subscriptionsViewModel
                    .setPersonalSubscriptionSorting(
                            SubscriptionSortings
                                    .Companion
                                    .lookupByCode(
                                            sharedPreferences
                                                    .getString(PERSONAL_SORTING_KEY, "N")));

            subscriptionsViewModel
                    .setFamilySubscriptionSorting(
                            SubscriptionSortings
                                    .Companion
                                    .lookupByCode(
                                            sharedPreferences
                                            .getString(FAMILY_SORTING_KEY, "N")
                                    )
            );

            subscriptionsViewModel
                    .setArchivedSubscriptionSorting(
                            SubscriptionSortings
                                    .Companion
                                    .lookupByCode(
                                            sharedPreferences
                                                    .getString(ARCHIVED_SORTING_KEY, "N")
                                    )
                    );
        }
    }

    void setSortByViews(View root) {
        initializeSortByViews(root);
        setPersonalSortByView();
        setFamilySortByView();
        setArchivedSortByView();
    }

    void initializeSortByViews(View root) {
        personalSortByView = root.findViewById(R.id.personalSort);
        familySortByView = root.findViewById(R.id.familySort);
        archivedSortByView = root.findViewById(R.id.archivedSort);
    }

    void setPersonalSortByView() {
        personalSortByView.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(getContext(), v, Gravity.END);
            popupMenu.getMenuInflater().inflate(R.menu.menu_sort_subscriptions, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                int id = item.getItemId();
                if (id == R.id.sort_name) {
                    subscriptionsViewModel.setPersonalSubscriptionSorting(SubscriptionSortings.NAME);
                    return true;
                }
                else if (id == R.id.sort_price) {
                    subscriptionsViewModel.setPersonalSubscriptionSorting(SubscriptionSortings.PRICE);
                    return true;
                }
                else if (id == R.id.sort_date) {
                    subscriptionsViewModel.setPersonalSubscriptionSorting(SubscriptionSortings.DATE);
                    return true;
                }
                return false;
            });
            popupMenu.show();
        });
    }

    void setFamilySortByView() {
        familySortByView.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(getContext(), v, Gravity.END);
            popupMenu.getMenuInflater().inflate(R.menu.menu_sort_subscriptions, popupMenu.getMenu());
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(item -> {
                int id = item.getItemId();
                if (id == R.id.sort_name) {
                    subscriptionsViewModel.setFamilySubscriptionSorting(SubscriptionSortings.NAME);
                    return true;
                }
                else if (id == R.id.sort_price) {
                    subscriptionsViewModel.setFamilySubscriptionSorting(SubscriptionSortings.PRICE);
                    return true;
                }
                else if (id == R.id.sort_date) {
                    subscriptionsViewModel.setFamilySubscriptionSorting(SubscriptionSortings.DATE);
                    return true;
                }
                return false;
            });
        });
    }

    void setArchivedSortByView() {
        archivedSortByView.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(getContext(), v, Gravity.END);
            popupMenu.getMenuInflater().inflate(R.menu.menu_sort_subscriptions, popupMenu.getMenu());
            popupMenu.show();
            popupMenu.setOnMenuItemClickListener(item -> {
                int id = item.getItemId();
                if (id == R.id.sort_name) {
                    subscriptionsViewModel.setArchivedSubscriptionSorting(SubscriptionSortings.NAME);
                    return true;
                }
                else if (id == R.id.sort_price) {
                    subscriptionsViewModel.setArchivedSubscriptionSorting(SubscriptionSortings.PRICE);
                    return true;
                }
                else if (id == R.id.sort_date) {
                    subscriptionsViewModel.setArchivedSubscriptionSorting(SubscriptionSortings.DATE);
                    return true;
                }
                return false;
            });
        });
    }

    void hideSubscriptionsIfAbsent(boolean isFirstRun) {
        hideOrShowPersonalIfAbsent(isFirstRun);
        hideOrShowFamilyIfAbsent(isFirstRun);
        hideOrShowArchivedIfAbsent(isFirstRun);
    }

    void hideOrShowPersonalIfAbsent(boolean isFirstRun) {
        boolean isDatabasePersonalAbsent = checkIfPersonalAbsent();
        if (!isFirstRun) {
            if (isDatabasePersonalAbsent && personalSubscriptionsContainer.getVisibility() == View.VISIBLE) {
                isPersonalAbsent = true;
                ViewAnimator.animateViewFade(personalSubscriptionsContainer, false);
            }
            else if (!isDatabasePersonalAbsent && personalSubscriptionsContainer.getVisibility() == View.GONE) {
                isPersonalAbsent = false;
                ViewAnimator.animateViewFade(personalSubscriptionsContainer, true);
            }
        }
        else {
            if (isDatabasePersonalAbsent) {
                isPersonalAbsent = true;
                personalSubscriptionsContainer.setVisibility(View.GONE);
            }
            else {
                isPersonalAbsent = false;
                personalSubscriptionsContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    boolean checkIfPersonalAbsent() {
        return DatabaseRepository.getInstance(getContext()).getPersonalSubscriptionsCount() == 0;
    }

    void hideOrShowFamilyIfAbsent(boolean isFirstRun) {
        boolean isDatabaseFamilyAbsent = checkIfFamilyIsAbsent();
        if (!isFirstRun) {
            if (isDatabaseFamilyAbsent && familySubscriptionsContainer.getVisibility() == View.VISIBLE) {
                isFamilyAbsent = true;
                ViewAnimator.animateViewFade(familySubscriptionsContainer, false);
            }
            else if (!isDatabaseFamilyAbsent && familySubscriptionsContainer.getVisibility() == View.GONE) {
                isFamilyAbsent = false;
                ViewAnimator.animateViewFade(familySubscriptionsContainer, true);
            }
        }
        else {
            if (isDatabaseFamilyAbsent) {
                isFamilyAbsent = true;
                familySubscriptionsContainer.setVisibility(View.GONE);
            }
            else {
                isFamilyAbsent = false;
                familySubscriptionsContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    boolean checkIfFamilyIsAbsent() {
        return DatabaseRepository.getInstance(getContext()).getFamilySubscriptionsCount() == 0;
    }

    void hideOrShowArchivedIfAbsent(boolean isFirstRun) {
        boolean isDatabaseArchivedAbsent = checkIfArchivedIsAbsent();
        if (!isFirstRun) {
            if (isDatabaseArchivedAbsent && archivedSubscriptionsContainer.getVisibility() == View.VISIBLE) {
                isArchivedAbsent = true;
                ViewAnimator.animateViewFade(archivedSubscriptionsContainer, false);
            }
            else if (!isDatabaseArchivedAbsent && archivedSubscriptionsContainer.getVisibility() == View.GONE) {
                isArchivedAbsent = false;
                ViewAnimator.animateViewFade(archivedSubscriptionsContainer, true);
            }
        }
        else {
            if (isDatabaseArchivedAbsent) {
                isArchivedAbsent = true;
                archivedSubscriptionsContainer.setVisibility(View.GONE);
            }
            else {
                isArchivedAbsent = false;
                archivedSubscriptionsContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    boolean checkIfArchivedIsAbsent() {
        return DatabaseRepository.getInstance(getContext()).getArchivedSubscriptionsCount() == 0;
    }

    void hideOrShowAllSubscriptionsPlaceholder() {
        if (checkIfPersonalAbsent() && checkIfFamilyIsAbsent() && checkIfArchivedIsAbsent()) {
            ViewAnimator.animateViewFadeInAndGoneOut(allSubscriptionsPlaceholderImageView, true);
        }
        else {
            if (allSubscriptionsPlaceholderImageView.getVisibility() == View.VISIBLE) {
                ViewAnimator.animateViewFadeInAndGoneOut(allSubscriptionsPlaceholderImageView, false);
            }
        }
    }

    public void deselectAllSelectedSubscriptions() {
        personalSubscriptionsAdapter.clearSelectedArrayList();
        familySubscriptionsAdapter.clearSelectedArrayList();
        archivedSubscriptionsAdapter.clearSelectedArrayList();
    }

    public boolean checkIfAnyNotArchivedItemSelected() {
        return personalSubscriptionsAdapter.getSelectedCount() != 0 || familySubscriptionsAdapter.getSelectedCount() != 0;
    }

    public boolean checkIfAnyArchivedItemSelected() {
        return archivedSubscriptionsAdapter.getSelectedCount() != 0;
    }

    public int getNotArchivedSelectedItemsCount() {
        return personalSubscriptionsAdapter.getSelectedCount() + familySubscriptionsAdapter.getSelectedCount();
    }

    public int getArchivedSelectedItemsCount() {
        return archivedSubscriptionsAdapter.getSelectedCount();
    }

    public ArrayList<Integer> getAllSelectedSubscriptionsIDs() {
        ArrayList<Integer> selectedSubscriptions = new ArrayList<>();
        selectedSubscriptions.addAll(personalSubscriptionsAdapter.getSelectedSubscriptionsIDs());
        selectedSubscriptions.addAll(familySubscriptionsAdapter.getSelectedSubscriptionsIDs());
        selectedSubscriptions.addAll(archivedSubscriptionsAdapter.getSelectedSubscriptionsIDs());
        return selectedSubscriptions;
    }
}