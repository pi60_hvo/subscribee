package com.vladyslavkhimich.subscribee.ui.subscription.archived.personal

import android.app.Application
import androidx.lifecycle.LiveData
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.partials.ManagingSubscription
import com.vladyslavkhimich.subscribee.database.entities.partials.PaymentDatesWithOperations
import com.vladyslavkhimich.subscribee.ui.subscription.archived.general.ArchivedSubscriptionViewModel

class PersonalArchivedSubscriptionViewModel(application: Application) : ArchivedSubscriptionViewModel(application) {
    lateinit var personalSubscription: LiveData<ManagingSubscription>
    lateinit var paymentsDatesWithOperations: LiveData<List<PaymentDatesWithOperations>>

    override fun loadSubscriptionFromDB(subscriptionID: Int) {
        personalSubscription = DatabaseRepository.getInstance(getApplication()).getObservableManagingSubscriptionByID(subscriptionID)
    }

    override fun loadPaymentsFromDB(subscriptionID: Int) {
        paymentsDatesWithOperations = DatabaseRepository.getInstance(getApplication()).getObservablePaymentsDatesWithOperationsBySubscriptionID(subscriptionID)
    }
}