package com.vladyslavkhimich.subscribee.ui.subscription.managing.personal;

import android.app.AlertDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.database.entities.FreePeriod;
import com.vladyslavkhimich.subscribee.database.entities.Operation;
import com.vladyslavkhimich.subscribee.database.entities.PaymentDate;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.database.entities.partials.ManagingSubscription;
import com.vladyslavkhimich.subscribee.database.entities.partials.PaymentDatesWithOperations;
import com.vladyslavkhimich.subscribee.models.Payment;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingActivity;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.PersonalPaymentsAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal.PersonalManagingCategoryDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal.PersonalManagingCurrencyDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal.PersonalManagingCycleDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal.PersonalManagingFreePeriodDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal.PersonalManagingIconPickerDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal.PersonalManagingNextPaymentDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.personal.PersonalManagingReminderDialog;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class PersonalManagingActivity extends ManagingActivity {
    public CheckBox familyPartCheckBox;
    public RecyclerViewEmptySupport paymentsRecyclerView;
    public PersonalPaymentsAdapter paymentsAdapter;
    public ShimmerFrameLayout paymentsShimmerFrameLayout;
    public TextView noPaymentsTextView;

    @Override
    protected void onResume() {
        super.onResume();
        startShimmerAnimation();
    }

    @Override
    protected void onPause() {
        stopShimmerAnimation();
        super.onPause();
    }

    void startShimmerAnimation() {
        paymentsShimmerFrameLayout.startShimmerAnimation();
    }

    void stopShimmerAnimation() {
        paymentsShimmerFrameLayout.stopShimmerAnimation();
    }

    @Override
    public void initializeViewModel() {
        subscriptionViewModel = new ViewModelProvider(this).get(PersonalManagingViewModel.class);
        ((PersonalManagingViewModel)subscriptionViewModel).loadSubscriptionFromDatabase(subscriptionID);
    }

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_managing_personal);
    }

    @Override
    public void initializeUI() {
        setLayouts();
        setTextViews();
        setTextInputs();
        setCheckboxes();
        setImageViews();
        setButtons();
        setRecyclerViews();
        setAdditionalOnClickListeners();
    }

    void setLayouts() {
        subscriptionConstraintLayout = findViewById(R.id.personalManagingConstraintLayout);
        iconContainerConstraintLayout = findViewById(R.id.personalManagingIconContainer);
        paymentsConstraintLayout = findViewById(R.id.personalManagingPaymentsContainer);
        iconChangeLinearLayout = findViewById(R.id.personalManagingChangeIconLayout);

        durationConstraintLayout = findViewById(R.id.duration_container);
        durationConstraintLayout.setVisibility(View.GONE);
    }

    void setTextViews() {
        noPaymentsTextView = findViewById(R.id.personalManagingNoPaymentsTextView);
        noPaymentsTextView.setVisibility(View.GONE);

        noteTextView = findViewById(R.id.next_payment_note_text);
        noteTextView.setVisibility(View.GONE);
    }

    void setTextInputs() {
        reminderTextInputLayout = findViewById(R.id.personalManagingReminderTextLayout);
        durationTextInputLayout = findViewById(R.id.personalManagingDurationTextLayout);
        nameTextInputLayout = findViewById(R.id.personalManagingNameTextLayout);
        descriptionTextInputLayout = findViewById(R.id.personalManagingDescriptionTextLayout);
        categoryTextInputLayout = findViewById(R.id.personalManagingCategoryTextLayout);
        cycleTextInputLayout = findViewById(R.id.personalManagingCycleTextLayout);
        nextPaymentTextInputLayout = findViewById(R.id.personalManagingNextPaymentTextLayout);
        priceTextInputLayout = findViewById(R.id.personalManagingPriceTextLayout);
        currencyTextInputLayout = findViewById(R.id.personalManagingCurrencyTextLayout);

        reminderTextInputEditText = findViewById(R.id.personalManagingReminderEditText);
        nextPaymentTextInputEditText = findViewById(R.id.personalManagingNextPaymentEditText);
        freePeriodTextInputEditText = findViewById(R.id.personalManagingDurationEditText);
        cycleTextInputEditText = findViewById(R.id.personalManagingCycleEditText);
        currencyTextInputEditText = findViewById(R.id.personalManagingCurrencyEditText);
        categoryTextInputEditText = findViewById(R.id.personalManagingCategoryEditText);
        nameTextInputEditText = findViewById(R.id.personalManagingNameEditText);
        descriptionTextInputEditText = findViewById(R.id.personalManagingDescriptionEditText);
        priceTextInputEditText = findViewById(R.id.personalManagingPriceEditText);
    }

    void setCheckboxes() {
        hasFreePeriodCheckBox = findViewById(R.id.personalManagingFreePeriodCheckBox);
        familyPartCheckBox = findViewById(R.id.personalManagingFamilyPartCheckBox);
    }

    void setImageViews() {
        iconImageView = findViewById(R.id.personalManagingIconImageView);
    }

    void setButtons() {
        editButton = findViewById(R.id.personalManagingEditButton);
    }

    void setRecyclerViews() {
        paymentsRecyclerView = findViewById(R.id.personalManagingPaymentsRecyclerView);
        paymentsShimmerFrameLayout = findViewById(R.id.personalManagingPaymentsShimmerLayout);
        setPaymentsRecyclerView();
    }

    public void setPaymentsRecyclerView() {
        paymentsAdapter = new PersonalPaymentsAdapter((view, position) -> {
            boolean isChecked = ((CheckBox)view).isChecked();
            if (isChecked) {
                Date checkedPaymentDate = paymentsAdapter.getPayment(position).getPaymentDate();
                int paymentDateID = DatabaseRepository.getInstance(this).getPaymentDateIDBySubscriptionIDAndDate(subscriptionID, checkedPaymentDate);

                if (paymentDateID == 0) {
                    PaymentDate paymentDateToInsert = new PaymentDate();
                    paymentDateToInsert.paymentDate = checkedPaymentDate;
                    paymentDateToInsert.subscriptionID = subscriptionID;
                    paymentDateID = (int) DatabaseRepository.getInstance(PersonalManagingActivity.this).insertOnePaymentDateAndReturnID(paymentDateToInsert);
                }

                Operation operationToInsert = new Operation();
                operationToInsert.paymentDateID = paymentDateID;
                operationToInsert.subscriptionID = subscriptionID;
                operationToInsert.isOwner = true;
                operationToInsert.operationDate = new Date();
                operationToInsert.currencyID = DatabaseRepository.getInstance(PersonalManagingActivity.this).getCurrencyIDBySubscriptionID(subscriptionID);

                FreePeriod subscriptionFreePeriod = DatabaseRepository.getInstance(PersonalManagingActivity.this).getFreePeriodBySubscriptionID(subscriptionID);
                if (subscriptionFreePeriod != null) {
                    Date subscriptionFreePeriodEndingDate = DateHelper.getFreePeriodDBEndingDate(subscriptionFreePeriod);
                    if (checkedPaymentDate.before(subscriptionFreePeriodEndingDate)) {
                        operationToInsert.price = 0.0;
                    }
                    else {
                        operationToInsert.price = -DatabaseRepository.getInstance(PersonalManagingActivity.this).getPriceBySubscriptionID(subscriptionID);
                    }
                }
                else {
                    operationToInsert.price = -DatabaseRepository.getInstance(PersonalManagingActivity.this).getPriceBySubscriptionID(subscriptionID);
                }

                DatabaseRepository.getInstance(PersonalManagingActivity.this).insertOneOperation(operationToInsert);

                paymentsAdapter.getPayment(position).setChecked(true);

                Date nextPaymentDateForSubscription = paymentsAdapter.getNextPaymentDateForSubscription().getPaymentDate();
                nextPaymentDate.setTime(nextPaymentDateForSubscription);

                DatabaseRepository
                        .getInstance(PersonalManagingActivity.this)
                        .updateSubscriptionNextPaymentDate(
                                subscriptionID,
                                nextPaymentDateForSubscription
                        );


                nextPaymentDialog.setNextPaymentDialogWithMinDate(nextPaymentDateForSubscription, paymentsAdapter.getMinimumNextPaymentDateForDialog());

            }
            else {
                new AlertDialog.Builder(PersonalManagingActivity.this)
                        .setMessage(R.string.dialog_message_payment_delete)
                        .setNegativeButton(R.string.no, (dialog, which) -> ((CheckBox)view).setChecked(true))
                        .setPositiveButton(R.string.yes, ((dialog, which) -> {
                            if (!paymentsAdapter.checkIfPositionBeforeLastCheckedPosition(position))
                                DatabaseRepository.getInstance(PersonalManagingActivity.this).deleteOnePaymentDate(subscriptionID, paymentsAdapter.getPayment(position).getPaymentDate());
                            else {
                                DatabaseRepository
                                        .getInstance(PersonalManagingActivity.this)
                                        .deleteOneOperationByPaymentDateID(
                                                DatabaseRepository
                                                .getInstance(PersonalManagingActivity.this)
                                                .getPaymentDateIDBySubscriptionIDAndDate(
                                                        subscriptionID,
                                                        paymentsAdapter.getPayment(position).getPaymentDate())
                                        );
                            }
                            paymentsAdapter.getPayment(position).setChecked(false);

                            Date nextPaymentDateForSubscription = paymentsAdapter.getNextPaymentDateForSubscription().getPaymentDate();
                            nextPaymentDate.setTime(nextPaymentDateForSubscription);

                            DatabaseRepository
                                    .getInstance(PersonalManagingActivity.this)
                                    .updateSubscriptionNextPaymentDate(
                                            subscriptionID,
                                            nextPaymentDateForSubscription
                                    );
                            nextPaymentDialog.setNextPaymentDialogWithMinDate(nextPaymentDateForSubscription, paymentsAdapter.getMinimumNextPaymentDateForDialog());
                        }))
                        .show();
            }
        });
        paymentsRecyclerView.setAdapter(paymentsAdapter);
        paymentsRecyclerView.setLayoutManager(new LinearLayoutManager(PersonalManagingActivity.this, LinearLayoutManager.HORIZONTAL, false));
        paymentsRecyclerView.setHasFixedSize(true);
        paymentsRecyclerView.setEmptyView(paymentsShimmerFrameLayout);
    }

    @Override
    public void setAdditionalOnClickListeners() {
        super.setAdditionalOnClickListeners();
    }

    @Override
    public void setDialogs() {
        setReminderDialog();
        setNextPaymentDialog();
        setFreePeriodDialog();
        setIconPickerDialog();
        setCyclePickerDialog();
        setCurrencyDialog();
        setCategoryDialog();
    }

    void setReminderDialog() {
        reminderDialog = new PersonalManagingReminderDialog();
        reminderDialog.initialize(PersonalManagingActivity.this);
    }

    void setNextPaymentDialog() {
        nextPaymentDialog = new PersonalManagingNextPaymentDialog(PersonalManagingActivity.this);
    }

    void setFreePeriodDialog() {
        freePeriodDialog = new PersonalManagingFreePeriodDialog();
        freePeriodDialog.initializeFreePeriodAlertDialog(PersonalManagingActivity.this);
    }

    void setIconPickerDialog() {
        iconPickerDialog = new PersonalManagingIconPickerDialog(PersonalManagingActivity.this);
    }

    void setCyclePickerDialog() {
        cycleDialog = new PersonalManagingCycleDialog();
        cycleDialog.initialize(PersonalManagingActivity.this);
    }

    void setCurrencyDialog() {
        currencyDialog = new PersonalManagingCurrencyDialog();
        currencyDialog.initialize(PersonalManagingActivity.this);
    }

    void setCategoryDialog() {
        categoryDialog = new PersonalManagingCategoryDialog();
        categoryDialog.initialize(PersonalManagingActivity.this);
    }

    @Override
    public void setAdditionalObservers() {
        super.setAdditionalObservers();

        ((PersonalManagingViewModel)subscriptionViewModel).getPayments().observe(this, payments -> paymentsAdapter.setPayments(payments));


    }

    @Override
    public void loadSubscription() {
        ((PersonalManagingViewModel)subscriptionViewModel).loadSubscriptionFromDatabase(subscriptionID);
    }

    @Override
    public void updateSubscription() {
            Objects.requireNonNull(((PersonalManagingViewModel) subscriptionViewModel).getManagingSubscription()).observe(this, updateObserver);
    }

    @Override
    public void enableAdditionalInputs() {
        familyPartCheckBox.setEnabled(true);
    }

    @Override
    public void disableAdditionalInputs() {
        familyPartCheckBox.setEnabled(false);
    }

    @Override
    public void loadPayments() {
        ManagingSubscription managingSubscription = DatabaseRepository.getInstance(this).getManagingSubscriptionByID(subscriptionID);
        List<PaymentDatesWithOperations> paymentDatesWithOperations = DatabaseRepository.getInstance(PersonalManagingActivity.this).getPaymentsDatesWithOperationsBySubscriptionID(subscriptionID);
        if (paymentDatesWithOperations.size() != 0) {
            ArrayList<Payment> payments = new ArrayList<>();
            for (PaymentDatesWithOperations paymentDate : paymentDatesWithOperations) {
                payments.add(new Payment(paymentDate.paymentDate.paymentDate, !paymentDate.operations.isEmpty()));
            }
            ArrayList<Date> cyclePaymentDates = DateHelper.getCycleDates(paymentDatesWithOperations.get(paymentDatesWithOperations.size() - 1).paymentDate.paymentDate, managingSubscription.cycle, 12, true);
            for (Date cyclePaymentDate : cyclePaymentDates) {
                payments.add(new Payment(cyclePaymentDate, false));
            }
            ((PersonalManagingViewModel)subscriptionViewModel).setPayments(payments);
            if (paymentDatesWithOperations.get(paymentDatesWithOperations.size() - 1).operations.size() == 0)
                DatabaseRepository.getInstance(PersonalManagingActivity.this).updateSubscriptionNextPaymentDate(subscriptionID, paymentDatesWithOperations.get(paymentDatesWithOperations.size() - 1).paymentDate.paymentDate);
            else
                DatabaseRepository.getInstance(PersonalManagingActivity.this).updateSubscriptionNextPaymentDate(subscriptionID, cyclePaymentDates.get(0));
        }
        else {
            ArrayList<Date> cyclePaymentDates = DateHelper.getCycleDates(managingSubscription.subscription.nextPaymentDate, managingSubscription.cycle, 12, false);
            ArrayList<Payment> payments = new ArrayList<>();
            for (Date cyclePaymentDate : cyclePaymentDates) {
                payments.add(new Payment(cyclePaymentDate, false));
            }
            ((PersonalManagingViewModel)subscriptionViewModel).setPayments(payments);
        }
    }

    @Override
    public void loadFallbackSubscription() {
        fallbackSubscription = DatabaseRepository.getInstance(this).getManagingSubscriptionByID(subscriptionID);
    }

    @Override
    public void fallbackToPreviousData() {
        DatabaseRepository.getInstance(this).updateOneSubscription(fallbackSubscription.subscription);
        setManagingDialogsFlagsToNull();
    }

    @Override
    public void clearFallbackSubscription() {
        fallbackSubscription = null;
    }

    @Override
    public void setAdditionalFieldsWhenUpdate(Subscription subscription) {
        subscription.isFamilyPart = familyPartCheckBox.isChecked();
    }

    @Override
    public void setAdditionalFieldsWhenObserve(ManagingSubscription managingSubscription) {
        nextPaymentDialog.setNextPaymentDialogWithMinDate(managingSubscription.subscription.nextPaymentDate, paymentsAdapter.getMinimumNextPaymentDateForDialog());
        familyPartCheckBox.setChecked(managingSubscription.subscription.isFamilyPart);
    }

    @Override
    public void updateCycle(int cycleID) {
        if (cycleDialog.getChangeAll() != null) {
            isNeedToLoadPayments = true;
            if (cycleDialog.getChangeAll()) {
                Cycle associatedCycle = DatabaseRepository.getInstance(PersonalManagingActivity.this).getCycleByID(cycleID);
                List<PaymentDate> paymentDates = DatabaseRepository.getInstance(PersonalManagingActivity.this).getPaymentDatesBySubscriptionID(subscriptionID);

                if (paymentDates != null) {
                    if (paymentDates.size() > 1) {
                        recalculatePaymentDates(associatedCycle, paymentDates);
                        DatabaseRepository.getInstance(PersonalManagingActivity.this).updatePaymentDates(paymentDates);
                    }
                }
            }
        }
    }
}
