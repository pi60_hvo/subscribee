package com.vladyslavkhimich.subscribee.ui.subscription.managing.personal;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.models.Payment;
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class PersonalManagingViewModel extends ManagingViewModel {

    MutableLiveData<ArrayList<Payment>> payments = new MutableLiveData<>();

    public PersonalManagingViewModel(@NonNull @NotNull android.app.Application application) {
        super(application);
    }

    public MutableLiveData<ArrayList<Payment>> getPayments() {
        return payments;
    }

    public void setPayments(ArrayList<Payment> payments) {
        this.payments.setValue(payments);
    }

    @Override
    public void loadSubscriptionFromDatabase(int id) {
        setManagingSubscription(DatabaseRepository.getInstance(getApplication().getApplicationContext()).getObservableManagingSubscriptionByID(id));
    }
}
