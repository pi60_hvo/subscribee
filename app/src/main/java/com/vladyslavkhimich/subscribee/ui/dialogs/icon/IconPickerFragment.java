package com.vladyslavkhimich.subscribee.ui.dialogs.icon;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.icon.IconPickerIconAdapter;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnResourceClickListenerCallback;

public class IconPickerFragment extends Fragment {
    GridView iconPickerGridView;
    IconPickerIconAdapter iconPickerIconAdapter;
    OnResourceClickListenerCallback onIconClickListenerCallback;

    public IconPickerFragment(OnResourceClickListenerCallback onIconClickListenerCallback, Context context) {
        this.onIconClickListenerCallback = onIconClickListenerCallback;
        iconPickerIconAdapter = new IconPickerIconAdapter(context);
    }

    public IconPickerIconAdapter getIconPickerIconAdapter() {
        return iconPickerIconAdapter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dialog_icon, container, false);
        initializeGridView(root);
        return root;
    }

    private void initializeGridView(View view) {
        iconPickerGridView = (GridView) view.findViewById(R.id.iconDialogIconGridView);
        iconPickerGridView.setOnItemClickListener((parent, view1, position, id) -> {
            iconPickerIconAdapter.setSelectedPosition(position);
            onIconClickListenerCallback.onResourceClicked(iconPickerIconAdapter.getItem(position));
            iconPickerIconAdapter.notifyDataSetChanged();
        });
        iconPickerGridView.setAdapter(iconPickerIconAdapter);
    }
}
