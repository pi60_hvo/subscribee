package com.vladyslavkhimich.subscribee.ui.subscription.archived.family

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithPaymentDates
import com.vladyslavkhimich.subscribee.models.FreePeriod
import com.vladyslavkhimich.subscribee.models.MemberWithDataForAdapter
import com.vladyslavkhimich.subscribee.models.OperationAndPaymentDate
import com.vladyslavkhimich.subscribee.models.Reminder
import com.vladyslavkhimich.subscribee.ui.subscription.archived.general.ArchivedSubscriptionActivity
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper
import com.vladyslavkhimich.subscribee.utils.types.primitives.DoubleHelper
import com.vladyslavkhimich.subscribee.utils.types.primitives.StringHelper
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport
import com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.archived.familymember.ArchivedFamilyMembersAdapter
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.button.MaterialButton
import java.util.*
import kotlin.collections.ArrayList

class FamilyArchivedSubscriptionActivity : ArchivedSubscriptionActivity() {
    private lateinit var membersRecyclerView: RecyclerViewEmptySupport
    private lateinit var membersAdapter: ArchivedFamilyMembersAdapter
    private lateinit var membersShimmerLayout: ShimmerFrameLayout
    private lateinit var addMemberButton: MaterialButton

    override fun onResume() {
        super.onResume()
        startShimmerAnimation()
    }

    private fun startShimmerAnimation() {
        membersShimmerLayout.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        stopShimmerAnimation()
    }

    private fun stopShimmerAnimation() {
        membersShimmerLayout.startShimmerAnimation()
    }

    override fun initializeViewModel() {
        archivedSubscriptionViewModel = ViewModelProvider(this).get(FamilyArchivedSubscriptionViewModel::class.java)
        archivedSubscriptionViewModel.loadSubscriptionFromDB(subscriptionID)
        archivedSubscriptionViewModel.loadPaymentsFromDB(subscriptionID)
    }

    override fun setContentView() {
        setContentView(R.layout.activity_managing_family)
    }

    override fun initializeUI() {
        setTextViews()
        setTextInputs()
        setCheckBoxes()
        setLayouts()
        setImageViews()
        setButtons()
        setRecyclerViews()
    }

    private fun setTextViews() {
        nextPaymentNoteTextView = findViewById(R.id.next_payment_note_text)
        durationNoteTextView = findViewById(R.id.duration_note_text)
    }

    private fun setTextInputs() {
        durationTextInputLayout = findViewById(R.id.familyManagingDurationTextLayout)
        nameTextInputEditText = findViewById(R.id.familyManagingNameEditText)
        categoryTextInputEditText = findViewById(R.id.familyManagingCategoryEditText)
        descriptionTextInputEditText = findViewById(R.id.familyManagingDescriptionEditText)
        cycleTextInputEditText = findViewById(R.id.familyManagingCycleEditText)
        nextPaymentTextInputEditText = findViewById(R.id.familyManagingNextPaymentEditText)
        freePeriodTextInputEditText = findViewById(R.id.familyManagingDurationEditText)
        priceTextInputEditText = findViewById(R.id.familyManagingPriceEditText)
        currencyTextInputEditText = findViewById(R.id.familyManagingCurrencyEditText)
        reminderTextInputEditText = findViewById(R.id.familyManagingReminderEditText)
    }

    private fun setCheckBoxes() {
        freePeriodCheckBox = findViewById(R.id.familyManagingFreePeriodCheckBox)
    }

    private fun setLayouts() {
        subscriptionConstraintLayout = findViewById(R.id.familyManagingConstraintLayout)
        iconContainerConstraintLayout = findViewById(R.id.familyManagingIconConstraintLayout)
        iconChangeLinearLayout = findViewById(R.id.familyManagingChangeIconLayout)
        membersShimmerLayout = findViewById(R.id.familyManagingMembersShimmerLayout)
    }

    private fun setImageViews() {
        iconImageView = findViewById(R.id.familyManagingIconImageView)
    }

    private fun setButtons() {
        addMemberButton = findViewById(R.id.familyManagingAddMemberButton)
    }

    private fun setRecyclerViews() {
        membersRecyclerView = findViewById(R.id.familyManagingMembersRecyclerView)
        setMembersRecyclerView()
    }

    private fun setMembersRecyclerView() {
        membersAdapter = ArchivedFamilyMembersAdapter()
        membersRecyclerView.adapter = membersAdapter
        membersRecyclerView.layoutManager = LinearLayoutManager(this)
        membersRecyclerView.setHasFixedSize(true)
        membersRecyclerView.setEmptyView(membersShimmerLayout)
    }

    override fun disableUI() {
        nameTextInputEditText.isEnabled = false
        categoryTextInputEditText.isEnabled = false
        descriptionTextInputEditText.isEnabled = false
        cycleTextInputEditText.isEnabled = false
        nextPaymentTextInputEditText.isEnabled = false
        freePeriodTextInputEditText.isEnabled = false
        priceTextInputEditText.isEnabled = false
        currencyTextInputEditText.isEnabled = false
        reminderTextInputEditText.isEnabled = false

        freePeriodCheckBox.isEnabled = false

        iconChangeLinearLayout.visibility = View.GONE
        addMemberButton.visibility = View.GONE
    }

    override fun setObservers() {
        (archivedSubscriptionViewModel as FamilyArchivedSubscriptionViewModel).familySubscription.observe(this, {
            if (it != null) {
                supportActionBar?.title = it.subscription.name
                nameTextInputEditText.setText(it.subscription.name)
                descriptionTextInputEditText.setText(it.subscription.description)
                priceTextInputEditText.setText(DoubleHelper.getStringFromDouble(it.subscription.price))
                nextPaymentTextInputEditText.setText(DateHelper.getNextPaymentDate(it.subscription.nextPaymentDate))
                currencyTextInputEditText.setText(it.currency.shortName)
                cycleTextInputEditText.setText(StringHelper.getStringFromCycle(this, it.cycle))

                if (it.reminder != null)
                    reminderTextInputEditText.setText(StringHelper.getStringFromReminder(this, Reminder(it.reminder)))

                if (it.category.isUserCreated)
                    categoryTextInputEditText.setText(it.category.name)
                else
                    categoryTextInputEditText.setText(ResourcesHelper.getStringIDFromName(this, it.category.name))

                toggleFreePeriodViews(it.freePeriod != null)
                if (it.freePeriod != null)
                    freePeriodTextInputEditText.setText(StringHelper.getStringFromFreePeriod(this, FreePeriod(it.freePeriod)))

                iconImageView.setImageResource(ResourcesHelper.getImageIDFromName(this, it.subscription.icon))

                val iconBackgroundDrawable: GradientDrawable = iconContainerConstraintLayout.background as GradientDrawable
                iconBackgroundDrawable.setColor(Color.parseColor(it.subscription.iconColor))
                iconContainerConstraintLayout.background = iconBackgroundDrawable

            }
        })

        (archivedSubscriptionViewModel as FamilyArchivedSubscriptionViewModel).membersWithData.observe(this, {
            val subscriptionAllPaymentDates = DatabaseRepository.getInstance(this).getPaymentDatesBySubscriptionID(subscriptionID)
            subscriptionAllPaymentDates.forEach {
                item ->
                if (!it[0].operationWithPaymentDates.any {it.paymentDates[0].paymentDate == item.paymentDate}) {
                    it.forEach {
                        memberWithPaymentDates -> memberWithPaymentDates.operationWithPaymentDates.add(OperationWithPaymentDates(null, listOf(item)))
                    }
                }
            }
            val membersWithDataForAdapter = ArrayList<MemberWithDataForAdapter>()
            val managerOperationsAndPaymentDates = it[0].operationWithPaymentDates
            it.forEach {
                item ->
                val memberOperationAndPaymentDateList = LinkedList<OperationAndPaymentDate>()
                for (operationAndPaymentDates: OperationWithPaymentDates in item.operationWithPaymentDates) {
                    memberOperationAndPaymentDateList.add(OperationAndPaymentDate(operationAndPaymentDates.operation, operationAndPaymentDates.paymentDates[0]))
                }
                managerOperationsAndPaymentDates.forEach {
                    managerItem ->
                    memberOperationAndPaymentDateList.add(OperationAndPaymentDate(null, managerItem.paymentDates[0]))
                }
                membersWithDataForAdapter.add(MemberWithDataForAdapter(item.member, TreeSet(memberOperationAndPaymentDateList)))
            }
            membersAdapter.setMembersWithDataAndNotify(membersWithDataForAdapter)
        })
    }
}