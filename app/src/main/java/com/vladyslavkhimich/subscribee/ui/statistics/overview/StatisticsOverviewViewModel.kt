package com.vladyslavkhimich.subscribee.ui.statistics.overview

import androidx.lifecycle.ViewModel
import com.vladyslavkhimich.subscribee.models.binding.statistics.*

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 16.12.2021
 * @email vladislav.himich@4k.com.ua
 */
class StatisticsOverviewViewModel : ViewModel() {
    val statisticsOverviewModel = StatisticsOverviewModel()

    fun setTotalSpentValue(statisticsMoneyModel: StatisticsMoneyModel) {
        statisticsOverviewModel.totalSpentValue.set(statisticsMoneyModel)
    }

    fun setDayAverageSpentValue(statisticsMoneyModel: StatisticsMoneyModel) {
        statisticsOverviewModel.dayAverageSpentValue.set(statisticsMoneyModel)
    }

    fun setComparedPercentage(percentage: Double) {
        statisticsOverviewModel.comparedPercentage.set(percentage)
    }

    fun setMostSpentSubscription(subscriptionModel: StatisticsSubscriptionModel) {
        statisticsOverviewModel.mostSpentSubscription.set(subscriptionModel)
    }

    fun setMostSpentCategory(categoryModel: StatisticsCategoryModel) {
        statisticsOverviewModel.mostSpentCategory.set(categoryModel)
    }

    fun setIsWithFamilyStatistics(isWithFamilyStatistics: Boolean) {
        statisticsOverviewModel.isWithFamilyStatistics.set(isWithFamilyStatistics)
    }

    fun setMostPaidSubscription(subscriptionModel: StatisticsSubscriptionModel) {
        statisticsOverviewModel.mostPaidSubscription.set(subscriptionModel)
    }

    fun setMostPaidMember(memberModel: StatisticsMemberModel) {
        statisticsOverviewModel.mostPaidMember.set(memberModel)
    }
}