package com.vladyslavkhimich.subscribee.ui.subscription.managing.general;

import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vladyslavkhimich.subscribee.database.entities.partials.ManagingSubscription;
import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.models.Reminder;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionViewModel;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;

import org.jetbrains.annotations.NotNull;

public abstract class ManagingViewModel extends SubscriptionViewModel {
    LiveData<ManagingSubscription> managingSubscription;
    MutableLiveData<Boolean> isCycleChangeAll = new MutableLiveData<>();

    public ManagingViewModel(@NonNull @NotNull android.app.Application application) {
        super(application);
        setCyclesFromDatabase();
        setCurrenciesFromDatabase();
        setCategoriesFromDatabase();
    }

    public void setIsCycleChangeAll(Boolean isCycleChangeAll) {
        this.isCycleChangeAll.setValue(isCycleChangeAll);
    }

    public void setManagingSubscription(LiveData<ManagingSubscription> managingSubscription) {
        this.managingSubscription = managingSubscription;
    }

    public LiveData<ManagingSubscription> getManagingSubscription() {
        return managingSubscription;
    }

    public MutableLiveData<Boolean> getIsCycleChangeAll() {
        return isCycleChangeAll;
    }

    public abstract void loadSubscriptionFromDatabase(int id);

    public void setAssociatedSubscriptionData(ManagingSubscription managingSubscription) {
        if (managingSubscription != null) {
            setSelectedCategory(managingSubscription.category);
            setSelectedCycle(managingSubscription.cycle);
            setSelectedCurrency(managingSubscription.currency);
            setSelectedIconColor(Color.parseColor(managingSubscription.subscription.iconColor));
            setSelectedIconID(ResourcesHelper.getImageIDFromName(getApplication().getApplicationContext(), managingSubscription.subscription.icon));
            if (managingSubscription.reminder != null)
                setSelectedReminder(new Reminder(managingSubscription.reminder));
            else
                setSelectedReminder(null);
            if (managingSubscription.freePeriod != null)
                setSelectedFreePeriod(new FreePeriod(managingSubscription.freePeriod));
            else
                setSelectedFreePeriod(null);
        }
    }
}
