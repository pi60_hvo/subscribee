package com.vladyslavkhimich.subscribee.ui.subscription.archived.family

import android.app.Application
import androidx.lifecycle.LiveData
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.partials.ManagingSubscription
import com.vladyslavkhimich.subscribee.database.entities.partials.MemberWithOperationsAndPaymentDates
import com.vladyslavkhimich.subscribee.ui.subscription.archived.general.ArchivedSubscriptionViewModel

class FamilyArchivedSubscriptionViewModel(application: Application) : ArchivedSubscriptionViewModel(application) {

    lateinit var familySubscription: LiveData<ManagingSubscription>
    lateinit var membersWithData: LiveData<List<MemberWithOperationsAndPaymentDates>>

    override fun loadSubscriptionFromDB(subscriptionID: Int) {
        familySubscription = DatabaseRepository.getInstance(getApplication()).getObservableManagingSubscriptionByID(subscriptionID)
    }

    override fun loadPaymentsFromDB(subscriptionID: Int) {
        membersWithData = DatabaseRepository.getInstance(getApplication()).getFamilySubscriptionMembersWithData(subscriptionID)
    }
}