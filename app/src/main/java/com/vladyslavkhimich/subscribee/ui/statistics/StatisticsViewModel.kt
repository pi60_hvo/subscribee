package com.vladyslavkhimich.subscribee.ui.statistics

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithDataForStats
import com.vladyslavkhimich.subscribee.models.binding.statistics.StatisticsModel
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper
import java.util.*

class StatisticsViewModel(application: Application) : AndroidViewModel(application) {
    val statisticsModel = StatisticsModel()

    private val selectedUpperTabIndex: MutableLiveData<Int> = MutableLiveData()
    fun getSelectedUpperTabIndex() : LiveData<Int> = selectedUpperTabIndex
    fun setSelectedUpperTabIndex(index: Int) {
        selectedUpperTabIndex.postValue(index)
    }

    private val selectedLowerTabIndex: MutableLiveData<Int> = MutableLiveData()
    fun getSelectedLowerTabIndex(): LiveData<Int> = selectedLowerTabIndex
    fun setSelectedLowerTabIndex(index: Int) {
        selectedLowerTabIndex.postValue(index)
    }

    private val selectedDate = MutableLiveData<Calendar>()
    fun getSelectedDate(): LiveData<Calendar> = selectedDate
    fun setSelectedDate(selectedDate: Calendar) {
        this.selectedDate.postValue(selectedDate)
    }

    private val operationsWithData: MutableLiveData<List<OperationWithDataForStats>> = MutableLiveData()
    fun getOperationsWithData() : LiveData<List<OperationWithDataForStats>> = operationsWithData
    fun setOperationsWithData(operationsWithData: List<OperationWithDataForStats>) {
        statisticsModel.isStatisticsDataPresent.set(operationsWithData.isNotEmpty())
        this.operationsWithData.postValue(operationsWithData)
    }

    var previousOperationsWithData: List<OperationWithDataForStats> = LinkedList()

    fun loadOperationsWithData() {
        val previousPeriodCalendar = selectedDate.value?.clone() as Calendar?
        if (previousPeriodCalendar != null) {
            DateHelper.subtractDays(previousPeriodCalendar, 2)
            previousOperationsWithData = DatabaseRepository.getInstance(getApplication())
                .getOperationsForStats(
                    DateHelper.getFirstDayOfMonth(previousPeriodCalendar),
                    DateHelper.getLastDayOfMonth(previousPeriodCalendar)
                )
        }

        operationsWithData.postValue(
            DatabaseRepository.getInstance(getApplication())
                .getOperationsForStats(
                    DateHelper.getFirstDayOfMonth(selectedDate.value),
                    DateHelper.getLastDayOfMonth(selectedDate.value)
                )
        )
    }
}