package com.vladyslavkhimich.subscribee.ui.subscription.general;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.database.entities.FreePeriod;
import com.vladyslavkhimich.subscribee.database.entities.Reminder;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ViewAnimator;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.StringHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CategoryDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CurrencyDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.FreePeriodDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.IconPickerDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.ReminderDialog;
import com.vladyslavkhimich.subscribee.utils.ui.validators.NumberValidator;
import com.vladyslavkhimich.subscribee.utils.ui.validators.StringValidator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public abstract class SubscriptionActivity extends AppCompatActivity {
    public CheckBox hasFreePeriodCheckBox;

    public TextInputLayout durationTextInputLayout;
    public TextInputLayout nameTextInputLayout;
    public TextInputLayout categoryTextInputLayout;
    public TextInputLayout cycleTextInputLayout;
    public TextInputLayout nextPaymentTextInputLayout;
    public TextInputLayout priceTextInputLayout;
    public TextInputLayout currencyTextInputLayout;

    public TextInputEditText nextPaymentTextInputEditText;
    public TextInputEditText reminderTextInputEditText;
    public TextInputEditText freePeriodTextInputEditText;
    public TextInputEditText cycleTextInputEditText;
    public TextInputEditText currencyTextInputEditText;
    public TextInputEditText categoryTextInputEditText;
    public TextInputEditText nameTextInputEditText;
    public TextInputEditText descriptionTextInputEditText;
    public TextInputEditText priceTextInputEditText;

    public ConstraintLayout durationConstraintLayout;
    public ConstraintLayout subscriptionConstraintLayout;
    public LinearLayout iconChangeLinearLayout;
    public ConstraintLayout iconContainerConstraintLayout;
    public ImageView iconImageView;
    public TextView noteTextView;

    public Calendar nextPaymentDate = Calendar.getInstance();
    public String nextPaymentDateString;
    public boolean isFieldsError;
    public boolean isNameHasError;
    public boolean isCategoryHasError;
    public boolean isCycleHasError;
    public boolean isNextPaymentHasError;
    public boolean isPriceHasError;
    public boolean isCurrencyHasError;

    public ReminderDialog reminderDialog;
    public FreePeriodDialog freePeriodDialog;
    public CurrencyDialog currencyDialog;
    public CategoryDialog categoryDialog;
    public IconPickerDialog iconPickerDialog;

    public SubscriptionViewModel subscriptionViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeViewModel();
        this.setContentView();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        initializeUI();
        setDialogs();
        setOnClickListeners();
        setObservers();
        setAdditionalObservers();
        setTextWatchers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(hasFreePeriodCheckBox.getWindowToken(), 0);
    }

    public void setOnClickListeners() {
        setCheckboxesOnClickListeners();
        setTextInputLayoutsOnClickListeners();
        setLayoutsOnClickListeners();
    }

    void setCheckboxesOnClickListeners() {
        hasFreePeriodCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> ViewAnimator.animateViewFade(subscriptionConstraintLayout, durationConstraintLayout, isChecked));
    }

    void setTextInputLayoutsOnClickListeners() {
        reminderTextInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isInTouchMode() && hasFocus)
                v.performClick();
        });
        reminderTextInputEditText.setOnClickListener(v -> {
            reminderTextInputEditText.requestFocus();
            reminderDialog.getReminderAlertDialog().show();
        });
        reminderTextInputEditText.setInputType(InputType.TYPE_NULL);

        nextPaymentTextInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isInTouchMode() && hasFocus)
                v.performClick();
        });

        freePeriodTextInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isInTouchMode() && hasFocus)
                v.performClick();
        });
        freePeriodTextInputEditText.setOnClickListener(v -> {
            freePeriodTextInputEditText.requestFocus();
            freePeriodDialog.getFreePeriodAlertDialog().show();
        });
        freePeriodTextInputEditText.setInputType(InputType.TYPE_NULL);

        currencyTextInputEditText.setOnClickListener(v -> {
            Log.i("CALLED", "Currency text input click called");
            currencyTextInputEditText.requestFocus();
            currencyDialog.getCurrencyAlertDialog().show();
            currencyDialog.scrollToSelectedPosition();
        });
        currencyTextInputEditText.setInputType(InputType.TYPE_NULL);

        categoryTextInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isInTouchMode() && hasFocus)
                v.performClick();
        });
        categoryTextInputEditText.setOnClickListener(v -> {
            categoryTextInputEditText.requestFocus();
            categoryDialog.getCategoryAlertDialog().show();
        });
        categoryTextInputEditText.setInputType(InputType.TYPE_NULL);
    }

    void setLayoutsOnClickListeners() {
        iconContainerConstraintLayout.setOnClickListener(v -> iconPickerDialog.show());
    }

    public void setObservers() {
        subscriptionViewModel.getSelectedReminder().observe(this, reminder -> {
            if (reminder != null)
                reminderTextInputEditText.setText(StringHelper.getStringFromReminder(getApplicationContext(), reminder));
            else
                reminderTextInputEditText.setText("");
        });

        subscriptionViewModel.getSelectedFreePeriod().observe(this, freePeriod -> {
            if (freePeriod != null)
                freePeriodTextInputEditText.setText(StringHelper.getStringFromFreePeriod(getApplicationContext(), freePeriod));
            else
                freePeriodTextInputEditText.setText("");
        });

        subscriptionViewModel.getSelectedIconColor().observe(this, color -> {
            GradientDrawable iconBackgroundDrawable = (GradientDrawable) iconContainerConstraintLayout.getBackground();
            iconBackgroundDrawable.setColor(color);
            iconContainerConstraintLayout.setBackground(iconBackgroundDrawable);
        });

        subscriptionViewModel.getSelectedIconID().observe(this, iconID -> iconImageView.setImageResource(iconID));

        subscriptionViewModel.getSelectedCycle().observe(this, cycle -> cycleTextInputEditText.setText(StringHelper.getStringFromCycle(getApplicationContext(), cycle)));

        subscriptionViewModel.getSelectedCurrency().observe(this, currency -> {
                Log.i("CALLED", "Selected currency set text called");
                currencyTextInputEditText.setText(currency.shortName);
                });

        subscriptionViewModel.getCategories().observe(this, categories -> categoryDialog.setAdapterCategories(categories));

        subscriptionViewModel.getSelectedCategory().observe(this, category -> {
            if (category != null) {
                if (category.isUserCreated)
                    categoryTextInputEditText.setText(category.name);
                else
                    categoryTextInputEditText.setText(getString(ResourcesHelper.getStringIDFromName(this, category.name)));
            }
            else
                categoryTextInputEditText.setText("");
        });
    }

    public void setNextPaymentDate(int year, int month, int dayOfMonth) {
        nextPaymentDate.set(Calendar.YEAR, year);
        nextPaymentDate.set(Calendar.MONTH, month);
        nextPaymentDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
    }

    public void setNextPaymentDate(Calendar calendar) {
        nextPaymentDate = calendar;
    }

    public void setNextPaymentDate(Date date) {
        nextPaymentDate.setTime(date);
    }

    public void setNextPaymentDateString() {
        nextPaymentDateString = DateFormat.getDateInstance().format(nextPaymentDate.getTime());
    }

    public void setNextPaymentTextInputText() {
        nextPaymentTextInputEditText.setText(nextPaymentDateString);
    }

    public void setTextWatchers() {
        nameTextInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0)
                {
                    nameTextInputLayout.setError(SubscriptionActivity.this.getResources().getText(R.string.error_required_field));
                    isNameHasError = true;
                }
                else if (StringValidator.isErrorText(s.toString())) {
                    nameTextInputLayout.setError(SubscriptionActivity.this.getResources().getText(R.string.error_text));
                    isNameHasError = true;
                }
                else {
                    setNameTextInputLayoutNotError();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        categoryTextInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    categoryTextInputLayout.setError(SubscriptionActivity.this.getResources().getText(R.string.error_required_field));
                    isCategoryHasError = true;
                }
                else {
                    setCategoryTextInputLayoutNotError();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cycleTextInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    cycleTextInputLayout.setError(SubscriptionActivity.this.getResources().getText(R.string.error_required_field));
                    isCycleHasError = true;
                }
                else {
                    setCycleTextInputLayoutNotError();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        nextPaymentTextInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    nextPaymentTextInputLayout.setError(SubscriptionActivity.this.getResources().getText(R.string.error_required_field));
                    isNextPaymentHasError = true;
                }
                else {
                    setNextPaymentTextInputLayoutNotError();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        priceTextInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    priceTextInputLayout.setError(SubscriptionActivity.this.getResources().getText(R.string.error_required_field));
                    isPriceHasError = true;
                }
                else {
                    if (!NumberValidator.isValidDoubleNumber(s.toString())) {
                        priceTextInputLayout.setError(SubscriptionActivity.this.getResources().getText(R.string.error_invalid_double));
                        isPriceHasError = true;
                    }
                    else {
                        setPriceTextInputLayoutNotError();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        currencyTextInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    currencyTextInputLayout.setError(SubscriptionActivity.this.getResources().getText(R.string.error_required_field));
                    isCurrencyHasError = true;
                }
                else {
                    setCurrencyTextInputLayoutNotError();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void hideKeyboardAndFinishActivity() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(reminderTextInputEditText.getWindowToken(), 0);
        finish();
    }

    public void validateFields() {
        setFieldEmptyTextIfItIsEmpty();
        isFieldsError = isNameHasError || isCategoryHasError || isCycleHasError || isNextPaymentHasError || isPriceHasError || isCurrencyHasError;
    }

    void setFieldEmptyTextIfItIsEmpty() {
        if (Objects.requireNonNull(nameTextInputEditText.getText()).length() == 0)
            nameTextInputEditText.setText("");
        if (Objects.requireNonNull(categoryTextInputEditText.getText()).length() == 0)
            categoryTextInputEditText.setText("");
        if (Objects.requireNonNull(cycleTextInputEditText.getText()).length() == 0)
            cycleTextInputEditText.setText("");
        if (Objects.requireNonNull(nextPaymentTextInputEditText.getText()).length() == 0)
            nextPaymentTextInputEditText.setText("");
        if (Objects.requireNonNull(priceTextInputEditText.getText()).length() == 0)
            priceTextInputEditText.setText("");
        if (Objects.requireNonNull(currencyTextInputEditText.getText()).length() == 0)
            currencyTextInputEditText.setText("");
    }

    public int createCycleIfNotExistAndReturnID() {
        Cycle selectedCycle = subscriptionViewModel.getSelectedCycleObject();
        Cycle cycleFromDB = DatabaseRepository.getInstance(getApplicationContext()).
                returnCycleIfExists(
                        selectedCycle.daysCount,
                        selectedCycle.weeksCount,
                        selectedCycle.monthsCount,
                        selectedCycle.yearsCount);
        long cycleID;
        if (cycleFromDB == null) {
            cycleID = DatabaseRepository.getInstance(getApplicationContext()).insertOneCycleAndReturnID(selectedCycle);
        }
        else {
            cycleID = cycleFromDB.cycleID;
        }
        return (int) cycleID;
    }

    public void createAssociatedReminder(int subscriptionID) {
        com.vladyslavkhimich.subscribee.models.Reminder selectedReminder = subscriptionViewModel.getSelectedReminderObject();
        if (selectedReminder != null) {
            Reminder reminderToInsert = new Reminder();
            reminderToInsert.daysCount = selectedReminder.getDaysCount();
            reminderToInsert.weeksCount = selectedReminder.getWeeksCount();
            reminderToInsert.monthsCount = selectedReminder.getMonthsCount();
            reminderToInsert.yearsCount = selectedReminder.getYearsCount();
            reminderToInsert.subscriptionID = subscriptionID;

            DatabaseRepository.getInstance(getApplicationContext()).insertOneReminder(reminderToInsert);
        }
    }

    public void createAssociatedFreePeriod(int subscriptionID) {
        com.vladyslavkhimich.subscribee.models.FreePeriod selectedFreePeriod = subscriptionViewModel.getSelectedFreePeriodObject();
        if (selectedFreePeriod != null) {
            FreePeriod freePeriodToInsert = new FreePeriod();
            freePeriodToInsert.subscriptionID = subscriptionID;

            if (selectedFreePeriod.getIsToNextPayment()) {
                freePeriodToInsert.daysCount = DateHelper.getDaysCountBetweenTwoDates(new Date(), nextPaymentDate.getTime());
            }
            else {
                freePeriodToInsert.daysCount = selectedFreePeriod.getDaysCount();
                freePeriodToInsert.weeksCount = selectedFreePeriod.getWeeksCount();
                freePeriodToInsert.monthsCount = selectedFreePeriod.getMonthsCount();
                freePeriodToInsert.yearsCount = selectedFreePeriod.getYearsCount();
            }

            Calendar startDateCalendar = Calendar.getInstance();
            DateHelper.getOnlyDateCalendar(startDateCalendar);
            freePeriodToInsert.startDate = startDateCalendar.getTime();

            DatabaseRepository.getInstance(getApplicationContext()).insertOneFreePeriod(freePeriodToInsert);
        }
    }

    public void updateAssociatedReminder(int reminderID) {
        com.vladyslavkhimich.subscribee.models.Reminder selectedReminder = subscriptionViewModel.getSelectedReminderObject();
        Reminder reminderToUpdate = DatabaseRepository.getInstance(SubscriptionActivity.this).getReminderByID(reminderID);
        if (selectedReminder != null) {
            reminderToUpdate.daysCount = selectedReminder.getDaysCount();
            reminderToUpdate.weeksCount = selectedReminder.getWeeksCount();
            reminderToUpdate.monthsCount = selectedReminder.getMonthsCount();
            reminderToUpdate.yearsCount = selectedReminder.getYearsCount();

            DatabaseRepository.getInstance(getApplicationContext()).updateOneReminder(reminderToUpdate);
        }
    }

    public void updateAssociatedFreePeriod(int freePeriodID) {
        com.vladyslavkhimich.subscribee.models.FreePeriod selectedFreePeriod = subscriptionViewModel.getSelectedFreePeriodObject();
        FreePeriod freePeriodToUpdate = DatabaseRepository.getInstance(getApplicationContext()).getFreePeriodByID(freePeriodID);
        if (selectedFreePeriod != null) {
            if (selectedFreePeriod.getIsToNextPayment()) {
                freePeriodToUpdate.daysCount = DateHelper.getDaysCountBetweenTwoDates(new Date(), nextPaymentDate.getTime());
                freePeriodToUpdate.weeksCount = 0;
                freePeriodToUpdate.monthsCount = 0;
                freePeriodToUpdate.yearsCount = 0;
            }
            else {
                freePeriodToUpdate.daysCount = selectedFreePeriod.getDaysCount();
                freePeriodToUpdate.weeksCount = selectedFreePeriod.getWeeksCount();
                freePeriodToUpdate.monthsCount = selectedFreePeriod.getMonthsCount();
                freePeriodToUpdate.yearsCount = selectedFreePeriod.getYearsCount();
            }

            DatabaseRepository.getInstance(getApplicationContext()).updateOneFreePeriod(freePeriodToUpdate);
        }
    }

    public void deleteFreePeriodByID(int id) {
        DatabaseRepository.getInstance(getApplicationContext()).deleteOneFreePeriod(id);
    }

    public void deleteReminderByID(int subscriptionID) {
        DatabaseRepository.getInstance(SubscriptionActivity.this).deleteOneReminderBySubscriptionID(subscriptionID);
    }

    public void setSelectedCurrencyByShortName(String shortName) {
        Currency selectedCurrency = null;
        ArrayList<Currency> allCurrencies = currencyDialog.getCurrencyAdapter().getAllCurrencies();
        for (Currency currency : allCurrencies) {
            if (shortName.equals(currency.shortName)) {
                selectedCurrency = currency;
                break;
            }
        }
        if (selectedCurrency != null) {
            currencyDialog.getCurrencyAdapter().setLastCheckedPosition(allCurrencies.indexOf(selectedCurrency));
            subscriptionViewModel.setSelectedCurrency(selectedCurrency);
            currencyTextInputLayout.requestFocus();
        }
    }

    public void setAllValidatedFieldsNotError() {
        setNameTextInputLayoutNotError();
        setCategoryTextInputLayoutNotError();
        setCycleTextInputLayoutNotError();
        setNextPaymentTextInputLayoutNotError();
        setPriceTextInputLayoutNotError();
        setCurrencyTextInputLayoutNotError();
    }

    public void setNameTextInputLayoutNotError() {
        nameTextInputLayout.setError(null);
        nameTextInputLayout.setErrorEnabled(false);
        isNameHasError = false;
    }

    public void setCategoryTextInputLayoutNotError() {
        categoryTextInputLayout.setError(null);
        categoryTextInputLayout.setErrorEnabled(false);
        isCategoryHasError = false;
    }

    public void setCycleTextInputLayoutNotError() {
        cycleTextInputLayout.setError(null);
        cycleTextInputLayout.setErrorEnabled(false);
        isCycleHasError = false;
    }

    public void setNextPaymentTextInputLayoutNotError() {
        nextPaymentTextInputLayout.setError(null);
        nextPaymentTextInputLayout.setErrorEnabled(false);
        isNextPaymentHasError = false;
    }

    public void setPriceTextInputLayoutNotError() {
        priceTextInputLayout.setErrorEnabled(false);
        priceTextInputLayout.setError(null);
        isPriceHasError = false;
    }

    public void setCurrencyTextInputLayoutNotError() {
        currencyTextInputLayout.setError(null);
        currencyTextInputLayout.setErrorEnabled(false);
        isCurrencyHasError = false;
    }

    public abstract void initializeViewModel();
    public abstract void setContentView();
    public abstract void initializeUI();
    public abstract void setDialogs();
    public abstract void setAdditionalObservers();
}
