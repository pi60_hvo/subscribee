package com.vladyslavkhimich.subscribee.ui.subscription.archived.general

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingActivity
import com.vladyslavkhimich.subscribee.utils.types.complex.ViewAnimator
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

abstract class ArchivedSubscriptionActivity : AppCompatActivity() {

    lateinit var durationTextInputLayout: TextInputLayout
    lateinit var nextPaymentTextInputEditText: TextInputEditText
    lateinit var reminderTextInputEditText: TextInputEditText
    lateinit var freePeriodTextInputEditText: TextInputEditText
    lateinit var cycleTextInputEditText: TextInputEditText
    lateinit var currencyTextInputEditText: TextInputEditText
    lateinit var categoryTextInputEditText: TextInputEditText
    lateinit var nameTextInputEditText: TextInputEditText
    lateinit var descriptionTextInputEditText: TextInputEditText
    lateinit var priceTextInputEditText: TextInputEditText

    lateinit var freePeriodCheckBox: CheckBox
    var subscriptionID: Int = 0

    lateinit var subscriptionConstraintLayout: ConstraintLayout
    lateinit var iconContainerConstraintLayout: ConstraintLayout
    lateinit var iconImageView: ImageView
    lateinit var iconChangeLinearLayout: LinearLayout

    lateinit var nextPaymentNoteTextView: TextView
    lateinit var durationNoteTextView: TextView

    lateinit var archivedSubscriptionViewModel: ArchivedSubscriptionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscriptionID = intent.extras!!.getInt(ManagingActivity.ID_KEY)
        initializeViewModel()
        this.setContentView()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initializeUI()
        disableUI()
        hideNotesTextViews()
        setObservers()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(ManagingActivity.ID_KEY, subscriptionID)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar_archived_subscription, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.unarchive_subscription_option -> {
                unarchiveSubscription()
                return true
            }
            R.id.delete_subscription_option -> {
                deleteSubscription()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun unarchiveSubscription() {
        DatabaseRepository.getInstance(this).unarchiveSubscription(subscriptionID)
        finish()
    }

    private fun deleteSubscription() {
        with(AlertDialog.Builder(this)) {
            setMessage(R.string.dialog_message_delete)
            setTitle(R.string.dialog_title_delete)
            setPositiveButton(R.string.yes) {_, _ ->
                DatabaseRepository.getInstance(this@ArchivedSubscriptionActivity).deleteSubscription(subscriptionID)
                finish()
            }
            setNegativeButton(R.string.no) { _, _ -> }
            show()
        }
    }
    fun toggleFreePeriodViews(isVisible: Boolean) {
        ViewAnimator.animateViewFade(subscriptionConstraintLayout, durationTextInputLayout, isVisible)
        freePeriodCheckBox.isChecked = isVisible
    }

    fun hideNotesTextViews() {
        nextPaymentNoteTextView.visibility = View.GONE
        durationNoteTextView.visibility = View.GONE
    }


    abstract fun initializeViewModel()
    abstract fun setContentView()
    abstract fun initializeUI()
    abstract fun disableUI()
    abstract fun setObservers()
}