package com.vladyslavkhimich.subscribee.ui.donation

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.vladyslavkhimich.subscribee.databinding.ActivityDonationBinding

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 25.11.2021
 * @email vladislav.himich@4k.com.ua
 */
class DonationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDonationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDonationBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this
        setContentView(binding.root)
        initToolbar()
        initClickListeners()
    }

    private fun initToolbar() {
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun initClickListeners() {
        binding.sodaContainer.setOnClickListener {
            showToast("Soda donated")
        }

        binding.hotDogContainer.setOnClickListener {
            showToast("Hot dog donated")
        }

        binding.burgerContainer.setOnClickListener {
            showToast("Burger donated")
        }

        binding.allContainer.setOnClickListener {
            showToast("All together donated")
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        toggleSelectionOfMarqueeTextViews(true)
    }

    override fun onPause() {
        super.onPause()
        toggleSelectionOfMarqueeTextViews(false)
    }

    private fun toggleSelectionOfMarqueeTextViews(isSelected: Boolean) {
        binding.sodaText.isSelected = isSelected
        binding.hotDogText.isSelected = isSelected
        binding.burgerText.isSelected = isSelected
        binding.allText.isSelected = isSelected
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}