package com.vladyslavkhimich.subscribee.ui.settings;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.utils.ui.preferences.currency.CurrencyPreference;
import com.vladyslavkhimich.subscribee.utils.ui.preferences.currency.CurrencyPreferenceDialogFragmentCompat;
import com.vladyslavkhimich.subscribee.utils.ui.preferences.time.TimePreference;
import com.vladyslavkhimich.subscribee.utils.ui.preferences.time.TimePreferenceDialogFragmentCompat;

import java.util.ArrayList;

public class SettingsFragment extends PreferenceFragmentCompat {

    public ArrayList<Currency> allCurrencies;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DatabaseRepository.getInstance(getContext()).getCurrencies().observe(this, currencies -> allCurrencies = new ArrayList<>(currencies));
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.root_preferences);
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        DialogFragment dialogFragment;
        if (preference instanceof TimePreference) {
            dialogFragment = TimePreferenceDialogFragmentCompat.newInstance(preference.getKey());

            dialogFragment.setTargetFragment(this, 0);
            dialogFragment.show(this.getParentFragmentManager(), "android.support.v7.preference.PreferenceFragment.DIALOG");
        }
        else if (preference instanceof CurrencyPreference) {
            dialogFragment = CurrencyPreferenceDialogFragmentCompat.newInstance(preference.getKey());
            dialogFragment.setTargetFragment(this, 0);
            dialogFragment.show(this.getParentFragmentManager(), "android.support.v7.preference.PreferenceFragment.DIALOG");
            ((CurrencyPreferenceDialogFragmentCompat)dialogFragment).setAllCurrencies(allCurrencies);
        }
        else {
            super.onDisplayPreferenceDialog(preference);
        }
    }
}
