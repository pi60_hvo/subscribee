package com.vladyslavkhimich.subscribee.ui.operations;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithAssociatedData;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;

import java.util.Calendar;
import java.util.List;

public class OperationsViewModel extends AndroidViewModel {
    public OperationsViewModel(@NonNull Application application) {
        super(application);
    }

    private final MutableLiveData<List<OperationWithAssociatedData>> operationsWithData = new MutableLiveData<>();

    public LiveData<List<OperationWithAssociatedData>> getOperationsWithData() {
        return operationsWithData;
    }

    public void setOperationsWithData(List<OperationWithAssociatedData> operationsWithData) {
        this.operationsWithData.postValue(operationsWithData);
    }

    private final MutableLiveData<Calendar> selectedDate = new MutableLiveData<>();

    public MutableLiveData<Calendar> getSelectedDate() {
        return selectedDate;
    }

    public Calendar getSelectedDateObject() {
        return selectedDate.getValue();
    }

    public void setSelectedDate(Calendar selectedDate) {
        this.selectedDate.postValue(selectedDate);
    }

    public void loadOperationsWithData() {
        Calendar selectedDate = getSelectedDateObject();
        if (selectedDate != null) {
            setOperationsWithData(
                    DatabaseRepository
                            .getInstance(
                                    getApplication().getApplicationContext())
                            .getOperationsBetweenDates(
                                    DateHelper.getFirstDayOfMonth(selectedDate),
                                    DateHelper.getLastDayOfMonth(selectedDate)
                            ));
        }
    }
}