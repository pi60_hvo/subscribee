package com.vladyslavkhimich.subscribee.ui.subscription.creation.personal;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.CheckBox;

import androidx.lifecycle.ViewModelProvider;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.general.CreationActivity;
import com.vladyslavkhimich.subscribee.utils.types.complex.ColorHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DoubleHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal.PersonalCreationCategoryDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal.PersonalCreationCurrencyDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal.PersonalCreationCycleDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal.PersonalCreationFreePeriodDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal.PersonalCreationIconPickerDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.personal.PersonalCreationReminderDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class PersonalCreationActivity extends CreationActivity {
    CheckBox familyPartCheckBox;

    @Override
    public void initializeViewModel() {
        subscriptionViewModel = new ViewModelProvider(this).get(PersonalCreationViewModel.class);
    }

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_creation_personal);
    }

    @Override
    public void initializeUI() {
        setLayouts();
        setTextInputs();
        setCheckboxes();
        setImageViews();
        setButtons();
    }

    void setLayouts() {
        subscriptionConstraintLayout = findViewById(R.id.personalSubscriptionConstraintLayout);
        iconContainerConstraintLayout = findViewById(R.id.iconContainer);

        durationConstraintLayout = findViewById(R.id.duration_container);
        durationConstraintLayout.setVisibility(View.GONE);
    }

    void setTextInputs() {
        durationTextInputLayout = findViewById(R.id.personalSubscriptionDurationTextLayout);
        //durationTextInputLayout.setVisibility(View.GONE);
        nameTextInputLayout = findViewById(R.id.personalSubscriptionNameTextLayout);
        categoryTextInputLayout = findViewById(R.id.personalSubscriptionCategoryTextLayout);
        cycleTextInputLayout = findViewById(R.id.personalSubscriptionCycleTextLayout);
        nextPaymentTextInputLayout = findViewById(R.id.personalSubscriptionNextPaymentTextLayout);
        priceTextInputLayout = findViewById(R.id.personalSubscriptionPriceTextLayout);
        currencyTextInputLayout = findViewById(R.id.personalSubscriptionCurrencyTextLayout);

        reminderTextInputEditText = findViewById(R.id.personalSubscriptionReminderEditText);
        nextPaymentTextInputEditText = findViewById(R.id.personalSubscriptionNextPaymentEditText);
        freePeriodTextInputEditText = findViewById(R.id.personalSubscriptionDurationEditText);
        cycleTextInputEditText = findViewById(R.id.personalSubscriptionCycleEditText);
        currencyTextInputEditText = findViewById(R.id.personalSubscriptionCurrencyEditText);
        categoryTextInputEditText = findViewById(R.id.personalSubscriptionCategoryEditText);
        nameTextInputEditText = findViewById(R.id.personalSubscriptionNameEditText);
        descriptionTextInputEditText = findViewById(R.id.personalSubscriptionDescriptionEditText);
        priceTextInputEditText = findViewById(R.id.personalSubscriptionPriceEditText);
    }

    void setCheckboxes() {
        hasFreePeriodCheckBox = findViewById(R.id.personalSubscriptionFreePeriodCheckBox);
        familyPartCheckBox = findViewById(R.id.personalSubscriptionFamilyPartCheckBox);
    }

    void setImageViews() {
        iconImageView = findViewById(R.id.personalSubscriptionIconImageView);
    }

    void setButtons() {
        createButton = findViewById(R.id.personalSubscriptionCreateButton);
    }

    @Override
    public void setDialogs() {
        setReminderDialog();
        setNextPaymentDateDialog();
        setFreePeriodDialog();
        setIconPickerDialog();
        setCyclePickerDialog();
        setCurrencyDialog();
        setCategoryDialog();
    }

    void setReminderDialog() {
        reminderDialog = new PersonalCreationReminderDialog();
        reminderDialog.initialize(PersonalCreationActivity.this);
    }

    void setNextPaymentDateDialog() {
        nextPaymentDatePickerDialog = new DatePickerDialog(PersonalCreationActivity.this, (view, year, month, dayOfMonth) -> {
            setNextPaymentDate(year, month, DateHelper.convertDayOfMonthToNotAfter28(dayOfMonth));
            setNextPaymentDateString();
            setNextPaymentTextInputText();
            freePeriodDialog.getFreePeriodsAdapter().addToNextPaymentFreePeriod();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        nextPaymentDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
    }

    void setFreePeriodDialog() {
        freePeriodDialog = new PersonalCreationFreePeriodDialog();
        freePeriodDialog.initializeFreePeriodAlertDialog(PersonalCreationActivity.this);
    }

    void setIconPickerDialog() {
        iconPickerDialog = new PersonalCreationIconPickerDialog(PersonalCreationActivity.this);
    }

    void setCyclePickerDialog() {
        cycleDialog = new PersonalCreationCycleDialog();
        cycleDialog.initialize(PersonalCreationActivity.this);
    }

    void setCurrencyDialog() {
        currencyDialog = new PersonalCreationCurrencyDialog();
        currencyDialog.initialize(PersonalCreationActivity.this);
    }

    void setCategoryDialog() {
        categoryDialog = new PersonalCreationCategoryDialog();
        categoryDialog.initialize(PersonalCreationActivity.this);
    }

    @Override
    public void createSubscription() {
        int cycleID = createCycleIfNotExistAndReturnID();

        Subscription subscriptionToInsert = new Subscription();

        subscriptionToInsert.cycleID = cycleID;
        subscriptionToInsert.categoryID = subscriptionViewModel.getSelectedCategoryObject().categoryID;
        subscriptionToInsert.currencyID = subscriptionViewModel.getSelectedCurrencyObject().currencyID;

        subscriptionToInsert.name = Objects.requireNonNull(nameTextInputEditText.getText()).toString();
        subscriptionToInsert.description = descriptionTextInputEditText.getText() == null ? "" : descriptionTextInputEditText.getText().toString();

        Double parsedPrice = DoubleHelper.parseDoubleFromString(Objects.requireNonNull(priceTextInputEditText.getText()).toString());
        if (parsedPrice != null)
            subscriptionToInsert.price = parsedPrice;

        subscriptionToInsert.iconColor = ColorHelper.intColorToHexString(subscriptionViewModel.getSelectedColorIDValue());
        subscriptionToInsert.icon = ResourcesHelper.getResourceNameFromID(getApplicationContext(), subscriptionViewModel.getSelectedIconIDValue());

        subscriptionToInsert.isArchived = false;
        subscriptionToInsert.isFamily = false;
        subscriptionToInsert.isFamilyPart = familyPartCheckBox.isChecked();

        Calendar onlyDateCalendar = Calendar.getInstance();
        onlyDateCalendar.setTime(nextPaymentDate.getTime());
        DateHelper.getOnlyDateCalendar(onlyDateCalendar);

        subscriptionToInsert.nextPaymentDate = onlyDateCalendar.getTime();
        subscriptionToInsert.startDate = new Date();

        int insertedSubscriptionID = (int) DatabaseRepository.getInstance(getApplicationContext()).insertOneSubscriptionAndReturnID(subscriptionToInsert);

        createAssociatedReminder(insertedSubscriptionID);
        createAssociatedFreePeriod(insertedSubscriptionID);
    }
}