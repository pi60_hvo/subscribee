package com.vladyslavkhimich.subscribee.ui.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.app.BaseActivity;
import com.vladyslavkhimich.subscribee.app.MyApplication;
import com.yariksoffice.lingver.Lingver;

public class SettingsActivity extends BaseActivity {
    SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.settings, new SettingsFragment())
                    .commit();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        sharedPreferenceChangeListener = (sharedPreferences, key) -> {
            if (key.equals(MyApplication.LANGUAGE_KEY)) {
                Lingver.getInstance().setLocale(getApplicationContext(), sharedPreferences.getString(key, "en"));
                restartApplication();
            }
            else if (key.equals(MyApplication.DARK_MODE_KEY)) {
                boolean isDarkMode = sharedPreferences.getBoolean(MyApplication.DARK_MODE_KEY, false);
                if (isDarkMode)
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                else
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        };
       PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }



    public void restartApplication() {
        Intent restartIntent = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        restartIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(restartIntent);
        finish();
    }
}