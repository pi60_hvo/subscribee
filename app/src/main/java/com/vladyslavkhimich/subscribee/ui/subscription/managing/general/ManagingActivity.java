package com.vladyslavkhimich.subscribee.ui.subscription.managing.general;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Cycle;
import com.vladyslavkhimich.subscribee.database.entities.PaymentDate;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.database.entities.partials.ManagingSubscription;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionActivity;
import com.vladyslavkhimich.subscribee.utils.types.complex.ColorHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ViewAnimator;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DoubleHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.CycleManagingDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.managing.NextPaymentManagingDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public abstract class ManagingActivity extends SubscriptionActivity {
    public static final String ID_KEY = "Subscription_ID";

    public TextInputLayout descriptionTextInputLayout;
    public TextInputLayout reminderTextInputLayout;
    Menu optionsMenu;
    public int subscriptionID;
    public boolean isEditing = false;
    public CycleManagingDialog cycleDialog;
    public NextPaymentManagingDialog nextPaymentDialog;
    public MaterialButton editButton;
    public ConstraintLayout paymentsConstraintLayout;
    public boolean isNeedToShowNextPaymentDisabledDialog;
    public boolean isNeedToLoadPayments;
    public ManagingSubscription fallbackSubscription;
    Calendar previousSelectedCalendar;

    public Observer<ManagingSubscription> updateObserver = new Observer<ManagingSubscription>() {
        @Override
        public void onChanged(ManagingSubscription personalSubscription) {

            int cycleID = createCycleIfNotExistAndReturnID();
            isNeedToLoadPayments = fallbackSubscription.cycle.cycleID != cycleID;

            Subscription subscription = personalSubscription.subscription;
            subscription.categoryID = subscriptionViewModel.getSelectedCategoryObject().categoryID;
            subscription.cycleID = cycleID;
            subscription.currencyID = subscriptionViewModel.getSelectedCurrencyObject().currencyID;

            subscription.icon = ResourcesHelper.getResourceNameFromID(getApplicationContext(), subscriptionViewModel.getSelectedIconIDValue());
            subscription.iconColor = ColorHelper.intColorToHexString(subscriptionViewModel.getSelectedColorIDValue());

            subscription.name = Objects.requireNonNull(nameTextInputEditText.getText()).toString();
            subscription.description = descriptionTextInputEditText.getText() == null ? "" : descriptionTextInputEditText.getText().toString();

            Double parsedPrice = DoubleHelper.parseDoubleFromString(Objects.requireNonNull(priceTextInputEditText.getText()).toString());
            if (parsedPrice != null)
                subscription.price = parsedPrice;

            setAdditionalFieldsWhenUpdate(subscription);

            if (nextPaymentDialog.isChangeLastPaid != null) {
                isNeedToLoadPayments = true;
                if (nextPaymentDialog.isChangeLastPaid) {
                    int id = DatabaseRepository.getInstance(ManagingActivity.this).getSubscriptionLastPaymentDateID(subscriptionID);
                    DatabaseRepository.getInstance(ManagingActivity.this).updatePaymentDateDate(id, nextPaymentDate.getTime());
                } else {
                    PaymentDate paymentDateToInsert = new PaymentDate();
                    paymentDateToInsert.subscriptionID = subscriptionID;
                    paymentDateToInsert.paymentDate = nextPaymentDate.getTime();
                    DatabaseRepository.getInstance(ManagingActivity.this).insertOnePaymentDate(paymentDateToInsert);
                }
            }
            if (subscription.nextPaymentDate.compareTo(nextPaymentDate.getTime()) != 0)
                isNeedToLoadPayments = true;
            subscription.nextPaymentDate = nextPaymentDate.getTime();

            updateCycle(cycleID);

            int associatedFreePeriodID = DatabaseRepository.getInstance(getApplicationContext()).getFreePeriodIDBySubscriptionID(personalSubscription.subscription.subscriptionID);

            if (!hasFreePeriodCheckBox.isChecked()) {
                if (associatedFreePeriodID != 0) {
                    deleteFreePeriodByID(associatedFreePeriodID);
                }
            } else {
                if (associatedFreePeriodID != 0)
                    updateAssociatedFreePeriod(associatedFreePeriodID);
                else
                    createAssociatedFreePeriod(personalSubscription.subscription.subscriptionID);
            }

            int associatedReminderID = DatabaseRepository.getInstance(ManagingActivity.this).getReminderIDBySubscriptionID(personalSubscription.subscription.subscriptionID);

            if (associatedReminderID != 0 && subscriptionViewModel.getSelectedReminderObject() != null)
                updateAssociatedReminder(associatedReminderID);
            else if (associatedReminderID == 0 && subscriptionViewModel.getSelectedReminderObject() != null) {
                createAssociatedReminder(personalSubscription.subscription.subscriptionID);
            } else {
                deleteReminderByID(personalSubscription.subscription.subscriptionID);
            }


            ((ManagingViewModel) subscriptionViewModel).getManagingSubscription().removeObserver(this);
            DatabaseRepository.getInstance(getApplicationContext()).updateOneSubscription(subscription);
            clearFallbackSubscription();
            setManagingDialogsFlagsToNull();
        }
    };

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            subscriptionID = bundle.getInt(ID_KEY);

        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        hideEditingMenuOptions();
        toggleTextInputsEnablement(false);
        loadPayments();
        setAdditionalOnClickListeners();
        nextPaymentDialog.addToNextPaymentFreePeriod();
    }

    @Override
    protected void onSaveInstanceState(@NonNull @NotNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(ID_KEY, subscriptionID);
    }

    @Override
    public void onBackPressed() {
        if (isEditing) {
            showEditingExitDialog();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        optionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_toolbar_not_archived_subscriprtion, menu);
        hideNotNecessaryMenuOptions();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull @NotNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (isEditing) {
                showEditingExitDialog();
            } else {
                hideKeyboardAndFinishActivity();
                return true;
            }
        } else if (id == R.id.edit_subscription_option) {
            isEditing = true;
            hideNotEditingMenuOptions();
            showEditingMenuOptions();
            toggleTextInputsEnablement(true);
            toggleEditButtonVisibility(true);
            loadFallbackSubscription();
            return true;
        } else if (id == R.id.complete_subscription_option) {
            validateFields();
            if (!isFieldsError) {
                updateSubscription();
                closeEditingMenu();
            }
            return true;
        } else if (id == R.id.archive_subscription_option) {
            archiveSubscription();
            return true;
        } else if (id == R.id.delete_subscription_option) {
            deleteSubscription();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setAdditionalObservers() {
        subscriptionViewModel.getCurrencies().observe(this, currencies -> currencyDialog.setAdapterCurrencies(currencies));

        subscriptionViewModel.getCycles().observe(this, cycles -> cycleDialog.setAdapterCycles(cycles));

        ((ManagingViewModel) subscriptionViewModel).getManagingSubscription().observe(this, managingSubscription -> {
            if (managingSubscription != null) {
                if (getSupportActionBar() != null)
                    getSupportActionBar().setTitle(managingSubscription.subscription.name);
                nameTextInputEditText.setText(managingSubscription.subscription.name);
                descriptionTextInputEditText.setText(managingSubscription.subscription.description);
                priceTextInputEditText.setText(DoubleHelper.getStringFromDouble(managingSubscription.subscription.price));
                nextPaymentTextInputEditText.setText(DateHelper.getNextPaymentDate(managingSubscription.subscription.nextPaymentDate));
                setNextPaymentDate(managingSubscription.subscription.nextPaymentDate);
                setAdditionalFieldsWhenObserve(managingSubscription);
                setSelectedCurrencyByShortName(managingSubscription.currency.shortName);
                ((ManagingViewModel) subscriptionViewModel).setAssociatedSubscriptionData(managingSubscription);
                toggleFreePeriodViews(managingSubscription.freePeriod != null);
                if (isNeedToLoadPayments) {
                    loadPayments();
                    isNeedToLoadPayments = false;
                }
            }
        });

        ((ManagingViewModel) subscriptionViewModel).getIsCycleChangeAll().observe(this, isChangeAll -> {
            if (isChangeAll == null || !isChangeAll) {
                isNeedToShowNextPaymentDisabledDialog = false;
                toggleNextPaymentEditText(true);
                if (previousSelectedCalendar != null)
                    nextPaymentDialog.setSelectedDate(previousSelectedCalendar);
            } else {
                isNeedToShowNextPaymentDisabledDialog = true;
                previousSelectedCalendar = (Calendar) nextPaymentDate.clone();
                if (fallbackSubscription != null) {
                    Calendar fallbackCalendar = Calendar.getInstance();
                    fallbackCalendar.setTime(fallbackSubscription.subscription.nextPaymentDate);
                    nextPaymentDialog.setSelectedDate(
                            fallbackCalendar.get(Calendar.YEAR),
                            fallbackCalendar.get(Calendar.MONTH),
                            fallbackCalendar.get(Calendar.DAY_OF_MONTH)
                    );
                }
            }
        });
    }

    public void hideNotNecessaryMenuOptions() {
        hideOptionInOptionsMenu(R.id.complete_subscription_option);
    }

    public void hideNotEditingMenuOptions() {
        hideOptionInOptionsMenu(R.id.edit_subscription_option);
        hideOptionInOptionsMenu(R.id.archive_subscription_option);
        hideOptionInOptionsMenu(R.id.delete_subscription_option);
    }

    public void showNotEditingMenuOptions() {
        showOptionInOptionsMenu(R.id.edit_subscription_option);
        showOptionInOptionsMenu(R.id.archive_subscription_option);
        showOptionInOptionsMenu(R.id.delete_subscription_option);
        changeNavigationIcon(true);
    }

    public void hideEditingMenuOptions() {
        hideOptionInOptionsMenu(R.id.complete_subscription_option);
    }

    public void showEditingMenuOptions() {
        showOptionInOptionsMenu(R.id.complete_subscription_option);
        changeNavigationIcon(false);
    }

    void changeNavigationIcon(boolean isDefault) {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            if (isDefault) {
                supportActionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);
            } else {
                supportActionBar.setHomeAsUpIndicator(R.drawable.ic_close_white);
            }
        }
    }

    public void hideOptionInOptionsMenu(int id) {
        if (optionsMenu != null) {
            MenuItem item = optionsMenu.findItem(id);
            item.setVisible(false);
        }
    }

    public void showOptionInOptionsMenu(int id) {
        if (optionsMenu != null) {
            MenuItem item = optionsMenu.findItem(id);
            item.setVisible(true);
        }
    }

    public void closeEditingMenu() {
        hideEditingMenuOptions();
        showNotEditingMenuOptions();
        toggleTextInputsEnablement(false);
        toggleEditButtonVisibility(false);
        isEditing = false;
    }

    public void toggleEditButtonVisibility(boolean isVisible) {
        ViewAnimator.animateViewFade(subscriptionConstraintLayout, paymentsConstraintLayout, !isVisible);
        ViewAnimator.animateViewFade(subscriptionConstraintLayout, editButton, isVisible);
    }

    public void setAdditionalOnClickListeners() {
        editButton.setOnClickListener(v -> {
            validateFields();
            if (!isFieldsError) {
                updateSubscription();
                closeEditingMenu();
            }
        });

        nextPaymentTextInputEditText.setOnClickListener(v -> {
            if (!isNeedToShowNextPaymentDisabledDialog) {
                nextPaymentTextInputEditText.requestFocus();
                nextPaymentDialog.showDatePickerDialog();
            } else {
                showNextPaymentEditTextDisabledDialog();
            }
        });
        nextPaymentTextInputEditText.setInputType(InputType.TYPE_NULL);

        cycleTextInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isInTouchMode() && hasFocus)
                v.performClick();
        });
        cycleTextInputEditText.setOnClickListener(v -> {
            cycleTextInputEditText.requestFocus();
            cycleDialog.getCycleAlertDialog().show();
        });
        cycleTextInputEditText.setInputType(InputType.TYPE_NULL);

        currencyTextInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
            Log.i("CALLED", "Managing activity currency onFocusChange called");
            if (v.isInTouchMode() && hasFocus)
                v.performClick();
        });
    }

    public void archiveSubscription() {
        new AlertDialog.Builder(ManagingActivity.this)
                .setTitle(R.string.dialog_title_archive)
                .setMessage(R.string.dialog_message_archive)
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    DatabaseRepository.getInstance(ManagingActivity.this).archiveSubscription(subscriptionID);
                    hideKeyboardAndFinishActivity();
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    public void deleteSubscription() {
        new AlertDialog.Builder(ManagingActivity.this)
                .setTitle(R.string.dialog_title_delete)
                .setMessage(R.string.dialog_message_delete)
                .setPositiveButton(R.string.yes, ((dialog, which) -> {
                    DatabaseRepository.getInstance(ManagingActivity.this).deleteSubscription(subscriptionID);
                    hideKeyboardAndFinishActivity();
                }))
                .setNegativeButton(R.string.no, null)
                .show();
    }

    public void recalculatePaymentDates(Cycle cycle, List<PaymentDate> paymentDates) {
        Calendar startCalendar = Calendar.getInstance();

        startCalendar.setTime(paymentDates.get(0).paymentDate);

        for (int i = 1; i < paymentDates.size(); i++) {
            startCalendar.add(Calendar.DATE, cycle.daysCount);
            startCalendar.add(Calendar.WEEK_OF_YEAR, cycle.weeksCount);
            startCalendar.add(Calendar.MONTH, cycle.monthsCount);
            startCalendar.add(Calendar.YEAR, cycle.yearsCount);

            paymentDates.get(i).paymentDate = startCalendar.getTime();
        }
    }

    public Date getFirstNotPaidPaymentDate(int cycleID) {
        Date lastPaidDate = DatabaseRepository.getInstance(this).getSubscriptionLastPaymentDate(subscriptionID);

        if (lastPaidDate != null) {
            Cycle cycle = DatabaseRepository.getInstance(this).getCycleByID(cycleID);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(lastPaidDate);
            DateHelper.addCyclePeriodToCalendar(calendar, cycle);
            return calendar.getTime();
        }

        return null;
    }

    public void toggleFreePeriodViews(boolean isVisible) {
        hasFreePeriodCheckBox.setChecked(isVisible);
    }

    public void toggleNextPaymentEditText(boolean isEnabled) {
        nextPaymentTextInputEditText.setEnabled(isEnabled);
    }

    public void showNextPaymentEditTextDisabledDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_message_next_payment_disabled)
                .setPositiveButton("OK", null)
                .show();
    }

    public void toggleIconChangeLinearLayoutVisibility(boolean isVisible) {
        iconChangeLinearLayout.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public void toggleNoteTextViewVisibility(boolean isVisible) {
        noteTextView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public void toggleTextInputsEnablement(boolean isEnabled) {
        if (isEnabled) {
            enableInputs();
            enableAdditionalInputs();
        } else {
            disableInputs();
            disableAdditionalInputs();
        }
        toggleIconChangeLinearLayoutVisibility(isEnabled);
        toggleNoteTextViewVisibility(isEnabled);
    }

    public void enableInputs() {
        nameTextInputLayout.setEnabled(true);
        categoryTextInputLayout.setEnabled(true);
        descriptionTextInputLayout.setEnabled(true);
        cycleTextInputLayout.setEnabled(true);
        nextPaymentTextInputLayout.setEnabled(true);
        durationTextInputLayout.setEnabled(true);
        priceTextInputLayout.setEnabled(true);
        currencyTextInputLayout.setEnabled(true);
        reminderTextInputLayout.setEnabled(true);
        hasFreePeriodCheckBox.setEnabled(true);
        iconContainerConstraintLayout.setEnabled(true);
    }

    public void disableInputs() {
        nameTextInputLayout.setEnabled(false);
        nameTextInputEditText.setEnabled(false);
        categoryTextInputLayout.setEnabled(false);
        descriptionTextInputLayout.setEnabled(false);
        cycleTextInputLayout.setEnabled(false);
        nextPaymentTextInputLayout.setEnabled(false);
        durationTextInputLayout.setEnabled(false);
        priceTextInputLayout.setEnabled(false);
        currencyTextInputLayout.setEnabled(false);
        reminderTextInputLayout.setEnabled(false);
        hasFreePeriodCheckBox.setEnabled(false);
        iconContainerConstraintLayout.setEnabled(false);
    }

    public void showEditingExitDialog() {
        new AlertDialog.Builder(ManagingActivity.this)
                .setTitle(R.string.dialog_title_edit_close)
                .setMessage(R.string.dialog_message_edit_close)
                .setNegativeButton(R.string.dialog_positive_edit_close, null)
                .setPositiveButton(R.string.dialog_negative_edit_close, (dialog, which) -> {
                    setAllValidatedFieldsNotError();
                    closeEditingMenu();
                    fallbackToPreviousData();
                    clearFallbackSubscription();
                })
                .show();
    }
    public void setManagingDialogsFlagsToNull() {
        cycleDialog.setChangeAllViewModelValue(null);
        nextPaymentDialog.isChangeLastPaid = null;
    }

    public abstract void loadSubscription();

    public abstract void updateSubscription();

    public abstract void loadPayments();

    public abstract void enableAdditionalInputs();

    public abstract void disableAdditionalInputs();

    public abstract void loadFallbackSubscription();

    public abstract void fallbackToPreviousData();

    public abstract void clearFallbackSubscription();

    public abstract void setAdditionalFieldsWhenUpdate(Subscription subscription);

    public abstract void setAdditionalFieldsWhenObserve(ManagingSubscription managingSubscription);

    public abstract void updateCycle(int cycleID);
}
