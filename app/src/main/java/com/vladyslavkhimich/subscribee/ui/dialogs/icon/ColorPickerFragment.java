package com.vladyslavkhimich.subscribee.ui.dialogs.icon;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.utils.ui.adapters.dialogs.icon.IconPickerColorAdapter;
import com.vladyslavkhimich.subscribee.utils.types.complex.ColorHelper;
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnColorClickListenerCallback;

public class ColorPickerFragment extends Fragment {
    GridView colorPickerGridView;
    IconPickerColorAdapter iconPickerColorAdapter;
    private final OnColorClickListenerCallback onColorClickListenerCallback;

    public ColorPickerFragment(OnColorClickListenerCallback onColorClickListenerCallback, Context context) {
        this.onColorClickListenerCallback = onColorClickListenerCallback;
        iconPickerColorAdapter = new IconPickerColorAdapter(context);
    }

    public IconPickerColorAdapter getIconPickerColorAdapter() {
        return  iconPickerColorAdapter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dialog_color, container, false);
        initializeGridView(root);
        return root;
    }

    void initializeGridView(View view) {
        colorPickerGridView = view.findViewById(R.id.iconDialogColorGridView);
        colorPickerGridView.setOnItemClickListener((parent, view1, position, id) -> {
            if (iconPickerColorAdapter.mainColorsArrayList.contains(iconPickerColorAdapter.getColorsArrayList().get(0))) {
                iconPickerColorAdapter.setColorsArrayList(ColorHelper.getColorShadesFromColor(iconPickerColorAdapter.getItem(position)));
                iconPickerColorAdapter.setIsSubColorCollection(true);
                iconPickerColorAdapter.setPreviousSelectedPosition(position);
                iconPickerColorAdapter.setCurrentSelectedPosition(5);
                onColorClickListenerCallback.onColorClicked(iconPickerColorAdapter.getItem(iconPickerColorAdapter.getCurrentSelectedPosition()), iconPickerColorAdapter.getIsSubColorCollection());
            }
            else {
                iconPickerColorAdapter.setCurrentSelectedPosition(position);
                onColorClickListenerCallback.onColorClicked(iconPickerColorAdapter.getItem(position), iconPickerColorAdapter.getIsSubColorCollection());
            }
            iconPickerColorAdapter.notifyDataSetChanged();
        });
        colorPickerGridView.setAdapter(iconPickerColorAdapter);
    }
}
