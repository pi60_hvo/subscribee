package com.vladyslavkhimich.subscribee.ui.statistics

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.tabs.TabLayout
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.databinding.FragmentStatisticsBinding
import com.vladyslavkhimich.subscribee.ui.statistics.overview.StatisticsOverviewFragment
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.MonthRangeDateSelector
import java.util.*

class StatisticsFragment : Fragment() {
    val statisticsViewModel by activityViewModels<StatisticsViewModel>()

    private lateinit var binding: FragmentStatisticsBinding

    lateinit var activeFragment: Fragment

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentStatisticsBinding.inflate(inflater)
        initModels()
        initTabLayouts()
        initListeners()
        initClickListeners()
        setDatePickerDate()
        return binding.root
    }

    private fun initModels() {
        binding.statisticsModel = statisticsViewModel.statisticsModel
    }

    private fun initTabLayouts() {
        initUpperTabLayout()
        initLowerTabLayout()
    }

    private fun initUpperTabLayout() {
        with (binding.upperTabLayout) {
            addTab(newTab().setText(R.string.text_tab_statistics_upper_personal))
            addTab(newTab().setText(R.string.text_tab_statistics_upper_family))
            addTab(newTab().setText(R.string.text_tab_statistics_upper_all))
            addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    if (tab != null)
                        statisticsViewModel.setSelectedUpperTabIndex(tab.position)
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })
        }
    }

    private fun initLowerTabLayout() {
        with (binding.lowerTabLayout) {
            addTab(newTab().setText(R.string.text_tab_statistics_lower_overview))
            addTab(newTab().setText(R.string.text_tab_statistics_lower_timeline))
            addTab(newTab().setText(R.string.text_tab_statistics_lower_radial))
            addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    if (tab != null)
                        statisticsViewModel.setSelectedLowerTabIndex(tab.position)
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })
        }
    }

    private fun initListeners() {
        statisticsViewModel.getSelectedDate().observe(viewLifecycleOwner) {
            startDateCalendar ->
            binding.monthSelector.datePickerTextView.text = DateHelper.getMonthYearString(startDateCalendar.time)
        }

        /*statisticsViewModel.getSelectedUpperTabIndex().observe(viewLifecycleOwner) {

        }*/

        statisticsViewModel.getSelectedLowerTabIndex().observe(viewLifecycleOwner) { tabIndex ->
            when (tabIndex) {
                0 -> {
                    activeFragment = StatisticsOverviewFragment()
                }
            }
            changeFragment()
        }
    }

    private fun changeFragment() {
        childFragmentManager.beginTransaction().replace(R.id.fragment_container, activeFragment).commit()
    }

    private fun initClickListeners() {
        binding.monthSelector.root.setOnClickListener {
            showDatePicker()
        }
    }

    @SuppressLint("RestrictedApi")
    private fun showDatePicker() {
        val monthRangeDateSelector = MonthRangeDateSelector()
        val materialDatePickerBuilder: MaterialDatePicker.Builder<androidx.core.util.Pair<Long, Long>> = MaterialDatePicker.Builder.customDatePicker(monthRangeDateSelector)
        val materialDatePicker: MaterialDatePicker<androidx.core.util.Pair<Long, Long>>
        with (materialDatePickerBuilder) {
            setTitleText(R.string.select_month)
            setTheme(R.style.ThemeOverlay_MaterialComponents_MaterialCalendar)
            materialDatePicker = build()
        }
        materialDatePicker.addOnPositiveButtonClickListener { selection ->
            selection?.let {
                val startDateCalendar = Calendar.getInstance()
                startDateCalendar.time = Date(selection.first)
                statisticsViewModel.setSelectedDate(startDateCalendar)
            }
        }
        materialDatePicker.show(parentFragmentManager, "TAG")
    }

    private fun setDatePickerDate() {
        statisticsViewModel.setSelectedDate(Calendar.getInstance())
    }
}