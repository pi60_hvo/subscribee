package com.vladyslavkhimich.subscribee.ui.subscription.creation.family;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.database.entities.Member;
import com.vladyslavkhimich.subscribee.database.entities.Subscription;
import com.vladyslavkhimich.subscribee.ui.subscription.creation.general.CreationActivity;
import com.vladyslavkhimich.subscribee.utils.types.complex.ColorHelper;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.DoubleHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family.FamilyCreationCategoryDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family.FamilyCreationCurrencyDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family.FamilyCreationCycleDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family.FamilyCreationFreePeriodDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family.FamilyCreationIconPickerDialog;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.creation.family.FamilyCreationReminderDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class FamilyCreationActivity extends CreationActivity {
    TextView youTextView;

    @Override
    public void initializeViewModel() {
        subscriptionViewModel = new ViewModelProvider(this).get(FamilyCreationViewModel.class);
    }

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_creation_family);
    }

    @Override
    public void initializeUI() {
        setLayouts();
        setTextInputs();
        setCheckboxes();
        setImageViews();
        setButtons();
        setTextViews();
    }

    void setLayouts() {
        subscriptionConstraintLayout = findViewById(R.id.familySubscriptionConstraintLayout);
        iconContainerConstraintLayout = findViewById(R.id.iconContainer);

        durationConstraintLayout = findViewById(R.id.duration_container);
        durationConstraintLayout.setVisibility(View.GONE);
    }

    void setTextInputs() {
        durationTextInputLayout = findViewById(R.id.familySubscriptionDurationTextLayout);
        //durationTextInputLayout.setVisibility(View.GONE);
        nameTextInputLayout = findViewById(R.id.familySubscriptionNameTextLayout);
        categoryTextInputLayout = findViewById(R.id.familySubscriptionCategoryTextLayout);
        cycleTextInputLayout = findViewById(R.id.familySubscriptionCycleTextLayout);
        nextPaymentTextInputLayout = findViewById(R.id.familySubscriptionNextPaymentTextLayout);
        priceTextInputLayout = findViewById(R.id.familySubscriptionPriceTextLayout);
        currencyTextInputLayout = findViewById(R.id.familySubscriptionCurrencyTextLayout);

        reminderTextInputEditText = findViewById(R.id.familySubscriptionReminderEditText);
        nextPaymentTextInputEditText = findViewById(R.id.familySubscriptionNextPaymentEditText);
        freePeriodTextInputEditText = findViewById(R.id.familySubscriptionDurationEditText);
        cycleTextInputEditText = findViewById(R.id.familySubscriptionCycleEditText);
        currencyTextInputEditText = findViewById(R.id.familySubscriptionCurrencyEditText);
        categoryTextInputEditText = findViewById(R.id.familySubscriptionCategoryEditText);
        nameTextInputEditText = findViewById(R.id.familySubscriptionNameEditText);
        descriptionTextInputEditText = findViewById(R.id.familySubscriptionDescriptionEditText);
        priceTextInputEditText = findViewById(R.id.familySubscriptionPriceEditText);
    }

    void setCheckboxes() {
        hasFreePeriodCheckBox = findViewById(R.id.familySubscriptionFreePeriodCheckBox);
    }

    void setImageViews() {
        iconImageView = findViewById(R.id.familySubscriptionIconImageView);
    }

    void setButtons() {
        createButton = findViewById(R.id.familySubscriptionCreateButton);
    }

    void setTextViews() {
        youTextView = findViewById(R.id.familySubscriptionYouTextView);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = sharedPreferences.getString("username", null);
        if (username != null) {
            youTextView.setText(getString(R.string.username_you, username));
        }
    }

    @Override
    public void setDialogs() {
        setReminderDialog();
        setNextPaymentDateDialog();
        setFreePeriodDialog();
        setIconPickerDialog();
        setCyclePickerDialog();
        setCurrencyDialog();
        setCategoryDialog();
    }

    @Override
    public void createSubscription() {
        int cycleID = createCycleIfNotExistAndReturnID();

        Subscription subscriptionToInsert = new Subscription();

        subscriptionToInsert.cycleID = cycleID;
        subscriptionToInsert.categoryID = subscriptionViewModel.getSelectedCategoryObject().categoryID;
        subscriptionToInsert.currencyID = subscriptionViewModel.getSelectedCurrencyObject().currencyID;

        subscriptionToInsert.name = Objects.requireNonNull(nameTextInputEditText.getText()).toString();
        subscriptionToInsert.description = descriptionTextInputEditText.getText() == null ? "" : descriptionTextInputEditText.getText().toString();

        Double parsedPrice = DoubleHelper.parseDoubleFromString(Objects.requireNonNull(priceTextInputEditText.getText()).toString());
        if (parsedPrice != null)
            subscriptionToInsert.price = parsedPrice;

        subscriptionToInsert.iconColor = ColorHelper.intColorToHexString(subscriptionViewModel.getSelectedColorIDValue());
        subscriptionToInsert.icon = ResourcesHelper.getResourceNameFromID(getApplicationContext(), subscriptionViewModel.getSelectedIconIDValue());

        subscriptionToInsert.isArchived = false;
        subscriptionToInsert.isFamily = true;

        Calendar onlyDateCalendar = Calendar.getInstance();
        onlyDateCalendar.setTime(nextPaymentDate.getTime());
        DateHelper.getOnlyDateCalendar(onlyDateCalendar);

        subscriptionToInsert.nextPaymentDate = onlyDateCalendar.getTime();
        subscriptionToInsert.startDate = new Date();

        int insertedSubscriptionID = (int) DatabaseRepository.getInstance(getApplicationContext()).insertOneSubscriptionAndReturnID(subscriptionToInsert);

        createAssociatedReminder(insertedSubscriptionID);
        createAssociatedFreePeriod(insertedSubscriptionID);

        int insertedOwnerID = createAssociatedOwnerAndReturnID(insertedSubscriptionID, Objects.requireNonNull(parsedPrice));
        DatabaseRepository.getInstance(getApplicationContext()).updateSubscriptionOwnerID(insertedSubscriptionID, insertedOwnerID);

    }

    int createAssociatedOwnerAndReturnID(int subscriptionID, double subscriptionShare) {
        Member memberToInsert = new Member();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = sharedPreferences.getString("username", null);

        if (username != null) {
            memberToInsert.name = username;
        }
        else {
            memberToInsert.name = "Manager";
        }

        memberToInsert.subscriptionShare = subscriptionShare;
        memberToInsert.subscriptionID = subscriptionID;

        return (int) DatabaseRepository.getInstance(getApplicationContext()).insertOneMemberAndReturnID(memberToInsert);
    }

    void setReminderDialog() {
        reminderDialog = new FamilyCreationReminderDialog();
        reminderDialog.initialize(FamilyCreationActivity.this);
    }

    void setNextPaymentDateDialog() {
        nextPaymentDatePickerDialog = new DatePickerDialog(FamilyCreationActivity.this, (view, year, month, dayOfMonth) -> {
            setNextPaymentDate(year, month, DateHelper.convertDayOfMonthToNotAfter28(dayOfMonth));
            setNextPaymentDateString();
            setNextPaymentTextInputText();
            freePeriodDialog.getFreePeriodsAdapter().addToNextPaymentFreePeriod();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        nextPaymentDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
    }

    void setFreePeriodDialog() {
        freePeriodDialog = new FamilyCreationFreePeriodDialog();
        freePeriodDialog.initializeFreePeriodAlertDialog(FamilyCreationActivity.this);
    }

    void setIconPickerDialog() {
        iconPickerDialog = new FamilyCreationIconPickerDialog(FamilyCreationActivity.this);
    }

    void setCyclePickerDialog() {
        cycleDialog = new FamilyCreationCycleDialog();
        cycleDialog.initialize(FamilyCreationActivity.this);
    }

    void setCurrencyDialog() {
        currencyDialog = new FamilyCreationCurrencyDialog();
        currencyDialog.initialize(FamilyCreationActivity.this);
    }

    void setCategoryDialog() {
        categoryDialog = new FamilyCreationCategoryDialog();
        categoryDialog.initialize(FamilyCreationActivity.this);
    }
}