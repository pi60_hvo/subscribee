package com.vladyslavkhimich.subscribee.ui.subscription.archived.general

import android.app.Application
import androidx.lifecycle.AndroidViewModel

abstract class ArchivedSubscriptionViewModel(application: Application) : AndroidViewModel(application) {
    abstract fun loadSubscriptionFromDB(subscriptionID: Int)
    abstract fun loadPaymentsFromDB(subscriptionID: Int)
}