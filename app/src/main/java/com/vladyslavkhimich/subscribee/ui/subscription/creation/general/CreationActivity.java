package com.vladyslavkhimich.subscribee.ui.subscription.creation.general;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.core.content.res.ResourcesCompat;
import androidx.preference.PreferenceManager;

import com.vladyslavkhimich.subscribee.R;
import com.vladyslavkhimich.subscribee.database.entities.Currency;
import com.vladyslavkhimich.subscribee.ui.subscription.general.SubscriptionActivity;
import com.vladyslavkhimich.subscribee.utils.types.complex.ResourcesHelper;
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.general.CycleDialog;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.Objects;

public abstract class CreationActivity extends SubscriptionActivity {

    public MaterialButton createButton;
    public DatePickerDialog nextPaymentDatePickerDialog;

    public CycleDialog cycleDialog;
    Currency initialSelectedCurrency;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setRandomColorAndIcon();
        setButtonsOnClickListeners();
        setAdditionalOnClickListeners();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (checkIfAnyValueIsSet()) {
                new AlertDialog.Builder(CreationActivity.this)
                        .setMessage(R.string.dialog_want_to_leave)
                        .setNegativeButton(R.string.no, null)
                        .setPositiveButton(R.string.yes, ((dialog, which) -> hideKeyboardAndFinishActivity())).show();
            }
            else {
                hideKeyboardAndFinishActivity();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (checkIfAnyValueIsSet()) {
            new AlertDialog.Builder(CreationActivity.this)
                    .setMessage(R.string.dialog_want_to_leave)
                    .setNegativeButton(R.string.no, null)
                    .setPositiveButton(R.string.yes, ((dialog, which) -> finish())).show();

        }
        else
            finish();
    }

    @Override
    public void setAdditionalObservers() {
        subscriptionViewModel.getCurrencies().observe(this, currencies -> {
            currencyDialog.setAdapterCurrencies(currencies);
            setCurrencyFromPrefs();
        });

        subscriptionViewModel.getCycles().observe(this, cycles -> cycleDialog.setAdapterCycles(cycles));
    }

    boolean checkIfAnyValueIsSet() {
        return Objects.requireNonNull(nameTextInputEditText.getText()).length() > 0 ||
                Objects.requireNonNull(categoryTextInputEditText.getText()).length() > 0 ||
                Objects.requireNonNull(descriptionTextInputEditText.getText()).length() > 0 ||
                Objects.requireNonNull(cycleTextInputEditText.getText()).length() > 0 ||
                Objects.requireNonNull(nextPaymentTextInputEditText.getText()).length() > 0 ||
                Objects.requireNonNull(freePeriodTextInputEditText.getText()).length() > 0 ||
                Objects.requireNonNull(priceTextInputEditText.getText()).length() > 0 ||
                !initialSelectedCurrency.equals(subscriptionViewModel.getSelectedCurrencyObject()) ||
                Objects.requireNonNull(reminderTextInputEditText.getText()).length() > 0;
    }

    void setButtonsOnClickListeners() {
        createButton.setOnClickListener((v) -> {
            validateFields();
            if (!isFieldsError) {
                createButton.setEnabled(false);
                createSubscription();
                Toast.makeText(this, R.string.toast_subscription_created, Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    void setRandomColorAndIcon() {
        subscriptionViewModel.setSelectedIconID(ResourcesHelper.getRandomIconID(CreationActivity.this));
        subscriptionViewModel.setSelectedIconColor(ResourcesCompat.getColor(CreationActivity.this.getResources(), ResourcesHelper.getRandomColor(CreationActivity.this), null));
    }

    void setCurrencyFromPrefs() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String currencyShortName = sharedPreferences.getString("currency", "USD");
        ArrayList<Currency> allCurrencies = currencyDialog.getCurrencyAdapter().getAllCurrencies();

        Currency selectedCurrency = null;
        for (Currency currency : allCurrencies) {
            if (currencyShortName.equals(currency.shortName)) {
                selectedCurrency = currency;
                break;
            }
        }

        if (selectedCurrency != null) {
            initialSelectedCurrency = selectedCurrency;
            currencyDialog.getCurrencyAdapter().setLastCheckedPosition(allCurrencies.indexOf(selectedCurrency));
            subscriptionViewModel.setSelectedCurrency(selectedCurrency);
            //currencyTextInputLayout.requestFocus();
            setCurrencyOnFocusChangeListener();
        }
    }

    void setAdditionalOnClickListeners() {
        cycleTextInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isInTouchMode() && hasFocus)
                v.performClick();
        });
        cycleTextInputEditText.setOnClickListener(v -> {
            cycleTextInputEditText.requestFocus();
            cycleDialog.getCycleAlertDialog().show();
        });
        cycleTextInputEditText.setInputType(InputType.TYPE_NULL);

        nextPaymentTextInputEditText.setOnClickListener(v -> {
            nextPaymentTextInputEditText.requestFocus();
            nextPaymentDatePickerDialog.show();
        });
        nextPaymentTextInputEditText.setInputType(InputType.TYPE_NULL);
    }

    void setCurrencyOnFocusChangeListener() {
        currencyTextInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.isInTouchMode() && hasFocus)
                v.performClick();
        });
    }

    public abstract void createSubscription();
}
