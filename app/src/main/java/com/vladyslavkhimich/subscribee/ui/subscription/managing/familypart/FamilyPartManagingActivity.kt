package com.vladyslavkhimich.subscribee.ui.subscription.managing.familypart

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelProvider
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.ui.subscription.managing.personal.PersonalManagingActivity
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.familypart.*

class FamilyPartManagingActivity : PersonalManagingActivity() {
    override fun initializeViewModel() {
        subscriptionViewModel = ViewModelProvider(this).get(FamilyPartManagingViewModel::class.java)
        (subscriptionViewModel as FamilyPartManagingViewModel).loadSubscriptionFromDatabase(subscriptionID)
    }

    override fun setContentView() {
        setContentView(R.layout.activity_managing_family_part)
    }

    override fun initializeUI() {
        setLayouts()
        setTextViews()
        setTextInputs()
        setCheckboxes()
        setImageViews()
        setButtons()
        setRecyclerViews()
    }

    private fun setLayouts() {
        subscriptionConstraintLayout = findViewById(R.id.familyPartManagingConstraintLayout)
        iconContainerConstraintLayout = findViewById(R.id.familyPartManagingIconContainer)
        paymentsConstraintLayout = findViewById(R.id.familyPartManagingPaymentsContainer)
        iconChangeLinearLayout = findViewById(R.id.familyPartManagingChangeIconLayout)

        durationConstraintLayout = findViewById(R.id.duration_container)
        durationConstraintLayout.visibility = View.GONE
    }

    private fun setTextViews() {
        noPaymentsTextView = findViewById(R.id.familyPartManagingNoPaymentsTextView)
        noPaymentsTextView.visibility = View.GONE

        noteTextView = findViewById(R.id.next_payment_note_text)
        noteTextView.visibility = View.GONE
    }

    private fun setTextInputs() {
        reminderTextInputLayout = findViewById(R.id.familyPartManagingReminderTextLayout)
        durationTextInputLayout = findViewById(R.id.familyPartManagingDurationTextLayout)
        nameTextInputLayout = findViewById(R.id.familyPartManagingNameTextLayout)
        descriptionTextInputLayout = findViewById(R.id.familyPartManagingDescriptionTextLayout)
        categoryTextInputLayout = findViewById(R.id.familyPartManagingCategoryTextLayout)
        cycleTextInputLayout = findViewById(R.id.familyPartManagingCycleTextLayout)
        nextPaymentTextInputLayout = findViewById(R.id.familyPartManagingNextPaymentTextLayout)
        priceTextInputLayout = findViewById(R.id.familyPartManagingPriceTextLayout)
        currencyTextInputLayout = findViewById(R.id.familyPartManagingCurrencyTextLayout)

        reminderTextInputEditText = findViewById(R.id.familyPartManagingReminderEditText)
        nextPaymentTextInputEditText = findViewById(R.id.familyPartManagingNextPaymentEditText)
        freePeriodTextInputEditText = findViewById(R.id.familyPartManagingDurationEditText)
        cycleTextInputEditText = findViewById(R.id.familyPartManagingCycleEditText)
        currencyTextInputEditText = findViewById(R.id.familyPartManagingCurrencyEditText)
        categoryTextInputEditText = findViewById(R.id.familyPartManagingCategoryEditText)
        nameTextInputEditText = findViewById(R.id.familyPartManagingNameEditText)
        descriptionTextInputEditText = findViewById(R.id.familyPartManagingDescriptionEditText)
        priceTextInputEditText = findViewById(R.id.familyPartManagingPriceEditText)
    }

    private fun setCheckboxes() {
        hasFreePeriodCheckBox = findViewById(R.id.familyPartManagingFreePeriodCheckBox)
        familyPartCheckBox = findViewById(R.id.familyPartManagingFamilyPartCheckBox)
    }

    private fun setImageViews() {
        iconImageView = findViewById(R.id.familyPartManagingIconImageView)
    }

    private fun setButtons() {
        editButton = findViewById(R.id.familyPartManagingEditButton)
    }

    private fun setRecyclerViews() {
        paymentsRecyclerView = findViewById(R.id.familyPartManagingPaymentsRecyclerView)
        paymentsShimmerFrameLayout = findViewById(R.id.familyPartManagingPaymentsShimmerLayout)
        setPaymentsRecyclerView()
    }


    override fun setDialogs() {
        setReminderDialog()
        setNextPaymentDialog()
        setFreePeriodDialog()
        setIconPickerDialog()
        setCyclePickerDialog()
        setCurrencyDialog()
        setCategoryDialog()
    }

    private fun setReminderDialog() {
        reminderDialog = FamilyPartManagingReminderDialog()
        reminderDialog.initialize(this)
    }

    private fun setNextPaymentDialog() {
        nextPaymentDialog = FamilyPartManagingNextPaymentDialog(this)
    }

    private fun setFreePeriodDialog() {
        freePeriodDialog = FamilyPartManagingFreePeriodDialog()
        freePeriodDialog.initializeFreePeriodAlertDialog(this)
    }

    private fun setIconPickerDialog() {
        iconPickerDialog = FamilyPartManagingIconPickerDialog(this)
    }

    private fun setCyclePickerDialog() {
        cycleDialog = FamilyPartManagingCycleDialog()
        cycleDialog.initialize(this)
    }

    private fun setCurrencyDialog() {
        currencyDialog = FamilyPartManagingCurrencyDialog()
        currencyDialog.initialize(this)
    }

    private fun setCategoryDialog() {
        categoryDialog = FamilyPartManagingCategoryDialog()
        categoryDialog.initialize(this)
    }

}