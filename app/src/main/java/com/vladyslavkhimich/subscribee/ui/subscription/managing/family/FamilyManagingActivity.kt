package com.vladyslavkhimich.subscribee.ui.subscription.managing.family

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updateLayoutParams
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.vladyslavkhimich.subscribee.R
import com.vladyslavkhimich.subscribee.database.DatabaseRepository
import com.vladyslavkhimich.subscribee.database.entities.Member
import com.vladyslavkhimich.subscribee.database.entities.Operation
import com.vladyslavkhimich.subscribee.database.entities.PaymentDate
import com.vladyslavkhimich.subscribee.database.entities.Subscription
import com.vladyslavkhimich.subscribee.database.entities.partials.ManagingSubscription
import com.vladyslavkhimich.subscribee.database.entities.partials.MemberWithOperationsAndPaymentDates
import com.vladyslavkhimich.subscribee.database.entities.partials.OperationWithPaymentDates
import com.vladyslavkhimich.subscribee.models.MemberWithDataForAdapter
import com.vladyslavkhimich.subscribee.models.OperationAndPaymentDate
import com.vladyslavkhimich.subscribee.ui.subscription.managing.general.ManagingActivity
import com.vladyslavkhimich.subscribee.utils.types.complex.ViewAnimator
import com.vladyslavkhimich.subscribee.utils.types.primitives.DateHelper
import com.vladyslavkhimich.subscribee.utils.types.primitives.DoubleHelper
import com.vladyslavkhimich.subscribee.utils.ui.RecyclerViewEmptySupport
import com.vladyslavkhimich.subscribee.utils.ui.adapters.payments.familymember.FamilyMembersAdapter
import com.vladyslavkhimich.subscribee.utils.ui.dialogs.realization.managing.family.*
import com.vladyslavkhimich.subscribee.utils.ui.interfaces.OnFamilyPaymentClickListenerCallback
import com.vladyslavkhimich.subscribee.utils.ui.validators.NumberValidator
import com.vladyslavkhimich.subscribee.utils.ui.validators.StringValidator
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.util.*
import kotlin.collections.ArrayList

class FamilyManagingActivity : ManagingActivity() {
    private lateinit var mainScrollView: ScrollView
    private lateinit var addMemberConstraintLayout: ConstraintLayout
    private lateinit var membersShimmerLayout: ShimmerFrameLayout
    private lateinit var memberNameTextInputLayout: TextInputLayout
    private lateinit var memberPriceShareTextInputLayout: TextInputLayout
    private lateinit var memberNameTextInputEditText: TextInputEditText
    private lateinit var memberPriceShareTextInputEditText: TextInputEditText
    private lateinit var addMemberButton: MaterialButton
    private lateinit var saveMemberButton: MaterialButton
    private lateinit var addMemberCloseImageView: ImageView
    private lateinit var equalMemberShareTextView: TextView
    private lateinit var membersRecyclerView: RecyclerViewEmptySupport
    private lateinit var membersAdapter: FamilyMembersAdapter
    private var isMemberNameHasError: Boolean = false
    private var isMemberPriceShareHasError: Boolean = false
    private var isMemberFieldsHasError: Boolean = false
    private var managerShare: Double = 0.0
    private var isEditingMember: Boolean = false
    private var memberToEdit: Member? = null

    private val membersWithOperationsObserver: Observer<List<MemberWithOperationsAndPaymentDates>> = Observer<List<MemberWithOperationsAndPaymentDates>> { it ->
        if (it.isNotEmpty()) {
        val subscriptionAllPaymentDates = DatabaseRepository.getInstance(this@FamilyManagingActivity).getPaymentDatesBySubscriptionID(subscriptionID)
        subscriptionAllPaymentDates.forEach { item ->
            if (!it[0].operationWithPaymentDates.any { it.paymentDates[0].paymentDate == item.paymentDate }) {
                it.forEach { memberWithPaymentDates ->
                    memberWithPaymentDates.operationWithPaymentDates.add(
                        OperationWithPaymentDates(
                            null,
                            listOf(item)
                        )
                    )
                }
            }
        }

        val membersWithDataForAdapter = ArrayList<MemberWithDataForAdapter>()
        val managerOperationsAndPaymentDates = ArrayList(it[0].operationWithPaymentDates)
        it.forEach {
            item ->
            val memberOperationAndPaymentDateList = LinkedList<OperationAndPaymentDate>()
            for (operationAndPaymentDates: OperationWithPaymentDates in item.operationWithPaymentDates) {
                memberOperationAndPaymentDateList.add(OperationAndPaymentDate(operationAndPaymentDates.operation, operationAndPaymentDates.paymentDates[0]))
            }
            managerOperationsAndPaymentDates.forEach {
                managerItem ->
                memberOperationAndPaymentDateList.add(OperationAndPaymentDate(null, managerItem.paymentDates[0]))
            }
            membersWithDataForAdapter.add(MemberWithDataForAdapter(item.member, TreeSet(memberOperationAndPaymentDateList)))
        }
        membersAdapter.setMembersWithDataAndNotify(membersWithDataForAdapter, isNeedToLoadPayments)


        val nextPaymentDateFromMembers = membersWithDataForAdapter[0].operationsAndPaymentDates.firstOrNull {it.operation == null}?.paymentDate?.paymentDate
        var minDate = membersWithDataForAdapter[0].operationsAndPaymentDates.lastOrNull { it.operation != null }?.paymentDate?.paymentDate
        if (nextPaymentDateFromMembers != null) {
            DatabaseRepository.getInstance(this@FamilyManagingActivity).updateSubscriptionNextPaymentDate(subscriptionID, nextPaymentDateFromMembers)
        }
            if (minDate != null) {

            val minDateCalendar = Calendar.getInstance()
            minDateCalendar.time = minDate

            DateHelper.addCyclePeriodToCalendar(minDateCalendar,
                    DatabaseRepository.getInstance(this@FamilyManagingActivity).getCycleBySubscriptionID(subscriptionID)
            )

            DatabaseRepository.getInstance(this@FamilyManagingActivity)
                    .updateSubscriptionNextPaymentDate(
                        subscriptionID,
                        minDateCalendar.time)
            }
            else {
                minDate = Date()
            }

        val nextPaymentDate = DatabaseRepository.getInstance(this@FamilyManagingActivity).getNextPaymentDateBySubscriptionID(subscriptionID)

        /*if (minDate == null) {
            minDate = DateHelper.addOneDayToDate(Date())
        }*/
        nextPaymentDialog.setNextPaymentDialogWithMinDate(nextPaymentDateFromMembers ?: nextPaymentDate, DateHelper.addOneDayToDate(minDate))
        toggleAddMemberButtonConstraints()
    }
    }

    override fun onResume() {
        super.onResume()
        startShimmerAnimation()
    }

    private fun startShimmerAnimation() {
        membersShimmerLayout.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        stopShimmerAnimation()
    }

    private fun stopShimmerAnimation() {
        membersShimmerLayout.stopShimmerAnimation()
    }

    override fun setContentView() {
        setContentView(R.layout.activity_managing_family)
    }

    override fun initializeViewModel() {
        subscriptionViewModel = ViewModelProvider(this).get(FamilyManagingViewModel::class.java)
        (subscriptionViewModel as FamilyManagingViewModel).loadSubscriptionFromDatabase(subscriptionID)
        (subscriptionViewModel as FamilyManagingViewModel).loadMembersWithDataFromDatabase(subscriptionID)
    }

    override fun initializeUI() {
        setLayouts()
        setTextInputs()
        setCheckBoxes()
        setImageViews()
        setTextViews()
        setButtons()
        setRecyclerViews()
    }

    private fun setLayouts() {
        mainScrollView = findViewById(R.id.familyManagingMainScrollView)
        subscriptionConstraintLayout = findViewById(R.id.familyManagingConstraintLayout)
        iconContainerConstraintLayout = findViewById(R.id.familyManagingIconConstraintLayout)
        paymentsConstraintLayout = findViewById(R.id.familyManagingMembersConstraintLayout)
        iconChangeLinearLayout = findViewById(R.id.familyManagingChangeIconLayout)
        addMemberConstraintLayout = findViewById(R.id.familyManagingAddMemberConstraintLayout)
        membersShimmerLayout = findViewById(R.id.familyManagingMembersShimmerLayout)

        durationConstraintLayout = findViewById(R.id.duration_container)
        durationConstraintLayout.visibility = View.GONE
    }

    private fun setTextInputs() {
        reminderTextInputLayout = findViewById(R.id.familyManagingReminderTextLayout)
        durationTextInputLayout = findViewById(R.id.familyManagingDurationTextLayout)
        nameTextInputLayout = findViewById(R.id.familyManagingNameTextLayout)
        descriptionTextInputLayout = findViewById(R.id.familyManagingDescriptionTextLayout)
        categoryTextInputLayout = findViewById(R.id.familyManagingCategoryTextLayout)
        cycleTextInputLayout = findViewById(R.id.familyManagingCycleTextLayout)
        nextPaymentTextInputLayout = findViewById(R.id.familyManagingNextPaymentTextLayout)
        priceTextInputLayout = findViewById(R.id.familyManagingPriceTextLayout)
        currencyTextInputLayout = findViewById(R.id.familyManagingCurrencyTextLayout)
        memberNameTextInputLayout = findViewById(R.id.familyManagingMemberNameTextLayout)
        memberPriceShareTextInputLayout = findViewById(R.id.familyManagingMemberShareTextLayout)

        reminderTextInputEditText = findViewById(R.id.familyManagingReminderEditText)
        nextPaymentTextInputEditText = findViewById(R.id.familyManagingNextPaymentEditText)
        freePeriodTextInputEditText = findViewById(R.id.familyManagingDurationEditText)
        cycleTextInputEditText = findViewById(R.id.familyManagingCycleEditText)
        currencyTextInputEditText = findViewById(R.id.familyManagingCurrencyEditText)
        categoryTextInputEditText = findViewById(R.id.familyManagingCategoryEditText)
        nameTextInputEditText = findViewById(R.id.familyManagingNameEditText)
        descriptionTextInputEditText = findViewById(R.id.familyManagingDescriptionEditText)
        priceTextInputEditText = findViewById(R.id.familyManagingPriceEditText)
        memberNameTextInputEditText = findViewById(R.id.familyManagingMemberNameEditText)
        memberPriceShareTextInputEditText = findViewById(R.id.familyManagingMemberShareEditText)
    }

    private fun setCheckBoxes() {
        hasFreePeriodCheckBox = findViewById(R.id.familyManagingFreePeriodCheckBox)
    }

    private fun setImageViews() {
        iconImageView = findViewById(R.id.familyManagingIconImageView)
    }

    private fun setTextViews() {
        equalMemberShareTextView = findViewById(R.id.familyManagingEqualMemberShareTextView)

        noteTextView = findViewById(R.id.next_payment_note_text)
        noteTextView.visibility = View.GONE
    }

    private fun setButtons() {
        editButton = findViewById(R.id.familyManagingEditButton)
        addMemberButton = findViewById(R.id.familyManagingAddMemberButton)
        saveMemberButton = findViewById(R.id.familyManagingSaveMemberButton)
        addMemberCloseImageView = findViewById(R.id.familyManagingAddMemberCloseImageView)
    }

    private fun setRecyclerViews() {
        membersRecyclerView = findViewById(R.id.familyManagingMembersRecyclerView)
        setMembersRecyclerView()
    }

    private fun setMembersRecyclerView() {
        membersAdapter = FamilyMembersAdapter(
                this,
                subscriptionID,
                { _, position ->
                    memberToEdit = Member(membersAdapter.membersWithData[position].member)
                    isEditingMember = true
                    setMemberFieldsForEditing(memberToEdit!!)
                    setEqualMemberShareText(membersAdapter.membersWithData.size)
                    toggleAddMemberLayout(true)
        },
                { _, position ->
                    val memberToDelete = membersAdapter.membersWithData[position].member
                    with(AlertDialog.Builder(this)) {
                        setPositiveButton(R.string.yes) { _, _ -> deleteMember(memberToDelete!!) }
                        setNegativeButton(R.string.no) {_, _ -> }
                        setTitle(getString(R.string.dialog_title_delete_member, memberToDelete!!.name))
                        setMessage(R.string.dialog_message_delete_member)
                        show()
                    }
                },
                object : OnFamilyPaymentClickListenerCallback {
                    override fun onItemClicked(view: View, adapterPosition: Int, familyMemberPosition: Int) {
                        val isChecked = (view as CheckBox).isChecked

                        val member = membersAdapter.membersWithData[familyMemberPosition].member

                        val checkedPaymentDate = membersAdapter.innerAdapters[familyMemberPosition]!!.memberPayments[adapterPosition].paymentDate
                        var paymentDateID = DatabaseRepository.getInstance(this@FamilyManagingActivity).getPaymentDateIDBySubscriptionIDAndDate(subscriptionID, checkedPaymentDate)
                            if (isChecked) {
                                if (paymentDateID == 0) {
                                    val paymentDateToInsert = PaymentDate()
                                    paymentDateToInsert.paymentDate = checkedPaymentDate
                                    paymentDateToInsert.subscriptionID = subscriptionID
                                    paymentDateID = DatabaseRepository.getInstance(this@FamilyManagingActivity).insertOnePaymentDateAndReturnID(paymentDateToInsert).toInt()
                                }

                                val operationToInsert = Operation()
                                operationToInsert.paymentDateID = paymentDateID
                                operationToInsert.subscriptionID = subscriptionID
                                operationToInsert.memberID = member!!.memberID
                                operationToInsert.isOwner = true
                                operationToInsert.operationDate = Date()
                                operationToInsert.currencyID = DatabaseRepository.getInstance(this@FamilyManagingActivity).getCurrencyIDBySubscriptionID(subscriptionID)

                                val subscriptionFreePeriod = DatabaseRepository.getInstance(this@FamilyManagingActivity).getFreePeriodBySubscriptionID(subscriptionID)
                                if (subscriptionFreePeriod != null) {
                                    val subscriptionFreePeriodEndingDate = DateHelper.getFreePeriodDBEndingDate(subscriptionFreePeriod)

                                    if (checkedPaymentDate.before(subscriptionFreePeriodEndingDate)) {
                                        operationToInsert.price = 0.0
                                    }
                                    else {
                                        operationToInsert.price = if (familyMemberPosition == 0) - member.subscriptionShare else member.subscriptionShare
                                    }
                                }
                                else {
                                    operationToInsert.price = if (familyMemberPosition == 0) - member.subscriptionShare else member.subscriptionShare
                                }


                                DatabaseRepository.getInstance(this@FamilyManagingActivity).insertOneOperation(operationToInsert)

                                membersAdapter.innerAdapters[familyMemberPosition]!!.memberPayments[adapterPosition].isChecked = true

                                changeNextPaymentDateData()
                            }
                            else {
                                with (AlertDialog.Builder(this@FamilyManagingActivity)) {
                                    setMessage(R.string.dialog_message_payment_delete)
                                    setNegativeButton(R.string.no) { _, _ -> view.isChecked = true }
                                    setPositiveButton(R.string.yes) {
                                        _, _ ->
                                        if (!membersAdapter.checkIfPositionBeforeLastCheckedPosition(adapterPosition)) {
                                            deleteOperation(paymentDateID, member!!.memberID)
                                            deletePaymentDateIfNoOperations(paymentDateID)
                                        }
                                        else {
                                            deleteOperation(paymentDateID, member!!.memberID)
                                        }

                                        membersAdapter.innerAdapters[familyMemberPosition]!!.memberPayments[adapterPosition].isChecked = false

                                        changeNextPaymentDateData()
                                    }
                                    show()
                                }
                            }
                    }
                })
        membersRecyclerView.adapter = membersAdapter
        membersRecyclerView.layoutManager = LinearLayoutManager(this)
        membersRecyclerView.setHasFixedSize(true)
        membersRecyclerView.setEmptyView(membersShimmerLayout)
    }

    private fun deleteMember(member: Member) {
        changeManagerSubscriptionShare(member.subscriptionShare)
        DatabaseRepository.getInstance(this).deleteOneMember(member)
    }

    private fun changeManagerSubscriptionShare(share: Double) {
        val manager = membersAdapter.membersWithData[0].member
        DatabaseRepository.getInstance(this).updateOneMemberSubscriptionShare(manager!!.memberID, manager.subscriptionShare + share)
    }

    private fun deletePaymentDateIfNoOperations(paymentDateID: Int) {
        val operationCount = DatabaseRepository.getInstance(this).getPaymentDateAssociatedOperationsCount(paymentDateID)

        if (operationCount == 0)
            DatabaseRepository.getInstance(this).deleteOnePaymentDate(paymentDateID)
    }

    private fun deleteOperation(paymentDateID: Int, memberID: Int) {
        DatabaseRepository.getInstance(this).deleteOneOperationByPaymentDateIDAndMemberID(paymentDateID, memberID)
    }

    private fun changeNextPaymentDateData() {
        val nextPaymentDateForSubscription = membersAdapter.getNextPaymentDateForSubscription().paymentDate
        nextPaymentDate.time = nextPaymentDateForSubscription

        DatabaseRepository.getInstance(this@FamilyManagingActivity).updateSubscriptionNextPaymentDate(subscriptionID, nextPaymentDateForSubscription)

        nextPaymentDialog.setNextPaymentDialogWithMinDate(nextPaymentDateForSubscription, membersAdapter.getMinimumNextPaymentDateForDialog())

    }

    override fun setDialogs() {
        setReminderDialog()
        setNextPaymentDialog()
        setFreePeriodDialog()
        setIconPickerDialog()
        setCyclePickerDialog()
        setCurrencyDialog()
        setCategoryDialog()
    }

    private fun setReminderDialog() {
        reminderDialog = FamilyManagingReminderDialog()
        reminderDialog.initialize(this@FamilyManagingActivity)
    }

    private fun setNextPaymentDialog() {
        nextPaymentDialog = FamilyManagingNextPaymentDialog(this@FamilyManagingActivity)
    }

    private fun setFreePeriodDialog() {
        freePeriodDialog = FamilyManagingFreePeriodDialog()
        freePeriodDialog.initializeFreePeriodAlertDialog(this@FamilyManagingActivity)
    }

    private fun setIconPickerDialog() {
        iconPickerDialog = FamilyManagingIconPickerDialog(this@FamilyManagingActivity)
    }

    private fun setCyclePickerDialog() {
        cycleDialog = FamilyManagingCycleDialog()
        cycleDialog.initialize(this@FamilyManagingActivity)
    }

    private fun setCurrencyDialog() {
        currencyDialog = FamilyManagingCurrencyDialog()
        currencyDialog.initialize(this@FamilyManagingActivity)
    }

    private fun setCategoryDialog() {
        categoryDialog = FamilyManagingCategoryDialog()
        categoryDialog.initialize(this@FamilyManagingActivity)
    }

    override fun loadSubscription() {
        //(subscriptionViewModel as FamilyManagingViewModel).loadSubscriptionFromDatabase(subscriptionID)
    }

    override fun updateSubscription() {
        (subscriptionViewModel as FamilyManagingViewModel).managingSubscription.observe(this, updateObserver)
    }

    override fun loadPayments() {
        (subscriptionViewModel as FamilyManagingViewModel).membersWithData?.removeObservers(this)
        (subscriptionViewModel as FamilyManagingViewModel).membersWithData?.observe(this, membersWithOperationsObserver)
    }

    override fun enableAdditionalInputs() {

    }

    override fun disableAdditionalInputs() {

    }

    override fun loadFallbackSubscription() {
        fallbackSubscription = DatabaseRepository.getInstance(this).getManagingSubscriptionByID(subscriptionID)
    }

    override fun fallbackToPreviousData() {
        DatabaseRepository.getInstance(this).updateOneSubscription(fallbackSubscription.subscription)
        setManagingDialogsFlagsToNull()
    }

    override fun clearFallbackSubscription() {
        fallbackSubscription = null
    }

    override fun setAdditionalFieldsWhenUpdate(subscription: Subscription?) {

    }

    override fun setAdditionalFieldsWhenObserve(managingSubscription: ManagingSubscription?) {

    }

    override fun updateCycle(cycleID: Int) {
        if (cycleDialog.changeAll != null) {
            if (cycleDialog.changeAll) {
                val associatedCycle = DatabaseRepository.getInstance(this).getCycleByID(cycleID)
                val paymentDates = DatabaseRepository.getInstance(this).getPaymentDatesBySubscriptionID(subscriptionID)

                if (paymentDates != null) {
                    if (paymentDates.size > 1) {
                        recalculatePaymentDates(associatedCycle, paymentDates)
                        DatabaseRepository.getInstance(this).updatePaymentDates(paymentDates)
                    }
                }
            }
            else {
                isNeedToLoadPayments = true
            }
        }
    }

    override fun setAdditionalOnClickListeners() {
        super.setAdditionalOnClickListeners()

        addMemberButton.setOnClickListener {
            toggleAddMemberLayout(true)
            setEqualMemberShareText(membersAdapter.membersWithData.size + 1)
            isEditingMember = false
        }

        addMemberCloseImageView.setOnClickListener {
            toggleAddMemberLayout(false)
            isEditingMember = false
        }

        saveMemberButton.setOnClickListener {
            validateMemberFields()
            if (!isMemberFieldsHasError) {
                if (!isEditingMember) addMember() else editMember()
                toggleAddMemberLayout(false)
                clearNewMemberTextInputs()
                setAllMemberValidatedFieldsNotError()
            }
        }
    }

    private fun toggleAddMemberLayout(isVisible: Boolean) {
        if (isVisible)
        {
            addMemberButton.visibility = View.GONE
            ViewAnimator.animateViewFade(paymentsConstraintLayout, addMemberConstraintLayout, true)
            getSystemService(Context.INPUT_METHOD_SERVICE).also {
                (it as InputMethodManager).toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
            }
            mainScrollView.postDelayed( {
                mainScrollView.fullScroll(View.FOCUS_DOWN)
                //nextPaymentTextInputEditText.clearFocus()
                memberNameTextInputLayout.requestFocus()
            }, 500)
            setSharesForValidating()
        }
        else
        {
            getSystemService(Context.INPUT_METHOD_SERVICE).also {
                (it as InputMethodManager).hideSoftInputFromWindow(saveMemberButton.windowToken, 0)
            }
            addMemberButton.visibility = View.VISIBLE
            addMemberConstraintLayout.visibility = View.GONE
        }
    }

    private fun setSharesForValidating() {
        managerShare = membersAdapter.membersWithData[0].member!!.subscriptionShare
    }

    private fun setMemberFieldsForEditing(member: Member) {
        memberNameTextInputEditText.setText(member.name)
        memberNameTextInputEditText.setSelection(memberNameTextInputEditText.text!!.length)
        memberPriceShareTextInputEditText.setText(DoubleHelper.getStringFromDouble(member.subscriptionShare))
    }

    private fun setEqualMemberShareText(membersCount: Int) {
        equalMemberShareTextView.text = getString(R.string.equal_share, DoubleHelper.getStringFromDouble(DoubleHelper.parseDoubleFromString(priceTextInputEditText.text.toString()) / membersCount))
    }

    private fun addMember() {
        val memberToAdd = Member()
        memberToAdd.name = memberNameTextInputEditText.text.toString()
        memberToAdd.subscriptionID = subscriptionID
        val memberToAddSubscriptionShare = DoubleHelper.parseDoubleFromString(memberPriceShareTextInputEditText.text.toString())
        memberToAdd.subscriptionShare = memberToAddSubscriptionShare
        DatabaseRepository.getInstance(this).insertOneMember(memberToAdd)
        val manager = membersAdapter.membersWithData[0].member
        DatabaseRepository.getInstance(this).updateOneMemberSubscriptionShare(manager!!.memberID, manager.subscriptionShare - memberToAddSubscriptionShare)
    }

    private fun editMember() {
        val oldSubscriptionShare = memberToEdit!!.subscriptionShare
        val newSubscriptionShare = DoubleHelper.parseDoubleFromString(memberPriceShareTextInputEditText.text.toString())
        memberToEdit!!.name = memberNameTextInputEditText.text.toString()
        memberToEdit!!.subscriptionShare = newSubscriptionShare
        changeManagerSubscriptionShare(oldSubscriptionShare - newSubscriptionShare)
        DatabaseRepository.getInstance(this).updateOneMember(memberToEdit)
        isEditingMember = false
    }

    override fun setTextWatchers() {
        super.setTextWatchers()

        memberNameTextInputEditText.addTextChangedListener(
                onTextChanged = { s: CharSequence?, _: Int, _: Int, _: Int ->
                    when {
                        s?.length == 0 -> {
                            memberNameTextInputLayout.error = this@FamilyManagingActivity.resources.getText(R.string.error_required_field)
                            isMemberNameHasError = true
                        }
                        StringValidator.isErrorText(s.toString()) -> {
                            memberNameTextInputLayout.error = this@FamilyManagingActivity.resources.getText(R.string.error_text)
                            isMemberNameHasError = true
                        }
                        else -> {
                            setMemberNameTextInputLayoutNotError()
                        }
                    }
                }
        )

        memberPriceShareTextInputEditText.addTextChangedListener(
                onTextChanged = {
                    s: CharSequence?, _: Int, _: Int, _: Int ->
                    when (s?.length) {
                        0 -> {
                            memberPriceShareTextInputLayout.error = this@FamilyManagingActivity.resources.getText(R.string.error_required_field)
                            isMemberPriceShareHasError = true
                        }
                        else -> {
                            when {
                                !NumberValidator.isValidDoubleNumber(s.toString()) -> {
                                    memberPriceShareTextInputLayout.error = this.resources.getText(R.string.error_invalid_double)
                                    isMemberPriceShareHasError = true
                                }
                                else -> {
                                    val priceShare = DoubleHelper.parseDoubleFromString(s.toString())
                                    if (!isEditingMember) {
                                        if (managerShare - priceShare < 0) {
                                            memberPriceShareTextInputLayout.error = this.resources.getText(R.string.error_share_exceeding_price)
                                            isMemberPriceShareHasError = true
                                        }
                                        else {
                                            setMemberPriceShareTextInputLayoutNotError(false)
                                        }
                                    }
                                    else {
                                        val oldPriceShare = memberToEdit!!.subscriptionShare
                                        if (priceShare > oldPriceShare + managerShare) {
                                            memberPriceShareTextInputLayout.error = this.resources.getText(R.string.error_share_exceeding_price)
                                            isMemberPriceShareHasError = true
                                        }
                                        else {
                                            setMemberPriceShareTextInputLayoutNotError(false)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        )
    }

    private fun setAllMemberValidatedFieldsNotError() {
        setMemberNameTextInputLayoutNotError()
        setMemberPriceShareTextInputLayoutNotError(true)
    }

    private fun setMemberNameTextInputLayoutNotError() {
        memberNameTextInputLayout.error = null
        memberNameTextInputLayout.isErrorEnabled = false
        isMemberNameHasError = false
    }

    private fun setMemberPriceShareTextInputLayoutNotError(isNeedToClearFocus: Boolean) {
        memberPriceShareTextInputLayout.error = null
        memberPriceShareTextInputLayout.isErrorEnabled = false
        if (isNeedToClearFocus) {
            memberPriceShareTextInputLayout.requestFocus()
            memberPriceShareTextInputLayout.clearFocus()
        }
        isMemberPriceShareHasError = false
    }

    private fun validateMemberFields() {
        setNewMemberFieldsEmptyIfItIsEmpty()
        isMemberFieldsHasError = isMemberNameHasError || isMemberPriceShareHasError
    }

    private fun setNewMemberFieldsEmptyIfItIsEmpty() {
        if (memberNameTextInputEditText.text?.length == 0)
            memberNameTextInputEditText.setText("")
        if (memberPriceShareTextInputEditText.text?.length == 0)
            memberPriceShareTextInputEditText.setText("")
    }

    private fun clearNewMemberTextInputs() {
        memberNameTextInputEditText.setText("")
        memberPriceShareTextInputEditText.setText("")
    }

    override fun setAdditionalObservers() {
        super.setAdditionalObservers()
        (subscriptionViewModel as FamilyManagingViewModel).membersWithData?.observe(this, membersWithOperationsObserver)
        /*(subscriptionViewModel as FamilyManagingViewModel).membersWithData?.observe(this) { it ->
            val subscriptionAllPaymentDates = DatabaseRepository.getInstance(this).getPaymentDatesBySubscriptionID(subscriptionID)
            subscriptionAllPaymentDates.forEach {
                item ->
                if (!it[0].operationWithPaymentDates.any {it.paymentDates[0].paymentDate == item.paymentDate}) {
                    it.forEach {
                        memberWithPaymentDates -> memberWithPaymentDates.operationWithPaymentDates.add(OperationWithPaymentDates(null, listOf(item)))
                    }
            }
            }
            val membersWithDataForAdapter = ArrayList<MemberWithDataForAdapter>()
            val managerOperationsAndPaymentDates = it[0].operationWithPaymentDates
            it.forEach {
                item ->
                val memberOperationAndPaymentDateList = LinkedList<OperationAndPaymentDate>()
                for (operationAndPaymentDates: OperationWithPaymentDates in item.operationWithPaymentDates) {
                    memberOperationAndPaymentDateList.add(OperationAndPaymentDate(operationAndPaymentDates.operation, operationAndPaymentDates.paymentDates[0]))
                }
                managerOperationsAndPaymentDates.forEach {
                    managerItem ->
                    memberOperationAndPaymentDateList.add(OperationAndPaymentDate(null, managerItem.paymentDates[0]))
                }
                membersWithDataForAdapter.add(MemberWithDataForAdapter(item.member, TreeSet(memberOperationAndPaymentDateList)))
            }
            Log.i("CALLED", "Members observer called")
            membersAdapter.setMembersWithDataAndNotify(membersWithDataForAdapter, isNeedToLoadPayments)
            val nextPaymentDate = DatabaseRepository.getInstance(this).getNextPaymentDateBySubscriptionID(subscriptionID)
            val minDate = membersWithDataForAdapter[0].operationsAndPaymentDates.lastOrNull { it.operation != null }?.paymentDate?.paymentDate
            nextPaymentDialog.setNextPaymentDialogWithMinDate(nextPaymentDate, DateHelper.addOneDayToDate(minDate ?: nextPaymentDate))
            toggleAddMemberButtonConstraints()
        }*/
    }

    private fun toggleAddMemberButtonConstraints() {
            addMemberButton.updateLayoutParams<ConstraintLayout.LayoutParams> {
                topToBottom = membersRecyclerView.id
        }
    }
}