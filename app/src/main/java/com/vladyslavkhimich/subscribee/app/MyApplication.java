package com.vladyslavkhimich.subscribee.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import com.vladyslavkhimich.subscribee.database.DatabaseRepository;
import com.vladyslavkhimich.subscribee.models.FreePeriod;
import com.vladyslavkhimich.subscribee.models.Reminder;
import com.vladyslavkhimich.subscribee.utils.enums.AppLocales;
import com.vladyslavkhimich.subscribee.utils.types.complex.GsonHelper;
import com.vladyslavkhimich.subscribee.utils.types.primitives.EnumHelper;
import com.yariksoffice.lingver.Lingver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class MyApplication extends Application {

    public static final int COLORS_COUNT = 19;
    public static final int ICONS_COUNT = 77;
    public static final String LANGUAGE_KEY = "language";
    public static final String DARK_MODE_KEY = "dark_mode";
    public static final String IS_SET_REMINDERS_KEY = "is_set_reminders";
    public static final String IS_SET_FREE_PERIODS_KEY = "is_set_free_periods";
    public static final String IS_SET_COLORS_KEY = "is_set_colors";
    public static final String IS_SET_ICONS_KEY = "is_set_icons";
    public static final String REMINDERS_JSON_KEY = "reminders_json";
    public static final String FREE_PERIODS_JSON_KEY = "free_periods_json";
    public static final String COLOR_PICKER_COLORS_JSON_KEY = "color_picker_colors_json";
    public static final String ICON_PICKER_ICONS_JSON_KEY = "icon_picker_icons_json";


    @Override
    public void onCreate() {
        super.onCreate();
        Lingver.init(this, getLocale(getApplicationContext()));
        setTheme(getApplicationContext());
        setRemindersSharedPrefs(getApplicationContext());
        setFreePeriodSharedPrefs(getApplicationContext());
        setPickerColorsSharedPrefs(getApplicationContext());
        setPickerIconsSharedPrefs(getApplicationContext());
        //getApplicationContext().deleteDatabase("AppDatabase.db");
        DatabaseRepository.getInstance(getApplicationContext());
    }

    public static Locale getLocale(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String systemLocaleString;
        if (sharedPreferences.contains(LANGUAGE_KEY)) {
            systemLocaleString = sharedPreferences.getString(LANGUAGE_KEY, "en");
        }
        else {
            systemLocaleString = Arrays.asList(EnumHelper.getStringArrayFromEnum(AppLocales.class)).contains(Locale.getDefault().getLanguage()) ? Locale.getDefault().getLanguage() : "en";
            sharedPreferences.edit().putString(LANGUAGE_KEY, systemLocaleString).apply();
        }
        return new Locale(systemLocaleString);
    }

    void setTheme(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isDarkMode = sharedPreferences.getBoolean(DARK_MODE_KEY, false);
        if (isDarkMode)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }

    void setRemindersSharedPrefs(Context context) {
        if (getIsNeedToSetReminders(context, true)) {
            String remindersArrayListJSON = GsonHelper.convertRemindersArrayListToJSON(generateRemindersArray());
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(REMINDERS_JSON_KEY, remindersArrayListJSON).apply();
            setIsNeedToSetReminders(context, false);
        }
    }

    public static ArrayList<Reminder> getRemindersFromSharedPrefs(Context context) {
        ArrayList<Reminder> resultArrayList = new ArrayList<>();
        String remindersArrayListJSON = PreferenceManager.getDefaultSharedPreferences(context).getString(REMINDERS_JSON_KEY, null);
        if (remindersArrayListJSON != null) {
            resultArrayList = new ArrayList<>(GsonHelper.convertRemindersJSONToArrayList(remindersArrayListJSON));
        }
        return resultArrayList;
    }

    ArrayList<Reminder> generateRemindersArray() {
        ArrayList<Reminder> subscriptionReminders = new ArrayList<>();

        Reminder sameDayReminder = new Reminder();
        sameDayReminder.setStringIDName("same_day_reminder");
        sameDayReminder.setDaysCount(0);
        subscriptionReminders.add(sameDayReminder);

        Reminder oneDayReminder = new Reminder();
        oneDayReminder.setStringIDName("one_day_before_reminder");
        oneDayReminder.setDaysCount(1);
        subscriptionReminders.add(oneDayReminder);

        Reminder threeDaysReminder = new Reminder();
        threeDaysReminder.setStringIDName("three_days_before_reminder");
        threeDaysReminder.setDaysCount(3);
        subscriptionReminders.add(threeDaysReminder);

        Reminder fiveDaysReminder = new Reminder();
        fiveDaysReminder.setStringIDName("five_days_before_reminder");
        fiveDaysReminder.setDaysCount(5);
        subscriptionReminders.add(fiveDaysReminder);

        Reminder oneWeekReminder = new Reminder();
        oneWeekReminder.setStringIDName("one_week_before_reminder");
        oneWeekReminder.setWeeksCount(1);
        subscriptionReminders.add(oneWeekReminder);

        Reminder neverReminder = new Reminder();
        neverReminder.setStringIDName("never_reminder");
        neverReminder.setNever(true);
        subscriptionReminders.add(neverReminder);

        return subscriptionReminders;
    }

    void setIsNeedToSetReminders(Context context, boolean isNeed) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(IS_SET_REMINDERS_KEY, isNeed).apply();
    }

    boolean getIsNeedToSetReminders(Context context, boolean defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(IS_SET_REMINDERS_KEY, defValue);
    }

    void setFreePeriodSharedPrefs(Context context) {
        if (getIsNeedToSetFreePeriods(context, true)) {
            String freePeriodArrayListJSON = GsonHelper.convertFreePeriodsArrayListToJSON(generateFreePeriodsArray());
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            sharedPreferences.edit().putString(FREE_PERIODS_JSON_KEY, freePeriodArrayListJSON).apply();
            setIsNeedToSetFreePeriods(context, false);
        }
    }

    public static ArrayList<FreePeriod> getFreePeriodsFromSharedPrefs(Context context) {
        ArrayList<FreePeriod> resultArrayList = new ArrayList<>();
        String freePeriodsArrayListJSON = PreferenceManager.getDefaultSharedPreferences(context).getString(FREE_PERIODS_JSON_KEY, null);
        if (freePeriodsArrayListJSON != null) {
            resultArrayList = new ArrayList<>(GsonHelper.convertFreePeriodsJSONToArrayList(freePeriodsArrayListJSON));
        }
        return resultArrayList;
    }

    ArrayList<FreePeriod> generateFreePeriodsArray() {
        ArrayList<FreePeriod> subscriptionFreePeriods = new ArrayList<>();

        FreePeriod toNextPaymentFreePeriod = new FreePeriod();
        toNextPaymentFreePeriod.setStringIDName("to_next_payment_free_period");
        toNextPaymentFreePeriod.setToNextPayment(true);
        subscriptionFreePeriods.add(toNextPaymentFreePeriod);

        FreePeriod threeDaysFreePeriod = new FreePeriod();
        threeDaysFreePeriod.setStringIDName("three_days_free_period");
        threeDaysFreePeriod.setDaysCount(3);
        subscriptionFreePeriods.add(threeDaysFreePeriod);

        FreePeriod oneWeekFreePeriod = new FreePeriod();
        oneWeekFreePeriod.setStringIDName("one_week_free_period");
        oneWeekFreePeriod.setWeeksCount(1);
        subscriptionFreePeriods.add(oneWeekFreePeriod);

        FreePeriod twoWeeksFreePeriod = new FreePeriod();
        twoWeeksFreePeriod.setStringIDName("two_weeks_free_period");
        twoWeeksFreePeriod.setWeeksCount(2);
        subscriptionFreePeriods.add(twoWeeksFreePeriod);

        FreePeriod fourWeeksFreePeriod = new FreePeriod();
        fourWeeksFreePeriod.setStringIDName("four_weeks_free_period");
        fourWeeksFreePeriod.setWeeksCount(4);
        subscriptionFreePeriods.add(fourWeeksFreePeriod);

        FreePeriod oneMonthFreePeriod = new FreePeriod();
        oneMonthFreePeriod.setStringIDName("one_month_free_period");
        oneMonthFreePeriod.setMonthsCount(1);
        subscriptionFreePeriods.add(oneMonthFreePeriod);

        return subscriptionFreePeriods;
    }

    void setIsNeedToSetFreePeriods(Context context, boolean isNeed) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(IS_SET_FREE_PERIODS_KEY, isNeed).apply();
    }

    boolean getIsNeedToSetFreePeriods(Context context, boolean defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(IS_SET_FREE_PERIODS_KEY, defValue);
    }

    void setPickerColorsSharedPrefs(Context context) {
        if (getIsNeedToSetColors(context, true)) {
            String pickerColorsJSON = GsonHelper.convertStringArrayToJSON(generatePickerColorsArray());
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(COLOR_PICKER_COLORS_JSON_KEY, pickerColorsJSON).apply();
            setIsNeedToSetColors(context, false);
        }
    }

    public static String[] getPickerColorsFromSharedPrefs(Context context) {
        String[] colorsFromSharedPrefs = new String[COLORS_COUNT];
        String colorsJSON = PreferenceManager.getDefaultSharedPreferences(context).getString(COLOR_PICKER_COLORS_JSON_KEY, null);
        if (colorsJSON != null)
            colorsFromSharedPrefs = GsonHelper.convertJSONToStringArray(colorsJSON);
        return colorsFromSharedPrefs;
    }

    String[] generatePickerColorsArray() {
        return new String[]{
                "color_picker_dark_orange",
                "color_picker_pink",
                "color_picker_violet",
                "color_picker_dark_violet",
                "color_picker_dark_blue",
                "color_picker_blue",
                "color_picker_light_blue",
                "color_picker_cyan",
                "color_picker_racing_green",
                "color_picker_green",
                "color_picker_green_yellow",
                "color_picker_yellow_green",
                "color_picker_yellow",
                "color_picker_darker_yellow",
                "color_picker_dark_yellow",
                "color_picker_orange",
                "color_picker_brown",
                "color_picker_gray",
                "color_picker_gray_blue"
        };
    }

    void setIsNeedToSetColors(Context context, boolean isNeed) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(IS_SET_COLORS_KEY, isNeed).apply();
    }

    boolean getIsNeedToSetColors(Context context, boolean defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(IS_SET_COLORS_KEY, defValue);
    }

    void setPickerIconsSharedPrefs(Context context) {
        if (getIsNeedToSetIcons(context, true)) {
            String pickerIconsJSON = GsonHelper.convertStringArrayToJSON(generatePickerIconsArray());
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(ICON_PICKER_ICONS_JSON_KEY, pickerIconsJSON).apply();
            setIsNeedToSetIcons(context, false);
        }
    }

    public static String[] getPickerIconsFromSharedPrefs(Context context) {
        String[] iconsFromSharedPrefs = new String[ICONS_COUNT];
        String iconsJSON = PreferenceManager.getDefaultSharedPreferences(context).getString(ICON_PICKER_ICONS_JSON_KEY, null);
        if (iconsJSON != null)
            iconsFromSharedPrefs = GsonHelper.convertJSONToStringArray(iconsJSON);
        return iconsFromSharedPrefs;
    }

    void setIsNeedToSetIcons(Context context, boolean isNeed) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(IS_SET_ICONS_KEY, isNeed).apply();
    }

    boolean getIsNeedToSetIcons(Context context, boolean defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(IS_SET_ICONS_KEY, defValue);
    }

    String[] generatePickerIconsArray() {
        return new String[] {
                "ic_dialog_waves",
                "ic_dialog_boombox",
                "ic_dialog_headphones",
                "ic_dialog_microphone",
                "ic_dialog_music_circle",
                "ic_dialog_music_square",
                "ic_dialog_music_note",
                "ic_dialog_radio",
                "ic_dialog_play",
                "ic_dialog_play_speed",
                "ic_dialog_hd",
                "ic_dialog_monitor_play",
                "ic_dialog_phone_playback",
                "ic_dialog_theater",
                "ic_dialog_tv",
                "ic_dialog_video",
                "ic_dialog_video_vintage",
                "ic_dialog_voice_chat",
                "ic_dialog_youtube",
                "ic_dialog_youtube_tv",
                "ic_dialog_google",
                "ic_dialog_mail",
                "ic_dialog_google_ads",
                "ic_dialog_google_drive",
                "ic_dialog_google_fit",
                "ic_dialog_google_play",
                "ic_dialog_instagram",
                "ic_dialog_android",
                "ic_dialog_android_2",
                "ic_dialog_apple",
                "ic_dialog_camera",
                "ic_dialog_cloud",
                "ic_dialog_cloud_download",
                "ic_dialog_controller",
                "ic_dialog_controller_square",
                "ic_dialog_devices",
                "ic_dialog_laptop_phone",
                "ic_dialog_mac",
                "ic_dialog_phone",
                "ic_dialog_phone_hangup",
                "ic_dialog_security",
                "ic_dialog_security_account",
                "ic_dialog_lock",
                "ic_dialog_send",
                "ic_dialog_sim",
                "ic_dialog_watch",
                "ic_dialog_book",
                "ic_dialog_basket",
                "ic_dialog_cart",
                "ic_dialog_store",
                "ic_dialog_suitcase",
                "ic_dialog_tag",
                "ic_dialog_wallet",
                "ic_dialog_bus",
                "ic_dialog_car",
                "ic_dialog_car_electric",
                "ic_dialog_fuel",
                "ic_dialog_moped",
                "ic_dialog_taxi",
                "ic_dialog_train",
                "ic_dialog_train_car",
                "ic_dialog_tram",
                "ic_dialog_wrench",
                "ic_dialog_baby_carriage",
                "ic_dialog_bed",
                "ic_dialog_charity",
                "ic_dialog_coffee",
                "ic_dialog_dumbbell",
                "ic_dialog_emoticon",
                "ic_dialog_flower",
                "ic_dialog_food",
                "ic_dialog_heart_pulse",
                "ic_dialog_paint",
                "ic_dialog_palette",
                "ic_dialog_puzzle",
                "ic_dialog_star",
                "ic_dialog_star_circle",
        };
    }
}
