package com.vladyslavkhimich.subscribee.app;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    /*Locale currentLocale;

    @Override
    protected void onStart() {
        super.onStart();
        currentLocale = getResources().getConfiguration().locale;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Locale locale = getLocale(this);

        if (!locale.equals(currentLocale)) {
            currentLocale = locale;
            recreate();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }*/

    /*public static Locale getLocale(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        String savedLanguage = sharedPreferences.getString("language", "english");
        String localeString = "en";
        switch (savedLanguage) {
            case "english":
                localeString = "en";
                break;
            case "russian":
                localeString = "ru";
                break;
            case "ukrainian":
                localeString = "uk";
                break;
        }
        return new Locale(localeString);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        Locale locale = getLocale(context);
        Locale.setDefault(locale);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResourcesLocale(context, locale);
        }

        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }*/

    /*@Override
    protected void onResume() {
        super.onResume();
        Locale currentActivityLocale = this.getResources().getConfiguration().locale;
        Locale currentApplicationLocale = new Locale(LocaleHelper.getLanguageISO(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("language", "english")));

        if (!currentActivityLocale.equals(currentApplicationLocale)) {
            recreate();
        }
    }*/

   /* @Override
    public void setContentView(int layoutResID) {
        Boolean themePreference = getSharedPreferences("myPrefs", MODE_PRIVATE).getBoolean("darkMode", false);
        if (!themePreference)
            setTheme(R.style.Theme_Subscribee);
        else
            setTheme(R.style.Theme_Subscribee);
        super.setContentView(layoutResID);
    }*/


}
