package com.vladyslavkhimich.subscribee.models.binding.statistics

import androidx.databinding.ObservableDouble
import androidx.databinding.ObservableField

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 13.12.2021
 * @email vladislav.himich@4k.com.ua
 */
data class StatisticsMoneyModel(
    val moneyValue: ObservableDouble = ObservableDouble(0.0),
    val currencySymbol: ObservableField<String> = ObservableField("")
)
