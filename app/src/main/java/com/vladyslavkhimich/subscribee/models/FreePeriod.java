package com.vladyslavkhimich.subscribee.models;

import java.util.Date;

public class FreePeriod {
    private String stringIDName;
    private int daysCount;
    private int weeksCount;
    private int monthsCount;
    private int yearsCount;
    private boolean isToNextPayment;
    private Date startDate = new Date();

    public String getStringIDName() {
        return stringIDName;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public int getWeeksCount() {
        return weeksCount;
    }

    public int getMonthsCount() {
        return monthsCount;
    }

    public int getYearsCount() {
        return yearsCount;
    }

    public boolean getIsToNextPayment() {
        return isToNextPayment;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStringIDName(String stringIDName) {
        this.stringIDName = stringIDName;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public void setWeeksCount(int weeksCount) {
        this.weeksCount = weeksCount;
    }

    public void setMonthsCount(int monthsCount) {
        this.monthsCount = monthsCount;
    }

    public void setYearsCount(int yearsCount) {
        this.yearsCount = yearsCount;
    }

    public void setToNextPayment(boolean toNextPayment) {
        isToNextPayment = toNextPayment;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isAllFieldsZero() {
        return getDaysCount() == 0 && getWeeksCount() == 0 && getMonthsCount() == 0 && getYearsCount() == 0;
    }

    public FreePeriod() {

    }

    public FreePeriod (com.vladyslavkhimich.subscribee.database.entities.FreePeriod databaseFreePeriod) {
        setDaysCount(databaseFreePeriod.daysCount);
        setWeeksCount(databaseFreePeriod.weeksCount);
        setMonthsCount(databaseFreePeriod.monthsCount);
        setYearsCount(databaseFreePeriod.yearsCount);
        setStartDate(databaseFreePeriod.startDate);
        setToNextPayment(isAllFieldsZero());
    }
}
