package com.vladyslavkhimich.subscribee.models.binding.statistics

import androidx.databinding.ObservableBoolean

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 13.12.2021
 * @email vladislav.himich@4k.com.ua
 */
data class StatisticsModel(
    val isStatisticsDataPresent: ObservableBoolean = ObservableBoolean(false)
)
