package com.vladyslavkhimich.subscribee.models;

import java.util.Date;

public class Payment {
    private Date paymentDate;
    private boolean isChecked;

    public Date getPaymentDate() {
        return paymentDate;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Payment() {

    }

    public Payment(Date paymentDate, boolean isChecked) {
        this.paymentDate = paymentDate;
        this.isChecked = isChecked;
    }
}
