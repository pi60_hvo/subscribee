package com.vladyslavkhimich.subscribee.models

import com.vladyslavkhimich.subscribee.database.entities.Member
import java.util.*

data class MemberWithDataForAdapter(val member: Member?, val operationsAndPaymentDates: SortedSet<OperationAndPaymentDate> = TreeSet())