package com.vladyslavkhimich.subscribee.models.stats.base

import com.vladyslavkhimich.subscribee.models.dto.StatsCategoryDto
import com.vladyslavkhimich.subscribee.models.dto.StatsSubscriptionDto

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
abstract class BaseStatsOverview {
    var totalSpent: Double = 0.0
    var dayAverageSpent: Double = 0.0
    var percentage: Double = 0.0
    var mostSpentSubscription: StatsSubscriptionDto? = null
    var mostSpentCategory: StatsCategoryDto? = null
}