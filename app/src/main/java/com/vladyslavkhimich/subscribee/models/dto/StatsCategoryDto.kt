package com.vladyslavkhimich.subscribee.models.dto

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
data class StatsCategoryDto(
    val color: String,
    val name: String,
    val moneyValue: Double
)
