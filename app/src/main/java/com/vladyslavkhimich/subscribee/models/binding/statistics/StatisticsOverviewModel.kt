package com.vladyslavkhimich.subscribee.models.binding.statistics

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableDouble
import androidx.databinding.ObservableField

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 13.12.2021
 * @email vladislav.himich@4k.com.ua
 */
data class StatisticsOverviewModel(
    val totalSpentValue: ObservableField<StatisticsMoneyModel> = ObservableField<StatisticsMoneyModel>(),
    val dayAverageSpentValue: ObservableField<StatisticsMoneyModel> = ObservableField<StatisticsMoneyModel>(),
    val comparedPercentage: ObservableDouble = ObservableDouble(0.0),
    val mostSpentSubscription: ObservableField<StatisticsSubscriptionModel> = ObservableField(),
    val mostSpentCategory: ObservableField<StatisticsCategoryModel> = ObservableField(),
    val isWithFamilyStatistics: ObservableBoolean = ObservableBoolean(false),
    val mostPaidSubscription: ObservableField<StatisticsSubscriptionModel> = ObservableField(),
    val mostPaidMember: ObservableField<StatisticsMemberModel> = ObservableField()
)
