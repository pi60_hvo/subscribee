package com.vladyslavkhimich.subscribee.models.dto

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 24.11.2021
 * @email vladislav.himich@4k.com.ua
 */
data class CategoryDto (
    val name: String? = null,
    val color: String? = null
)