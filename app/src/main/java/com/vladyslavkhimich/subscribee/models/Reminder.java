package com.vladyslavkhimich.subscribee.models;

public class Reminder {
    private String stringIDName;
    private int daysCount;
    private int weeksCount;
    private int monthsCount;
    private int yearsCount;
    private boolean isNever;
    private boolean isSameDay;

    public void setStringIDName(String stringIDName) {
        this.stringIDName = stringIDName;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public void setMonthsCount(int monthsCount) {
        this.monthsCount = monthsCount;
    }

    public void setWeeksCount(int weeksCount) {
        this.weeksCount = weeksCount;
    }

    public void setYearsCount(int yearsCount) {
        this.yearsCount = yearsCount;
    }

    public void setNever(boolean never) {
        isNever = never;
    }

    public void setSameDay(boolean sameDay) {
        isSameDay = sameDay;
    }

    public String getStringIDName() {
        return stringIDName;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public int getMonthsCount() {
        return monthsCount;
    }

    public int getWeeksCount() {
        return weeksCount;
    }

    public int getYearsCount() {
        return yearsCount;
    }

    public boolean getIsNever() {
        return isNever;
    }

    public boolean getIsSameDay() {
        return isSameDay;
    }

    public boolean isAllFieldsZero() {
        return getDaysCount() == 0 && getWeeksCount() == 0 && getMonthsCount() == 0 && getYearsCount() == 0;
    }

    public Reminder() {

    }

    public Reminder(com.vladyslavkhimich.subscribee.database.entities.Reminder databaseReminder) {
        setDaysCount(databaseReminder.daysCount);
        setWeeksCount(databaseReminder.weeksCount);
        setMonthsCount(databaseReminder.monthsCount);
        setYearsCount(databaseReminder.yearsCount);
        setSameDay(isAllFieldsZero());
    }
}
