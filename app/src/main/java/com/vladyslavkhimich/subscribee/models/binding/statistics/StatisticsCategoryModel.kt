package com.vladyslavkhimich.subscribee.models.binding.statistics

import androidx.databinding.ObservableDouble
import androidx.databinding.ObservableField

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 13.12.2021
 * @email vladislav.himich@4k.com.ua
 */
data class StatisticsCategoryModel(
    val color: ObservableField<String> = ObservableField(""),
    val name: ObservableField<String> = ObservableField(""),
    val moneyModel: ObservableField<StatisticsMoneyModel> = ObservableField<StatisticsMoneyModel>()
)
