package com.vladyslavkhimich.subscribee.models

import com.vladyslavkhimich.subscribee.database.entities.Operation
import com.vladyslavkhimich.subscribee.database.entities.PaymentDate
import java.util.*

class OperationAndPaymentDate (val operation: Operation?, val paymentDate: PaymentDate) : Comparable<OperationAndPaymentDate> {
    override fun compareTo(other: OperationAndPaymentDate): Int {
        val calendarThis = Calendar.getInstance()
        calendarThis.time = this.paymentDate.paymentDate

        val calendarOther = Calendar.getInstance()
        calendarOther.time = other.paymentDate.paymentDate

        return calendarThis.compareTo(calendarOther)
    }

}