package com.vladyslavkhimich.subscribee.models

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 24.11.2021
 * @email vladislav.himich@4k.com.ua
 */
data class OperationModel(
    val subscriptionName: String,
    val currencySymbol: String,
    val price: Double = 0.0,
    val iconColor: String,
    val iconName: String,
    val memberName: String? = null
)
