package com.vladyslavkhimich.subscribee.models.stats.family

import com.vladyslavkhimich.subscribee.models.dto.StatsMemberDto
import com.vladyslavkhimich.subscribee.models.dto.StatsSubscriptionDto
import com.vladyslavkhimich.subscribee.models.stats.base.BaseStatsOverview

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
open class FamilyStatsOverview : BaseStatsOverview() {
    var mostPaidSubscription: StatsSubscriptionDto? = null
    var mostPaidMember: StatsMemberDto? = null
}