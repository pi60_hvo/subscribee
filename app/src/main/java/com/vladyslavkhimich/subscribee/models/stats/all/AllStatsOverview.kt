package com.vladyslavkhimich.subscribee.models.stats.all

import com.vladyslavkhimich.subscribee.models.stats.base.BaseStatsOverview
import com.vladyslavkhimich.subscribee.models.stats.family.FamilyStatsOverview

/**
 * @project Subscribee
 * @company 4k-Soft
 * @author Vladyslav Khimich on 14.12.2021
 * @email vladislav.himich@4k.com.ua
 */
class AllStatsOverview : FamilyStatsOverview() {
}